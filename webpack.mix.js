let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Setup
mix.setPublicPath('.environment/public');
mix.setResourceRoot('../');
mix.disableNotifications();

// Vendor Libraries
mix.js('src/BaseHandler/Resources/assets/js/vendor.js', '.environment/public/js').version();
mix.sass('src/BaseHandler/Resources/assets/scss/vendor.scss', '.environment/public/css').version();

// DTV Framework Assets
mix.scripts([
    'src/BaseHandler/Resources/assets/js/helpers.js',
    'src/BaseHandler/Resources/assets/js/App.js',
    'src/BaseHandler/Resources/assets/js/views/*.js',
    'src/BaseHandler/Resources/assets/js/elements/*.js',
    'src/BaseHandler/Resources/assets/js/utils/*.js',
], '.environment/public/js/dtv-framework.js').version();
mix.styles([
    'src/BaseHandler/Resources/assets/css/dtv-framework.css',
    'src/BaseHandler/Resources/assets/css/dtv-listview.css',
    'src/DashboardHandler/Resources/assets/css/dtv-dashboard.css'
], '.environment/public/css/dtv-framework.css').version();