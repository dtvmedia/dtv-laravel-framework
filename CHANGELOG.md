# DTV Laravel Framework 
Here should be noted all breaking changes and some other notices of the upcoming version. This is may not be a complete changelog!

## 1.6.0 (XX.XX.2024)
- Added support for laravel 11
- Fixed missing mail schema after laravel 11 upgrade
- Added min, max and step support also for datetime inputs
- Fixed validations for the sms create form
- Fixed CustomFormView.js for initially hidden inputs
- Added hasInput method to CustomFormView for convenience
- Fixed compatibility of the mainOutAccount with the newer mail config structure
- Fixed datetime value loading for disabledSeconds

## 1.5.0 (XX.XX.2024)
- Added support for laravel 10
- Added the possibility for dynamic list button parameters to use methods 
- Made the validation for mail address names less restrictive in length
- Removed deprecated CM Telecom legacy SMS provider
- Added error handling for SMTP 501 invalid email
- Fixed error with buttons for outgoing mails without proper mail account relation
- Added the ability to add custom attributes to text inputs
- Added Support for BackedEnums in Input Values
- Performance fix for the mark as read function for mail inboxes
- Switched the HERE auth from the deprecated app code login to the new api key login
- Added the Conditionable trait for the JsonResponse class
- Switched base of the Oxygen::formatLocalized function the the isoFormat() function
- Added the ability to change the disallowed characters for the escapeHtmlLabel function and added "/"
- Fixed the download function for email attachments with slash or backslashed in their filename
- Added compatibility with laravel-phone v5
- Improved compatibility with more phone number formats
- Added return types for some MailAddressCollection methods in order to be in sync with the related interface
- Added isNotNull method for Oxygen
- Fixed bug in the rendering of additional attributes from buttons
- Fixed bug in the check mails command in case the date header is missing
- Added optional telescope link to the admin navigation
- Improved exception handler and added reload handling for authentication and token mismatch exceptions
- Removed several optional function call in favor of the nullsafe operator
- Removed the deprecated dates property of several models in favor of the casts property
- Added more type support
- Refactored RoutingHandler
- Added missing validations for the sms create form

## 1.4.1 (30.02.2023)
- Fixed mail handling by removing some swift mailer dependencies
- Refactored mail attachment classes
- Added new mail attachment type InlineMailAttachment
- Fixed Return Bug in the CheckMailsCommand 
- Added support for a shortened version of morph IDs using a class map
- Fixed bug in the queue status check logic
- Added the possibility to define the array for the settings option array type via an closure
- Added resend mail and resend mail to other receiver functions
- Made the models date mutator compatible with the via cast defined date fields
- Minor improvement for the log viewer

## 1.4.0 (08.02.2022)
- Added Laravel 9.0 Support
- Switched min version of PHP to v8.0
- Fixed composer version extract regex

## 1.3.9 (16.07.2022)
- Fixed composer version extract regex *(Backport from 1.4)*
- Fixed Return Bug in the CheckMailsCommand *(Backport from 1.4.1)*
- Added support for a shortened version of morph IDs using a class map *(Backport from 1.4.1)*

## 1.3.8 (13.11.2021)
- Changed the timeout before the model form autofocus from 500 to 50ms
- Added versions page to the admin area (including App, Laravel, PHP and Composer package versions)

## 1.3.7 (23.07.2021)
- Fixed support for multiple custom HTML form inputs in a single form
- Added support for relations in dynamic parameters of listview buttons
- Added methods to disable the sorting or searching of a listview column
- Added type to comment buttons to prevent unwanted submits when using them inside a form
- Reverted the custom form view event handlers change for date and time inputs from version v1.3.5 due to some more problems with the delete button in firefox 

## 1.3.6 (25.06.2021)
- Added support for disabled select options

## 1.3.5 (11.06.2021)
- Fixed custom form view event handlers for date and time inputs

## 1.3.4 (04.06.2021)
- Fixed bug in the settings controller for settings keys which included the group name in the key name

## 1.3.3 (31.05.2021)
- Improved error handling of the SendMailJob
- Fixed error handling in the CmProvider
- Fixed sorting error in the SMS overview
- Added an useView property for widgets in order to have the ability to use Listview Widgets with custom views

## 1.3.2 (19.04.2021)
- Improved error handling for the getModel method of the MorphIdHandler
- Added new config options to define the message timeout defaults for the 3 types (success, warning and error)

## 1.3.1 (12.04.2021)
- Switched from now on to semantic versioning (no hotfix versions anymore)
- Added an getLabel method for email accounts in order to be able to identify mail account with the same name
- Added an overlay to the HERE API Map View for better UX

## 1.3.0.1 (23.03.2021)
- Added .env var for the main mail out account (MAIL_MAIN_OUT_ACCOUNT_ID)
- The session overview in the admin area now shows the id if the logged in account is not resolvable as user
- Fixed an bug in the searching logic of the listview
- Added the possibility for the getBoolSelectOptions and getYearSelectOption functions to enable a no option
- Fixed bug in the email.move gate check

## 1.3.0 (19.03.2021)
- Added Support for Laravel 8
- Removed Support for Laravel 5.6
- Changed minumum PHP version to 7.3
- Fixed critical bug in the JS Fetch class
- Some adjustments to the email overviews, the email account parameter was removed
- Adjusted email and sms modal view styling
- Added default select option functions for bools and years
- newRequest on now possible directly after a page reload
- Fixed a bug, were the category unique check didn't respect the specific category scope
- Added the ability to define eager loading relations for the get select options query
- Adjusted the maintenance view to the latest laravel 8 changes
- The MailAddressCollection has now string support
- Improved the mail attachment system
- Added support for mail attachment from the disk
- Added the ability to add a delay for the MailBuilders saveAndQueue method
- Added the ability to change the step of an time input
- Added the ability to use a real mail account as main out account with the new config entry "main_out_account_id"
- Added an getEmailName method to the NotifiableByMail trait and interface which will be used in the MailAddressCollection class
- Renamed the SMS CmProvider Class to CmLegacyProvider and added an new CmProvider based on the new API package
- Fixed minor bug in the Time Form Input getMutator function
- Fixed the usage of data-ajax-json in combination with an confirm action
- Added an basic jobs and failed jobs overview to the queue page of the admin area
- Added an default sorting via name for the category overview
- Added select/deselect all button to mutliple selects
- Converted an select key for the "is-selected"-check to string
- Added diverse as a gender
- Added support for different modal sizes for normal form views

## 1.2.6.1 (21.06.2020)
- Fixed an case where the usage of the str_contains functions collides with the new implementation of the function php8.0

## 1.2.6 (03.05.2020)
- Laravel 7 support
- Fixed critical bug in the QueueHandler
- Fixed a rare case were the email embed file link couldn't resolve a matching file and would break the link 

## 1.2.5.1 (12.03.2020)
- Fixed minor bug in the CheckMailsCommand regarding mail dates
- Slightly improved admin log view UX
- Switched the category fields value column back to nullable

## 1.2.5 (22.02.2020)
- Fixed that the setDate method of the JsonResponse class rejected null as value
- Automatically triggered request have now an extra "_auto_triggered" request parameter
- Defined the MorphIdHandler prefix as constant and changed the first char to an underscore in order to better fit as frontend selector
- Added an async hash js helper function
- Added the semantic of the parameter from the hideModal method from the JsonResponse class. Now you can select which modal should be hidden
- Added an additional key to the event dispatcher functions in order to differ between multiple events of the same type but for different views (or modals)
- Added an Session clear button to the admin session overview
- Several bugfixes for the CommentHandler
    - NOTE: if you overwrite comments.blade, comment.blade, comment_textarea.blade or comment_form.blade you manually need to migrate these changes!
    - Fixed inconsistent comment min length check
    - Fixed an bug with the "no comments" label
    - Fixed multi comment view support (eg. in modals)
    - Fixed bug with multi event registration in modals
    - Added an new option commentModalMode, if true the edit form redirects to the previously openened modal
    - Added the option to overwrite the comment create title
    - Removed the in the previously version added ID customization option

## 1.2.4 (22.01.2020)
- Added the option to customize the ID of the comment form in order to prevent ID collission when using comments in modals
- Added the possibility for listviews to define the fixed secondary order logic via an closure
- Added better support for models in the notifiable process
- Fixed the JS getSelect function for multiple selects

## 1.2.3 (14.12.2019)
- Some Bugfixes for PHP 7.4 compatibility
- Fixed an case were isCurrentRoute would fail 
- Improved exception status code handling
- Improved the custom form view js handling for color inputs
- Increased timeout for autofocusing the first modal input
- Several refactorings regarding the controller form method, form views and form input handling
    - Some general code refactorings
    - Refactored the form_view.blade
    - Divided the form request mutators in pre and post validation mutators and outsourced them to the related form inputs
    - Also outsourced and unified the get value mutator to the related form input classes
    - Refactored the Validator and ResponseHandler JS classes: used there the new Input JS class 

## 1.2.2.1 (02.12.2019)
- Fixed critical bug in the Money input class

## 1.2.2 (01.12.2019)
- Added an isNationwideHoliday which returns whether or not a given date is an nation wide holiday
- Fixed an wrong route parameter in the email view 
- Added basic support for custom units in the money input
- Added support for min and max values of date inputs
- Added an simple queue status monitoring to the admin backend
- Added an dedicated setDate function to the JsonResponse class
- The placeholder function translates now also its value if it tends to be a translation key
- Refactored money and added an more generic parent decimal form input
- Removed the in the previous version as deprecated marked methods
- Added an setValue method to the DecimalInput JS class
- Added an new Input JS class

## 1.2.1 (24.11.2019)
- Added the "Weltkindertag" to the Oxygen Holiday class
- Added ManagerContract defining the basic interface of the 3 manager classes
- Changed the abstract name of the WidgetManager singleton from "dashboards" to "widgets"
- Keyed the form view inputs by id in order to improve performance for custom form views
- The option helper translates the value now only if it contains an "."
- Several code refactorings

**Deprecations**
- Deprecated the getWidgets() method of the WidgetManager class, consider using the all() method instead
- Deprecated the registerCategory() method of the CategoryManager class, consider using the register() method instead
- Deprecated the hasCategory() method of the CategoryManager class, consider using the has() method instead
- Deprecated the getCategory() method of the CategoryManager class, consider using the get() method instead
- Deprecated the getCategoryOrFail() method of the CategoryManager class, consider using the getOrFail() method instead
- Deprecated the registerMailable() method of the MailableManager class, consider usign the register() method instead
- Deprecated the getMailables() method of the MailableManager class, consider usign the all() method instead

## 1.2.0 (18.11.2019)
- Changed nullable icon checkbox behaviour so that the NULL-state is now reachable after 3 clicks
- Added the disabled class to the labels when a checkbox is disabled
- Fixed an bug where not existing directory were registered as view and lang dirs (view:cache should now work)
- The specific object name will now be shown when deleting an object, added name translations for all objects
- Added directly the ability to overwrite the used git branch via the GIT_BRANCH env variable
- Refactored all cases where get_called_class() was used
- Added setInput() and setCheckbox functions to the JsonResponse class
- Changed the signature of the isCurrentRoute helper function. You need to refactor all usages. In case you used this function in views consider using the @active blade directive instead
- Removed finally the "scripts" section, consider using the "scripts" stack instead
- Added an basic hidden user id system (new config and env variable)
- Fixed an bug in the default dashboard handling
- Added an new routeExists() helper function
- Added an new formatNumber() helper function
- Added route existent check for the navigation helper function
- Added email account delete function
- Added the possibility of an main out mail account which uses the .ENV data for sending some basic mails e.g. password reset mails
- Fixed an email check command bug were older emails were constantly imported
- Fixed an rare case where the mail contents needs to be utf8 encoded before saving it to the DB
- Fixed nullable icon checkbox styling
- Added support for href confirm links
- Added support for overwriting the money input vars via data attributes
- Added the possibility of auto expanding textareas and switched the category and comment textareas to this mode. Therefore if you overwrite the related views, make sure to compare your file with the latest view.
- Added support for encrypted model fields
- Added new JS helper function empty()
- Added the option to close an JS message programmatically before the timeout
- Added an config entry to whitelist routes in order to expose them to JS. Implemented an route method for the App JS class.
- Added an class to the oxygen module for the calculation of german holidays
- Added an data-translation attribute to all form inputs
- Added an general hook function with which you can extend several things from the app side e.g. header buttons and listviews
- Improved the RoutingHandler so that the route object will now be returned and can be further customized
- Listview changes:
    - Implemented an basic relation search and order functionality for listviews
    - Added an fix for searching in listviews for category records
    - The add* methods (e.g. addColumn) are now public instead of protected
- Admin Handler changes
    - An new user overview section was added to the admin area
    - There you can now simply login as an chosen user 
- Comment Handler changes:
    - The Comment Handler works now completly without full refreshes. Therefore we had to make some changes resulting in some incompatibilities
    - We need now an extra view representing a single comment (dtv.comments::comment)
    - Note that there were links with incorrect route parameters (use "comment" instead of "id")
    - This comment needs an ID in the following format: "comment-{{ $id }}"
    - The comment form view holds now the JS handling of these changes, if you've overwritten this view you need to apply these changes
    - The comments wrapper section should now have an class comment-order-asc or desc indicating how the comments are ordered. This is added in the example view automatically via an $order variable you can pass to the comments.blade view 
- Category Handler changes:
    - Category messages show now the specific category object
       - If you use app related categories you need to overwrite the $translationFile property in the app category model to null or set it to the specific file in every derived category model 
    - Added support for category field defaults (and also refactored category __get function)
    - Changed category description and the category field value from VARCHAR to TEXT in order to support longer content
    - Added an ability to disable the category description field 
    - Added an ability for category fields to customize the text field height 
    - In the category overview is now only an description excerpt shown
    - Added the ability to set an category name which will be used in the URL instead of the category type ID
        - You need to adjust all category links because the previous links with the category type id won't work anymore
    - Added an category delete function
        - You need to add an translation for your_category.buttons.delete
    - Now the category config options (create, edit, delete) finally enable/disable the specific buttons
    - Added an hasCategory() method to the CategoryManager class
    - Added the ability to limit the number of category model records via the config
    - Added an unique check for the category name and added the ability to disable this check
    - Improved the category builder in order to support closures and orWhere calls
    - Changed the $categoryType and $categoryName properties to static
    - Improved the field definition of custom options: this can now be passed as closure in order to prevent errors when calling there functions like trans()

## 1.1.7 (30.07.2019)
- Added an new NullableIconCheckbox using the indeterminated state and a seperate hidden input for holding the value
- Therefore the coloring for non checked (and not nullable) icon checkboxes was changed from gray to red
- The email check looks now for emails from the previous day in order to prevent import bugs
- Added money request mutator
- Added number input to the form view template
- Fixed the onchange bug when clearing date/time inputs
- Fixed the handling for select/arrays as category field types
- Fixed activity handler for activities created via console
- Added updated_from user field to the comments and some other minor comment adjustments
- Added support for an static optionLabelIconCallback function in models which could return an item for the select options based on an given model
- Added support for initally hidden inputs in an CustomFormView via an extra css class on these inputs
- Added cache busting
- Added an authId to the App js class and an isLoggedIn function

## 1.1.6 (28.06.2019)
- Version definition is now moved from the AppServiceProvider to the composer.json
- If you return an JsonResponse in the success callback of the deleteModel function, the given JsonResponse is now used instead of the generic one
- Fixed critical bug were mails without message id couldn't be imported
- Fixed bug in the client validation js class where nullable was ignored

## 1.1.5 (23.06.2019)
- Added the preventSubmitOnEnter also for the edit mode of CustomFormViews
- The email already imported log message will now only be logged in debug mode

## 1.1.4 (14.06.2019)
- Fixed critical email sync problem
- Added gate before callback for super admins, 
- Added isSuperAdmin function to the user model

## 1.1.3 (07.06.2019)
- Added the ability to change the default list view page length --> update your config
- Added the ability to change the comment title and the comment date format --> update your config
- Fixed performance problem in the email handler gates
- Added some more List View Column Breakpoints

## 1.1.2 (05.06.2019)
- Removed the model function of the Model class, use the instance function instead
- Fixed preventSubmitOnEnter js function
- Fixed simple formatting for non html comments
- Removed the duplicate session view title, specified button type
- Added file input type
- Experimental Fix for critical bug where some mails were not imported if the were received some seconds after an mail check run
- Added force flag to the check mails command, now mails which were ignored because they are already imported will be logged
- Added support for dates back to the year 1800
- Fixed checkbox nullable handling request rewrites
- Listview default page length fix --> You need to update your derived listview template

## v1.1.1 (29.05.2019)
- Added the last access information to the sessions tab
- Fixed an oxygen null handling bug after the latest changes

## v1.1.0 (22.05.2019)
- Added admin handler backend module with the following functions:
    - Seperate login
    - Session viewer
    - Maintenance mode switcher
    - Log viewer

## v1.0.1 (20.05.2019)
- Fixed critical bug in the response handler js class
- Fixed minor bug in the RoleHandler class
- adjusted some translations

## v.1.0.0 (19.05.2019)
- This version marks the initial v1 release due to the first production system which is build on top of the framework
- Major JS Changes under the hood in order to remove the jquery dependency step by step. 
This process will take a while and as long as bootstrap does'nt drop the jquery dependency we will use it aswell for some parts. 
But we tend to remove all usages during the next weeks.
    - Added various new helper functions
    - Several js code refactorings
    - Renamed the js files and some classes, update your webpack.mix.js file
    - Added maxlength for comments
    - data-modal-route was removed
    - ajaxModal was removed
    - newModalRequest was also removed
    - For that the newRequest system was added
    - Added an new Fetch wrapper class and switched all framework ajax calls to the new Fetch API
    - Added an new Validator class (and removed old validation functions)
    - Rewrote moneyinput as plain js class
    - Removed old JsonResponse callback system
    - Added an EventDispatcher class instead
    - Removed the message function and added an Message class instead
    - Removed the loadingScreen function and added an LoadingScreen class instead
    - Removed the ajaxForm function
    - Added BaseFormView and FormView classes instead
    - These form view classes handle now also the input initialization, therefore the initForm, initSelects and initMoneyInput functions were removed
    - Added an central singleton App class, all things which were handled via global constants are now handled via this class
    - Due to that changes you need to move the include of javascript_constant.blade.php after the include of the dtv-framework.js
    - Removed handleResponse function and added an ResponseHandler class instead
    - Renamed the javascript_constants.blade to app_js_init.blade
- Added the ability to change the height of textareas
- Added the validation wrapper div for custom form view inputs and the validation wrapper div respects now the enableClientValidation flag
- Some responses which previously triggered a full page refresh trigger now just a listview reload
- Fixed bug in the Oxygen class, which results in the fact that the format function is no longer overwritten by oxygen
- Fixed delete behaviour for the pivot model
- Fixed type in CustomFormView (enableClientValditions -> enableClientValidations)

## v.0.10.0
- Added Money Form Input
- Added Number Form Input
- Added 2 send_queue config entries for the mail and sms modules
- Changed the active blade directive to a experimental version with eval
- Improved mail account error handling and logging
- Added the ability to hide the view title  
- Fixed bug in the mail answer function
- Added the ability to add meta information to the listview rows which means that if you've published the listview template you need to rework your version
- Added email drag'n'drop system
- Improved email folder management
- Added hard delete system for mails
- All links in mails are now opened in an new tab

## v0.9.0 
- Moved the Notifiable and NotifiableGroup Contracts to the support package
- Use of the scripts section in modals is deprecated, use the script stack instead!
- Fixed an bug in the JS callback functionality. You should test if all your callbacks works as intended.
- Removed the JS awesomplete dependency
- Refactored the service provider loading (this shouldn't break things) 
- There are several new settings for the emails module (see dtv.php)
- Made the getDataField ListView function public static in order to be available for different use cases aswell and changed the null failure handling a bit
- The base user model use now the HasRole trait directly
- Some testing environment fixes (these shouldn't break things)
- Applied some phpstan changes (these shouldn't break things)

## v0.8.0 
- Added type casting to the category handler (for fields), and added type cast support for some more types for the settings handler.
- The formatColor signature was changed (breaks if you used the size parameter before)
- The Dashboard Handler styles were moved to an extra asset file you may need to publish
- added new sms handler search config entry
- Booleans will now be casted to int by the form view getValue function in order to prevent problems during select preselection
- Removed in v0.7 deprecated functions

## v0.7.0 

- Due to the new SmsHandler an new settings block was added to the ```dtv.php``` config
- An new setting "button_type" is available in the ```dtv.php``` config which handles which bootstrap class should be used for all header and modal buttons
- Changed the "type" column of the categories table to "category_type" in order to prevent blocking an category field named just type
- category fields "value" column is now nullable (change without update migration)
- changed categories handler urls (but names are the same as before)
- the user relation of activities has now an default "system"-user
- The Select class constructer signature has changed (but the addSelect function is still the same)
- adjustments to the form handling due to the custom form view addition (but should'nt break things)
- The Oxygen class was reworked and if you published the translation files you need to update them via ```vendor:publish --force```
- The form-template handling was reworked (this shouldn't break things)
- During this rework the input id prefix system of the form view was marked as deprecated. The prefix will not be used anymore, so if you use these functions make sure to rework this part too (you can now set the id of an input directly via setId() ).
- Select Value comparison uses now strict comparison (This could break some existing selects!)
- The JS functions ajax_form and ajax_modal are now deprecated and will be removed in v0.8. These functions exist now under the names ajaxForm and ajaxModal.
- The JS function inArray is now deprecated and will be removed in v0.8

*Changelog for v0.7.0 is not complete!*
