<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Commands;

    use DTV\EmailHandler\Models\Email;
    use Illuminate\Console\Command;
    use Illuminate\Support\Facades\Storage;

    /**
     * Check Mails Command Class
     *
     * @package   DTV\EmailHandler\Commands
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class HardDeleteMailCommand extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'email:hard-delete';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Hard deletes all marked emails and their attachments';

        /**
         * Number of deleted mails
         *
         * @var int
         */
        private int $countDeleted = 0;

        /**
         * Execute the console command.
         */
        public function handle()
        {
            foreach ( Email::query()->withTrashed()->hardDeleted()->get() as $email ) {
                if ( $email->received_at->gte( today() ) ) {
                    continue;
                }

                $disk = Storage::disk( 'local' );
                $email->references()->forceDelete();

                // Delete attachment file on disk
                foreach ( $email->attachments()->pluck( 'path' )->toArray() as $attachment ) {
                    if ( $disk->exists( $attachment ) ) {
                        $disk->delete( $attachment );
                    }
                }
                $email->attachments()->forceDelete();
                $email->forceDelete();

                $this->countDeleted++;
            }

            // Command output
            $this->info( 'Mails deleted: ' . $this->countDeleted );
        }
    }
