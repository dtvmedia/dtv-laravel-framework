<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Commands;

    use Carbon\Carbon;
    use DateTimeZone;
    use Ddeboer\Imap\Exception\AuthenticationFailedException;
    use Ddeboer\Imap\Exception\MailboxDoesNotExistException;
    use Ddeboer\Imap\MessageInterface;
    use Ddeboer\Imap\Search\Date\Since;
    use Ddeboer\Imap\SearchExpression;
    use DTV\CommentsHandler\CommentHandler;
    use DTV\EmailHandler\Models\Email;
    use DTV\EmailHandler\Models\EmailAccount;
    use DTV\EmailHandler\Models\EmailAttachment;
    use DTV\Oxygen\Oxygen;
    use Exception;
    use Illuminate\Console\Command;
    use Illuminate\Support\Facades\Storage;
    use Throwable;

    /**
     * Check Mails Command Class
     *
     * @package   DTV\EmailHandler\Commands
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CheckMailsCommand extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'email:check {--force}';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Checks all mailboxes for new emails';

        /**
         * Number of imported mails
         *
         * @var int
         */
        private int $countImported = 0;

        /**
         * Number of failed mails
         *
         * @var int
         */
        private int $countFailed = 0;

        /**
         * Execute the console command.
         *
         * @throws Throwable
         */
        public function handle()
        {
            foreach ( EmailAccount::active() as $emailAccount ) {
                try {
                    if ( !$emailAccount->isCheckRequired() && $this->option( 'force' ) === false ) {
                        continue;
                    }

                    $this->info( 'Checking  ' . $emailAccount->email );

                    // connect to the mail server
                    $connection = $emailAccount->connect();

                    // get mailboxes
                    $mailbox_inbox = $connection->getMailbox( $emailAccount->mailbox_inbox );
                    $mailbox_failed = $connection->getMailbox( $emailAccount->mailbox_failed );

                    // get already received message ids
                    $receivedMails = $emailAccount->emails()->withTrashed()
                        ->whereDate( 'created_at' , '>=' , $emailAccount->getLastRetrieval()->copy()->subDay()->format( 'Y-m-d' ) )
                        ->pluck( 'message_id' )->toArray();

                    // define date conditions (NOTE: no time is allowed here, only full dates!)
                    $search = new SearchExpression();
                    $search->addCondition( new Since( $emailAccount->getLastRetrieval()->copy()->subDay() ) );

                    // get the new last retrieval date
                    $newLastRetrieved = Oxygen::now();

                    // handle new messages
                    foreach ( $mailbox_inbox->getMessages( $search ) as $message ) {
                        try {
                            // Get message id
                            $messageId = $this->getMessageId( $message );

                            // check if the mail is already imported
                            if ( !in_array( $messageId , $receivedMails ) ) {
                                // create new email in the database
                                $this->createEmail( $message , $emailAccount );

                                $this->countImported ++;
                            } elseif ( isDebugMode() ) {
                                logger( 'Mail is already imported: ' . $message->getSubject() . ' (' . $messageId . ')' );
                            }
                        } catch ( Exception $e ) {
                            // mail import failed
                            logger( 'Import of email failed: ' . $e->getMessage() );

                            // Move mail to the failed mailbox
                            $message->move( $mailbox_failed );
                            $this->countFailed ++;
                        }
                    }

                    // close connection
                    $connection->expunge();
                    $connection->close();

                    // touch model timestamps and update error counter
                    $newErrors = 0;
                    $emailAccount->update( [
                        'last_retrieved' => $newLastRetrieved ,
                        'errors'         => $newErrors ,
                        'last_try'       => $newLastRetrieved ,
                    ] );
                } catch ( AuthenticationFailedException $e ) {
                    $this->handleError( $emailAccount , 'Wrong authentication credentials for the IMAP server ' . $emailAccount->in_server . ': ' . $e->getMessage() );
                } catch ( MailboxDoesNotExistException $e ) {
                    $this->handleError( $emailAccount , 'Mailbox not found in the IMAP server ' . $emailAccount->in_server . ': ' . $e->getMessage() );
                } catch ( Exception $e ) {
                    $this->handleError( $emailAccount , 'General mail error: ' . $e->getMessage() );
                }

                // Command output
                $this->info( 'Mails imported: ' . $this->countImported );
                $this->info( 'Mails failed: ' . $this->countFailed );

                $this->countImported = 0;
                $this->countFailed = 0;
            }
        }

        /**
         * Creates an new mail in the database
         *
         * @param MessageInterface $message
         * @param EmailAccount     $emailAccount
         *
         * @throws Throwable
         * @return Email
         */
        protected function createEmail( MessageInterface $message , EmailAccount $emailAccount )
        {
            // We initialy assume that we have an html mail
            $html = true;
            $content = $message->getBodyHtml();

            // if we couln't get any content this way we have an text mail instead
            if ( $content == null ) {
                $html = false;
                $content = $message->getBodyText() ?? '';
            }

            $to = [];
            foreach ( $message->getTo() as $emailAddress ) {
                $to[] = str_replace( '"' , '' , $emailAddress->getFullAddress() );
            }

            $cc = [];
            foreach ( $message->getCc() as $emailAddress ) {
                $cc[] = str_replace( '"' , '' , $emailAddress->getFullAddress() );
            }

            $bcc = [];
            foreach ( $message->getBcc() as $emailAddress ) {
                $bcc[] = str_replace( '"' , '' , $emailAddress->getFullAddress() );
            }

            $subject = $message->getSubject();
            $from = $message->getFrom()?->getFullAddress() ?? 'Unknown Sender';
            $date = $message->getDate();

            if ( $date !== null ) {
                $date = $date->setTimezone( new DateTimeZone( config( 'app.timezone' ) ) );
            } else {
                $date = now();
            }

            // todo add spam check
            //$message->getFrom()->getAddress()
            $emailData = [
                'email_account_id' => $emailAccount->id ,
                'incoming'         => true ,
                'subject'          => ( empty( $subject ) ) ? trans( 'dtv.emails::emails.fields.no_subject' ) : $subject ,
                'from'             => str_replace( '"' , '' , $from ) ,
                'to'               => $to ,
                'cc'               => $cc ,
                'bcc'              => $bcc ,
                'message_id'       => $this->getMessageId( $message ) ,
                'size'             => $message->getSize() ,
                'spam'             => false ,
                'content'          => $content ,
                'received_at'      => $date ,
                'html'             => $html
            ];

            try {
                /** @var Email $email */
                $email = Email::query()->create( $emailData );
            } catch ( Throwable $exception ) {
                logger()->warning( $exception->getMessage() );
                $emailData[ 'content' ] = utf8_encode( $emailData[ 'content' ] );

                $email = Email::query()->create( $emailData );
            }

            // Handle attachments
            foreach ( $message->getAttachments() as $attachment ) {
                try {
                    $content = $attachment->getDecodedContent();
                } catch (Exception) {
                    $content = base64_decode( $attachment->getContent() );
                }

                $name = 'mail_attachments/' . md5( $content ) . '_' . str_random( 6 );
                Storage::disk( 'local' )->put( $name , $content );

                EmailAttachment::query()->create( [
                    'email_id'   => $email->id ,
                    'name'       => $attachment->getFilename() ?? 'Unknown' ,
                    'type'       => strtolower( $attachment->getType() . '/' . $attachment->getSubtype() ) ,
                    'size'       => $attachment->getSize() ?? strlen( $content ) ,
                    'path'       => $name ,
                    'content_id' => $attachment->getStructure()?->id ?? null,
                ] );
            }

            return $email;
        }

        /**
         * Handles mail account errors
         *
         * @param EmailAccount $emailAccount
         * @param string       $message
         */
        protected function handleError( EmailAccount $emailAccount , string $message )
        {
            $commentHandler = new CommentHandler();

            $message = '[' . $emailAccount->id . '] ' . $message;
            logger( $message );
            $this->info( $message );
            $commentHandler->create( $message , EmailAccount::class , $emailAccount->id , false );

            $status = $emailAccount->status;
            $newErrors = $emailAccount->errors + 1;

            if ( $newErrors >= 10 ) {
                $status = EmailAccount::STATUS_INACTIVE;
            }
            $emailAccount->update( [
                'errors'   => $newErrors ,
                'status'   => $status ,
                'last_try' => Carbon::now() ,
            ] );
        }

        /**
         * Returns the message id or an generated equivalent
         *
         * @param MessageInterface $message
         *
         * @return string|null
         */
        protected function getMessageId( MessageInterface $message )
        {
            return $message->getId() ?? md5( $message->getDate()?->format( 'd.m.Y' ) . $message->getSubject() . $message->getSize() );
        }
    }
