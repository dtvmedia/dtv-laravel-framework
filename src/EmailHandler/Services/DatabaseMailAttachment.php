<?php

/**
 * DTV Media Solutions
 *
 * PHP Version 7.0
 */

namespace DTV\EmailHandler\Services;

use DTV\EmailHandler\Models\EmailAttachment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use InvalidArgumentException;

/**
 * Mail Attachment Class
 *
 * @package   DTV\EmailHandler\Services
 * @copyright 2018 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
class DatabaseMailAttachment extends MailAttachment
{
    /**
     * Underlaying attachment/file object
     *
     * @var EmailAttachment|Model
     */
    protected EmailAttachment|Model $file;

    /**
     * MailAttachment constructor
     *
     * @param int|EmailAttachment $file
     */
    public function __construct($file)
    {
        // If the given file is an int we assume it is an email attachment model id and load it from the database
        if (is_int($file)) {
            $this->file = EmailAttachment::query()->findOrFail($file);

            return;
        }

        // if the given file is an EmailAttachment object we can use it directly
        if ($file instanceof EmailAttachment) {
            $this->file = $file;

            return;
        }

        throw new InvalidArgumentException('Invalid argument file given');
    }

    /**
     * Returns the name of the file
     *
     * @return string
     */
    public function getName()
    {
        return $this->file->name;
    }

    /**
     * Returns the MIME type of the file
     *
     * @return null|string
     */
    public function getType()
    {
        return $this->file->type;
    }

    /**
     * Returns the content of the file
     *
     * @return string|null
     */
    public function getContent()
    {
        return Storage::disk('local')->get($this->file->path);
    }

    /**
     * Returns the size of the file
     *
     * @return int
     */
    public function getSize()
    {
        return $this->file->size;
    }

    /**
     * This method will create an EmailAttachment model from it,
     * save it in the database and returns it.
     *
     * @param int|null $email_id
     *
     * @return EmailAttachment|Model
     */
    public function save($email_id = null)
    {
        if ($email_id !== null) {
            $this->file->email_id = $email_id;
        }

        $this->file->save();

        return $this->file;
    }
}
