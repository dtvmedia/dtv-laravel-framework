<?php

/**
 * DTV Media Solutions
 *
 * PHP Version 7.0
 */

namespace DTV\EmailHandler\Services;

use DTV\EmailHandler\Models\EmailAttachment;
use Illuminate\Http\UploadedFile;
use InvalidArgumentException;

/**
 * Uploaded Mail Attachment Class
 *
 * @package   DTV\EmailHandler\Services
 * @copyright 2018 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
class UploadedMailAttachment extends MailAttachment
{
    /**
     * Underlaying attachment/file object
     *
     * @var UploadedFile
     */
    protected UploadedFile $file;

    /**
     * MailAttachment constructor
     *
     * @param int|EmailAttachment|UploadedFile $file
     */
    public function __construct($file)
    {
        // if the given file is an UploadedFile we can use it directly
        if ($file instanceof UploadedFile) {
            $this->file = $file;

            return;
        }

        throw new InvalidArgumentException('Invalid argument file given');
    }

    /**
     * Returns the name of the file
     *
     * @return string
     */
    public function getName()
    {
        return $this->file->getClientOriginalName();
    }

    /**
     * Returns the MIME type of the file
     *
     * @return null|string
     */
    public function getType()
    {
        return $this->file->getMimeType();
    }

    /**
     * Returns the content of the file
     *
     * @return bool|string
     */
    public function getContent()
    {
        return file_get_contents($this->file->getPathname());
    }

    /**
     * Returns the size of the file
     *
     * @return int
     */
    public function getSize()
    {
        return filesize($this->file->getPathname());
    }
}
