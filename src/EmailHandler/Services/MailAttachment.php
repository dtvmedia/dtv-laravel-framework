<?php

/**
 * DTV Media Solutions
 *
 * PHP Version 7.0
 */

namespace DTV\EmailHandler\Services;

use DTV\EmailHandler\Models\EmailAttachment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Mail Attachment Class
 *
 * @package   DTV\EmailHandler\Services
 * @copyright 2018 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
abstract class MailAttachment
{
    /**
     * Returns the name of the file
     *
     * @return string
     */
    abstract public function getName();

    /**
     * Returns the MIME type of the file
     *
     * @return null|string
     */
    abstract public function getType();

    /**
     * Returns the content of the file
     *
     * @return bool|string
     */
    abstract public function getContent();

    /**
     * Returns the size of the file
     *
     * @return int
     */
    abstract public function getSize();

    /**
     * This method will create an EmailAttachment model from it, save it in the database and returns it.
     *
     * @param int|null $email_id
     *
     * @return EmailAttachment|Model
     */
    public function save($email_id = null)
    {
        $name = 'mail_attachments/' . md5($this->getContent()) . '_' . str_random(6);
        Storage::disk('local')->put($name, $this->getContent());

        return EmailAttachment::query()->create(
            [
                'email_id' => $email_id,
                'name'     => $this->getName(),
                'type'     => $this->getType(),
                'size'     => $this->getSize(),
                'path'     => $name,
            ]
        );
    }
}
