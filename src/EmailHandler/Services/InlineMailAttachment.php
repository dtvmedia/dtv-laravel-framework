<?php

/**
 * DTV Media Solutions
 *
 * PHP Version 7.0
 */

namespace DTV\EmailHandler\Services;

use Illuminate\Support\Str;

/**
 * Inline Mail Attachment Class
 *
 * @package   DTV\EmailHandler\Services
 * @copyright 2020 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
class InlineMailAttachment extends MailAttachment
{
    /**
     * File path
     *
     * @var string
     */
    protected string $content;

    /**
     * File disk
     *
     * @var string
     */
    protected string $name;

    protected string $mimeType;

    /**
     * MailAttachment constructor
     *
     * @param string $content
     * @param string $name
     * @param string $mimeType
     */
    public function __construct(string $content, string $name, string $mimeType)
    {
        $this->content = $content;
        $this->name = $name;
        $this->mimeType = $mimeType;
    }

    /**
     * Returns the name of the file
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the MIME type of the file
     *
     * @return string
     */
    public function getType()
    {
        return $this->mimeType;
    }

    /**
     * Returns the content of the file
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Returns the size of the file
     *
     * @return int
     */
    public function getSize()
    {
        return Str::length($this->getContent());
    }
}
