<?php

    /** @noinspection PhpUnused */

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Services;

    use DTV\EmailHandler\Contracts\NotifiableByEmail;
    use DTV\EmailHandler\Jobs\SendMailJob;
    use DTV\EmailHandler\Models\Email;
    use DTV\EmailHandler\Models\EmailAccount;
    use DTV\EmailHandler\Models\EmailReference;
    use DTV\EmailHandler\Services\MailAddressCollection as Addresses;
    use Exception;
    use Illuminate\Contracts\Filesystem\FileNotFoundException;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Mail\Mailer;
    use Illuminate\Mail\Message;

    /**
     * Mail Builder Class
     *
     * @package   DTV\EmailHandler\Services
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class MailBuilder
    {
        /**
         * Related mail account
         *
         * @var EmailAccount
         */
        protected EmailAccount $account;

        /**
         * The underlaying email model instance
         *
         * @var Email|null
         */
        protected ?Email $model = null;

        /**
         * Mail subject
         *
         * @var string
         */
        protected string $subject;

        /**
         * Mail content
         *
         * @var string
         */
        protected string $content;

        /**
         * To Mail Address Collection
         *
         * @var MailAddressCollection
         */
        protected Addresses $to;

        /**
         * CC Mail Address Collection
         *
         * @var MailAddressCollection
         */
        protected Addresses $cc;

        /**
         * BCC Mail Address Collection
         *
         * @var MailAddressCollection
         */
        protected Addresses $bcc;

        /**
         * Array of mail attachment object
         *
         * @var MailAttachment[]
         */
        protected array $attachments = [];

        /**
         * HTML mail mode flag
         *
         * @var bool
         */
        protected bool $html;

        /**
         * References Array
         *
         * @var array
         */
        protected array $references = [];

        /**
         * Mail constructor.
         *
         * @param EmailAccount|int|null $account
         * @param string                $subject
         * @param string                $content
         * @param bool                  $html
         * @param MailAddressCollection $to
         *
         * @throws Exception
         */
        public function __construct($account, string $subject, string $content, bool $html, Addresses $to)
        {
            if ($account !== null) {
                $this->account = EmailAccount::instance($account, false);
            } else {
                $this->account = EmailAccount::mainOutAccount();
            }

            $this->subject = $subject;
            $this->content = $content;
            $this->html = $html;
            $this->to = $to;

            $this->cc(new Addresses([]));
            $this->bcc(new Addresses([]));
        }

        /**
         * Sets the underlaying email model instance
         *
         * @param Email $email
         *
         * @return $this
         */
        private function useModel(Email $email): self
        {
            $this->model = $email;

            return $this;
        }

        /**
         * Adds an CC mail address collection
         *
         * @param MailAddressCollection $cc
         *
         * @return $this
         */
        public function cc(Addresses $cc)
        {
            $this->cc = $cc;

            return $this;
        }

        /**
         * Adds an BCC mail address collection
         *
         * @param MailAddressCollection $bcc
         *
         * @return $this
         */
        public function bcc(Addresses $bcc)
        {
            $this->bcc = $bcc;

            return $this;
        }

        /**
         * Adds an Mail Attachment object to the mail
         *
         * @param MailAttachment $attachment
         *
         * @return $this
         */
        public function attachment(MailAttachment $attachment)
        {
            $this->attachments[] = $attachment;

            return $this;
        }

        /**
         * Adds an Mail Attachment object from the disk to the mail
         *
         * @param string $path
         * @param string $disk
         *
         * @throws FileNotFoundException
         * @return $this
         */
        public function diskAttachment(string $path, string $disk = 'local')
        {
            $this->attachments[] = new DiskMailAttachment($path, $disk);

            return $this;
        }

        /**
         * Adds an reference to the mail
         *
         * @param Model $reference
         *
         * @return $this
         */
        public function reference(Model $reference)
        {
            $this->references[] = $reference;

            return $this;
        }

        /**
         * Appends some content to the mail (useful for the initial content of an answered/forwarded mail)
         *
         * @param Email $email
         *
         * @return $this
         */
        public function appendInitialMail(Email $email)
        {
            $tmpMail = MailConcatenator::initFromBuilder($this, $email)->concatenate();

            $this->content = $tmpMail->content;
            $this->html = $tmpMail->html;

            return $this;
        }

        /**
         * Returns the content of the mail
         *
         * @return string
         */
        public function getContent()
        {
            return $this->content;
        }

        /**
         * Returns whether or not this is an html mail
         *
         * @return bool
         */
        public function getHtml()
        {
            return $this->html;
        }

        /**
         * Saves the built mail to the database
         *
         * @param bool $asDraft
         *
         * @return Email
         */
        public function save(bool $asDraft = false): Email
        {
            if ($this->model !== null) {
                return $this->model;
            }

            $this->model = Email::query()->create([
                'email_account_id' => $this->account->id,
                'incoming'         => false,
                'subject'          => $this->subject,
                'from'             => $this->account->getFromMailLabel(),
                'to'               => $this->to->getAsLabelArray(),
                'cc'               => $this->cc->getAsLabelArray(),
                'bcc'              => $this->bcc->getAsLabelArray(),
                'size'             => strlen($this->content),
                'content'          => $this->content,
                'status'           => ($asDraft) ? Email::STATUS_OUT_DRAFT : Email::STATUS_OUT_PENDING,
                'html'             => $this->html,
            ]);

            // Receiver References
            foreach (['to', 'cc', 'bcc'] as $mode) {
                foreach ($this->{$mode} as $recipient) {
                    if ($recipient['reference'] instanceof NotifiableByEmail) {
                        $recipient['reference']->emails()->attach($this->model->id, ['receiver' => true]);
                    }
                }
            }

            // Non-receiver References
            foreach ($this->references as $reference) {
                EmailReference::query()->create([
                    'email_id'       => $this->model->id,
                    'reference_type' => get_class($reference),
                    'reference_id'   => $reference->id,
                    'receiver'       => false,
                ]);
            }

            foreach ($this->attachments as $attachment) {
                $attachment->save($this->model->id);
            }

            return $this->model;
        }

        /**
         * Sends the built mail
         */
        public function send()
        {
            try {
                // init account connection
                $this->account->initSend();

                // try to send mail
                $mode = ($this->html) ? 'html' : 'raw';
                self::newMail()->$mode($this->content, function (Message $mail) {
                    $mail->subject($this->subject);
                    $mail->from(config('mail.from.address'), config('mail.from.name'));
                    $mail->to($this->to->getAsSwiftMailerArray());

                    if ($this->cc->count() > 0) {
                        $mail->cc($this->cc->getAsSwiftMailerArray());
                    }

                    if ($this->bcc->count() > 0) {
                        $mail->bcc($this->bcc->getAsSwiftMailerArray());
                    }

                    foreach ($this->attachments as $attachment) {
                        $mail->attachData($attachment->getContent(), $attachment->getName());
                    }
                });

                // update model
                $this->model?->update([
                    'sent_at' => now(),
                    'status'  => Email::STATUS_OUT_SENT,
                ]);
            } catch (Exception $e) {
                // error while sending the mail
                $this->model?->update(['status' => Email::STATUS_OUT_FAILED]);

                throw $e;
            }
        }

        /**
         * Saves the built mail to the database and sends it
         *
         * @throws Exception
         * @return Email
         */
        public function saveAndSend()
        {
            $model = $this->save();

            $this->send();

            return $model;
        }

        /**
         * Saves the built mail to the database and queues it for sending
         *
         * @param null $delay
         *
         * @return Email
         */
        public function saveAndQueue($delay = null)
        {
            $model = $this->save();

            $job = SendMailJob::dispatch($model)
                ->onQueue(config('dtv.modules.emails.send_queue', 'default'));

            if ($delay !== null) {
                $job->delay($delay);
            }

            return $model;
        }

        /**
         * Returns an mailer instance
         *
         * @return Mailer
         */
        private static function newMail(): Mailer
        {
            return app(Mailer::class);
        }

        /**
         * Creates an new mail builder from an email model instance
         *
         * @param Email $email
         * @param bool  $asNewEmail
         *
         * @throws Exception
         * @return self
         */
        public static function instance(Email $email, bool $asNewEmail = false)
        {
            $mailBuilder = new self($email->emailAccount, $email->subject, $email->content, $email->html, new Addresses($email->to));

            foreach ($email->attachments as $attachment) {
                $mailBuilder->attachment(new DatabaseMailAttachment($attachment));
            }

            if ($asNewEmail === false) {
                $mailBuilder->useModel($email);
            }

            return $mailBuilder
                ->cc(new Addresses($email->cc))
                ->bcc(new Addresses($email->bcc));
        }
    }
