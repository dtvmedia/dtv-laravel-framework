<?php

/**
 * DTV Media Solutions
 *
 * PHP Version 7.0
 */

namespace DTV\EmailHandler\Services;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;

/**
 * Disk Mail Attachment Class
 *
 * @package   DTV\EmailHandler\Services
 * @copyright 2020 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
class DiskMailAttachment extends MailAttachment
{
    /**
     * File path
     *
     * @var string
     */
    protected string $path;

    /**
     * File disk
     *
     * @var string
     */
    protected string $disk;

    /**
     * MailAttachment constructor
     *
     * @param string $path File path
     * @param string $disk File disk
     *
     * @throws FileNotFoundException
     */
    public function __construct(string $path, string $disk = 'local')
    {
        $this->path = $path;
        $this->disk = $disk;

        if (!Storage::disk($disk)->exists($path)) {
            throw new FileNotFoundException('File "' . $path . '" was not found!');
        }
    }

    /**
     * Returns the name of the file
     *
     * @return string
     */
    public function getName()
    {
        return basename($this->path);
    }

    /**
     * Returns the MIME type of the file
     *
     * @return null|string
     */
    public function getType()
    {
        return Storage::disk($this->disk)->mimeType($this->path);
    }

    /**
     * Returns the content of the file
     *
     * @return null|string
     */
    public function getContent()
    {
        return Storage::disk($this->disk)->get($this->path);
    }

    /**
     * Returns the size of the file
     *
     * @return int
     */
    public function getSize()
    {
        return Storage::disk($this->disk)->size($this->path);
    }
}
