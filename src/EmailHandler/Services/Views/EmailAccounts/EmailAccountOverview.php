<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Services\Views\EmailAccounts;

    use DTV\BaseHandler\Views\ListView;
    use DTV\EmailHandler\Models\Email;
    use DTV\EmailHandler\Models\EmailAccount;

    /**
     * Email Account Overview Class
     *
     * @package   DTV\EmailHandler\Services\Views\EmailAccounts
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class EmailAccountOverview extends ListView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.emails::email_accounts.views.index';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-database';

        /**
         * Translation file used for translating column names etc
         *
         * @var string
         */
        protected $transPath = 'dtv.emails::email_accounts';

        /**
         * Private Mode Flag
         *
         * @var bool
         */
        protected $private;

        /**
         * EmailAccountOverview constructor.
         *
         * @param null $query
         * @param bool $private
         *
         * @throws \Exception
         */
        public function __construct( $query = null , bool $private = false )
        {
            $this->private = $private;

            parent::__construct( $query );
        }

        /**
         * Adds the columns
         */
        protected function columns()
        {
            $this->addColumn( 'name' , 'getNameLabel' , 'name' )->enableHtml();
            if ( !$this->private ) {
                $this->addColumn( 'in' , 'getInLabel' , [ 'in_server' , 'in_port' , 'in_flags' ] );
                $this->addColumn( 'out' , 'getOutLabel' , [ 'out_server' , 'out_port' , 'out_flags' ] );
            }
            $this->addColumn( 'email' , 'email' , 'email' );
            $this->addColumn( 'status' , 'getStatusLabel' , 'status' )->enableHtml();
        }

        /**
         * Adds the buttons for each record
         */
        protected function buttons()
        {
            $editParams = [];
            if ( $this->private ) {
                $editParams = [ 'private' => 1 ];
            }

            $this->addButton( 'show' , 'fa-list' , 'email_accounts.show' );
            $this->addButton( 'edit' , 'fa-pencil' , 'email_accounts.edit' )
                ->setParameter( $editParams );
            $this->addButton( 'addUsers' , 'fa-user-plus' , 'email_accounts.addUsers' )
                ->setCondition( function ( EmailAccount $emailAccount ) {
                    return $emailAccount->owner_id == null;
                } );
            $this->addConfirmButton( 'delete' , 'fa-times' , 'email_accounts.delete' );
        }
    }