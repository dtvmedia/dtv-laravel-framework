<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Services\Views\EmailAccounts;

    use DTV\BaseHandler\Models\User;
    use DTV\BaseHandler\Views\FormView;
    use DTV\EmailHandler\Models\EmailAccount;
    use Illuminate\Database\Eloquent\Model;

    /**
     * Mail Account Form View Class
     *
     * @package   DTV\EmailHandler\Services\Views\EmailAccounts
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class EmailAccountForm extends FormView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.emails::email_accounts.views.create';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-plus';

        /**
         * Translation file used for translating column names etc
         *
         * @var string
         */
        protected $transPath = 'dtv.emails::email_accounts';

        /**
         * Private Mode Flag
         *
         * @var bool
         */
        protected $private;

        /**
         * EmailAccountForm constructor.
         *
         * @param Model|null $model
         * @param bool       $private
         */
        public function __construct( Model $model = null , bool $private = false )
        {
            $this->private = $private;

            parent::__construct( $model );
        }

        /**
         * Adds the columns
         */
        public function inputs()
        {
            $userClass = config( 'dtv.models.user' , User::class );
            $users = [ null => option( 'dtv.emails::email_accounts.fields.multiple_users' , 'fa-users' ) ] + $userClass::getSelectOptions();

            $this->addText( 'name' )->setValidationRule( 'required' );
            $owner = $this->addSelect( 'owner_id' , $users );

            if ( $this->private ) {
                $owner->disabled( true );
            }

            $flagsIn = [
                'ssl' => option( 'dtv.emails::email_accounts.flags.ssl' , 'fas fa-lock text-success' ) ,
                'tls' => option( 'dtv.emails::email_accounts.flags.tls' , 'fas fa-lock text-success' ) ,
            ];
            $flagsOut = [
                'ssl' => option( 'dtv.emails::email_accounts.flags.ssl' , 'fa-lock text-success' ) ,
                'tls' => option( 'dtv.emails::email_accounts.flags.tls' , 'fa-lock text-success' ) ,
                ''    => option( 'dtv.emails::email_accounts.flags.noEncryption' , 'fa-exclamation-triangle text-warning' ) ,
            ];

            $this->addText( 'email' )->setValidationRule( 'required|email' );
            $this->addPassword( 'password' )->setValidationRule( 'required' );

            $this->addText( 'in_server' )->setSize( 5 )->setValidationRule( 'required' );
            $this->addText( 'in_port' , 'port' )->setSize( 3 )->setValidationRule( 'required|numeric|integer' );
            $this->addSelect( 'in_flags' , $flagsIn , 'encryption' )->setSize( 4 );

            $this->addText( 'out_server' )->setSize( 5 )->setValidationRule( 'required' );
            $this->addText( 'out_port' , 'port' )->setSize( 3 )->setValidationRule( 'required|numeric|integer' );
            $this->addSelect( 'out_flags' , $flagsOut , 'encryption' )->setSize( 4 );

            $this->addSelect( 'status' , [
                EmailAccount::STATUS_INACTIVE => option( 'dtv.emails::email_accounts.status.' . EmailAccount::STATUS_INACTIVE , 'fa-times text-danger' ) ,
                EmailAccount::STATUS_ACTIVE   => option( 'dtv.emails::email_accounts.status.' . EmailAccount::STATUS_ACTIVE , 'fa-check text-success' ) ,
            ] );
        }
    }