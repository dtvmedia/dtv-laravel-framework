<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Services\Views\EmailAccounts;

    use DTV\BaseHandler\Models\User;
    use DTV\BaseHandler\Views\FormView;

    /**
     * Mail Account Add User Form View Class
     *
     * @package   DTV\EmailHandler\Services\Views\EmailAccounts
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class EmailAccountUserForm extends FormView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.emails::email_accounts.views.addUser';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-user-plus';

        /**
         * Translation file used for translating column names etc
         *
         * @var string
         */
        protected $transPath = 'dtv.emails::email_accounts';

        /**
         * Adds the columns
         */
        public function inputs()
        {
            $this->addSelect( 'authorized_users' , User::getSelectOptions() )->setSize( 12 )->setMultiple( true );
        }
    }