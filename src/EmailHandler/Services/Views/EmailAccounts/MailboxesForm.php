<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Services\Views\EmailAccounts;

    use Ddeboer\Imap\ConnectionInterface;
    use DTV\BaseHandler\Views\FormView;
    use DTV\EmailHandler\Models\EmailAccount;
    use Illuminate\Database\Eloquent\Model;

    /**
     * Choose Mailboxes Form View Class
     *
     * @package   DTV\EmailHandler\Services\Views\EmailAccounts
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class MailboxesForm extends FormView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.emails::email_accounts.views.chooseMailboxes';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-inbox';

        /**
         * Translation file used for translating column names etc
         *
         * @var string
         */
        protected $transPath = 'dtv.emails::email_accounts';

        /**
         * Adds the columns
         */
        public function inputs()
        {
            $mailboxes = [];
            foreach ( $this->connect()->getMailboxes() as $mailbox ) {
                $mailboxes[ $mailbox->getName() ] = option( $mailbox->getName() , 'fa-inbox-in' );
            }

            $this->addSelect( 'mailbox_inbox' , $mailboxes )->setValidationRule( 'required' );
            $this->addSelect( 'mailbox_failed' , $mailboxes )->setValidationRule( 'required' );
        }

        /**
         * Connects to the email account
         *
         * @return ConnectionInterface
         */
        private function connect()
        {
            return $this->model->connect();
        }
    }