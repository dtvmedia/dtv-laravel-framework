<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Services\Views\Emails;

    use DTV\BaseHandler\Views\ListView;
    use DTV\EmailHandler\Models\Email;
    use DTV\EmailHandler\Models\EmailAccount;

    /**
     * Outgoing Email Overview Class
     *
     * @package   DTV\EmailHandler\Services\Views\Emails
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class OutgoingEmailOverview extends ListView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.emails::emails.views.index';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-envelope';

        /**
         * Translation file used for translating column names etc
         *
         * @var string
         */
        protected $transPath = 'dtv.emails::emails';

        /**
         * Order Column
         *
         * @var string
         */
        protected $orderColumn = 'sent_at';

        /**
         * Order Direction
         *
         * @var string
         */
        protected $orderDirection = 'desc';

        /**
         * Adds the columns
         */
        protected function columns()
        {
            $this->addColumn( 'sent_at' , 'sent_at' , 'sent_at' );
            $this->addColumn( 'to' , 'getToShort' , 'to' )->enableHtml();
            $this->addColumn( 'subject' , 'getSubjectLabel' , [ 'subject' , 'status' ] )->enableHtml();
        }

        /**
         * Adds the buttons for each record
         */
        protected function buttons()
        {
            $hasMailAccountCheck = function (Email $email) {
                return !empty($email->email_account_id);
            };

            $this->addButton( 'show' , 'fa-folder-open' , 'emails.show' )
                ->setDynamicParameter( [ 'emailAccount' => 'email_account_id', 'id' => 'id' ] )
                ->setCondition($hasMailAccountCheck);
            $this->addConfirmButton( 'delete' , 'fa-times' , 'emails.delete' )
                ->setDynamicParameter( [ 'emailAccount' => 'email_account_id', 'id' => 'id' ] )
                ->setCondition($hasMailAccountCheck);
        }
    }
