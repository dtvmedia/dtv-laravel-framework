<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Services\Views\Emails;

    use DTV\BaseHandler\Views\ListView;
    use DTV\EmailHandler\Models\Email;
    use DTV\EmailHandler\Models\EmailAccount;

    /**
     * Incoming Email Overview Class
     *
     * @package   DTV\EmailHandler\Services\Views\Emails
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class IncomingEmailOverview extends ListView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.emails::emails.views.index';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-envelope';

        /**
         * Translation file used for translating column names etc
         *
         * @var string
         */
        protected $transPath = 'dtv.emails::emails';

        /**
         * Order Column
         *
         * @var string
         */
        protected $orderColumn = 'received_at';

        /**
         * Order Direction
         *
         * @var string
         */
        protected $orderDirection = 'desc';

        /**
         * Adds the columns
         */
        protected function columns()
        {
            $this->addMeta( 'id' );
            $this->addColumn( 'received_at' , 'received_at.toDateString' , 'received_at' );
            $this->addColumn( 'from' , 'getFromShort' , 'from' )->enableHtml();
            $this->addColumn( 'subject' , 'getSubjectLabel' , [ 'subject' , 'status' ] )->enableHtml();
        }

        /**
         * Adds the buttons for each record
         */
        protected function buttons()
        {
            $this->addButton( 'show' , 'fa-folder-open' , 'emails.show' )
                ->setDynamicParameter( [ 'emailAccount' => 'email_account_id' , 'id' => 'id' ] );
            $this->addConfirmButton( 'markAsSpam' , 'fa-ban' , 'emails.move' )
                ->setParameter( [ 'box' => 'spambox' ] )
                ->setDynamicParameter( [ 'emailAccount' => 'email_account_id' , 'id' => 'id' ] )
                ->setCondition( function ( Email $email ) {
                    return !$email->isSpam();
                } )
                ->setMessage( 'dtv.emails::emails.messages.markAsSpam' )
                ->setConfirmButtonLabel( 'dtv.emails::emails.buttons.markAsSpam' );
            $this->addButton( 'unmarkAsSpam' , 'fa-undo' , 'emails.move' )
                ->setParameter( [ 'box' => 'inbox' ] )
                ->setDynamicParameter( [ 'emailAccount' => 'email_account_id' , 'id' => 'id' ] )
                ->setCondition( function ( Email $email ) {
                    return $email->isSpam();
                } );
            $this->addConfirmButton( 'delete' , 'fa-times' , 'emails.move' )
                ->setParameter( [ 'box' => 'trashbox' ] )
                ->setDynamicParameter( [ 'emailAccount' => 'email_account_id' , 'id' => 'id' ] )
                ->setCondition( function ( Email $email ) {
                    return $email->deleted_at->isNull();
                } );
            $this->addConfirmButton( 'hardDelete' , 'fa-times' , 'emails.hardDelete' )
                ->setDynamicParameter( [ 'emailAccount' => 'email_account_id' , 'id' => 'id' ] )
                ->setCondition( function ( Email $email ) {
                    return !$email->deleted_at->isNull();
                } );
        }
    }