<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Services\Views\Emails;

    use DTV\BaseHandler\Views\FormView;

    /**
     * Move Mail Form View Class
     *
     * @package   DTV\EmailHandler\Services\Views\EmailFolders
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class ResendToOtherForm extends FormView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.emails::emails.buttons.resend_other';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-repeat';

        /**
         * Translation file used for translating column names etc
         *
         * @var string
         */
        protected $transPath = 'dtv.emails::emails';

        /**
         * Adds the columns
         */
        public function inputs()
        {
            $this->addText('to')
                ->setValidationRule('required|email:strict')
                ->setSize(12);
        }
    }
