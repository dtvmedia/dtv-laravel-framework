<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Services\Views\EmailFolders;

    use DTV\BaseHandler\Views\FormView;

    /**
     * Mail Folder Form View Class
     *
     * @package   DTV\EmailHandler\Services\Views\EmailFolders
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class EmailFolderForm extends FormView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.emails::email_accounts.views.createFolder';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-folder-plus';

        /**
         * Translation file used for translating column names etc
         *
         * @var string
         */
        protected $transPath = 'dtv.emails::email_accounts';

        /**
         * Adds the columns
         */
        public function inputs()
        {
            $this->addText( 'name' )
                ->setValidationRule( 'required|max:64' )
                ->setSize( 12 );
        }
    }