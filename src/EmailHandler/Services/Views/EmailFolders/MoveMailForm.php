<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Services\Views\EmailFolders;

    use DTV\BaseHandler\Views\FormView;
    use DTV\EmailHandler\Models\EmailAccount;
    use DTV\EmailHandler\Models\EmailFolder;

    /**
     * Move Mail Form View Class
     *
     * @package   DTV\EmailHandler\Services\Views\EmailFolders
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class MoveMailForm extends FormView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.emails::emails.views.move';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-exchange';

        /**
         * Translation file used for translating column names etc
         *
         * @var string
         */
        protected $transPath = 'dtv.emails::emails';

        /**
         * Email Account Instance
         *
         * @var EmailAccount
         */
        protected $emailAccount;

        /**
         * MoveMailForm constructor.
         *
         * @param EmailAccount $emailAccount
         */
        public function __construct( EmailAccount $emailAccount )
        {
            $this->emailAccount = $emailAccount;

            parent::__construct( null );
        }

        /**
         * Adds the columns
         */
        public function inputs()
        {
            $this->addSelect( 'email_folder_id' , EmailFolder::getSelectOptions( $this->emailAccount->emailFolders() ) )
                ->setValidationRule( 'required' )
                ->setSize( 12 );
        }
    }