<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Services;

    use DTV\EmailHandler\Layouts\Mailable;
    use DTV\Support\Contracts\ManagerContract;
    use InvalidArgumentException;

    /**
     * Mailable Manager
     *
     * @package   DTV\EmailHandler\Services
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class MailableManager implements ManagerContract
    {
        /**
         * Array of registered mailables
         *
         * @var array
         */
        protected array $mailables = [];

        /**
         * Registers an mailable layout class
         *
         * @param string $class
         *
         * @return void
         */
        public function register(string $class): void
        {
            if (!is_a($class, Mailable::class, true)) {
                throw new InvalidArgumentException($class . ' is not a valid mailable class!');
            }

            $key = $class::getKey();

            if ($this->has($key)) {
                logger()->warning('The mailable key ' . $key . ' is registered twice!');
            }

            $this->mailables[$key] = $class;
        }

        /**
         * Returns if an given key is already registered
         *
         * @param string $key
         *
         * @return bool
         */
        public function has(string $key): bool
        {
            return isset($this->mailables[$key]);
        }

        /**
         * Returns an specific mailable
         *
         * @param string $key
         *
         * @return string|null
         */
        public function get(string $key): ?string
        {
            if (isset($this->mailables[$key])) {
                return $this->mailables[$key];
            }

            return null;
        }

        /**
         * Returns the array of all registered mailable classes
         *
         * @return array
         */
        public function all(): array
        {
            return $this->mailables;
        }

        /**
         * Returns the mailable select options
         *
         * @return array
         */
        public static function getSelectOptions(): array
        {
            $options = [];

            foreach (app('mailables')->all() as $key => $mailable) {
                if ($mailable::$displayName === null) {
                    $mailable::$displayName = class_basename($mailable);
                }

                $options[$key] = option($mailable::$displayName, $mailable::$optionIcon);
            }

            return $options;
        }
    }
