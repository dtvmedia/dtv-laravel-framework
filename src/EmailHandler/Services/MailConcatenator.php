<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Services;

    use DTV\EmailHandler\Models\Email;
    use Html2Text\Html2Text;

    /**
     * Mail Concatenator Class
     *
     * @package   DTV\EmailHandler\Services
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class MailConcatenator
    {
        /**
         * New mail instance
         *
         * @var Email
         */
        protected Email $newMail;

        /**
         * Old mail instance
         *
         * @var Email
         */
        protected Email $oldMail;

        /**
         * MailConcatenator constructor.
         *
         * @param Email $newMail
         * @param Email $oldMail
         */
        public function __construct(Email $newMail, Email $oldMail)
        {
            $this->newMail = $newMail;
            $this->oldMail = $oldMail;
        }

        /**
         * Inits an mail concatenator instance by an mail builder instance
         *
         * @param MailBuilder $builder
         * @param Email       $oldMail
         *
         * @return MailConcatenator
         */
        public static function initFromBuilder(MailBuilder $builder, Email $oldMail)
        {
            $tmpMail = new Email([
                'content' => $builder->getContent(),
                'html'    => $builder->getHtml(),
            ]);

            return new self($tmpMail, $oldMail);
        }

        /**
         * Concatenates the 2 given mails
         *
         * @return Email
         */
        public function concatenate()
        {
            $html = true;

            if ($this->newMail->isTextMail() && $this->oldMail->isTextMail()) {
                $html = false;
                $content = $this->concatenateTextAndText();
            } elseif ($this->newMail->isTextMail() && $this->oldMail->isHtmlMail()) {
                $content = $this->concatenateTextAndHtml();
            } elseif ($this->newMail->isHtmlMail() && $this->oldMail->isTextMail()) {
                $content = $this->concatenateHtmlAndText();
            } else {
                $content = $this->concatenateHtmlAndHtml();
            }

            return new Email(compact('html', 'content'));
        }

        /**
         * Returns the intro to the appended mail
         *
         * @param bool $html
         *
         * @return string
         */
        private function getIntro(bool $html = false)
        {
            $intro = "\n\n";

            if ($html) {
                $intro .= '<hr>';
            } else {
                $intro .= str_repeat('-', 40) . "\n";
            }

            $intro .= $this->oldMail->from . ' '
                      . trans('dtv.emails::emails.fields.wrote_at') . ' '
                      . $this->oldMail->received_at->toDateTimeString() . ": \n\n";

            if ($html) {
                return nl2br($intro);
            }

            return $intro;
        }

        /**
         * Concatenates two text emails
         *
         * @return string
         */
        private function concatenateTextAndText()
        {
            return $this->newMail->content . $this->getIntro() . $this->oldMail->content;
        }

        /**
         * Concatenates a html mail to a text email
         *
         * @return string
         */
        private function concatenateTextAndHtml()
        {
            return nl2br($this->newMail->content) . $this->getIntro(true) . $this->oldMail->content;
        }

        /**
         * Concatenates a text mail to a html email
         *
         * @return string
         */
        private function concatenateHtmlAndText()
        {
            return $this->newMail->content . $this->getIntro(true) . nl2br($this->oldMail->content);
        }

        /**
         * Concatenates two html emails
         *
         * @return string
         */
        private function concatenateHtmlAndHtml()
        {
            return $this->newMail->content . $this->getIntro(true) . $this->simplifyHtml($this->oldMail->content);
        }

        /**
         * Simplifies some html content
         *
         * @param string $content
         *
         * @return string
         */
        private function simplifyHtml( string $content )
        {
            $html = new Html2Text($content, [
                'do_links' => 'bbcode',
            ]);

            $text = trim($html->getText());

            return nl2br(preg_replace_callback('/\[url=(.*)]([\s\S]*)\[\/url]/mU', function ($matches) {
                if (empty(trim($matches[2]))) {
                    return '';
                }

                return '<a href="' . $matches[1] . '">' . $matches[2] . '</a>';
            }, $text));
        }
    }
