<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Services;

    use Countable;
    use DTV\EmailHandler\Contracts\NotifiableByEmail;
    use DTV\Support\MorphIdHandler;
    use Exception;
    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Support\Facades\Validator;
    use Illuminate\Validation\ValidationException;
    use Iterator;

    /**
     * MailAddressCollection
     *
     * @package   DTV\EmailHandler\Services
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class MailAddressCollection implements Iterator , Arrayable , Countable
    {
        /**
         * Array of email addresses
         *
         * @var array
         */
        protected $addresses = [];

        /**
         * Iterator position
         *
         * @var int
         */
        private $position = 0;

        /**
         * MailAddressCollection constructor.
         *
         * @param array|string $addresses
         *
         * @throws Exception
         */
        public function __construct( $addresses = [] )
        {
            $addresses = array_wrap( $addresses );

            foreach ( $addresses as $address ) {
                if ( $address instanceof NotifiableByEmail ) {
                    $this->add( $address->getEmail() , $address->getEmailName() , $address );

                    continue;
                }

                if ( is_string( $address ) ) {
                    [ $email , $name , $reference ] = $this->parseMailFromAddressString( $address );

                    $this->add( $email , $name , $reference );

                    continue;
                }

                if ( is_array( $address ) && isset( $address[ 'email' ] ) ) {
                    $this->add( $address[ 'email' ] , optional( $address )[ 'name' ] , optional( $address )[ 'reference' ] );

                    continue;
                }

                throw new Exception( 'Invalid addresses' );
            }
        }

        /**
         * Parses the mail from an address string and returns it
         *
         * @param string $address
         *
         * @throws Exception
         * @return array
         */
        protected function parseMailFromAddressString( string $address )
        {
            $name = null;
            $reference = null;

            if ( MorphIdHandler::isMorphId( $address ) ) {
                $address = MorphIdHandler::getModel( $address );
            }

            if ( $address instanceof NotifiableByEmail ) {
                $name = $address->getEmailName();
                $email = $address->getEmail();
                $reference = $address;
            } elseif ( str_contains( $address , '<' ) ) {
                [ $name , $email ] = explode( '<' , $address , 2 );
                $name = trim( $name );
                $email = trim( $email , " \t\n\r\0\x0B><" );
            } else {
                $email = $address;
            }

            return [ $email , $name , $reference ];
        }

        /**
         * Adds an email address to the collection
         *
         * @param string                 $email
         * @param string|null            $name
         * @param NotifiableByEmail|null $reference
         *
         * @throws ValidationException
         */
        public function add( string $email , string $name = null , NotifiableByEmail $reference = null )
        {
            $address = [
                'email'     => $email ,
                'name'      => $name ,
                'reference' => $reference ,
            ];

            Validator::make( $address , [
                'email' => 'required|email' ,
                'name'  => 'nullable|string|max:64' ,
            ] )->validate();

            $this->addresses[] = $address;
        }

        /**
         * Returns the address array as mail to labels
         *
         * @return array
         */
        public function getAsLabelArray()
        {
            $addresses = [];

            foreach ( $this->addresses as $address ) {
                $addresses[] = ( ( $address[ 'name' ] == null ) ? $address[ 'email' ] : $address[ 'name' ] . ' <' . $address[ 'email' ] . '>' );
            }

            return $addresses;
        }

        /**
         * Returns the address array in a swift mailer compatible way
         *
         * @return array
         */
        public function getAsSwiftMailerArray()
        {
            $addresses = [];

            foreach ( $this->addresses as $address ) {
                if ( $address[ 'name' ] == null ) {
                    $addresses[] = $address[ 'email' ];
                } else {
                    $addresses[ $address[ 'email' ] ] = $address[ 'name' ];
                }
            }

            return $addresses;
        }

        /**
         * Return the current element
         * @link  http://php.net/manual/en/iterator.current.php
         * @return mixed Can return any type.
         * @since 5.0.0
         */
        public function current(): mixed
        {
            return $this->addresses[ $this->position ];
        }

        /**
         * Move forward to next element
         * @link  http://php.net/manual/en/iterator.next.php
         * @return void Any returned value is ignored.
         * @since 5.0.0
         */
        public function next(): void
        {
            ++ $this->position;
        }

        /**
         * Return the key of the current element
         * @link  http://php.net/manual/en/iterator.key.php
         * @return mixed scalar on success, or null on failure.
         * @since 5.0.0
         */
        public function key(): mixed
        {
            return $this->position;
        }

        /**
         * Checks if current position is valid
         * @link  http://php.net/manual/en/iterator.valid.php
         * @return boolean The return value will be casted to boolean and then evaluated.
         * Returns true on success or false on failure.
         * @since 5.0.0
         */
        public function valid(): bool
        {
            return isset( $this->addresses[ $this->position ] );
        }

        /**
         * Rewind the Iterator to the first element
         * @link  http://php.net/manual/en/iterator.rewind.php
         * @return void Any returned value is ignored.
         * @since 5.0.0
         */
        public function rewind(): void
        {
            $this->position = 0;
        }

        /**
         * Get the instance as an array.
         *
         * @return array
         */
        public function toArray()
        {
            return $this->addresses;
        }

        /**
         * Count elements of an object
         * @link  http://php.net/manual/en/countable.count.php
         * @return int The custom count as an integer.
         * </p>
         * <p>
         * The return value is cast to an integer.
         * @since 5.1.0
         */
        public function count(): int
        {
            return count( $this->addresses );
        }
    }
