# EmailHandler - DTV Laravel Framework

## Requirements
- ddeboer/imap
- html2text/html2text
- PHP ext-imap

## Setup Hints
In order to support larger email attachments (>1MB) you need to change following mysql settings:

- max_allowed_packet = 8M
- innodb_log_file_size = 64M

This would support attachments up to 8 MB. The log file size should be around 10 times larger than the allowed packet size.

## Configs
- *enabled*: Package activation switch
- *routes_enabled*: Activation switch for the package routes seperated from the package switch itself
- *debug*: Defines the connection credentials for an debug mail account or an service like debugmail.io
- *search*: Defines which models (keys) and fields (values) the autocompletion should search for (see below for details)
- *layout_color*: Defines an colour which will be used for the default html mail layout (needs to be a valid css color value)
- *default_layout*: Defines the by default selected mail layout
- *send_queue*: Defines in which queue the sendMailJobs should be pushed

## Usage 
### General hints regarding mail account access

- Email Accounts can either be private or non-private accounts
- If an account is private, then the account is only visible to the related user itself (and the management also for admins)
- Users can also manage their private mail accounts
- If an user has also access to some non-private accounts he can only see the mails but not the account management
- admins can give users access to non-private mail accounts
- private mail accounts only belong to exactly one user

### Register Mailable Layouts

In order to register an custom mailable class add the following line to the AppServiceProviders register function:
```
$this->app->make( 'mailables' )->register( YourMailable::class );
```

### Configure Email Autocompletion
The email autocompletion can be configured via the ``search`` config value. There you can define an array of models and there fields which should be used for the autocompletion search.
You can of course define multiple search field per model. You can also use raw query definitions for things like combined search fields:
```php
'search' => [
    \App\Models\Customer::class        => [ 'email' , 'company' ] ,
    \App\Models\CustomerContact::class => [ 'email' , 'RAW::CONCAT(first_name, " ", last_name)' ]
] ,
``` 
