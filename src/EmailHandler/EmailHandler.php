<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler;

    use DTV\EmailHandler\Layouts\Mailable;
    use DTV\EmailHandler\Models\EmailAccount;
    use DTV\EmailHandler\Services\MailAddressCollection as Addresses;
    use DTV\EmailHandler\Services\MailAddressCollection;
    use DTV\EmailHandler\Services\MailBuilder;
    use Exception;
    use Illuminate\Database\Eloquent\Model;

    /**
     * Email Handler
     *
     * @package   DTV\EmailHandler
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class EmailHandler
    {
        /**
         * Builds an mail
         *
         * @param EmailAccount|int      $account
         * @param string                $subject
         * @param Mailable              $mailable
         * @param MailAddressCollection $to
         *
         * @throws Exception
         *
         * @return MailBuilder
         */
        public function createMail( $account , string $subject , Mailable $mailable , Addresses $to )
        {
            $account = EmailAccount::instance( $account , false );

            return new MailBuilder( $account , $subject , $mailable->build()->render() , $mailable->isHtmlMail() , $to );
        }

        /**
         * Tests an new mail account connection
         *
         * @param array $attr
         *
         * @throws Exception
         *
         * @return EmailAccount
         */
        public function testNewMailAccount( array $attr )
        {
            $account = new EmailAccount();
            $account->name = $attr[ 'name' ];
            $account->in_server = $attr[ 'in_server' ];
            $account->in_port = $attr[ 'in_port' ];
            $account->in_flags = $attr[ 'in_flags' ];
            $account->out_server = $attr[ 'out_server' ];
            $account->out_port = $attr[ 'out_port' ];
            $account->out_flags = $attr[ 'out_flags' ];
            $account->email = $attr[ 'email' ];
            $account->password = $attr[ 'password' ];
            $account->status = $attr[ 'status' ];
            $account->owner_id = $attr[ 'owner_id' ];

            if ( $account->testInConnection() === false ) {
                throw new Exception( 'Invalid incoming server data' );
            }

            if ( $account->testOutConnection() === false ) {
                throw new Exception( 'Invalid outgoing server data' );
            }

            return $account;
        }

        /**
         * Creates an new email account
         *
         * @param array $attr
         *
         * @throws Exception
         *
         * @return EmailAccount
         */
        public function createMailAccount( array $attr )
        {
            $mailAccount = $this->testNewMailAccount( $attr );

            $mailAccount->status = EmailAccount::STATUS_INACTIVE;
            $mailAccount->mailbox_inbox = 'INBOX';
            $mailAccount->mailbox_failed = 'INBOX';
            $mailAccount->save();

            $mailAccount->users()->attach( auth()->id() );

            return $mailAccount;
        }

        /**
         * Edits an given email account
         *
         * @param EmailAccount $emailAccount
         * @param array        $attr
         *
         * @throws Exception
         *
         * @return EmailAccount
         */
        public function editMailAccount( EmailAccount $emailAccount , array $attr )
        {
            $oldOwnerID = $emailAccount->owner_id;
            $attr[ 'errors' ] = 0;
            $emailAccount->fill( $attr );

            if ( $emailAccount->testInConnection() === false ) {
                throw new Exception( 'Invalid incoming server data' );
            }

            if ( $emailAccount->testOutConnection() === false ) {
                throw new Exception( 'Invalid outgoing server data' );
            }

            $emailAccount->save();

            if ( $oldOwnerID != $emailAccount->owner_id ) {
                if ( $oldOwnerID == null || $emailAccount->owner_id != null ) {
                    // if we switched from an public account to an private we need to remove all access relation beside the owner
                    // and if we just have another owner we also need to remove the previous access relation and add the owner
                    $emailAccount->users()->sync( $emailAccount->owner_id );
                }
            }

            return $emailAccount;
        }
    }
