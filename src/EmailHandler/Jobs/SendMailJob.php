<?php

/**
 * DTV Media Solutions
 *
 * PHP Version 7.0
 */

namespace DTV\EmailHandler\Jobs;

use DTV\EmailHandler\Models\Email;
use DTV\EmailHandler\Services\MailBuilder;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Symfony\Component\Mailer\Exception\TransportException;

/**
 * Send Email Job
 *
 * @package   DTV\EmailHandler\Jobs
 * @copyright 2019 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
class SendMailJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * Email instance
     *
     * @var Email
     */
    protected $email;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Create a new job instance.
     *
     * @param  Email $email
     *
     * @return void
     */
    public function __construct( Email $email )
    {
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @throws \Throwable
     * @return void
     */
    public function handle()
    {
        try {
            MailBuilder::instance($this->email)->send();
        } catch (\Throwable $e) {
            // In case of SMTP 501 - invalid email, we just log a debug message but mark the job as success
            if ($e instanceof TransportException && $e->getCode() === 501) {
                logger('Invalid email given: ' . json_encode($this->email->to));

                return;
            }

            // For all other errors we do the default error retry handling
            logger()->warning($e);

            if ($this->attempts() > ($this->tries - 1)) {
                throw $e;
            }

            // If it fails, release it an try it 10 minutes later again
            $this->release(60 * 10);
        }
    }
}
