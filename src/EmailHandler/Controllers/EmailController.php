<?php /** @noinspection PhpUnused */

/**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Controllers;

    use Carbon\Carbon;
    use DTV\BaseHandler\Controllers\Controller;
    use DTV\BaseHandler\JsonResponse;
    use DTV\BaseHandler\Models\Model;
    use DTV\BaseHandler\Views\Page;
    use DTV\EmailHandler\Contracts\NotifiableByEmail;
    use DTV\EmailHandler\EmailHandler;
    use DTV\EmailHandler\Models\Email;
    use DTV\EmailHandler\Models\EmailAccount;
    use DTV\EmailHandler\Models\EmailFolder;
    use DTV\EmailHandler\Services\DatabaseMailAttachment;
    use DTV\EmailHandler\Services\MailAddressCollection;
    use DTV\EmailHandler\Services\MailBuilder;
    use DTV\EmailHandler\Services\UploadedMailAttachment;
    use DTV\EmailHandler\Services\Views\Emails\IncomingEmailOverview;
    use DTV\EmailHandler\Services\Views\Emails\OutgoingEmailOverview;
    use DTV\EmailHandler\Services\Views\Emails\ResendToOtherForm;
    use DTV\Support\Contracts\NotifiableGroup;
    use DTV\Support\MorphIdHandler;
    use Exception;
    use Illuminate\Contracts\View\View;
    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Http\Request;
    use Illuminate\Http\Response;
    use Illuminate\Support\Facades\DB;
    use Throwable;

    /**
     * Email Controller
     *
     * @package   DTV\EmailHandler\Controllers
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class EmailController extends Controller
    {
        /**
         * Returnes the json data for the sms autocomplete options
         *
         * @param Request $request
         *
         * @return \Illuminate\Http\JsonResponse
         */
        public function getAutocompleteOptions( Request $request )
        {
            $results = [];
            $search = $request->get( 'term' , '' );

            foreach ( config( 'dtv.modules.emails.search' , [] ) as $model => $cols ) {
                if (!is_a($model, NotifiableByEmail::class, true) || !$model instanceof Model) {
                    continue;
                }

                $query = $model::query();
                $cols = array_wrap( $cols );

                foreach ( $cols as $col ) {
                    if ( starts_with( $col , 'RAW::' ) ) {
                        $col = DB::raw( str_replace( 'RAW::' , '' , $col ) );
                    }

                    $query->orWhere( $col , 'LIKE' , '%' . $search . '%' );
                }

                foreach ( $query->limit( 10 )->get() as $item ) {
                    /** @var NotifiableByEmail $item */
                    if ( !empty( $item->getEmail() ) ) {
                        $results[] = [
                            'label' => $item->getEmailName() ,
                            'value' => MorphIdHandler::getIdByModel( $item ) ,
                            'email' => $item->getEmail()
                        ];
                    }
                }
            }

            return response()->json( $results );
        }

        /**
         * Email index page
         *
         * @throws Throwable
         *
         * @return Page|string
         */
        public function index()
        {
            $emailAccount = EmailAccount::own()->orderBy( 'owner_id' , 'DESC' )->first();

            if ( $emailAccount !== null ) {
                return redirect()->route( 'emails.inbox' , [ 'emailAccount' => $emailAccount->id ] );
            }

            return $this->view( 'dtv.emails::indexNoAccountAvailable' );
        }

        /**
         * Shows the incoming emails overview
         *
         * @param EmailAccount $emailAccount
         *
         * @throws Throwable
         *
         * @return Page|string
         */
        public function inbox( EmailAccount $emailAccount )
        {
            $emails = $emailAccount->emails()->inbox();

            return $this->inview( $emails , $emailAccount );
        }

        /**
         * Shows the spam emails overview
         *
         * @param EmailAccount $emailAccount
         *
         * @throws Throwable
         *
         * @return Page|string
         */
        public function spambox( EmailAccount $emailAccount )
        {
            $emails = $emailAccount->emails()->spambox();

            return $this->inview( $emails , $emailAccount );
        }

        /**
         * Shows the outgoing emails overview
         *
         * @param EmailAccount $emailAccount
         *
         * @throws Throwable
         *
         * @return Page|string
         */
        public function trashbox( EmailAccount $emailAccount )
        {
            $emails = $emailAccount->emails()->trashbox();

            return $this->inview( $emails , $emailAccount );
        }

        /**
         * Shows the emails overview of the given email folder
         *
         * @param EmailAccount $emailAccount
         * @param int          $id
         *
         * @throws Throwable
         *
         * @return Page|string
         */
        public function folder( EmailAccount $emailAccount , $id )
        {
            $emails = $emailAccount->emails()->folder( $id );

            return $this->inview( $emails , $emailAccount );
        }

        /**
         * Shows the incoming mail view for different filters
         *
         * @param Builder      $builder
         * @param EmailAccount $emailAccount
         *
         * @throws Throwable
         *
         * @return Page|string
         */
        private function inview( $builder , EmailAccount $emailAccount )
        {
            $this->addHeaderButton( 'dtv.emails::emails.buttons.create' , 'fa-plus' , 'emails.create' );

            return $this->view( 'dtv.emails::index' )
                ->with( 'mailAccount' , $emailAccount )
                ->with( 'emailAccounts' , EmailAccount::own()->orderBy( 'owner_id' , 'DESC' )->withCount( 'newIncomingEmails' )->get() )
                ->withView( 'emailOverview' , ( new IncomingEmailOverview( $builder->withCount( 'notEmbeddedAttachments' ) ) )->hideTitle() );
        }

        /**
         * Shows the outgoing emails overview
         *
         * @param EmailAccount $emailAccount
         *
         * @throws Throwable
         *
         * @return Page|string
         */
        public function outbox( EmailAccount $emailAccount )
        {
            $emails = $emailAccount->emails()->outbox();

            $this->addHeaderButton( 'dtv.emails::emails.buttons.create' , 'fa-plus' , 'emails.create' );

            return $this->view( 'dtv.emails::index' )
                ->with( 'mailAccount' , $emailAccount )
                ->with( 'emailAccounts' , EmailAccount::own()->orderBy( 'owner_id' , 'DESC' )->withCount( 'newIncomingEmails' )->get() )
                ->withView( 'emailOverview' , ( new OutgoingEmailOverview( $emails->withCount( 'notEmbeddedAttachments' ) ) )->hideTitle() );
        }

        /**
         * Shows the email details page
         *
         * @param EmailAccount $emailAccount
         * @param int          $id
         *
         * @throws Throwable
         * @return JsonResponse|Page|Response|string
         */
        public function show( EmailAccount $emailAccount , $id )
        {
            $email = $emailAccount->emails()->withTrashed()->findOrFail( $id );

            // Update status to read for new incoming mails
            if ( $email->isIncoming() && $email->status == Email::STATUS_IN_NEW ) {
                $email->update( [ 'status' => Email::STATUS_IN_READ ] );
            }

            // Buttons
            if ( $email->isIncoming() ) {
                if ( !$email->isSpam() ) {
                    $this->addHeaderButton( 'dtv.emails::emails.buttons.markAsSpam' , 'fa-ban' , 'emails.move' ,
                        [ 'id' => $id , 'emailAccount' => $emailAccount->id , 'box' => 'spambox' ] , true )
                        ->setMessage( 'dtv.emails::emails.messages.markAsSpam' )
                        ->setConfirmButtonLabel( 'dtv.emails::emails.buttons.markAsSpam' )
                        ->setMethod( 'POST' );
                } else {
                    $this->addHeaderButton( 'dtv.emails::emails.buttons.unmarkAsSpam' , 'fa-undo' , 'emails.move' ,
                        [ 'id' => $id , 'emailAccount' => $emailAccount->id , 'box' => 'inbox' ] );
                }

                if ( $email->deleted_at->isNull() ) {
                    $this->addHeaderButton( 'dtv.emails::emails.buttons.answer' , 'fa-reply' , 'emails.answer' ,
                        [ 'id' => $id , 'emailAccount' => $emailAccount->id ] );
                    $this->addHeaderButton( 'dtv.emails::emails.buttons.forward' , 'fa-share' , 'emails.forward' ,
                        [ 'id' => $id , 'emailAccount' => $emailAccount->id ] );
                }

                if ( $email->deleted_at->isNull() ) {
                    $this->addHeaderButton( 'dtv.emails::emails.buttons.delete' , 'fa-times' , 'emails.move' ,
                        [ 'id' => $id , 'emailAccount' => $emailAccount->id , 'box' => 'trashbox' ] , true );
                } else {
                    $this->addHeaderButton( 'dtv.emails::emails.buttons.hardDelete' , 'fa-times' , 'emails.hardDelete' ,
                        [ 'id' => $id , 'emailAccount' => $emailAccount->id ] , true );
                }
            } else {
                $this->addHeaderButton( 'dtv.emails::emails.buttons.resend' , 'fa-repeat' , 'emails.resend' ,
                    [ 'id' => $id , 'emailAccount' => $emailAccount->id ] , true )
                    ->setConfirmButtonLabel('dtv.emails::emails.buttons.resend')
                    ->setMessage('dtv.emails::emails.messages.resendConfirmation')
                    ->setMethod('GET');
                $this->addHeaderButton( 'dtv.emails::emails.buttons.resend_other' , 'fa-repeat' , 'emails.resendToOther' ,
                    [ 'id' => $id , 'emailAccount' => $emailAccount->id ] );
                $this->addHeaderButton( 'dtv.emails::emails.buttons.delete' , 'fa-times' , 'emails.delete' ,
                    [ 'id' => $id , 'emailAccount' => $emailAccount->id ] , true );
            }

            $iframeUrl = route( 'emails.content', [ 'id' => $email->id , 'emailAccount' => $emailAccount->id  ] );

            return $this->modal( 'dtv.emails::show' , compact( 'emailAccount' , 'email' , 'iframeUrl' ) );
        }

        /**
         * Returns the mail content
         *
         * @param EmailAccount $emailAccount
         * @param int          $id
         *
         * @return View
         */
        public function content( EmailAccount $emailAccount , $id )
        {
            $email = $emailAccount->emails()->withTrashed()->findOrFail( $id );
            $content = $email->content;

            if ( $email->isTextMail() ) {
                $content = nl2br( $content );
            }

            if ( $email->isSpam() ) {
                $content = strip_tags( $content , '<br><p>' );
            } else {
                // Search for any cid links and replace them with the links to the related file
                $content = preg_replace_callback( '/src="cid:(.*)"/U' , function ( $match ) use ( $email , $emailAccount ) {
                    $file = $email->attachments()->where( 'content_id' , '<' . $match[ 1 ] . '>' )->first();

                    if ($file === null) {
                        return 'src=""';
                    }

                    $url = route('emails.attachments.show', [
                        'emailAccount' => $emailAccount->id,
                        'mid'          => $email->id,
                        'id'           => $file->id,
                    ]);

                    return 'src="' . $url . '"';
                } , $content );

                // replace http images with https links if https is enabled in order to prevent mixed content errors
                if ( request()->isSecure() ) {
                    $content = preg_replace( '/src="http:\/\//m' , 'src="https://' , $content );
                }
            }

            return view( 'dtv.emails::content' , compact( 'content' ) );
        }


        /**
         * MOVE FUNCTIONS
         */

        /**
         * Deletes an given outgoing mail and the related attachments
         *
         * @param EmailAccount $emailAccount
         * @param int          $id
         *
         * @throws Exception
         *
         * @return JsonResponse
         */
        public function delete( EmailAccount $emailAccount , $id )
        {
            $email = $emailAccount->emails()->where( 'incoming' , 0 )->findOrFail( $id );

            return $this->deleteModel( $email , [] , [ 'attachments' ] )->reloadListView( IncomingEmailOverview::class )->hideModal();
        }

        /**
         * Hard deletes an incoming mail and all related attachments and references
         *
         * @param EmailAccount $emailAccount
         * @param int          $id
         *
         * @return JsonResponse
         */
        public function hardDelete( EmailAccount $emailAccount , $id )
        {
            // Mark email as hard deleted
            $emailAccount->emails()->trashbox()->findOrFail( $id )->update( [
                'hard_deleted_at' => Carbon::now() ,
            ] );

            return $this->json()->success( 'dtv.emails::emails.messages.hardDeleted' )->reload();
        }

        /**
         * Moves an incoming mail to another box or an custom folder
         *
         * @param EmailAccount $emailAccount
         * @param string       $id
         * @param string|int   $box
         *
         * @throws Exception
         * @return JsonResponse
         */
        public function move( EmailAccount $emailAccount , $id , $box )
        {
            $email = $emailAccount->emails()->withTrashed()->where( 'incoming' , 1 )->findOrFail( $id );
            if ( !in_array( $box , [ 'inbox' , 'spambox' , 'trashbox' ] ) ) {
                $folder = $emailAccount->emailFolders()->findOrFail( $box );
            } else {
                $folder = $box;
            }

            switch ( $box ) {
                case 'inbox':
                    // Move to inbox
                    $email->update( [
                        'spam'            => false ,
                        'deleted_at'      => null ,
                        'email_folder_id' => null ,
                    ] );

                    return $this->json()
                        ->success( 'dtv.emails::emails.messages.moved' )
                        ->reloadListView( IncomingEmailOverview::class )
                        ->hideModal();
                case 'spambox':
                    // Move to spambox (Mark as spam)
                    if ( $email->deleted_at->isNull() ) {
                        $query = $emailAccount->emails()->where( 'from' , $email->from );
                    } else {
                        $query = $email;
                    }

                    $query->update( [
                        'spam'            => true ,
                        'deleted_at'      => null ,
                        'email_folder_id' => null ,
                    ] );

                    return $this->json()
                        ->success( 'dtv.emails::emails.messages.markedAsSpam' )
                        ->reloadListView( IncomingEmailOverview::class )
                        ->hideModal();
                case 'trashbox':
                    // Move to trashbox (Delete)
                    $email->update( [
                        'spam'            => false ,
                        'deleted_at'      => Carbon::now() ,
                        'email_folder_id' => null ,
                    ] );

                    return $this->json()
                        ->success( 'dtv.base::general.messages.deleteSuccess' )
                        ->reloadListView( IncomingEmailOverview::class )
                        ->hideModal();
                default:
                    // Move to custom folder
                    /** @var EmailFolder $folder */
                    $email->update( [
                        'spam'            => false ,
                        'deleted_at'      => null ,
                        'email_folder_id' => $folder->id ,
                    ] );

                    return $this->json()
                        ->success( 'dtv.emails::emails.messages.moved' )
                        ->reloadListView( IncomingEmailOverview::class )
                        ->hideModal();
            }
        }

        /**
         * Marks all emails of the given box/folder as read
         *
         * @param EmailAccount $emailAccount
         * @param string|int   $box
         *
         * @return JsonResponse
         */
        public function markAsRead( EmailAccount $emailAccount , $box )
        {
            if ( !in_array( $box , [ 'inbox' , 'spambox' , 'trashbox' ] ) ) {
                $folder = $emailAccount->emailFolders()->findOrFail( $box );
                $query = $emailAccount->emails()->folder( $folder->id );
            } else {
                $query = $emailAccount->emails()->$box();
            }

            /**
             * Update mail query
             *
             * @var $query Builder
             */
            $query->where('status', '!=', Email::STATUS_IN_READ)->update( [
                'status' => Email::STATUS_IN_READ
            ] );

            return $this->json()
                ->reload()
                ->success( 'dtv.emails::emails.messages.markedAsRead' );
        }

        /**
         * Deletes all emails of the given box/folder
         *
         * @param EmailAccount $emailAccount
         * @param string|int   $box
         *
         * @return JsonResponse
         */
        public function deleteAll( EmailAccount $emailAccount , $box )
        {
            if ( !in_array( $box , [ 'inbox' , 'spambox' ] ) ) {
                $folder = $emailAccount->emailFolders()->findOrFail( $box );
                $query = $emailAccount->emails()->folder( $folder->id );
            } else {
                $query = $emailAccount->emails()->$box();
            }

            /**
             * Delete mail query
             *
             * @var $query Builder
             */
            $query->delete();

            return $this->json()
                ->reload()
                ->success( 'dtv.emails::emails.messages.deletedAll' );
        }

        /**
         * Hard deletes all mails in the trashbox (or mark them as hard deleted)
         *
         * @param EmailAccount $emailAccount
         *
         * @return JsonResponse
         */
        public function hardDeleteAll( EmailAccount $emailAccount )
        {
            $emailAccount->emails()->trashbox()->update( [
                'hard_deleted_at' => Carbon::now() ,
            ] );

            return $this->json()
                ->reload()
                ->success( 'dtv.emails::emails.messages.deletedAll' );
        }


        /**
         * SEND FUNCTIONS
         */

        /**
         * Shows the send new mail form
         *
         * @throws Throwable
         * @return JsonResponse|Page|Response|string
         */
        public function create()
        {
            $emailAccounts = EmailAccount::getSelectOptions( auth()->user()->emailAccounts()->where( 'status' , EmailAccount::STATUS_ACTIVE ) );

            return $this->modal( 'dtv.emails::create' , compact( 'emailAccounts' ) );
        }

        /**
         * Shows the send new mail form and prefills the given notifiable(s)
         *
         * @param string  $reference
         * @param Request $request
         *
         * @throws Throwable
         *
         * @return Page|Response|string
         */
        public function createBy( Request $request , $reference = null )
        {
            if ( $reference !== null ) {
                MorphIdHandler::getModel( $reference , false );
            }

            $to = [];
            $bcc = [];
            $rawTo = array_wrap( $request->get( 'to' , [] ) );
            $emailAccounts = EmailAccount::getSelectOptions( auth()->user()->emailAccounts()->where( 'status' , EmailAccount::STATUS_ACTIVE ) );

            foreach ( $rawTo as $receiver ) {
                $useBcc = false;
                $model = MorphIdHandler::getModel( $receiver , false );

                if ( $model instanceof NotifiableGroup ) {
                    $notifiables = $model->notifiables;
                    $useBcc = true;
                } elseif ( $model instanceof NotifiableByEmail ) {
                    $notifiables = collect( [ $model ] );
                } else {
                    throw new Exception( 'Invalid morph id given!' );
                }

                foreach ( $notifiables as $notifiable ) {
                    if ( $useBcc ) {
                        $bcc[ MorphIdHandler::getIdByModel( $notifiable ) ] = $notifiable->getEmail();
                    } else {
                        $to[ MorphIdHandler::getIdByModel( $notifiable ) ] = $notifiable->getEmail();
                    }
                }
            }

            return $this->modal( 'dtv.emails::create' , compact( 'emailAccounts' , 'to' , 'bcc' , 'reference' ) );
        }

        /**
         * Handles the send new mail form
         *
         * @param Request      $request
         * @param EmailHandler $emailHandler
         *
         * @throws Exception
         * @throws Throwable
         *
         * @return JsonResponse
         */
        public function store( Request $request , EmailHandler $emailHandler )
        {
            logger( $request->all() );

            // Input validation
            $request->validate( [
                'from'    => 'required|exists:email_accounts,id' ,
                'layout'  => 'required|string' ,
                'to'      => 'required|array' ,
                'cc'      => 'nullable|array' ,
                'bcc'     => 'nullable|array' ,
                'subject' => 'required|string|min:4|max:300' ,
            ] );

            // get initial mail
            if ( $request->has( 'answer' ) ) {
                /** @var Email $answerMail */
                $answerMail = Email::query()->findOrFail( $request->answer );
            } elseif ( $request->has( 'forward' ) ) {
                /** @var Email $forwardMail */
                $forwardMail = Email::query()->findOrFail( $request->forward );
            }

            // create mail builder
            $mailableClass = app( 'mailables' )->get( $request->layout );

            if ( $mailableClass === null ) {
                $this->json()->warning( 'dtv.emails::emails.messages.invalidMailable' );
            }

            $mailable = new $mailableClass( $request->subject , $request->text );
            $mail = $emailHandler->createMail( $request->from , $request->subject , $mailable , new MailAddressCollection( $request->get( 'to' , [] ) ) );

            if ( $request->has( 'cc' ) && $request->filled( 'cc' ) ) {
                $mail->cc( new MailAddressCollection( $request->get( 'cc' , [] ) ) );
            }

            if ( $request->has( 'bcc' ) && $request->filled( 'bcc' ) ) {
                $mail->bcc( new MailAddressCollection( $request->get( 'bcc' , [] ) ) );
            }

            // Reference
            $reference = $request->reference;
            if ( $reference !== null ) {
                $mail->reference( MorphIdHandler::getModel( $reference , false ) );
            }

            // append initial mail
            if ( isset( $answerMail ) ) {
                $mail->appendInitialMail( $answerMail );
            } elseif ( isset( $forwardMail ) ) {
                $mail->appendInitialMail( $forwardMail );
            }

            // Attachments
            foreach ( $request->file( 'attachments' , [] ) as $file ) {
                $mail->attachment( new UploadedMailAttachment( $file ) );
            }
            if ( isset( $forwardMail ) ) {
                foreach ( $forwardMail->attachments as $attachment ) {
                    $mail->attachment( new DatabaseMailAttachment( $attachment ) );
                }
            }

            $mail->saveAndQueue();

            // update initial mails
            if ( isset( $answerMail ) ) {
                $answerMail->update( [ 'answered' => true ] );
            } elseif ( isset( $forwardMail ) ) {
                $forwardMail->update( [ 'forwarded' => true ] );
            }

            return $this->json()->success( 'dtv.emails::emails.messages.sent' )->reload();
        }

        /**
         * Answers an email, shows the send mail form
         *
         * @param EmailAccount $emailAccount
         * @param int          $id
         *
         * @throws Throwable
         * @return JsonResponse|Page|Response|string
         */
        public function answer( EmailAccount $emailAccount , $id )
        {
            $email = $emailAccount->emails()->findOrFail( $id );
            $emailAccounts = EmailAccount::getSelectOptions( EmailAccount::query()->where( 'status' , EmailAccount::STATUS_ACTIVE ) );

            $subject = trans( 'dtv.emails::emails.subject_abbrevations.answered' ) . ': ' . $email->subject;
            $to = [ $email->from => $email->from ];
            $answer = $email->id;

            return $this->modal( 'dtv.emails::create' , compact( 'emailAccounts' , 'subject' , 'to' , 'answer' ) );
        }

        /**
         * Forwards an email, shows the send mail form
         *
         * @param EmailAccount $emailAccount
         * @param int          $id
         *
         * @throws Throwable
         * @return JsonResponse|Page|Response|string
         */
        public function forward( EmailAccount $emailAccount , $id )
        {
            $email = $emailAccount->emails()->findOrFail( $id );
            $emailAccounts = EmailAccount::getSelectOptions( EmailAccount::query()->where( 'status' , EmailAccount::STATUS_ACTIVE ) );

            $subject = trans( 'dtv.emails::emails.subject_abbrevations.forwarded' ) . ': ' . $email->subject;
            $forward = $email->id;

            return $this->modal( 'dtv.emails::create' , compact( 'emailAccounts' , 'subject' , 'forward' ) );
        }

        /**
         * Resends the given email once again to the previous receiver
         *
         * @param EmailAccount $emailAccount
         * @param int          $id
         *
         * @throws Exception
         * @return JsonResponse
         */
        public function resend(EmailAccount $emailAccount, $id)
        {
            /** @var Email $email */
            $email = $emailAccount->emails()->findOrFail($id);

            MailBuilder::instance($email, true)->saveAndQueue();

            return $this->json()->success('dtv.emails::emails.messages.sent')->reload();
        }

        /**
         * Resends the given email to a alternative receiver
         *
         * @param EmailAccount $emailAccount
         * @param int          $id
         * @param Request      $request
         *
         * @throws Throwable
         * @return JsonResponse|Page|string
         */
        public function resendToOther(EmailAccount $emailAccount, $id, Request $request)
        {
            /** @var Email $email */
            $email = $emailAccount->emails()->findOrFail($id);

            return $this->form($request, new ResendToOtherForm(), function (Request $request) use ($email) {
                MailBuilder::instance($email, true)->cc(new MailAddressCollection($request->to))->saveAndQueue();

                return $this->json()->success('dtv.emails::emails.messages.sent')->reload();
            });
        }
    }
