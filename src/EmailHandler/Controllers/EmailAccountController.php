<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Controllers;

    use DTV\BaseHandler\Controllers\Controller;
    use DTV\EmailHandler\EmailHandler;
    use DTV\EmailHandler\Models\EmailAccount;
    use DTV\EmailHandler\Services\Views\EmailAccounts\EmailAccountForm;
    use DTV\EmailHandler\Services\Views\EmailAccounts\EmailAccountOverview;
    use DTV\EmailHandler\Services\Views\EmailAccounts\EmailAccountUserForm;
    use DTV\EmailHandler\Services\Views\EmailAccounts\MailboxesForm;
    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Http\Request;

    /**
     * Email Account Controller
     *
     * @package   DTV\EmailHandler\Controllers
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class EmailAccountController extends Controller
    {
        /**
         * Shows the overview page with all available email accounts
         *
         * @return \DTV\BaseHandler\Views\Page|string
         */
        public function index()
        {
            $this->addHeaderButton( 'dtv.emails::email_accounts.views.create' , 'fa-plus' , 'email_accounts.create' );

            return $this->overview( EmailAccount::query() );
        }

        /**
         * Shows the overview page with the private email accounts of the current user
         *
         * @return \DTV\BaseHandler\Views\Page|string
         */
        public function private()
        {
            return $this->overview( auth()->user()->privateEmailAccounts()->getQuery() , true );
        }

        /**
         * Builds the email account overview
         *
         * @param Builder $builder
         * @param bool    $private
         *
         * @throws \Exception
         *
         * @return \DTV\BaseHandler\Views\Page|string
         */
        private function overview( Builder $builder , bool $private = false )
        {
            return $this->view( new EmailAccountOverview( $builder , $private ) );
        }

        /**
         * Shows and handles the create mail account form
         *
         * @param Request      $request
         * @param EmailHandler $emailHandler
         *
         * @throws \Throwable
         *
         * @return \DTV\BaseHandler\JsonResponse|\Illuminate\Http\JsonResponse|string
         */
        public function create( Request $request , EmailHandler $emailHandler )
        {
            return $this->form( $request , new EmailAccountForm() , function ( Request $request ) use ( $emailHandler ) {
                // test the given connection
                try {
                    $mailAccount = $emailHandler->createMailAccount( $request->all() );
                } catch ( \Exception $e ) {
                    logger( $e->getMessage() );

                    return $this->json()->warning( 'dtv.emails::email_accounts.messages.invalidConnectionData' );
                }

                return $this->json()->newRequest( 'email_accounts.setMailboxes' , [ 'id' => $mailAccount->id ] )
                    ->success( 'dtv.emails::email_accounts.messages.created' )->reload();
            } );
        }

        /**
         * Shows and handles the edit mail account form
         *
         * @param int          $id
         * @param Request      $request
         * @param EmailHandler $emailHandler
         *
         * @throws \Throwable
         *
         * @return \DTV\BaseHandler\JsonResponse|\Illuminate\Http\JsonResponse|string
         */
        public function edit( $id , Request $request , EmailHandler $emailHandler )
        {
            $emailAccount = EmailAccount::query()->findOrFail( $id );

            $form = new EmailAccountForm( $emailAccount , $request->has( 'private' ) );
            $form->setIcon( 'fa-pencil' );
            $form->setLabel( 'dtv.emails::email_accounts.views.edit' );

            return $this->form( $request , $form , function ( Request $request ) use ( $emailAccount , $emailHandler ) {
                // test the given connection
                try {
                    $mailAccount = $emailHandler->editMailAccount( $emailAccount , $request->all() );
                } catch ( \Exception $e ) {
                    logger( $e->getMessage() );

                    return $this->json()->warning( 'dtv.emails::email_accounts.messages.invalidConnectionData' );
                }

                $response = $this->json();

                if ( $request->status == EmailAccount::STATUS_ACTIVE ) {
                    $response->newRequest( 'email_accounts.setMailboxes' , [ 'id' => $mailAccount->id ] );
                }

                return $response->success( 'dtv.emails::email_accounts.messages.edited' )->reload();
            } );
        }

        /**
         * Shows and handles the set mailbox form
         *
         * @param int     $id Mail Account ID
         * @param Request $request
         *
         * @throws \Throwable
         *
         * @return \DTV\BaseHandler\JsonResponse|\Illuminate\Http\JsonResponse|string
         */
        public function setMailboxes( $id , Request $request )
        {
            $emailAccount = EmailAccount::query()->findOrFail( $id );

            return $this->form( $request , new MailboxesForm( $emailAccount ) , function ( Request $request ) use ( $emailAccount ) {
                if ( $request->mailbox_inbox == $request->mailbox_failed ) {
                    return $this->json()->warning( 'dtv.emails::email_accounts.messages.invalidInboxes' );
                }

                $emailAccount->update( [
                    'mailbox_inbox'  => $request->mailbox_inbox ,
                    'mailbox_failed' => $request->mailbox_failed ,
                    'status'         => EmailAccount::STATUS_ACTIVE ,
                ] );

                return $this->json()->success( 'dtv.emails::email_accounts.messages.mailboxesUpdated' )->reload();
            } );
        }

        /**
         * Shows the error log of the given mail account
         *
         * @param int $id Mail Account ID
         *
         * @return $this|\DTV\BaseHandler\Views\Page|\Illuminate\Http\Response|string
         */
        public function show( $id )
        {
            $emailAccount = EmailAccount::query()->findOrFail( $id );

            return $this->modal( 'dtv.emails::account.show' , [ 'emailAccount' => $emailAccount ] );
        }

        /**
         * Shows and handles the add users form
         *
         * @param int     $id Mail Account ID
         * @param Request $request
         *
         * @throws \Throwable
         *
         * @return \DTV\BaseHandler\JsonResponse|\Illuminate\Http\JsonResponse|string
         */
        public function addUsers( $id , Request $request )
        {
            $emailAccount = EmailAccount::query()->whereNull( 'owner_id' )->findOrFail( $id );
            $emailAccount->authorized_users = $emailAccount->users()->pluck( 'users.id' )->toArray();

            return $this->form( $request , new EmailAccountUserForm( $emailAccount ) , function ( Request $request ) use ( $emailAccount ) {
                $emailAccount->users()->sync( $request->authorized_users );

                return $this->json()->success( 'dtv.emails::email_accounts.messages.updatedUsers' )->reload();
            } );

        }

        /**
         * Tries to delete an given email account
         *
         * @param int $id
         *
         * @throws \Exception
         *
         * @return \DTV\BaseHandler\JsonResponse
         */
        public function delete( $id )
        {
            $emailAccount = EmailAccount::query()->findOrFail( $id );

            return $this->deleteModel( $emailAccount );
        }
    }