<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Controllers;

    use DTV\EmailHandler\Models\Email;
    use DTV\EmailHandler\Models\EmailAccount;
    use Illuminate\Support\Facades\Storage;

    /**
     * Email Attachment Controller
     *
     * @package   DTV\EmailHandler\Controllers
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class EmailAttachmentController
    {
        /**
         * Returns an email attachment file as download
         *
         * @param EmailAccount $emailAccount
         * @param int  $mid
         * @param int  $id
         * @param bool $forceDownload
         *
         * @return \Symfony\Component\HttpFoundation\StreamedResponse
         */
        public function download( EmailAccount $emailAccount , $mid , $id , $forceDownload = true )
        {
            $email = $emailAccount->emails()->findOrFail( $mid );
            $attachments = $email->attachments()->findOrFail( $id );

            $callback = function () use ( $attachments ) {
                echo Storage::disk( 'local' )->get( $attachments->path );
            };

            if ($forceDownload) {
                $filename = str_replace(['/', '\\'], '-', $attachments->name);

                return response()->streamDownload($callback, $filename, ['content-type' => $attachments->type]);
            }

            return response()->stream( $callback , 200 , [ 'content-type' => $attachments->type ] );
        }

        /**
         * Tries to show an email attachments or downloads it if can't be rendered in the browser
         *
         * @param EmailAccount $emailAccount
         * @param int          $mid
         * @param int          $id
         *
         * @return \Symfony\Component\HttpFoundation\StreamedResponse
         */
        public function show( EmailAccount $emailAccount , $mid , $id )
        {
            return $this->download( $emailAccount , $mid , $id , false );
        }
    }
