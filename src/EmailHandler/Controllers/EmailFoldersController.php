<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Controllers;

    use DTV\BaseHandler\Controllers\Controller;
    use DTV\BaseHandler\JsonResponse;
    use DTV\BaseHandler\Views\Page;
    use DTV\EmailHandler\Models\EmailAccount;
    use DTV\EmailHandler\Models\EmailFolder;
    use DTV\EmailHandler\Services\Views\EmailFolders\EmailFolderForm;
    use Exception;
    use Illuminate\Http\Request;
    use Throwable;

    /**
     * Email Folders Controller
     *
     * @package   DTV\EmailHandler\Controllers
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class EmailFoldersController extends Controller
    {
        /**
         * Shows and handles the email folder create form
         *
         * @param EmailAccount $emailAccount
         * @param Request      $request
         * @param int|null     $parent
         *
         * @throws Throwable
         *
         * @return Page|JsonResponse|string
         */
        public function create(EmailAccount $emailAccount, Request $request, $parent = null)
        {
            if ($parent !== null) {
                $parent = $emailAccount->emailFolders()->findOrFail($parent);
            }

            return $this->form($request, new EmailFolderForm(), function (Request $request) use ($emailAccount, $parent) {
                EmailFolder::query()->create([
                    'email_account_id' => $emailAccount->id,
                    'name'             => $request->name,
                    'parent_folder_id' => $parent?->id,
                ]);

                return $this->json()
                    ->success('dtv.emails::email_accounts.messages.folderCreated')
                    ->reload();
            });
        }

        /**
         * Shows and handles the folder rename form
         *
         * @param EmailAccount $emailAccount
         * @param int          $id
         * @param Request      $request
         *
         * @throws Throwable
         *
         * @return JsonResponse|Page|string
         */
        public function rename(EmailAccount $emailAccount, $id, Request $request)
        {
            $folder = $emailAccount->emailFolders()->findOrFail($id);

            return $this->form($request, new EmailFolderForm($folder), function (Request $request) use ($folder) {
                $folder->update([
                    'name' => $request->name,
                ]);

                return $this->json()->success('dtv.emails::email_accounts.messages.folderRenamed')->reload();
            });
        }

        /**
         * Tries to delete an email folder
         *
         * @param EmailAccount $emailAccount
         * @param int          $id
         *
         * @throws Exception
         *
         * @return JsonResponse
         */
        public function delete(EmailAccount $emailAccount, $id)
        {
            $folder = $emailAccount->emailFolders()->findOrFail($id);

            return $this->deleteModel($folder, 'subFolders', 'emails')
                ->redirect('emails.inbox', ['emailAccount' => $emailAccount->id]);
        }
    }
