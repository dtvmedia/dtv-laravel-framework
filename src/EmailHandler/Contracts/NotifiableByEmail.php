<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Contracts;

    use DTV\Support\Contracts\Notifiable;
    use Illuminate\Database\Eloquent\Relations\MorphToMany;

    /**
     * Notifiable By Email Contract
     *
     * @package   DTV\EmailHandler\Contracts
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    interface NotifiableByEmail extends Notifiable
    {
        /**
         * Get all of the emails for this object
         *
         * @return MorphToMany
         */
        public function emails();

        /**
         * Returns the mail
         *
         * @return string
         */
        public function getEmail();

        /**
         * Returns the name
         *
         * @return string|null
         */
        public function getEmailName();
    }