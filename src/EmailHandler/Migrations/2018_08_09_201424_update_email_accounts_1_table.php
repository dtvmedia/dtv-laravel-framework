<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Updates Email Account Table Migration
     *
     * @package   -
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class UpdateEmailAccounts1Table extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table( 'email_accounts' , function ( Blueprint $table ) {
                $table->dateTime( 'last_retrieved' )->nullable();
                $table->integer( 'errors' )->default( 0 );
                $table->dropColumn( 'mailbox_imported' );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table( 'email_accounts' , function ( Blueprint $table ) {
                $table->dropColumn( 'last_retrieved' );
                $table->dropColumn( 'errors' );
                $table->string( 'mailbox_imported' );
            } );
        }
    }
