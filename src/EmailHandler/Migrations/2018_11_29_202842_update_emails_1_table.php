<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Updates Emails Table Migration
     *
     * @package   -
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class UpdateEmails1Table extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table( 'emails' , function ( Blueprint $table ) {
                $table->integer( 'email_folder_id' )->unsigned()->nullable();

                $table->foreign( 'email_folder_id' )->references( 'id' )->on( 'email_folders' );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table( 'emails' , function ( Blueprint $table ) {
                $table->dropColumn( 'email_folder_id' );
            } );
        }
    }
