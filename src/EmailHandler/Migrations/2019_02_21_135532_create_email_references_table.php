<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Creates Email References Table Migration
     *
     * @package   -
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CreateEmailReferencesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create( 'email_references' , function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->integer( 'email_id' )->unsigned();
                $table->morphs( 'reference' );
                $table->timestamps();
                $table->softDeletes();
            } );

            Schema::table( 'email_references' , function ( Blueprint $table ) {
                $table->foreign( 'email_id' )->references( 'id' )->on( 'emails' );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists( 'email_references' );
        }
    }
