<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Updates Emails Table Migration
     *
     * @package   -
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class UpdateEmails3Table extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table( 'emails' , function ( Blueprint $table ) {
                DB::statement( 'ALTER TABLE `emails` CHANGE `email_account_id` `email_account_id` INT(10) UNSIGNED NULL;' );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table( 'emails' , function ( Blueprint $table ) {
                DB::statement( 'ALTER TABLE `emails` CHANGE `email_account_id` `email_account_id` INT(10) UNSIGNED;' );
            } );
        }
    }
