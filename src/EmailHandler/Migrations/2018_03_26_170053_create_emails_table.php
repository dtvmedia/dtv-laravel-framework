<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Creates Email Table Migration
     *
     * @package   -
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CreateEmailsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create( 'emails' , function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->integer( 'email_account_id' )->unsigned();
                $table->boolean( 'incoming' )->index();
                $table->string( 'subject' , 255 );
                $table->string( 'from' );
                $table->text( 'to' );
                $table->text( 'cc' );
                $table->text( 'bcc' );
                $table->string( 'message_id' , 255 )->nullable();
                $table->integer( 'size' );
                $table->mediumText( 'content' );
                $table->tinyInteger( 'status' )->index()->default( 0 );
                $table->boolean( 'spam' )->index()->default( false );
                $table->boolean( 'answered' )->default( false );
                $table->boolean( 'forwarded' )->default( false );
                $table->dateTime( 'received_at' )->nullable();
                $table->dateTime( 'sent_at' )->nullable();
                $table->boolean( 'html' );
                $table->timestamps();
                $table->softDeletes();
            } );

            Schema::table( 'emails' , function ( $table ) {
                $table->foreign( 'email_account_id' )->references( 'id' )->on( 'email_accounts' );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists( 'emails' );
        }
    }
