<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Creates Email Attachment Table Migration
     *
     * @package   -
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CreateEmailAttachmentsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create( 'email_attachments' , function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->integer( 'email_id' )->unsigned()->nullable();
                $table->string( 'name' );
                $table->string( 'type' );
                $table->integer( 'size' );
                $table->string( 'path' );
                $table->string( 'content_id' )->nullable();
                $table->timestamps();
                $table->softDeletes();
            } );

            Schema::table( 'email_attachments' , function ( $table ) {
                $table->foreign( 'email_id' )->references( 'id' )->on( 'emails' );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists( 'email_attachments' );
        }
    }
