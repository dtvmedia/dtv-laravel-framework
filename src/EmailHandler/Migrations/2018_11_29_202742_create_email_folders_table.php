<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Creates Email Folders Table Migration
     *
     * @package   -
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CreateEmailFoldersTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create( 'email_folders' , function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->integer( 'email_account_id' )->unsigned();
                $table->string( 'name' , 64 );
                $table->integer( 'parent_folder_id' )->unsigned()->nullable();
                $table->timestamps();
                $table->softDeletes();
            } );

            Schema::table( 'email_folders' , function ( $table ) {
                $table->foreign( 'email_account_id' )->references( 'id' )->on( 'email_accounts' );
                $table->foreign( 'parent_folder_id' )->references( 'id' )->on( 'email_folders' );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists( 'email_folders' );
        }
    }
