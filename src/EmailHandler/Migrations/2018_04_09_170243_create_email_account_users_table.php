<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Creates Email Account User Link Table Migration
     *
     * @package   -
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CreateEmailAccountUsersTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create( 'email_account_users' , function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->integer( 'email_account_id' )->unsigned();
                $table->integer( 'user_id' )->unsigned();
                $table->timestamps();
            } );

            Schema::table( 'email_account_users' , function ( Blueprint $table ) {
                $table->foreign( 'email_account_id' )->references( 'id' )->on( 'email_accounts' );
                $table->foreign( 'user_id' )->references( 'id' )->on( config( 'dtv.tables.user' ) );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists( 'email_account_users' );
        }
    }
