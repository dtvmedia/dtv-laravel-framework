<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Creates Email Account Table Migration
     *
     * @package   -
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CreateEmailAccountsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create( 'email_accounts' , function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->string( 'name' );
                $table->string( 'in_server' );
                $table->integer( 'in_port' );
                $table->string( 'in_flags' )->nullable();
                $table->string( 'out_server' );
                $table->integer( 'out_port' );
                $table->string( 'out_flags' )->nullable();
                $table->string( 'email' );
                $table->string( 'password' );
                $table->integer( 'status' );
                $table->string( 'mailbox_inbox' );
                $table->string( 'mailbox_failed' );
                $table->string( 'mailbox_imported' );
                $table->integer( 'owner_id' )->unsigned()->nullable();
                $table->timestamps();
                $table->softDeletes();
            } );

            Schema::table( 'email_accounts' , function ( $table ) {
                $table->foreign( 'owner_id' )->references( 'id' )->on( 'users' );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists( 'email_accounts' );
        }
    }
