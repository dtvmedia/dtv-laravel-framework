<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Layouts;

    /**
     * TextMail Layout
     *
     * @package   DTV\EmailHandler\Layouts
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class TextMail extends Mailable
    {
        /**
         * Mail Content
         *
         * @var string
         */
        protected $content;

        /**
         * Html mail flag
         *
         * @var bool
         */
        protected $htmlMode = false;

        /**
         * Mailable display name (or translation string)
         *
         * @var string
         */
        public static $displayName = 'dtv.emails::emails.mailables.text_mail';

        /**
         * Mailable select option icon
         *
         * @var string
         */
        public static $optionIcon = 'fa-align-center';

        /**
         * Create a new message instance.
         *
         * @param string $subject
         * @param string $content
         */
        public function __construct( string $subject , string $content )
        {
            $this->subject = $subject;
            $this->content = $content;
        }

        /**
         * Build the message.
         *
         * @return $this
         */
        public function build()
        {
            return $this->view( 'dtv.emails::mails.text' , [
                'subject' => $this->subject ,
                'content' => $this->content ,
            ] );
        }
    }
