<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Layouts;

    /**
     * Test Mail Layout
     *
     * @package   DTV\EmailHandler\Layouts
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class TestMail extends Mailable
    {
        /**
         * Mail Content
         *
         * @var string
         */
        protected $content;

        /**
         * Html mail flag
         *
         * @var bool
         */
        protected $htmlMode = false;

        /**
         * Build the message.
         *
         * @return $this
         */
        public function build()
        {
            return $this->view( 'dtv.emails::mails.text' , [
                'subject' => trans( 'dtv.emails::emails.messages.testMailSubject' , [ 'name' => config( 'app.name' ) ] ) ,
                'content' => trans( 'dtv.emails::emails.messages.testMailContent' , [ 'name' => config( 'app.name' ) ] ) ,
            ] );
        }
    }
