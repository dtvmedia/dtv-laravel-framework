<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Layouts;

    use Illuminate\Bus\Queueable;
    use Illuminate\Mail\Mailable as BaseMailable;
    use Illuminate\Queue\SerializesModels;

    /**
     * Abstract Base Mailable Class
     *
     * @package   DTV\EmailHandler\Layouts
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    abstract class Mailable extends BaseMailable
    {
        use Queueable , SerializesModels;

        /**
         * Html mail flag
         *
         * @var bool
         */
        protected $htmlMode = true;

        /**
         * Mailable unique Key
         *
         * @var string|null
         */
        public static $key;

        /**
         * Mailable display name (or translation string)
         *
         * @var string
         */
        public static $displayName;

        /**
         * Mailable select option icon
         *
         * @var string
         */
        public static $optionIcon = 'fa-object-ungroup';

        /**
         * Build the message.
         *
         * @return $this
         */
        public abstract function build();

        /**
         * Returns whether or not this is an html mail
         *
         * @return bool
         */
        public function isHtmlMail()
        {
            return $this->htmlMode;
        }

        /**
         * Returns whether or not this is an text mail
         *
         * @return bool
         */
        public function isTextMail()
        {
            return !$this->htmlMode;
        }

        /**
         * Returns the mailable key
         *
         * @return string
         */
        public static function getKey()
        {
            if ( isset( static::$key ) ) {
                return static::$key;
            }

            return snake_case( class_basename( static::class ) );
        }
    }