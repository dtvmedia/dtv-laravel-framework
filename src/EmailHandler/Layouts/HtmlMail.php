<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Layouts;

    /**
     * HTML Mail Layout
     *
     * @package   DTV\EmailHandler\Layouts
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class HtmlMail extends Mailable
    {
        /**
         * Mail Content
         *
         * @var string
         */
        protected $content;

        /**
         * Html mail flag
         *
         * @var bool
         */
        protected $htmlMode = true;

        /**
         * Mailable display name (or translation string)
         *
         * @var string
         */
        public static $displayName = 'dtv.emails::emails.mailables.html_mail';

        /**
         * Create a new message instance.
         *
         * @param string $subject
         * @param string $content
         */
        public function __construct( string $subject , string $content )
        {
            $this->subject = $subject;
            $this->content = $content;
        }

        /**
         * Build the message.
         *
         * @return $this
         */
        public function build()
        {
            return $this->view( 'dtv.emails::mails.html' , [
                'subject' => $this->subject ,
                'content' => $this->content ,
            ] );
        }
    }
