<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler;

    use DTV\BaseHandler\BaseServiceProvider;
    use DTV\BaseHandler\RoutingHandler;
    use DTV\EmailHandler\Commands\CheckMailsCommand;
    use DTV\EmailHandler\Commands\HardDeleteMailCommand;
    use DTV\EmailHandler\Controllers\EmailAccountController;
    use DTV\EmailHandler\Controllers\EmailAttachmentController;
    use DTV\EmailHandler\Controllers\EmailController;
    use DTV\EmailHandler\Controllers\EmailFoldersController;
    use DTV\EmailHandler\Layouts\HtmlMail;
    use DTV\EmailHandler\Layouts\TextMail;
    use DTV\EmailHandler\Models\EmailAccount;
    use DTV\EmailHandler\Services\MailableManager;
    use Exception;
    use Illuminate\Console\Scheduling\Schedule;
    use Illuminate\Contracts\Container\BindingResolutionException;

    /**
     * Email Handler Service Provider
     *
     * @package   DTV\EmailHandler
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class EmailHandlerServicesProvider extends BaseServiceProvider
    {
        /**
         *  Namespace for resources of the module
         *
         * @var string
         */
        protected $resourcesNamespace = 'dtv.emails';

        /**
         * Array of commans
         *
         * @var array
         */
        protected $commands = [
            CheckMailsCommand::class ,
            HardDeleteMailCommand::class ,
        ];

        /**
         * Config path for the module enable switch
         *
         * @var string|null
         */
        public static $config = 'dtv.modules.emails.enabled';

        /**
         * Bootstrap any application services.
         *
         * @return void
         */
        public function boot()
        {
            parent::boot();

            // schedule check email command for every ten minutes
            $this->app->booted( function () {
                /** @var Schedule $schedule */
                $schedule = app( Schedule::class );
                $schedule->command( CheckMailsCommand::class )->everyFiveMinutes()->withoutOverlapping();
                $schedule->command( HardDeleteMailCommand::class )->dailyAt( '03:00' );
            } );
        }

        /**
         * Register the service provider.
         *
         * @throws BindingResolutionException
         * @return void
         */
        public function register()
        {
            parent::register();

            $this->app->singleton( 'mailables' , function () {
                return new MailableManager();
            } );

            $this->app->make( 'mailables' )->register( TextMail::class );
            $this->app->make( 'mailables' )->register( HtmlMail::class );
        }

        /**
         * Registers the Modules routes
         *
         * @throws Exception
         */
        public function routes()
        {
            // Email Account Routes
            $emailRoutes = new RoutingHandler( EmailAccountController::class , 'email_accounts' , 'email_accounts.' , RoutingHandler::ACCESS_AUTH );
            $emailRoutes->get( '/' , 'index' , 'index' , 'can:email_accounts.view' );
            $emailRoutes->get( '/private' , 'private' , 'private' );
            $emailRoutes->any( '/create' , 'create' , 'create' , 'can:email_accounts.change' );
            $emailRoutes->any( '/{id}/edit' , 'edit' , 'edit' , 'can:email_accounts.change' );
            $emailRoutes->any( '/{id}/setMailboxes' , 'setMailboxes' , 'setMailboxes' , 'can:email_accounts.change' );
            $emailRoutes->get( '/{id}' , 'show' , 'show' , 'can:email_accounts.view' );
            $emailRoutes->any( '/{id}/addUsers' , 'addUsers' , 'addUsers' , 'can:email_accounts.change' );
            $emailRoutes->delete( '/{id}/delete' , 'delete' , 'delete' , 'can:email_accounts.change' );

            // Email Folder Routes
            // Todo permission prüfen
            $emailFolderRoutes = new RoutingHandler( EmailFoldersController::class , 'email_accounts/{emailAccount}/folders' , 'email_accounts.folders' , RoutingHandler::ACCESS_AUTH );
            $emailFolderRoutes->any( '/create/{parent?}' , 'create' , 'create' , 'can:email_accounts.change' );
            $emailFolderRoutes->any( '/{id}/rename' , 'rename' , 'rename' , 'can:email_accounts.change' );
            $emailFolderRoutes->delete( '/{id}/delete' , 'delete' , 'delete' , 'can:email_accounts.change' );

            // Email Routes
            $emailRoutes = new RoutingHandler( EmailController::class , 'emails' , 'emails.' , RoutingHandler::ACCESS_AUTH );
            $emailRoutes->get( '/getAutocompleteOptions' , 'getAutocompleteOptions' , 'getAutocompleteOptions' , 'can:emails.send' );
            $emailRoutes->get( '/' , 'index' , 'index' , 'can:emails.view,emailAccount' );
            $emailRoutes->get( '/{emailAccount}/inbox/' , 'inbox' , 'inbox' , 'can:emails.view,emailAccount' );
            $emailRoutes->get( '/{emailAccount}/spambox' , 'spambox' , 'spambox' , 'can:emails.view,emailAccount' );
            $emailRoutes->get( '/{emailAccount}/outbox' , 'outbox' , 'outbox' , 'can:emails.view,emailAccount' );
            $emailRoutes->get( '/{emailAccount}/trashbox' , 'trashbox' , 'trashbox' , 'can:emails.view,emailAccount' );
            $emailRoutes->get( '/{emailAccount}/folder/{id}' , 'folder' , 'folder' , 'can:emails.view,emailAccount' );
            $emailRoutes->get( '/create' , 'create' , 'create' , 'can:emails.send' );
            $emailRoutes->get( '/createBy/{reference?}' , 'createBy' , 'createBy' , 'can:emails.send' );
            $emailRoutes->post( '/store' , 'store' , 'store' , 'can:emails.send' );
            $emailRoutes->get( '/{emailAccount}/{id}/content' , 'content' , 'content' , 'can:emails.view,emailAccount' );
            $emailRoutes->any( '/{emailAccount}/{id}/move/{box}' , 'move' , 'move' , 'can:emails.move,emailAccount' );
            $emailRoutes->delete( '/{emailAccount}/{id}/delete' , 'delete' , 'delete' , 'can:emails.move,emailAccount' );
            $emailRoutes->delete( '/{emailAccount}/{id}/hardDelete' , 'hardDelete' , 'hardDelete' , 'can:emails.move,emailAccount' );
            $emailRoutes->get( '/{emailAccount}/{id}' , 'show' , 'show' , 'can:emails.view,emailAccount' );
            $emailRoutes->get( '/{emailAccount}/{id}/answer' , 'answer' , 'answer' , 'can:emails.send,emailAccount' );
            $emailRoutes->get( '/{emailAccount}/{id}/forward' , 'forward' , 'forward' , 'can:emails.send,emailAccount' );
            $emailRoutes->get( '/{emailAccount}/{id}/resend' , 'resend' , 'resend' , 'can:emails.send,emailAccount' );
            $emailRoutes->any( '/{emailAccount}/{id}/resendToOther' , 'resendToOther' , 'resendToOther' , 'can:emails.send,emailAccount' );
            $emailRoutes->get( '/{emailAccount}/markAsRead/{box}' , 'markAsRead' , 'markAsRead' , 'can:emails.view,emailAccount' );
            $emailRoutes->delete( '/{emailAccount}/deleteAll/{box}' , 'deleteAll' , 'deleteAll' , 'can:emails.move,emailAccount' );
            $emailRoutes->delete( '/{emailAccount}/hardDeleteAll' , 'hardDeleteAll' , 'hardDeleteAll' , 'can:emails.move,emailAccount' );

            // Email Attachment Routes
            $attachmentRoutes = new RoutingHandler( EmailAttachmentController::class , '/{emailAccount}/emails/{mid}/attachments' , 'emails.attachments.' ,
                RoutingHandler::ACCESS_AUTH );
            $attachmentRoutes->get( '/{id}/download' , 'download' , 'download' , 'can:emails.view,emailAccount' );
            $attachmentRoutes->get( '/{id}/show' , 'show' , 'show' , 'can:emails.view,emailAccount' );
        }

        /**
         * Defines all the modules gates
         */
        public function gates()
        {
            gate()->define('emails.view', function ($user, $emailAccount) {
                $emailAccount = EmailAccount::instance($emailAccount);

                return $user->hasPermission(['emails.view']) && (
                        $emailAccount === null ||
                        $emailAccount->owner_id === $user->id ||
                        $user->emailAccounts->find($emailAccount->id) !== null
                    );
            });
            gate()->define('emails.move', function ($user, $emailAccount) {
                $emailAccount = EmailAccount::instance($emailAccount);

                return $user->hasPermission(['emails.view', 'emails.move']) && (
                        $emailAccount === null ||
                        $emailAccount->owner_id === $user->id ||
                        $user->emailAccounts->find($emailAccount->id) !== null
                    );
            });
            gate()->define('emails.send', function ($user, $emailAccount = null) {
                $emailAccount = EmailAccount::instance($emailAccount);

                return $user->hasPermission(['emails.view', 'emails.send']) && (
                        $emailAccount === null ||
                        $emailAccount->owner_id === $user->id ||
                        $user->emailAccounts->find($emailAccount->id) !== null
                    );
            });

            gate()->define('email_accounts.view', function ($user) {
                return $user->hasPermission(['email_accounts.view']);
            });
            gate()->define('email_accounts.change', function ($user) {
                return $user->hasPermission(['email_accounts.view', 'email_accounts.change']);
            });
        }
    }
