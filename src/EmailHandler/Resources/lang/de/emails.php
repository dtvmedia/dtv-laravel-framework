<?php

    return [

        'name' => 'Email' ,

        'views' => [
            'index'  => 'Emails' ,
            'create' => 'Senden Sie eine neue Email' ,
            'move'   => 'Email in Ordner verschieben' ,
        ] ,

        'buttons' => [
            'create'         => 'Neue Email' ,
            'show'           => 'Email Details' ,
            'delete'         => 'Email löschen' ,
            'hardDelete'     => 'Email endgültig löschen' ,
            'markAsSpam'     => 'Als Spam markieren' ,
            'unmarkAsSpam'   => 'Markierung als Spam aufheben' ,
            'answer'         => 'Antworten' ,
            'forward'        => 'Weiterleiten' ,
            'move'           => 'Email in Ordner verschieben' ,
            'send'           => 'Email senden' ,
            'markAsRead'     => 'Alle Emails als gelesen markieren' ,
            'emptyFolder'    => 'Ordner leeren' ,
            'deleteAllMails' => 'Alle Emails löschen' ,
            'addSubfolder'   => 'Unterordner hinzufügen' ,
            'renameFolder'   => 'Ordner umbenennen' ,
            'deleteFolder'   => 'Ordner löschen' ,
            'resend'         => 'Erneut senden',
            'resend_other'   => 'An anderen Empfänger senden'
        ] ,

        'fields' => [
            'received_at'     => 'Empfangen am' ,
            'sent_at'         => 'Gesendet an' ,
            'from'            => 'Von' ,
            'to'              => 'An' ,
            'cc'              => 'CC' ,
            'bcc'             => 'BCC' ,
            'subject'         => 'Betreff' ,
            'no_subject'      => 'Kein Betreff' ,
            'attachments'     => 'Anhänge' ,
            'status'          => 'Status' ,
            'wrote_at'        => 'schrieb an' ,
            'email_folder_id' => 'Ordner' ,
            'references'      => 'Referenzen',
            'reference'       => 'Referenz' ,
        ] ,

        'subject_abbrevations' => [
            'forwarded' => 'FWD' ,
            'answered'  => 'RE' ,
        ] ,

        'flags' => [
            'hasAttachments' => 'Diese Email enthält Anhänge' ,
            'wasForwarded'   => 'Diese Email wurde weitergeleitet' ,
            'wasAnswered'    => 'Diese Email wurde beantwortet' ,
        ] ,

        'status' => [
            'in'  => [
                0 => 'Neu' ,
                1 => 'Gelesen' ,
                2 => 'Abgeschlossen' ,
            ] ,
            'out' => [
                - 1 => 'Fehlgeschlagen' ,
                0   => 'Ausstehend' ,
                1   => 'Gesendet' ,
                2   => 'Entwurf' ,
            ] ,
        ] ,

        'mailables' => [
            'text_mail' => 'Textmail Layout' ,
            'html_mail' => 'Standard HTML Layout' ,
        ] ,

        'messages' => [
            'spamModeMessage'    => 'Diese Email wurde als Spam markiert! Es ist nur der Textmodus verfügbar.',
            'markAsSpam'         => 'Möchten Sie diese Email wirklich als Spam markieren?',
            'markedAsSpamTitle'  => 'Als Spam gekennzeichnet',
            'markedAsSpamText'   => 'Erfolgreiche Kennzeichnung der Email als Spam',
            'noMailAccount'      => 'Sie haben derzeit kein Email-Konto Ihrem Konto zugeordnet',
            'addFirstAccount'    => 'Beginnen Sie jetzt, indem Sie Ihr Email-Konto hinzufügen!',
            'sentTitle'          => 'Email gesendet',
            'sentText'           => 'Ihre Email wurde erfolgreich gesendet',
            'testMailSubject'    => ':name Testnachricht',
            'testMailContent'    => 'Diese Email-Nachricht wurde von :name automatisch während des Testens der Kontoeinstellungen gesendet. ',
            'movedTitle'         => 'Email verschoben',
            'movedText'          => 'Die Email wurde erfolgreich in den gewählten Ordner verschoben',
            'markedAsReadTitle'  => 'Emails als gelesen markiert',
            'markedAsReadText'   => 'Alle Emails des gegebenen Ordners wurden als gelesen markiert',
            'deletedAllTitle'    => 'Alle Emails gelöscht',
            'deletedAllText'     => 'Alle Emails des gewählten Ordners wurden in den Papierkorb verschoben',
            'hardDeletedTitle'   => 'Email endgültig gelöscht',
            'hardDeletedText'    => 'Die gewählte Email wurde endgültig gelöscht',
            'resendConfirmation' => 'E-Mail erneut an Empfänger senden',
        ],

    ];
