<?php

    return [

        'name' => 'E-Mail-Konto' ,
        'no_option' => 'Kein E-Mail-Konto' ,

        'views' => [
            'index'           => 'E-Mail-Konten' ,
            'private'         => 'Ihre E-Mail-Konten' ,
            'create'          => 'Neues E-Mail-Konto' ,
            'edit'            => 'E-Mail-Konto bearbeiten' ,
            'chooseMailboxes' => 'Postfächer auswählen' ,
            'errorsFor'       => 'E-Mail-Kontofehler für \':mailaccount\'' ,
            'addUser'         => 'Benutzern Zugriff gewähren' ,
            'createFolder'    => 'Ordner erstellen' ,
            'details'         => 'Verbindungsdetails' ,
        ] ,

        'buttons' => [
            'create'       => 'Neues E-Mail-Konto' ,
            'show'         => 'E-Mail-Kontodetails anzeigen' ,
            'edit'         => 'E-Mail-Konto bearbeiten' ,
            'delete'       => 'E-Mail-Konto löschen' ,
            'createFolder' => 'Ordner erstellen' ,
            'addUsers'     => 'Benutzern Zugriff gewähren' ,
        ] ,

        'fields' => [
            'name'             => 'Name' ,
            'in'               => 'Posteingangsserver' ,
            'in_server'        => 'Eingehender Server' ,
            'in_port'          => 'Eingehender Port' ,
            'in_flags'         => 'Eingehende Flaggen' ,
            'out'              => 'Postausgangsserver' ,
            'out_server'       => 'Ausgehender Server' ,
            'out_port'         => 'Ausgehender Port' ,
            'out_flags'        => 'Ausgehende Flaggen' ,
            'email'            => 'Mail' ,
            'password'         => 'Kennwort' ,
            'status'           => 'Status' ,
            'mailbox_inbox'    => 'Postfach-Standard' ,
            'mailbox_failed'   => 'Postfach fehlgeschlagen' ,
            'owner_id'         => 'Eigentümer' ,
            'private'          => 'Privat' ,
            'port'             => 'Port' ,
            'encryption'       => 'Verschlüsselung' ,
            'multiple_users'   => 'Mehrere Benutzer' ,
            'authorized_users' => 'Autorisierte Benutzer' ,
            'errors'           => 'Fehlerlevel' ,
            'last_retrieved'   => 'Zuletzt aktualisiert' ,
            'last_try'         => 'Letzter Verbindungsversuch' ,
            'next_try'         => 'Nächster Verbindungsversuch' ,
        ] ,

        'status' => [
            0 => 'Inaktiv' ,
            1 => 'Aktiv' ,
        ] ,

        'flags' => [
            'ssl'          => 'SSL' ,
            'tls'          => 'TLS' ,
            'noEncryption' => 'Keine Verschlüsselung' ,
        ] ,

        'mailboxes' => [
            'inbox'    => 'Posteingang' ,
            'outbox'   => 'Postausgang' ,
            'spambox'  => 'Spam' ,
            'trashbox' => 'Papierkorb' ,
        ] ,

        'messages' => [
            'noMailAccountsAvailable'    => 'Keine E-Mail-Konten verfügbar...' ,
            'invalidConnectionDataTitle' => 'Ungültige Verbindungsdaten' ,
            'invalidConnectionDataText'  => 'Die angegebenen Verbindungsdaten sind ungültig. Überprüfen Sie Ihre Eingabe.' ,
            'createdTitle'               => 'Mail-Konto erstellt' ,
            'createdText'                => 'Ihr E-Mail-Konto wurde erfolgreich erstellt. Wählen Sie jetzt die zu verwendenden Postfächer aus.' ,
            'editedTitle'                => 'E-Mail-Konto bearbeitet' ,
            'editedText'                 => 'Das E-Mail-Konto wurde erfolgreich bearbeitet. Bestätigen Sie jetzt die zu verwendenden Postfächer.' ,
            'mailboxesUpdatedTitle'      => 'Postfächer aktualisiert' ,
            'mailboxesUpdatedText'       => 'Die zu verwendenden Posteingänge wurden aktualisiert' ,
            'updatedUsersTitle'          => 'Zugriff gewährt' ,
            'updatedUsersText'           => 'Zugriff auf das E-Mail-Konto für alle ausgewählten Benutzer erfolgreich gewährt' ,
            'invalidInboxesTitle'        => 'Ungültige Posteingänge' ,
            'invalidInboxesText'         => 'Stellen Sie sicher, dass Sie verschiedene Postfächer für ausgefallene Mails und den allgemeinen Posteingang auswählen' ,
            'folderCreatedTitle'         => 'Ordern hinzugefügt' ,
            'folderCreatedText'          => 'Der Ordner wurde erfolgreich hinzugefügt' ,
            'folderRenamedTitle'         => 'Ordner umbenannt' ,
            'folderRenamedText'          => 'Der gewählte Ordner wurde erfolgreich umbenannt' ,
        ] ,

    ];