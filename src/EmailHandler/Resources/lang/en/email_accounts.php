<?php

    return [

        'name'      => 'Email Account' ,
        'no_option' => 'No Email Account' ,

        'views' => [
            'index'           => 'Email Accounts' ,
            'private'         => 'Your Email Accounts' ,
            'create'          => 'New Email Account' ,
            'edit'            => 'Edit Email Account' ,
            'chooseMailboxes' => 'Choose Mailboxes' ,
            'errorsFor'       => 'Email Account Errors for \':mailaccount\'' ,
            'addUser'         => 'Grant users access' ,
            'createFolder'    => 'Create Folder' ,
            'details'         => 'Connection details' ,
        ] ,

        'buttons' => [
            'create'       => 'New Email Account' ,
            'show'         => 'Show Email Account details' ,
            'edit'         => 'Edit Email Account' ,
            'delete'       => 'Delete the Email Account' ,
            'createFolder' => 'Create Folder' ,
            'addUsers'     => 'Grant users access' ,
        ] ,

        'fields' => [
            'name'             => 'Name' ,
            'in'               => 'Incoming Mail Server' ,
            'in_server'        => 'Incoming Server' ,
            'in_port'          => 'Incoming Port' ,
            'in_flags'         => 'Incoming Flags' ,
            'out'              => 'Outgoing Mail Server' ,
            'out_server'       => 'Outgoing Server' ,
            'out_port'         => 'Outgoing Port' ,
            'out_flags'        => 'Outgoing Flags' ,
            'email'            => 'Email' ,
            'password'         => 'Password' ,
            'status'           => 'Status' ,
            'mailbox_inbox'    => 'Mailbox Default' ,
            'mailbox_failed'   => 'Mailbox Failed' ,
            'owner_id'         => 'Owner' ,
            'private'          => 'Private' ,
            'port'             => 'Port' ,
            'encryption'       => 'Encryption' ,
            'multiple_users'   => 'Multiple Users' ,
            'authorized_users' => 'Authorized Users' ,
            'errors'           => 'Error level' ,
            'last_retrieved'   => 'Last updated' ,
            'last_try'         => 'Last connection attempt' ,
            'next_try'         => 'Next connection attempt' ,
        ] ,

        'status' => [
            \DTV\EmailHandler\Models\EmailAccount::STATUS_INACTIVE => 'Inactive' ,
            \DTV\EmailHandler\Models\EmailAccount::STATUS_ACTIVE   => 'Active' ,
        ] ,

        'flags' => [
            'ssl'          => 'SSL' ,
            'tls'          => 'TLS' ,
            'noEncryption' => 'No Encryption'
        ] ,

        'mailboxes' => [
            'inbox'    => 'Inbox' ,
            'outbox'   => 'Outbox' ,
            'spambox'  => 'Spam' ,
            'trashbox' => 'Trash' ,
        ] ,

        'messages' => [
            'noMailAccountsAvailable'    => 'No email accounts available...' ,
            'invalidConnectionDataTitle' => 'Invalid Connection Data' ,
            'invalidConnectionDataText'  => 'The given connection data is invalid. Check your input.' ,
            'createdTitle'               => 'Mail account created' ,
            'createdText'                => 'Your mail account was successfully created. Please choose now the mailboxes to be used' ,
            'editedTitle'                => 'Mail account edited' ,
            'editedText'                 => 'The mail account was successfully edited. Please confirm now the mailboxes to be used' ,
            'mailboxesUpdatedTitle'      => 'Mailboxes updated' ,
            'mailboxesUpdatedText'       => 'The inboxes to be used have been updated' ,
            'updatedUsersTitle'          => 'Access granted' ,
            'updatedUsersText'           => 'Successfully granted access to the mail account for all selected users' ,
            'invalidInboxesTitle'        => 'Invalid Inboxes' ,
            'invalidInboxesText'         => 'Make sure to choose different mailboxes for failed mails and the general inbox' ,
            'folderCreatedTitle'         => 'Created Folder' ,
            'folderCreatedText'          => 'The folder was successfully created' ,
            'folderRenamedTitle'         => 'Folder renamed' ,
            'folderRenamedText'          => 'The folder was successfully renamed' ,
        ]

    ];