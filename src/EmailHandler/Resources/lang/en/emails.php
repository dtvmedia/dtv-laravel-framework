<?php

    return [

        'name' => 'Email' ,

        'views' => [
            'index'  => 'Emails' ,
            'create' => 'Send a new Email' ,
            'move'   => 'Move mail to folder' ,
        ] ,

        'buttons' => [
            'create'         => 'New mail' ,
            'show'           => 'Show mail details' ,
            'delete'         => 'Delete the mail' ,
            'hardDelete'     => 'Final delete the mail' ,
            'markAsSpam'     => 'Mark as spam' ,
            'unmarkAsSpam'   => 'Unmark as spam' ,
            'answer'         => 'Answer mail' ,
            'forward'        => 'Forward mail' ,
            'move'           => 'Move mail to folder' ,
            'send'           => 'Send mail' ,
            'markAsRead'     => 'Mark all emails as read' ,
            'emptyFolder'    => 'Empty Folder' ,
            'deleteAllMails' => 'Delete all emails' ,
            'addSubfolder'   => 'Add subfolder' ,
            'renameFolder'   => 'Rename folder' ,
            'deleteFolder'   => 'Delete folder' ,
            'resend'         => 'Resend',
            'resend_other'   => 'Resend to other receiver'
        ] ,

        'fields' => [
            'received_at'     => 'Received at' ,
            'sent_at'         => 'Sent at' ,
            'from'            => 'From' ,
            'to'              => 'To' ,
            'cc'              => 'CC' ,
            'bcc'             => 'BCC' ,
            'subject'         => 'Subject' ,
            'no_subject'      => 'No Subject' ,
            'attachments'     => 'Attachments' ,
            'status'          => 'Status' ,
            'wrote_at'        => 'wrote at' ,
            'email_folder_id' => 'Folder' ,
            'references'      => 'References',
            'reference'       => 'Reference' ,
        ] ,

        'subject_abbrevations' => [
            'forwarded' => 'FWD' ,
            'answered'  => 'RE' ,
        ] ,

        'flags' => [
            'hasAttachments' => 'This email has attachments' ,
            'wasForwarded'   => 'This email was forwarded' ,
            'wasAnswered'    => 'This email was answered'
        ] ,

        'status' => [
            'in'  => [
                \DTV\EmailHandler\Models\Email::STATUS_IN_NEW      => 'New' ,
                \DTV\EmailHandler\Models\Email::STATUS_IN_READ     => 'Read' ,
            ] ,
            'out' => [
                \DTV\EmailHandler\Models\Email::STATUS_OUT_FAILED  => 'Failed' ,
                \DTV\EmailHandler\Models\Email::STATUS_OUT_PENDING => 'Pending' ,
                \DTV\EmailHandler\Models\Email::STATUS_OUT_SENT    => 'Sent' ,
                \DTV\EmailHandler\Models\Email::STATUS_OUT_DRAFT   => 'Draft' ,
            ] ,
        ] ,

        'mailables' => [
            'text_mail' => 'Text mail layout' ,
            'html_mail' => 'Basic HTML layout' ,
        ] ,

        'messages' => [
            'spamModeMessage'   => 'This mail was marked as spam! Only text mode is available.' ,
            'markAsSpam'        => 'Are you sure you want to mark this mail as spam?' ,
            'markedAsSpamTitle' => 'Marked as spam' ,
            'markedAsSpamText'  => 'Succesfully marked the mail as spam' ,
            'noMailAccount'     => 'You have currently no email account assigned to your account' ,
            'addFirstAccount'   => 'Just start now by adding your mail account!' ,
            'sentTitle'         => 'Email sent' ,
            'sentText'          => 'Your email was successfully sent' ,
            'testMailSubject'   => ':name test messsage' ,
            'testMailContent'   => 'This email was automatically sent by :name during the mail account setup process. ' ,
            'movedTitle'        => 'Email moved' ,
            'movedText'         => 'The email was successfully moved to the chosen folder' ,
            'markedAsReadTitle' => 'Marked emails as read' ,
            'markedAsReadText'  => 'All emails of the given folder were marked as read' ,
            'deletedAllTitle'   => 'Deleted all emails' ,
            'deletedAllText'    => 'All emails of the given folder were moved to the trashbin' ,
            'hardDeletedTitle'  => 'Final deleted email' ,
            'hardDeletedText'   => 'The mail was finally deleted' ,
            'resendConfirmation' => 'Resend email to receiver',
        ]

    ];
