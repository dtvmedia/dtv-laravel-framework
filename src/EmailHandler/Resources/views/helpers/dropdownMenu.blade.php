<div class="dropdown-menu">
    @if( $box !== 'outbox' )
        <a class="dropdown-item" href="javascript:void(0)" data-ajax-action="{{ route('emails.markAsRead',[ 'emailAccount' => $emailAccount->id , 'box' => $box ]) }}">
            <i class="far fa-fw fa-check"></i>
            @lang('dtv.emails::emails.buttons.markAsRead')
        </a>
    @endif
    @if( $box === 'trashbox' )
        <a class="dropdown-item" href="javascript:void(0)" data-confirm-method="DELETE" data-confirm-action="{{ route('emails.hardDeleteAll',[ 'emailAccount' => $emailAccount->id ]) }}">
            <i class="far fa-fw fa-trash"></i>
            @lang('dtv.emails::emails.buttons.emptyFolder')
        </a>
    @else
        <a class="dropdown-item" href="javascript:void(0)" data-confirm-method="DELETE" data-confirm-action="{{ route('emails.deleteAll',[ 'emailAccount' => $emailAccount->id , 'box' => $box ]) }}">
            <i class="far fa-fw fa-trash-alt"></i>
            @lang('dtv.emails::emails.buttons.deleteAllMails')
        </a>
    @endif
    @if( is_int( $box ) )
        <div class="dropdown-divider"></div>
        @if( isset($folderLevel) && $folderLevel < 4 )
            <a class="dropdown-item" href="javascript:void(0)" data-ajax-action="{{ route('email_accounts.folders.create',[ 'emailAccount' => $emailAccount->id , 'parent' => $box ]) }}">
                <i class="far fa-fw fa-folder-plus"></i>
                @lang('dtv.emails::emails.buttons.addSubfolder')
            </a>
        @endif
        <a class="dropdown-item" href="javascript:void(0)" data-ajax-action="{{ route('email_accounts.folders.rename',[ 'emailAccount' => $emailAccount->id , 'id' => $box ]) }}">
            <i class="far fa-fw fa-pencil"></i>
            @lang('dtv.emails::emails.buttons.renameFolder')
        </a>
        <a class="dropdown-item" href="javascript:void(0)" data-confirm-method="DELETE" data-confirm-action="{{ route('email_accounts.folders.delete',[ 'emailAccount' => $emailAccount->id , 'id' => $box ]) }}">
            <i class="far fa-fw fa-times"></i>
            @lang('dtv.emails::emails.buttons.deleteFolder')
        </a>
    @endif
</div>