@foreach( $emailFolders as $folder )
    <div class="list-group-item list-group-item-action @if( request()->route()->getName() == 'emails.folder' && request()->route()->parameter( 'id' ) == $folder->id ) active @endif">
        <div class="list-group-item-link">
            <a href="{{ route( 'emails.folder' , [ 'emailAccount' => $emailAccount->id , 'id' => $folder->id ] ) }}"
               class="folder-level-{{ $folderLevel }}" title="{{ $folder->name }}"
               @if( request()->route()->parameter('emailAccount')->id == $emailAccount->id ) data-email-account="{{ $emailAccount->id }}" data-box="{{ $folder->id  }}" data-droptarget="true" @endif >
                <i class="far fa-fw fa-folder"></i> {{ $folder->name }}
            </a>
        </div>
        <div class="list-group-item-dropdown">
            <div class="dropdown">
                <button class="btn" type="button" data-toggle="dropdown">
                    <i class="fas fa-fw fa-bars"></i>
                </button>
                @include( 'dtv.emails::helpers.dropdownMenu', [ 'box' => $folder->id ] )
            </div>
        </div>
    </div>

    @if( $folder->subFolders->count() > 0 )
        @include( 'dtv.emails::helpers.folderItem', [ 'emailFolders' => $folder->subFolders->sortBy( 'name' ) , 'folderLevel' => ($folderLevel + 1) ])
    @endif
@endforeach