@extends( $layout )

@section('title')
    @lang('dtv.emails::emails.views.create')
@endsection

@section('buttons')
    <button type="submit" id="email-submit" class="btn btn-save btn-{{ config( 'dtv.app.button_type' ) }}">
        <i class="far fa-fw fa-paper-plane" title=""></i> @lang('dtv.emails::emails.buttons.send')
    </button>
@endsection

@section('content')
    <style>
        .mail-input-addon .input-group-text {
            width: 90px;
            text-align: center;
            background: none;
            border: none;
            font-weight: bold;
            font-size: 18px;
            color: black;
        }

        .mail-input-addon .input-group-text i {
            font-size: 1.1633333333em;
        }

        .mail-input-addon i {
            margin-right: 5px;
        }

        .mail-subject {
            font-weight: bold;
            font-size: 17px;
            border: none;
            border-bottom: 2px solid gainsboro;
            border-radius: 0;
            height: auto;
            padding: 10px;
        }

        .mail-text {
            min-height: 400px;
            font-size: 16px;
            border: none;
            border-radius: 0;
            padding: 15px;
        }

        .mail-header {
            background: rgb(255,255,255);
            background: linear-gradient(180deg, rgba(255,255,255,1) 0%, rgba(245,245,250,1) 33%, rgba(211,219,221,1) 100%);
            margin-top: -15px;
            padding-top: 15px;
            border-bottom: 1px solid gainsboro;
        }

        .mail-body > .col-12 {
            margin-bottom: -15px;
        }

        .mail-header .bootstrap-select {
            width: calc(100% - 90px) !important;
        }

        .mail-append-hint {
            margin: 15px -15px -15px -15px;
            display: block;
            padding: 4px 15px;
            background: rgb(229, 249, 255);
            border-top: 1px solid gainsboro;
        }

        .mail-header input[type="file"] {
            height: auto;
        }

        #new-mail .input-group > .form-control:not(:first-child) {
            border-radius: 0.25rem;
        }

        .input-group .form-taggable {
            position: relative;
            -webkit-box-flex: 1;
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            width: 1%;
            margin-bottom: 0;
        }

        .taggle_list {
            margin: 0;
            width: 100%;
            display: block;
            padding: 0.2rem 0.25rem 1px;
            font-size: 1rem;
            line-height: 1.6;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }

        .taggle_input {
            border: none;
            outline: none;
            font-size: 16px;
            font-weight: 300;
        }

        .taggle_list li {
            float: left;
            display: inline-block;
            white-space: nowrap;
            font-weight: 500;
        }

        .taggle_list .taggle {
            margin-right: 8px;
            background: #E2E1DF;
            padding: 3px 15px;
            border-radius: 15px;
            position: relative;
            cursor: pointer;
            margin-bottom: 2px;
        }

        .taggle_list::after {
            display: block;
            clear: both;
            content: "";
        }

        .taggle_list .taggle .close {
            font-size: 1.1rem;
            text-decoration: none;
            color: rgba(0, 0, 0, 0.8);
            border: 0;
            background: none;
            cursor: pointer;
            margin-left: 7px;
            margin-top: 3px;
        }

        .taggle_placeholder {
            position: absolute;
            color: #CCC;
            top: 7px;
            left: 15px;
            transition: opacity, .25s;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .taggle_input {
            padding: 4px;
            float: left;
            background: none;
            width: 100% !important;
            max-width: 100%;
        }

        .taggle_sizer {
            padding: 0;
            margin: 0;
            position: absolute;
            top: -500px;
            z-index: -1;
            visibility: hidden;
        }

        /** Autocomplete Menu Items **/
        .ui-menu.ui-autocomplete {
            background: white;
            border: 1px solid #c3c3c3;
            border-bottom-left-radius: 5px;
            border-bottom-right-radius: 5px;
        }
        .ui-menu-item-wrapper {
            padding: 4px 8px 6px !important;
        }

        .ui-menu .ui-state-focus,
        .ui-menu .ui-state-active {
            margin: 0;
            background: #f0f0f0;
        }

        .ui-menu-item-main {
            font-size: 13px;
            font-weight: bold;
        }
        .ui-menu-item-second {
            font-size: 11px;
            color: #696969;
            line-height: 10px;
        }
    </style>
    <form method="post" id="new-mail" action="{{ route('emails.store') }}">
        @csrf()
        <input type="hidden" name="reference" value="{{ $reference ?? '' }}">
        @if( isset( $forward ) )
            <input type="hidden" name="forward" value="{{ $forward }}"/>
        @endif
        @if( isset( $answer ) )
            <input type="hidden" name="answer" value="{{ $answer }}"/>
        @endif
        <div class="row mail-header">
            <div class="col-12">
                <div class="row">
                    <div class="col-11 pr-0">
                        <div class="row">
                            <div class="col-6 pr-0">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend mail-input-addon">
                                        <label class="input-group-text" for="from">
                                            <i class="far fa-fw fa-database"></i> Acc.
                                        </label>
                                    </div>
                                    <select id="from" class="form-select show-tick input-max-width" data-live-search="true"
                                            name="from" data-width="100%" data-validation="required">
                                        @foreach( $emailAccounts as $key => $value )
                                            <option value="{{ $key }}" @if( 'VALUE' == $key ) selected @endif
                                            @if( $value['icon'] != null ) data-icon="{{ $value['icon'] }}" @endif >{{ $value['value'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend mail-input-addon">
                                        <label class="input-group-text" for="layout">
                                            Layout
                                        </label>
                                    </div>
                                    <select id="layout" class="form-select show-tick input-max-width" data-live-search="true"
                                            name="layout" data-width="100%" data-validation="required">
                                        @foreach( \DTV\EmailHandler\Services\MailableManager::getSelectOptions() as $key => $value )
                                            <option value="{{ $key }}" @if( config('dtv.modules.emails.default_layout', \DTV\EmailHandler\Layouts\HtmlMail::class )::getKey() == $key ) selected @endif
                                            @if( $value['icon'] != null ) data-icon="{{ $value['icon'] }}" @endif >{{ $value['value'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-1">
                        <div class="dropdown">
                            <button class="btn btn-outline-secondary w-100" type="button" data-toggle="dropdown">
                                <i class="fas fa-fw fa-ellipsis-v"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#" id="add-cc">
                                    <i class="far fa-fw fa-user"></i> CC hinzufügen
                                </a>
                                <a class="dropdown-item" href="#" id="add-bcc">
                                    <i class="far fa-fw fa-user"></i> BCC hinzufügen
                                </a>
                                <a class="dropdown-item" href="#" id="add-files">
                                    <i class="far fa-fw fa-paperclip"></i> Datei hinzufügen
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend mail-input-addon">
                                <span class="input-group-text"><i class="far fa-fw fa-user"></i> To</span>
                            </div>
                            <div id="to" class="form-taggable clearfix"></div>
                        </div>
                    </div>
                    <div class="col-12 d-none" id="cc-row">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend mail-input-addon">
                                <span class="input-group-text"><i class="far fa-fw fa-user"></i> CC</span>
                            </div>
                            <div id="cc" class="form-taggable clearfix"></div>
                        </div>
                    </div>
                    <div class="col-12 d-none" id="bcc-row">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend mail-input-addon">
                                <span class="input-group-text"><i class="far fa-fw fa-user"></i> BCC</span>
                            </div>
                            <div id="bcc" class="form-taggable clearfix"></div>
                        </div>
                    </div>
                    <div class="col-12 d-none" id="file-row">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend mail-input-addon">
                                <span class="input-group-text"><i class="far fa-fw fa-paperclip"></i> File</span>
                            </div>
                            <input type="file" name="attachments[]" class="form-control" multiple>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mail-body">
            <div class="col-12 p-0">
                <input type="text" name="subject" class="form-control mail-subject" value="{{ $subject ?? '' }}" placeholder="Subject">
                <textarea class="form-control mail-text mb-0" name="text" placeholder="Text">{!! $text ?? '' !!}</textarea>
            </div>
        </div>
        @if( isset( $forward ) || isset( $answer ) )
            <small class="text-info mail-append-hint">
                <i class="fas fa-fw fa-info-circle"></i> The initial mail content will automatically be appended after the content of this mail.
            </small>
        @endif
    </form>
@endsection

@push('scripts')
    <script>
        var DTV = DTV || {};

        /**
         * DTV Sms Class
         */
        DTV.Email = class {
            /**
             * Constructor method
             */
            constructor() {
                this.preselected = {
                    'to': @json( $to ?? [] ),
                    'cc': @json( $cc ?? [] ),
                    'bcc': @json( $bcc ?? [] ),

                };
                this.taggle = {
                    'to': null,
                    'cc': null,
                    'bcc': null
                };

                this.init();
            }

            /**
             * Inits the sms create form
             */
            init() {
                ([ 'to', 'cc', 'bcc' ]).forEach(function ( input ) {
                    // Init Taggle Input
                    this.taggle[input] = new Taggle( input, {
                        preserveCase: true,
                        hiddenInputName: input+'[]',
                        tags: Object.keys( this.preselected[input] ),
                        tagFormatter: function ( li ) {
                            var text = li.querySelector( '.taggle_text' );

                            if ( this.preselected[input][ text.textContent ] ) {
                                text.textContent = this.preselected[input][ text.textContent ];
                            }

                            return li;
                        }.bind( this ),
                        placeholder: input.toUpperCase(),
                        onBeforeTagAdd: function ( event, tag ) {
                            console.log( event );
                            if ( event !== null && event.which === 13 ) {
                                const key = Object.keys( this.preselected[input] ).find( key => this.preselected[input][ key ] === tag );

                                if ( key !== undefined ) {
                                    this.taggle[input].add( key );
                                    return false;
                                }
                            }
                        }.bind( this ),
                        saveOnBlur: true,
                    } );
                    var container = this.taggle[input].getContainer();

                    $( this.taggle[input].getInput() ).autocomplete( {
                        source: url( '/emails/getAutocompleteOptions' ),
                        appendTo: container,
                        minLength: 3,
                        autoFocus: true,
                        delay: 500,
                        position: { at: "left bottom", of: container },
                        select: function ( event, data ) {
                            event.preventDefault();

                            if ( event.which === 13 ) {
                                return false;
                            }

                            //Add the tag if user clicks
                            this.preselected[input][ data.item.value ] = data.item.email;
                            this.taggle[input].add( data.item.value );
                        }.bind( this ),
                        focus: function ( event, data ) {
                            event.preventDefault();
                            this.preselected[input][ data.item.value ] = data.item.email;
                            this.taggle[input].getInput().value = data.item.email;
                        }.bind( this )
                    } ).autocomplete( "instance" )._renderItem = function ( ul, item ) {
                        return $( '<li>' )
                            .append( '<div><div class="ui-menu-item-main">' + item.label + '</div><div class="ui-menu-item-second">' + item.email + '</div></div>' )
                            .appendTo( ul );
                    };
                }.bind(this));

                $( '#add-files' ).one( 'click', function () {
                    $( '#file-row' ).removeClass( 'd-none' );
                    $( this ).addClass( 'disabled' );
                } );

                ([ 'cc', 'bcc' ]).forEach( function ( input ) {
                    if ( this.preselected[ input ].length === 0 ) {
                        $( '#add-' + input ).one( 'click', function () {
                            $( '#' + input + '-row' ).removeClass( 'd-none' );
                            $( this ).addClass( 'disabled' );
                        } );
                    } else {
                        $( '#' + input + '-row' ).removeClass( 'd-none' );
                        $( '#add-' + input ).addClass( 'disabled' );
                    }
                }.bind( this ) );

                // Init Form
                new DTV.FormView(
                    document.querySelector( '#new-mail' ),
                    document.querySelector( '#email-submit' ),
                );
            }
        };

        var email = new DTV.Email();
    </script>
@endpush