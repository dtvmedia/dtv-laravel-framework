@extends( $layout )

@section('title')
    {{ $email->subject }}
@endsection

@section('content')
    <style>
        #ajax_view_modal .modal-body > .row {
            background: rgb(255, 255, 255);
            background: linear-gradient(180deg, rgba(255, 255, 255, 1) 0%, rgba(245, 245, 250, 1) 33%, rgba(211, 219, 221, 1) 100%);
        }

        .email-content {
            display: block;
            height: 600px;
            overflow: auto;
            margin: 0 -15px -15px -15px;
            border: none;
            border-top: 1px solid darkgray;
            background: white;
            width: calc(100% + 30px);
        }

        .email-header {
            margin-bottom: 15px;
        }

        .email-header td, .email-header th {
            padding: 1px;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <table class="table-style-2 email-header">
                <tr>
                    <th>@lang('dtv.emails::emails.fields.from')</th>
                    <td>
                        {{ $email->from }}
                        <small class="float-right">
                            @if( $email->isIncoming() )
                                @lang('dtv.emails::emails.fields.received_at') {{ $email->received_at }}
                            @else
                                @lang('dtv.emails::emails.fields.sent_at') {{ $email->sent_at }}
                            @endif
                        </small>
                    </td>
                </tr>
                <tr>
                    <th>@lang('dtv.emails::emails.fields.to')</th>
                    <td>
                        {{ implode( ', ', $email->to ) }}
                    </td>
                </tr>
                @if( count( $email->cc ) > 0 )
                    <tr>
                        <th>@lang('dtv.emails::emails.fields.cc')</th>
                        <td>
                            {{ implode( ', ', $email->cc ) }}
                        </td>
                    </tr>
                @endif
                @if( count( $email->notEmbeddedAttachments ) > 0 )
                    <tr>
                        <th>@lang('dtv.emails::emails.fields.attachments')</th>
                        <td>
                            @foreach( $email->notEmbeddedAttachments as $attachment )
                                <a href="{{ route( 'emails.attachments.download' , [ 'emailAccount' => $emailAccount->id , 'mid' => $email->id , 'id' => $attachment->id ] ) }}">
                                    <i class="fa fa-fw fa-file-alt"></i> {{ $attachment->name }}
                                </a>
                            @endforeach
                        </td>
                    </tr>
                @endif
            </table>
        </div>
        @if( $email->isSpam() )
            <div class="col-md-12 alert-warning text-center p-2">
                <i class="far fa-fw fa-lg fa-exclamation-circle"></i> @lang('dtv.emails::emails.messages.spamModeMessage')
            </div>
        @endif
    </div>
    <iframe class="email-content" src="{{ $iframeUrl }}"></iframe>
@endsection