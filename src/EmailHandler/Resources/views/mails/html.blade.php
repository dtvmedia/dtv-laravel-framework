<!DOCTYPE html>
<html lang="de">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>{{ $subject }}</title>
        <style type="text/css">
            .body {
                background: #e7e7e7;
            }

            .wrapper {
                background: white;
                border-top: 6px solid {{ config('dtv.modules.emails.layout_color','deepskyblue') }};
                font-family: sans-serif;
                line-height: 24px;
            }

            .wrapper h1 {
                color: {{ config('dtv.modules.emails.layout_color','deepskyblue') }};
                font-weight: bold;
                line-height: 30px;
            }
        </style>
    </head>
    <body class="body">
        <!-- Wrapper -->
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin-bottom:20px;">
            <tbody>
                <tr>
                    <td width="*"></td>
                    <td width="800">
                        <br>
                        <table class="wrapper" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <td colspan="3" height="60">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="*"></td>
                                    <td width="90%">
                                        <h1>{!! $subject !!}</h1>
                                        {!! nl2br( $content ) !!}
                                    </td>
                                    <td width="*"></td>
                                </tr>
                                <tr>
                                    <td colspan="3" height="60">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="*"></td>
                </tr>
            </tbody>
        </table>
    </body>
</html>