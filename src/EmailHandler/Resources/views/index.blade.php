@extends( $layout )

@section('window-title')
    @if( request()->route()->getName() === 'emails.folder' )
        {{ $mailAccount->emailFolders->firstWhere( 'id' , request()->route()->parameter( 'id' ) )->name }}
    @else
        @lang( str_replace( 'emails.', 'dtv.emails::email_accounts.mailboxes.', request()->route()->getName() ))
    @endif
    @if( $mailAccount !== null )
        - {{ $mailAccount->email }}
    @endif
@endsection

@section('title')
    @if( $mailAccount !== null )
        <span class="sub-heading">{{ $mailAccount->email }}</span><br>
    @endif
    @if( str_contains( request()->route()->getName() , 'folder' ) )
        {{ $mailAccount->emailFolders->firstWhere( 'id' , request()->route()->parameter( 'id' ) )->name }}
    @else
        @lang( str_replace( 'emails.', 'dtv.emails::email_accounts.mailboxes.', request()->route()->getName() ))
    @endif
@endsection

@section('content')
    <style>
        .mail-account {
            margin-bottom: 10px;
        }
        .mail-account-name {
            font-weight: bold;
            margin-bottom: 5px;
            cursor: pointer;
            letter-spacing: -1px;
        }
        .mail-account .list-group .list-group-item {
            border: none;
            padding: 0;
        }
        .mail-account .list-group .list-group-item i {
            margin-right: 5px;
        }
        .sub-heading {
            font-size: 16px;
            color: gray;
        }
        .mails td {
            font-size: 14px;
            padding: 0.5rem;
        }

        .folder-level-2 {
            padding-left: 42px !important;
        }
        .folder-level-3 {
            padding-left: 62px !important;
        }
        .folder-level-4 {
            padding-left: 82px !important;
        }
        .drop-active {
            box-shadow: inset 0 0 0 1px rgba(0,0,0,0.3);
            z-index:99909;
        }

        .list-group-item-link {
            float: left;
            width: calc(100% - 30px)
        }

        .list-group-item-link a {
            display: block;
            padding: 4px 0 4px 22px;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            text-decoration: none;
            color:black;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .list-group-item-dropdown {
            float: left;
            width: 30px;
            text-align: center
        }
        .list-group-item-dropdown .btn {
            padding:0;
            height: 25px;
        }
        .list-group-item-dropdown .btn .fas {
            margin-right: 0 !important;
        }
        .list-group-item .dropdown {
            display: none;
        }
        .list-group-item:hover .dropdown, .list-group-item .dropdown.show, .list-group-item.active .dropdown {
            display: block !important;
        }
        .list-group-item:hover {
            z-index:3;
        }

    </style>

    @if( count( $emailAccounts ) === 0 )
        <div class="alert alert-warning">
            @lang( 'dtv.emails::email_accounts.messages.noMailAccountsAvailable' )
        </div>
    @else
        <div class="row">
            <div class="col-md-3">
                <h3>@lang('dtv.emails::email_accounts.views.index')</h3>
                @foreach( $emailAccounts as $emailAccount )
                    <div class="mail-account">
                        <div class="mail-account-name mt-4">
                            <i class="fas fa-fw fa-caret-down"></i>
                            <span>{{ $emailAccount->email }}</span>
                        </div>
                        <div class="list-group list-group-flush">
                            <div class="list-group-item list-group-item-action @if( request()->route()->getName() == 'emails.inbox' && request()->route()->parameter('emailAccount')->id == $emailAccount->id ) active @endif">
                                <div class="list-group-item-link">
                                    <a href="{{ route( 'emails.inbox' , [ 'emailAccount' => $emailAccount->id ] ) }}"
                                       @if( request()->route()->parameter('emailAccount')->id == $emailAccount->id ) data-email-account="{{ $emailAccount->id }}" data-box="inbox" data-droptarget="true" @endif >
                                        <i class="far fa-fw fa-envelope"></i> @lang('dtv.emails::email_accounts.mailboxes.inbox')
                                        @if( $emailAccount->new_incoming_emails_count > 0 )
                                            <span class="badge badge-warning badge-pill">{{ $emailAccount->new_incoming_emails_count }}</span>
                                        @endif
                                    </a>
                                </div>
                                <div class="list-group-item-dropdown">
                                    <div class="dropdown">
                                        <button class="btn" type="button" data-toggle="dropdown">
                                            <i class="fas fa-fw fa-bars"></i>
                                        </button>
                                        @include('dtv.emails::helpers.dropdownMenu',['box' => 'inbox'])
                                    </div>
                                </div>
                            </div>
                            <div class="list-group-item list-group-item-action @if( request()->route()->getName() == 'emails.outbox' && request()->route()->parameter('emailAccount')->id == $emailAccount->id ) active @endif">
                                <div class="list-group-item-link">
                                    <a href="{{ route( 'emails.outbox' , [ 'emailAccount' => $emailAccount->id ] ) }}">
                                        <i class="far fa-fw fa-envelope"></i> @lang('dtv.emails::email_accounts.mailboxes.outbox')
                                    </a>
                                </div>
                                <div class="list-group-item-dropdown">
                                    <div class="dropdown">
                                        <button class="btn" type="button" data-toggle="dropdown">
                                            <i class="fas fa-fw fa-bars"></i>
                                        </button>
                                        @include('dtv.emails::helpers.dropdownMenu',['box' => 'outbox'])
                                    </div>
                                </div>
                            </div>
                            <div class="list-group-item list-group-item-action @if( request()->route()->getName() == 'emails.spambox' && request()->route()->parameter('emailAccount')->id == $emailAccount->id ) active @endif">
                                <div class="list-group-item-link">
                                    <a href="{{ route( 'emails.spambox' , [ 'emailAccount' => $emailAccount->id ] ) }}"
                                       @if( request()->route()->parameter('emailAccount')->id == $emailAccount->id ) data-email-account="{{ $emailAccount->id }}" data-box="spambox" data-droptarget="true" @endif >
                                        <i class="far fa-fw fa-ban"></i> @lang('dtv.emails::email_accounts.mailboxes.spambox')
                                    </a>
                                </div>
                                <div class="list-group-item-dropdown">
                                    <div class="dropdown">
                                        <button class="btn" type="button" data-toggle="dropdown">
                                            <i class="fas fa-fw fa-bars"></i>
                                        </button>
                                        @include('dtv.emails::helpers.dropdownMenu',['box' => 'spambox'])
                                    </div>
                                </div>
                            </div>
                            <div class="list-group-item list-group-item-action @if( request()->route()->getName() == 'emails.trashbox' && request()->route()->parameter('emailAccount')->id == $emailAccount->id ) active @endif">
                                <div class="list-group-item-link">
                                    <a href="{{ route( 'emails.trashbox' , [ 'emailAccount' => $emailAccount->id ] ) }}"
                                       @if( request()->route()->parameter('emailAccount')->id == $emailAccount->id ) data-email-account="{{ $emailAccount->id }}" data-box="trashbox" data-droptarget="true" @endif >
                                        <i class="far fa-fw fa-trash-alt"></i> @lang('dtv.emails::email_accounts.mailboxes.trashbox')
                                    </a>
                                </div>
                                <div class="list-group-item-dropdown">
                                    <div class="dropdown">
                                        <button class="btn" type="button" data-toggle="dropdown">
                                            <i class="fas fa-fw fa-bars"></i>
                                        </button>
                                        @include('dtv.emails::helpers.dropdownMenu',['box' => 'trashbox'])
                                    </div>
                                </div>
                            </div>
                            @include('dtv.emails::helpers.folderItem', [ 'emailFolders' => $emailAccount->mainEmailFolders->sortBy( 'name' ) , 'folderLevel' => 1 ] )
                            <div class="list-group-item list-group-item-action">
                                <div class="list-group-item-link">
                                    <a href="javascript:void(0)" data-ajax-action="{{ route( 'email_accounts.folders.create' , [ 'emailAccount' => $emailAccount->id ] ) }}">
                                        <i class="far fa-fw fa-plus"></i> @lang('dtv.emails::email_accounts.buttons.createFolder')
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-md-9">
                <h3>@lang('dtv.emails::emails.views.index')</h3>
                <div class="mails">
                    {!! $emailOverview !!}
                </div>
            </div>
        </div>
    @endif
@endsection

@push('scripts')
    <script>
        'use strict';

        var DTV = DTV || {};

        DTV.MailClient = class {
            /**
             * Mail Client Constructor
             */
            constructor() {
                this.tbody = document.querySelector( '#dtv_emails__emails_views_index tbody' );
                this.observer = null;

                this.initMailAccountToggle();
                this.initMutationObserver();
                this.initDragAndDropHandling();
            }

            /**
             * Inits the mail account name toggle handling
             */
            initMailAccountToggle() {
                const mailAccounts = document.querySelectorAll( '.mail-account-name' );

                mailAccounts.forEach( function ( mailAccount ) {
                    mailAccount.addEventListener( 'click', function () {
                        const wrapper = this.parentElement;
                        const icon = this.querySelector( 'i' );
                        const mailboxes = wrapper.querySelector( '.list-group' );

                        // Todo find vanilla js solution
                        $( mailboxes ).slideToggle( 'fast' );

                        if ( icon !== null ) {
                            icon.classList.toggle( 'fa-caret-down' );
                            icon.classList.toggle( 'fa-caret-right' );
                        }
                    } )
                });
            }

            /**
             * Inits the mutation observer for the draggable table rows
             */
            initMutationObserver() {
                // Mutation Observer
                this.observer = new MutationObserver( function ( mutationsList, observer ) {
                    for ( let mutation of mutationsList ) {
                        for ( let tr of mutation.addedNodes ) {
                            tr.setAttribute( 'draggable', 'true' );
                        }
                    }
                } );

                // Start observing the target node for configured mutations
                this.observer.observe( this.tbody, { childList: true } );
            }

            /**
             * Inits the drag an drop handling
             */
            initDragAndDropHandling() {
                // Init Draggables
                this.tbody.querySelectorAll( 'tr' ).forEach( function ( tr ) {
                    tr.setAttribute( 'draggable', 'true' );
                } );

                // Init Droptargets
                const droptargets = document.querySelectorAll('[data-droptarget="true"]');

                droptargets.forEach( function ( target ) {
                    target.addEventListener( 'drop', this.onDropHandler.bind( this ) );
                    target.addEventListener( 'dragover', this.onDragOverHandler.bind( this ) );
                    target.addEventListener( 'dragleave', this.onDragLeaveHandler.bind( this ) );
                }, this );

                // Init Dragstart
                this.tbody.addEventListener( 'dragstart', function ( ev ) {
                    if ( matches( ev.target, 'tr' ) ) {
                        ev.dataTransfer.setData( 'text', ev.target.dataset.id );
                    }
                }, false );
            }

            /**
             * Helper function which return the drop link related to the given object
             */
            getDropLink( object ) {
                if ( object instanceof Text ) {
                    object = object.parentElement;
                }

                if ( !matches( object, 'a' ) ) {
                    object = object.closest( 'a' );
                }

                return object;
            }

            /**
             * On Drop Event Handler
             *
             * @param ev
             */
            onDropHandler( ev ) {
                ev.preventDefault();

                let object = this.getDropLink( ev.target );

                const email = ev.dataTransfer.getData( 'text' );
                const emailAccount = object.getAttribute( 'data-email-account' );
                const box = object.getAttribute( 'data-box' );

                // Remove drop css class
                if ( !matches( object, '.list-group-item-action' ) ) {
                    object = object.closest( '.list-group-item-action' );
                }
                object.classList.remove( 'drop-active' );

                // Check for valid drop item
                if ( email === '' || !(!isNaN( parseFloat( email ) ) && isFinite( email )) ) {
                    console.log( email );
                    new DTV.Message( 'warning', 'Invalid item', '', 5 );

                    return;
                }

                // Remove email row
                const mailRow = document.querySelector( '[data-id="' + email + '"]' );

                if ( mailRow !== null ) {
                    mailRow.remove();
                }

                // Send move request
                new DTV.Fetch( 'GET', url( '/emails/' + emailAccount + '/' + email + '/move/' + box ) )
                    .send();
            }

            /**
             * On Dragover Event Handler
             *
             * @param ev
             */
            onDragOverHandler( ev ) {
                ev.preventDefault();

                let object = this.getDropLink( ev.target );

                if ( !matches( object, '.list-group-item-action' ) ) {
                    object = object.closest( '.list-group-item-action' );
                }

                object.classList.add( 'drop-active' );
            }

            /**
             * On Dragleave Event Handler
             *
             * @param ev
             */
            onDragLeaveHandler( ev ) {
                ev.preventDefault();

                let object = this.getDropLink( ev.target );

                if ( !matches(object, '.list-group-item-action' ) ) {
                    object = object.closest( '.list-group-item-action' );
                }

                object.classList.remove( 'drop-active' );
            }
        };

        new DTV.MailClient();
    </script>
@endpush