@extends( $layout )

@section('title')
    @lang( 'dtv.emails::email_accounts.views.errorsFor', [ 'mailaccount' => $emailAccount->name ] )
@endsection

@section('content')
    <div class="col-md-12" style="margin-top:-15px;">
        <h3 class="mt-4">@lang( 'dtv.emails::email_accounts.views.details' )</h3>
        <table class="table-style-2">
            <tr>
                <th>@lang( 'dtv.emails::email_accounts.fields.last_retrieved' )</th>
                <td>{{ $emailAccount->last_retrieved }}</td>
            </tr>
            <tr>
                <th>@lang( 'dtv.emails::email_accounts.fields.last_try' )</th>
                <td>{{ $emailAccount->last_try }}</td>
            </tr>
            <tr>
                <th>@lang( 'dtv.emails::email_accounts.fields.errors' )</th>
                <td>{{ $emailAccount->errors }}</td>
            </tr>
            <tr>
                <th>@lang( 'dtv.emails::email_accounts.fields.status' )</th>
                <td>{{ $emailAccount->getStatusLabel() }}</td>
            </tr>
            <tr>
                <th>@lang( 'dtv.emails::email_accounts.fields.next_try' )</th>
                <td>{{ $emailAccount->getNextTryTime() ?? '-' }}</td>
            </tr>
        </table>

        @include('dtv.comments::comments', [ 'comments' => $emailAccount->comments()->orderBy( 'created_at' , 'DESC' )->limit( 10 )->get() ] )
    </div>
@endsection