@extends( $layout )

@section('title')
    @lang('dtv.emails::emails.views.index')
@endsection

@section('content')
    <div class="row">
        <div class="col" >
            <div class="empty-container jumbotron">
                <p>
                    <i class="fal fa-fw fa-2x fa-frown"></i><br>
                    @lang('dtv.emails::emails.messages.noMailAccount')<br>
                    @lang('dtv.emails::emails.messages.addFirstAccount')
                </p>
                {!! button( 'dtv.emails::email_accounts.buttons.create' , 'email_accounts.create' , [] , 'fa-plus' )->setType( config( 'dtv.app.button_type' ) )->setSize('lg')->useAjax() !!}
            </div>
        </div>
    </div>
@endsection