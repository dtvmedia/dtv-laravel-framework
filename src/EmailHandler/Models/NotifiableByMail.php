<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Models;

    /**
     * Notifiable By Mail Trait
     *
     * @package   DTV\EmailHandler\Models
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    trait NotifiableByMail
    {
        /**
         * Returns all emails of the current model
         *
         * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
         */
        public function emails()
        {
            return $this->morphToMany( Email::class , 'reference' , 'email_references' )
                ->withPivot( 'receiver' );
        }

        /**
         * Returns the email address
         *
         * @return string
         */
        public function getEmail()
        {
            return $this->email;
        }

        /**
         * Returns the name
         *
         * @return string|null
         */
        public function getEmailName()
        {
            return $this->getModelValue();
        }
    }