<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Models;

    use DTV\BaseHandler\Models\Model;

    /**
     * Email Folder Model
     *
     * @package   DTV\EmailHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class EmailFolder extends Model
    {
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'email_account_id' , 'name' , 'parent_folder_id' ,
        ];

        /**
         * The Icon Class used by the getSelectOptions Function for the options labels
         *
         * @var string
         */
        protected static $optionLabelIcon = 'fa-folder';

        /**
         * Returns the related Email Account
         *
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function emailAccount()
        {
            return $this->belongsTo( EmailAccount::class );
        }

        /**
         * Returns the related parent folder if such exists
         *
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function parentFolder()
        {
            return $this->belongsTo( EmailFolder::class , 'parent_folder_id' );
        }

        /**
         * Returns all related sub folders if such exist
         *
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function subFolders()
        {
            return $this->hasMany( EmailFolder::class , 'parent_folder_id' )->with( 'subFolders' );
        }

        /**
         * Returns all related emails
         *
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function emails()
        {
            return $this->hasMany( Email::class , 'email_folder_id' );
        }
    }
