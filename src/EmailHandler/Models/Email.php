<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Models;

    use DTV\BaseHandler\Models\Model;
    use Html2Text\Html2Text;
    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Database\Eloquent\Relations\HasMany;

    /**
     * Email Model
     *
     * @package   DTV\EmailHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Email extends Model
    {
        /**
         * Status constants
         */
        const STATUS_IN_NEW = 0;
        const STATUS_IN_READ = 1;

        const STATUS_OUT_FAILED = - 1;
        const STATUS_OUT_PENDING = 0;
        const STATUS_OUT_SENT = 1;
        const STATUS_OUT_DRAFT = 2;

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'email_account_id' , 'incoming' , 'subject' , 'from' , 'to' , 'cc' , 'bcc' , 'message_id' ,
            'size' , 'content' , 'status' , 'spam' , 'answered' , 'forwarded' , 'received_at' , 'sent_at' ,
            'html' , 'email_folder_id' , 'deleted_at' , 'hard_deleted_at'
        ];

        /**
         * Fields that should be casted to native types
         *
         * @var array
         */
        protected $casts = [
            'to'        => 'array' ,
            'cc'        => 'array' ,
            'bcc'       => 'array' ,
            'incoming'  => 'boolean' ,
            'spam'      => 'boolean' ,
            'forwarded' => 'boolean' ,
            'answered'  => 'boolean' ,
            'html'      => 'boolean' ,
            'received_at' => 'datetime' ,
            'sent_at'    => 'datetime' ,
            'hard_deleted_at'    => 'datetime' ,
        ];

        /**
         * The name of the related translation file
         * If null the getTranslationFile method will try to build it from the model name
         *
         * @var string
         */
        protected static $translationFile = 'dtv.emails::emails';

        /**
         * Returns the related Email Account
         *
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function emailAccount()
        {
            return $this->belongsTo( EmailAccount::class );
        }

        /**
         * Returns all related attachments
         *
         * @return HasMany
         */
        public function attachments()
        {
            return $this->hasMany( EmailAttachment::class );
        }

        /**
         * Returns all mail references
         *
         * @return HasMany
         */
        public function references()
        {
            return $this->hasMany( EmailReference::class );
        }

        /**
         * Returns all mail receiver references
         *
         * @return HasMany
         */
        public function receiverReferences()
        {
            return $this->hasMany(EmailReference::class)->where('receiver', true);
        }

        /**
         * Returns all linked mail references
         *
         * @return HasMany
         */
        public function linkReferences()
        {
            return $this->hasMany(EmailReference::class)->where('receiver', false);
        }

        /**
         * Returns all related not embedded attachments
         *
         * @return HasMany
         */
        public function notEmbeddedAttachments()
        {
            return $this->hasMany( EmailAttachment::class )->whereNull( 'content_id' );
        }

        /**
         * Returns whether or not the mail is a spam mail
         *
         * @return bool
         */
        public function isSpam()
        {
            return $this->spam;
        }

        /**
         * Returns whether or not the email is an incoming mail
         *
         * @return bool
         */
        public function isIncoming()
        {
            return $this->incoming == true;
        }

        /**
         * Returns whether or not the email is an outgoing mail
         *
         * @return bool
         */
        public function isOutgoing()
        {
            return $this->incoming == false;
        }

        /**
         * Returns the translated status
         *
         * @return mixed|string|\Symfony\Component\Translation\TranslatorInterface
         */
        public function getStatus()
        {
            if ( $this->isIncoming() ) {
                return self::trans( 'status.in.' . $this->status );
            }

            return self::trans( 'status.out.' . $this->status );
        }

        /**
         * Returns an status label
         *
         * @return mixed|string|\Symfony\Component\Translation\TranslatorInterface
         */
        public function getStatusLabel()
        {
            if ( $this->isIncoming() ) {
                switch ( $this->status ) {
                    case Email::STATUS_IN_NEW:
                        return badge( $this->getStatus() , 'warning' )->addClass( 'badge-md' );
                    case Email::STATUS_IN_READ:
                        return badge( $this->getStatus() , 'info' )->addClass( 'badge-md' );
                    default:
                        return $this->getStatus();
                }
            }

            switch ( $this->status ) {
                case Email::STATUS_OUT_FAILED:
                    return badge( $this->getStatus() , 'danger' )->addClass( 'badge-md' );
                case Email::STATUS_OUT_PENDING:
                    return badge( $this->getStatus() , 'warning' )->addClass( 'badge-md' );
                case Email::STATUS_OUT_SENT:
                    return badge( $this->getStatus() , 'success' )->addClass( 'badge-md' );
                case Email::STATUS_OUT_DRAFT:
                    return badge( $this->getStatus() , 'info' )->addClass( 'badge-md' );
                default:
                    return $this->getStatus();
            }
        }

        /**
         * Returns an string with icon for all email flags
         *
         * @return string
         */
        public function getFlags()
        {
            $flags = '';

            if ( $this->not_embedded_attachments_count > 0 ) {
                $flags .= icon( 'fa-paperclip' , self::trans( 'flags.hasAttachments' ) );
            }

            if ( $this->forwarded == true ) {
                $flags .= icon( 'fa-share' , self::trans( 'flags.wasForwarded' ) );
            }

            if ( $this->answered == true ) {
                $flags .= icon( 'fa-reply' , self::trans( 'flags.wasAnswered' ) );
            }

            return $flags;
        }

        /**
         * Returns an subject label with the status label and the flag icons
         *
         * @return string
         */
        public function getSubjectLabel()
        {
            return $this->getStatusLabel() . ' ' . e( $this->subject ) . ' ' . $this->getFlags();
        }

        /**
         * Returns the sender of the mail in the short version
         *
         * @return string
         */
        public function getFromShort()
        {
            $icon = icon( 'fa-user' );

            if ( str_contains( $this->from , '<' ) ) {
                [ $fromName , $fromMail ] = explode( '<' , $this->from , 2 );

                return $icon . ' ' . e( $fromName );
            }

            return $icon . ' ' . e( $this->from );
        }

        /**
         * Returns the receivers of the mail in the short version
         *
         * @return string
         */
        public function getToShort()
        {
            $toArray = [];

            foreach ( $this->to as $to ) {
                $icon = icon( 'fa-user' );

                if ( str_contains( $to , '<' ) ) {
                    [ $toName , $toMail ] = explode( '<' , $to , 2 );

                    $toArray[] = $icon . ' ' . e( $toName );
                } else {
                    $toArray[] = $icon . ' ' . e( $to );
                }
            }

            return implode( ', ' , $toArray );
        }

        /**
         * Returns an message preview string
         *
         * @param int $length
         *
         * @return string
         */
        public function getMessagePreview( $length = 100 )
        {
            $html = new Html2Text( $this->content , [ 'do_links' => 'none' ] );

            return str_excerpt( trim( $html->getText() , " \t\n\r\0\x0B\xC2\xA0" ) , $length );
        }

        /**
         * Returns whether or not this mail is an text mail
         *
         * @return bool
         */
        public function isTextMail()
        {
            return !$this->html;
        }

        /**
         * Returns whether or not this mail is an html mail
         *
         * @return bool
         */
        public function isHtmlMail()
        {
            return $this->html;
        }

        /**
         * Scope a query to only include inbox emails.
         *
         * @param Builder $query
         *
         * @return Builder
         */
        public function scopeInbox( $query )
        {
            return $query->where( 'spam' , 0 )->where( 'incoming' , true )->whereNull( 'email_folder_id' );
        }

        /**
         * Scope a query to only include outbox emails.
         *
         * @param Builder $query
         *
         * @return Builder
         */
        public function scopeOutbox( $query )
        {
            return $query->where( 'incoming' , false );
        }

        /**
         * Scope a query to only include spambox emails.
         *
         * @param Builder $query
         *
         * @return Builder
         */
        public function scopeSpambox( $query )
        {
            return $query->where( 'spam' , 1 )->where( 'incoming' , true );
        }

        /**
         * Scope a query to only include trashbox emails.
         *
         * @param Builder $query
         *
         * @return Builder
         */
        public function scopeTrashbox( $query )
        {
            return $query->where( 'incoming' , true )->onlyTrashed();
        }

        /**
         * Scope a query to only include trashbox emails.
         *
         * @param Builder $query
         *
         * @return Builder
         */
        public function scopeHardDeleted( $query )
        {
            return $query->where( 'incoming' , true )->withoutGlobalScope( 'withoutHardDeleted' )->whereNotNull( 'hard_deleted_at' );
        }

        /**
         * Scope a query to only include emails of a specific folder.
         *
         * @param Builder $query
         * @param int     $folder
         *
         * @return Builder
         */
        public function scopeFolder( $query , $folder )
        {
            return $query->where( 'email_folder_id' , $folder )->where( 'incoming' , true );
        }

        /**
         * The "booting" method of the model.
         *
         * @return void
         */
        protected static function boot()
        {
            parent::boot();

            static::addGlobalScope( 'withoutHardDeleted' , function ( Builder $builder ) {
                $builder->whereNull( 'hard_deleted_at' );
            } );
        }
    }
