<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Models;

    use DTV\BaseHandler\Models\Model;
    use DTV\BaseHandler\Models\Pivot;

    /**
     * Email Reference Model
     *
     * @package   DTV\EmailHandler\Models
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class EmailReference extends Pivot
    {
        /**
         * The table associated with the model.
         *
         * @var string
         */
        protected $table = 'email_references';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'email_id' , 'reference_type' , 'reference_id' , 'receiver'
        ];

        /**
         * Returns the related Email
         *
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function email()
        {
            return $this->belongsTo( Email::class );
        }

        /**
         * Returns the related reference
         *
         * @return \Illuminate\Database\Eloquent\Relations\MorphTo
         */
        public function reference()
        {
            return $this->morphTo();
        }
    }
