<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Models;

    use DTV\BaseHandler\Models\Model;

    /**
     * Email Attachment Model
     *
     * @package   DTV\EmailHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class EmailAttachment extends Model
    {
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'email_id' , 'name' , 'type' , 'size' , 'path' , 'content_id'
        ];

        /**
         * The attributes that should be cast to native types.
         *
         * @var array
         */
        protected $casts = [
            'embed' => 'boolean' ,
        ];

        /**
         * Returns the related Email
         *
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function email()
        {
            return $this->belongsTo( Email::class );
        }
    }
