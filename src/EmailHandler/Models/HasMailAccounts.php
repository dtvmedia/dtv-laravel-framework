<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Models;

    /**
     * Has Email Account Trait
     *
     * @package   DTV\EmailHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    trait HasMailAccounts
    {
        /**
         * Returns all private mail accounts of the user
         *
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function privateEmailAccounts()
        {
            return $this->hasMany( EmailAccount::class , 'owner_id' );
        }

        /**
         * Returns all public mail accounts of the user
         *
         * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
         */
        public function publicEmailAccounts()
        {
            return $this->belongsToMany( EmailAccount::class , 'email_account_users' )->withTimestamps()->where( 'owner_id' , '=' , 0 );
        }

        /**
         * Returns all related email Accounts
         *
         * @return mixed
         */
        public function emailAccounts()
        {
            return $this->belongsToMany( EmailAccount::class , 'email_account_users' )->withTimestamps();
        }
    }