<?php

    /** @noinspection PhpUnused */

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\EmailHandler\Models;

    use Carbon\Carbon;
    use Ddeboer\Imap\ConnectionInterface;
    use Ddeboer\Imap\Exception\AuthenticationFailedException;
    use Ddeboer\Imap\Server;
    use DTV\BaseHandler\Models\Model;
    use DTV\BaseHandler\Views\Components\Badge;
    use DTV\CommentsHandler\Models\Commentable;
    use DTV\EmailHandler\EmailHandler;
    use DTV\EmailHandler\Layouts\TestMail;
    use DTV\EmailHandler\Services\MailAddressCollection;
    use DTV\Oxygen\Oxygen;
    use Exception;
    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Database\Eloquent\Collection;
    use Illuminate\Database\Eloquent\Relations\BelongsTo;
    use Illuminate\Database\Eloquent\Relations\BelongsToMany;
    use Illuminate\Database\Eloquent\Relations\HasMany;
    use Illuminate\Support\Facades\Mail;
    use Symfony\Component\Mailer\Transport\Dsn;
    use Symfony\Component\Mailer\Transport\Smtp\EsmtpTransportFactory;

    /**
     * Email Account Model
     *
     * @package   DTV\EmailHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class EmailAccount extends Model
    {
        use Commentable;

        /**
         * Status constants
         */
        const STATUS_INACTIVE = 0;
        const STATUS_ACTIVE = 1;

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'name' , 'in_server' , 'in_port' , 'in_flags' , 'out_server' , 'out_port' , 'out_flags' , 'email' , 'password' ,
            'status' , 'mailbox_inbox' , 'mailbox_failed' , 'owner_id' , 'last_retrieved' , 'errors' , 'last_try'
        ];

        /**
         * The attributes that should be cast.
         *
         * @var array
         */
        protected $casts = [
            'last_retrieved' => 'datetime',
            'last_try' => 'datetime'
        ];

        /**
         * The name of the related translation file
         * If null the getTranslationFile method will try to build it from the model name
         *
         * @var string
         */
        protected static $translationFile = 'dtv.emails::email_accounts';

        /**
         * The column used by the getSelectOptions Function for the options labels
         *
         * @var string
         */
        public static $optionLabelColumn = 'getLabel';

        /**
         * Returns all related emails
         *
         * @return HasMany
         */
        public function emails()
        {
            return $this->hasMany( Email::class );
        }

        /**
         * Returns all related incoming emails with the status new/unread
         *
         * @return Builder
         */
        public function newIncomingEmails()
        {
            return $this->emails()->where( 'status' , Email::STATUS_IN_NEW )->inbox();
        }

        /**
         * Returns all related email folders
         *
         * @return HasMany
         */
        public function emailFolders()
        {
            return $this->hasMany( EmailFolder::class );
        }

        /**
         * Returns the related main email folders and loads all sub folders via eager loading
         *
         * @return HasMany
         */
        public function mainEmailFolders()
        {
            return $this->emailFolders()->with( 'subFolders' )->whereNull( 'parent_folder_id' );
        }

        /**
         * Returns all users which have access to this public account
         *
         * @return BelongsToMany
         */
        public function users()
        {
            return $this->belongsToMany( config( 'dtv.models.user' ) , 'email_account_users' )->withTimestamps();
        }

        /**
         * Returns the owner of this private account
         *
         * @return BelongsTo
         */
        public function owner()
        {
            return $this->belongsTo( config( 'dtv.models.user' ) , 'owner_id' );
        }

        /**
         * Returns whether or not the mail account is active
         *
         * @return bool
         */
        public function isActive()
        {
            return $this->status == self::STATUS_ACTIVE;
        }

        /**
         * Returns whether or not the mail account is inactive
         *
         * @return bool
         */
        public function isInactive()
        {
            return $this->status == self::STATUS_INACTIVE;
        }

        /**
         * Returns whether or not the email account is an private account
         *
         * @return bool
         */
        public function isPrivate()
        {
            return $this->owner_id != 0;
        }

        /**
         * Connects to the incoming mail server account
         *
         * @return ConnectionInterface
         */
        public function connect()
        {
            $server = new Server( $this->in_server , $this->in_port , '/imap/' . $this->in_flags . '/validate-cert' );

            // $connection is instance of \Ddeboer\Imap\Connection
            return $server->authenticate( $this->email , $this->password );
        }

        /**
         * Initializes the account send connection
         *
         * @throws Exception
         */
        public function initSend()
        {
            config()->set( 'mail.from.name' , $this->name );
            config()->set( 'mail.from.address' , $this->email );

            if ( !isDebugMode() ) {
                if ( empty( $this->out_server ) || empty( $this->out_port ) || empty( $this->email ) || empty( $this->password ) ) {
                    throw new Exception( 'Missing mail server data for server_out: ' . $this );
                }

                // set mail account connection
                $dns = new Dsn(
                    !empty($this->out_flags) && $this->out_flags === 'tls' ? (($this->out_port == 465) ? 'smtps' : 'smtp') : 'smtp',
                    $this->out_server,
                    $this->email,
                    $this->password,
                    $this->out_port
                );
            } else {
                // set debugmail connection
                $dns = new Dsn(
                    !empty(config( 'dtv.modules.emails.debug.encryption' )) && config( 'dtv.modules.emails.debug.encryption' ) === 'tls' ? ((config( 'dtv.modules.emails.debug.port' ) == 465) ? 'smtps' : 'smtp') : 'smtp',
                    config( 'dtv.modules.emails.debug.host' ),
                    config( 'dtv.modules.emails.debug.username' ),
                    config( 'dtv.modules.emails.debug.password' ),
                    config( 'dtv.modules.emails.debug.port' )
                );
            }

            // Set the mailer
            $factory = new EsmtpTransportFactory;
            Mail::setSymfonyTransport($factory->create($dns));
        }

        /**
         * Tests the incoming email account connection
         *
         * @return bool
         */
        public function testInConnection()
        {
            try {
                imap_timeout(IMAP_OPENTIMEOUT, 6);
                $this->connect()->close();

                return true;
            } catch (AuthenticationFailedException) {
                return false;
            }
        }

        /**
         * Tests the outgoing email account connection
         *
         * @return bool
         */
        public function testOutConnection()
        {
            try {
                $to = new MailAddressCollection( [ $this->email ] );
                $emailHandler = new EmailHandler();
                $emailHandler->createMail( $this , 'Mail Account Connection Test Mail' , new TestMail() , $to )->send();

                return true;
            } catch ( Exception $e ) {
                logger( $e->getMessage() );

                return false;
            }
        }

        /**
         * Returns the from label for mails
         *
         * @return string
         */
        public function getFromMailLabel()
        {
            return $this->name . ' <' . $this->email . '>';
        }

        /**
         * Returns all active email accounts
         *
         * @return Collection<EmailAccount>
         */
        public static function active()
        {
            return self::query()->where( 'status' , self::STATUS_ACTIVE )->get();
        }

        /**
         * Returns an query prefiltered for own accounts which are marked as active
         *
         * @return Builder
         */
        public static function own()
        {
            if ( in_array( auth()->id() , explode( ',' , config( 'dtv.modules.admin.hidden_user_id' ) ) ) ) {
                return static::query()->where( 'status' , self::STATUS_ACTIVE );
            }

            return auth()->user()->emailAccounts()->where( 'status' , self::STATUS_ACTIVE );
        }

        /**
         * Returns a email account label
         *
         * @return string
         */
        public function getLabel()
        {
            return $this->name . ' (' . $this->email . ')';
        }

        /**
         * Returns an name label with an lock icon if the account is private
         *
         * @return string
         */
        public function getNameLabel()
        {
            $label = e( $this->name );

            if ( $this->isPrivate() ) {
                $label .= ' ' . icon( 'fa-lock' , self::trans( 'fields.private' ) , 'text-warning' )->setIconBase( 'fas' ) . ' ';
            }

            return $label;
        }

        /**
         * Get Connection Data Label for the incoming server
         *
         * @return string
         */
        public function getInLabel()
        {
            return $this->in_server . ':' . $this->in_port . ( ( empty( $this->in_flags ) ) ? '' : '/' . $this->in_flags );
        }

        /**
         * Get Connection Data Label for the outgoing server
         *
         * @return string
         */
        public function getOutLabel()
        {
            return $this->out_server . ':' . $this->out_port . ( ( empty( $this->out_flags ) ) ? '' : '/' . $this->out_flags );
        }

        /**
         * Returns the translated status
         *
         * @return string
         */
        public function getStatus()
        {
            return self::trans( 'status.' . $this->status );
        }

        /**
         * Returns an status label with the translated status
         *
         * @return Badge
         */
        public function getStatusLabel()
        {
            if ( $this->status == self::STATUS_ACTIVE ) {
                if ( $this->errors > 0 ) {
                    return badge( icon( 'fa-exclamation-triangle' , $this->getStatus() )->showLabel() , 'warning' )->addClass( 'badge-md' );
                }

                return badge( icon( 'fa-check' , $this->getStatus() )->showLabel() , 'success' )->addClass( 'badge-md' );
            }

            return badge( icon( 'fa-times' , $this->getStatus() )->showLabel() , 'danger' )->addClass( 'badge-md' );
        }

        /**
         * Get the last retrieval date
         *
         * @return Oxygen
         */
        public function getLastRetrieval()
        {
            $lastRetrieved = $this->last_retrieved;

            if ( $lastRetrieved->isNull() ) {
                $lastRetrieved = Oxygen::create(2000);
            }

            return $lastRetrieved;
        }

        /**
         * Returns if an mail check is required
         *
         * @return bool
         */
        public function isCheckRequired()
        {
            if ( $this->last_try->isNull() && $this->errors == 0 ) {
                return true;
            }

            $nextTryTime = $this->getNextTryTime();

            if ( $nextTryTime === null ) {
                return false;
            }

            if ( $nextTryTime->subMinute()->lte( Carbon::now() ) ) {
                return true;
            }

            return false;
        }

        /**
         * Returns the time when the mail account will be check the next time
         *
         * @return Carbon
         */
        public function getNextTryTime()
        {
            $checkIntervals = [
                0 => 5 ,
                1 => 10 ,
                2 => 15 ,
                3 => 20 ,
                4 => 30 ,
                5 => 60 ,
                6 => 120 ,
                7 => 180 ,
                8 => 240 ,
                9 => 300 ,
            ];

            if ( $this->errors >= 10 ) {
                return null;
            }

            return $this->last_try->addMinutes( $checkIntervals[ $this->errors ] );
        }

        /**
         * Returns the main out mail account
         *
         * @return EmailAccount
         */
        public static function mainOutAccount()
        {
            $id = config('dtv.modules.emails.main_out_account_id');
            if ($id !== null) {
                /** @var EmailAccount $account */
                $account = EmailAccount::query()->find($id);

                if ($account !== null) {
                    return $account;
                }
            }

            return new EmailAccount([
                'id'             => 0,
                'name'           => config('app.name'),
                'in_server'      => null,
                'in_port'        => null,
                'in_flags'       => null,
                'out_server'     => config('mail.mailers.smtp.host', config('mail.host')),
                'out_port'       => config('mail.mailers.smtp.port', config('mail.port')),
                'out_flags'      => config('mail.mailers.smtp.encryption', config('mail.encryption')),
                'email'          => config('mail.mailers.smtp.username', config('mail.username')),
                'password'       => config('mail.mailers.smtp.password', config('mail.password')),
                'status'         => EmailAccount::STATUS_ACTIVE,
                'mailbox_inbox'  => null,
                'mailbox_failed' => null,
                'owner_id'       => null,
                'last_retrieved' => null,
                'errors'         => 0,
                'last_try'       => null,
            ]);
        }
    }
