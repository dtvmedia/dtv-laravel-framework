<?php

    /** @noinspection PhpUnused */

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SmsHandler\Controllers;

    use DTV\BaseHandler\Controllers\Controller;
    use DTV\BaseHandler\JsonResponse;
    use DTV\BaseHandler\Models\Model;
    use DTV\BaseHandler\Views\Page;
    use DTV\SmsHandler\Contracts\NotifiableBySms;
    use DTV\SmsHandler\Models\Sms;
    use DTV\SmsHandler\Services\Views\SmsOverview;
    use DTV\SmsHandler\SmsHandler;
    use DTV\Support\Contracts\NotifiableGroup;
    use DTV\Support\MorphIdHandler;
    use Exception;
    use Illuminate\Http\Request;
    use Illuminate\Http\Response;
    use Illuminate\Support\Facades\DB;
    use Throwable;

    /**
     * SMS Controller
     *
     * @package   DTV\SmsHandler\Controllers
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class SmsController extends Controller
    {
        /**
         * Returnes the json data for the sms autocomplete options
         *
         * @param Request $request
         *
         * @return \Illuminate\Http\JsonResponse
         */
        public function getAutocompleteOptions(Request $request)
        {
            $results = [];
            $search = $request->get('term', '');

            foreach (config('dtv.modules.sms.search', []) as $model => $cols) {
                if (!is_a($model, NotifiableBySms::class, true) || !$model instanceof Model) {
                    continue;
                }

                $query = $model::query();
                $cols = array_wrap($cols);

                foreach ($cols as $col) {
                    if (starts_with($col, 'RAW::')) {
                        $col = DB::raw(str_replace('RAW::', '', $col));
                    }

                    $query->orWhere($col, 'LIKE', '%' . $search . '%');
                }

                foreach ($query->limit(10)->get() as $item) {
                    /** @var NotifiableBySms $item */
                    if (!empty($item->getMobile())) {
                        $results[] = [
                            'label'  => $item->getModelValue(),
                            'value'  => MorphIdHandler::getIdByModel($item),
                            'mobile' => $item->getMobile(),
                        ];
                    }
                }
            }

            return response()->json($results);
        }

        /**
         * Shows the sms overview page
         *
         * @throws Throwable
         * @return Page|string
         */
        public function index()
        {
            $this->addHeaderButton('dtv.sms::sms.buttons.create', 'fa-plus', 'sms.create');

            return $this->view(new SmsOverview(Sms::query()));
        }

        /**
         * Shows the SMS create form
         *
         * @throws Throwable
         * @return Page|Response|string
         */
        public function create()
        {
            return $this->modal('dtv.sms::create');
        }

        /**
         * Shows the SMS create form and prefills the given notifiable(s)
         *
         * @param string  $reference
         * @param Request $request
         *
         * @throws Exception
         * @throws Throwable
         *
         * @return Page|Response|string
         */
        public function createBy(Request $request, $reference = null)
        {
            if ($reference !== null) {
                MorphIdHandler::getModel($reference, false);
            }

            $to = [];
            $rawTo = array_wrap($request->get('to', []));
            foreach ($rawTo as $receiver) {
                $model = MorphIdHandler::getModel($receiver, false);

                if ($model instanceof NotifiableGroup) {
                    $notifiables = $model->notifiables;
                } elseif ($model instanceof NotifiableBySms) {
                    $notifiables = collect([$model]);
                } else {
                    throw new Exception('Invalid morph id given!');
                }

                foreach ($notifiables as $notifiable) {
                    $to[MorphIdHandler::getIdByModel($notifiable)] = $notifiable->getModelValue();
                }
            }

            return $this->modal('dtv.sms::create', compact('to', 'reference'));
        }

        /**
         * Handles the SMS create form
         *
         * @param Request $request
         *
         * @throws Exception
         * @throws Throwable
         * @return JsonResponse
         */
        public function store(Request $request)
        {
            $request->validate([
                'reference' => 'nullable|string',
                'to'        => 'required|array',
                'text'      => 'required|string',
            ]);

            $reference = $request->reference;
            if ($reference !== null) {
                $reference = MorphIdHandler::getModel($reference, false);
            }

            $smsHandler = SmsHandler::create()
                ->setTo($request->to)
                ->setBody($request->text)
                ->setReference($reference);

            $smsHandler->saveAndQueue();

            return $this->json()->hideModal()->success('dtv.sms::sms.messages.sent');
        }

        /**
         * Shows an specific sms
         *
         * @param int $id SMS ID
         *
         * @throws Throwable
         * @return JsonResponse|Page|Response|string
         */
        public function show($id)
        {
            $sms = Sms::query()->findOrFail($id);

            return $this->modal('dtv.sms::show', compact('sms'));
        }
    }
