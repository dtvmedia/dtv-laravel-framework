<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SmsHandler\Jobs;

    use DTV\EmailHandler\Services\MailBuilder;
    use DTV\SmsHandler\Models\Sms;
    use DTV\SmsHandler\SmsHandler;
    use Illuminate\Bus\Queueable;
    use Illuminate\Queue\SerializesModels;
    use Illuminate\Queue\InteractsWithQueue;
    use Illuminate\Contracts\Queue\ShouldQueue;
    use Illuminate\Foundation\Bus\Dispatchable;

    /**
     * Send Sms Job
     *
     * @package   DTV\SmsHandler\Jobs
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class SendSmsJob implements ShouldQueue
    {
        use Dispatchable , InteractsWithQueue , Queueable , SerializesModels;

        /**
         * Sms instance
         *
         * @var Sms
         */
        protected $sms;

        /**
         * Create a new job instance.
         *
         * @param Sms $sms
         *
         * @return void
         */
        public function __construct( Sms $sms )
        {
            $this->sms = $sms;
        }

        /**
         * Execute the job.
         *
         * @return void
         */
        public function handle()
        {
            SmsHandler::instance( $this->sms )->send();
        }
    }