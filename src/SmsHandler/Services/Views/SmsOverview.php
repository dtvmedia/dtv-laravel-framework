<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SmsHandler\Services\Views;

    use DTV\BaseHandler\Views\ListView;
    use DTV\EmailHandler\Models\Email;
    use DTV\EmailHandler\Models\EmailAccount;

    /**
     * SMS Overview Class
     *
     * @package   DTV\SmsHandler\Services\Views
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class SmsOverview extends ListView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.sms::sms.views.index';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-mobile-android-alt';

        /**
         * Translation file used for translating column names etc
         *
         * @var string
         */
        protected $transPath = 'dtv.sms::sms';

        /**
         * Adds the columns
         */
        protected function columns()
        {
            $this->addColumn( 'created_at' , 'created_at' , 'created_at' );
            $this->addColumn( 'receiver' , 'getReceiver' , 'to' )->enableHtml();
            $this->addColumn( 'message' , 'getMessagePreview' , 'message' );
            $this->addColumn( 'status' , 'getStatusLabel' , 'status' )->enableHtml();
        }

        /**
         * Adds the buttons for each record
         */
        protected function buttons()
        {
            $this->addButton( 'show' , 'fa-folder-open' , 'sms.show' );
        }
    }