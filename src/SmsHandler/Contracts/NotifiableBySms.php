<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SmsHandler\Contracts;

    use DTV\Support\Contracts\Notifiable;

    /**
     * Notifiable By Sms Contract
     *
     * @package   DTV\SmsHandler\Contracts
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    interface NotifiableBySms extends Notifiable
    {
        /**
         * Returns the mobile number
         *
         * @return string
         */
        public function getMobile();

        /**
         * Returns all sms of the current object
         *
         * @return \Illuminate\Database\Eloquent\Relations\MorphMany
         */
        public function sms();
    }