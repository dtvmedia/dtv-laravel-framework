<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SmsHandler\Contracts;

    use DTV\SmsHandler\Models\Sms;
    use Illuminate\Database\Eloquent\Model;

    /**
     * Provider Contract
     *
     * @package   DTV\SmsHandler\Contracts
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    interface ProviderContract
    {
        /**
         * Creates and returns an client instance for the communication with the provider
         *
         * @return mixed
         */
        public function getClient();

        /**
         * Adds an sms model
         *
         * @param Sms $sms
         *
         * @return $this
         */
        public function setModel( Sms $sms );

        /**
         * Sets the receivers of the message
         *
         * @param array|string $to
         *
         * @return $this
         */
        public function setTo( $to );

        /**
         * Sets the message body
         *
         * @param string $body
         *
         * @return $this
         */
        public function setBody( string $body );

        /**
         * Sets the sender number (if not set the global setting will be used)
         *
         * @param string $from
         *
         * @return $this
         */
        public function setFrom( string $from );

        /**
         * Sets the sms reference model
         *
         * @param Model $reference
         *
         * @return $this
         */
        public function setReference( ?Model $reference );

        /**
         * Returns the resolved mobile numbers
         *
         * @throws \Exception
         *
         * @return array
         */
        public function resolveReceivers(): array;

        /**
         * Updates the status of the previously created models
         *
         * @param bool $success
         *
         * @return bool
         */
        public function updateModels( bool $success );

        /**
         * Sends the message
         */
        public function send();

        /**
         * Saves the message to the database
         *
         * @return Model[]
         */
        public function save();
    }