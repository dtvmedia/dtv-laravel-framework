# SmsHandler - DTV Laravel Framework

## Requirements
- propaganistas/laravel-phone
- cmtelecom/messaging-php (For CM Telecom Provider)

## Configs
- *enabled*: Package activation switch
- *routes_enabled*: Activation switch for the package routes seperated from the package switch itself
- *default_provider*: Defines the SMS Provider class which should be used
- *search*: Defines which models (keys) and fields (values) the autocompletion should search for (see below for details)
- *send_queue*: Defines in which queue the SendSmsJobs should be pushed

## Usage
### Configure Number Autocompletion
The number autocompletion can be configured via the ``search`` config value. There you can define an array of models and there fields which should be used for the autocompletion search.
You can of course define multiple search field per model. You can also use raw query definitions for things like combined search fields:
```php
'search' => [
    \App\Models\Extra::class => [ 'mobile' , 'RAW::CONCAT(first_name, " ", last_name)' ]
] ,
``` 


