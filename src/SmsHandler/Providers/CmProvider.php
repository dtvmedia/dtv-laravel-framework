<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SmsHandler\Providers;

    use CMText\TextClient;
    use CMText\TextClientStatusCodes;
    use Exception;
    use Throwable;

    /**
     * CM SMS Provider
     *
     * Needed Environment Keys:
     * - CM_TELECOM_FROM
     * - CM_TELECOM_PRODUCT_TOKEN
     *
     * @package   DTV\SmsHandler\Providers
     * @copyright 2020 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CmProvider extends Provider
    {
        /**
         * SmsHandler constructor
         */
        public function __construct()
        {
            $this->setFrom(env('CM_TELECOM_FROM', config('app.name')));
        }

        /**
         * Creates and returns an client instance for the communication with the provider
         *
         * @return TextClient
         */
        public function getClient()
        {
            return new TextClient(env('CM_TELECOM_PRODUCT_TOKEN'));
        }

        /**
         * Builds the recipients array
         *
         * @throws Exception
         * @throws Throwable
         */
        private function build()
        {
            if ( empty( $this->body ) ) {
                throw new Exception( 'Empty message body' );
            }

            $to = $this->resolveReceivers();

            if ( empty( $to ) ) {
                throw new Exception( 'Empty message recipients' );
            }

            return $to;
        }

        /**
         * Sends the message
         */
        public function send()
        {
            try {
                $to = $this->build();

                $result = $this->getClient()->SendMessage($this->body, $this->from, $to);

                if ($result->statusCode !== TextClientStatusCodes::OK) {
                    // Not all messages were accepted
                    logger('[SmsHandler/CmProvider] Not all messages were accepted');
                    logger('[SmsHandler/CmProvider] ' . $result->statusMessage);

                    return $this->updateModels(false);
                }

                // All messages were accepted
                return $this->updateModels(true);
            } catch (Throwable $e) {
                // Something unexpected happened
                logger('[SmsHandler/CmProvider] Something unexpected happened: ' . $e->getMessage());

                return $this->updateModels(false);
            }
        }
    }
