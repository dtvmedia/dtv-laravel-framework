<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SmsHandler\Providers;

    use DTV\SmsHandler\Contracts\NotifiableBySms;
    use DTV\SmsHandler\Contracts\ProviderContract;
    use DTV\SmsHandler\Models\Sms;
    use DTV\SmsHandler\PhoneNumber;
    use DTV\Support\MorphIdHandler;
    use Illuminate\Database\Eloquent\Model;
    use Throwable;

    /**
     * SMS Base Provider
     *
     * @package   DTV\SmsHandler\Providers
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    abstract class Provider implements ProviderContract
    {
        /**
         * Message sender number
         *
         * @var string
         */
        protected string $from;

        /**
         * Array of message receiver numbers
         *
         * @var array
         */
        protected array $to;

        /**
         * Message body
         *
         * @var string
         */
        protected string $body;

        /**
         * Array of SMS Models
         *
         * @var Sms[]|array
         */
        protected array $models = [];

        /**
         * Reference Model
         *
         * @var Model|null
         */
        protected ?Model $reference;

        /**
         * Adds an sms model
         *
         * @param Sms $sms
         *
         * @return $this
         */
        public function setModel( Sms $sms )
        {
            $this->models[] = $sms;

            return $this;
        }

        /**
         * Sets the receivers of the message
         *
         * @param array|string|NotifiableBySms|NotifiableBySms[] $to
         *
         * @return $this
         */
        public function setTo( $to )
        {
            $this->to = array_wrap( $to );

            return $this;
        }

        /**
         * Sets the message body
         *
         * @param string $body
         *
         * @return $this
         */
        public function setBody( string $body )
        {
            $this->body = $body;

            return $this;
        }

        /**
         * Sets the sender number (if not set the global setting will be used)
         *
         * @param string $from
         *
         * @return $this
         */
        public function setFrom( string $from )
        {
            $this->from = $from;

            return $this;
        }

        /**
         * Sets the sms reference model
         *
         * @param Model|null $reference
         *
         * @return $this
         */
        public function setReference( ?Model $reference )
        {
            $this->reference = $reference;

            return $this;
        }

        /**
         * Returns the resolved mobile numbers
         *
         * @throws Throwable
         *
         * @return array
         */
        public function resolveReceivers(): array
        {
            $to = $this->to;
            foreach ( $to as $key => $receiver ) {
                if ( MorphIdHandler::isMorphId( $receiver ) ) {
                    $receiver = MorphIdHandler::getModel( $receiver );
                }

                // Todo put getCountry in NotifiableBySms
                if ( $receiver instanceof NotifiableBySms ) {
                    $to[ $key ] = $receiver->getMobile();
                }

                try {
                    $to[$key] = PhoneNumber::normalize($to[$key]);
                } catch ( Throwable $exception ) {
                    logger()->warning( '[SmsHandler] ' . $exception->getMessage() . ': "' . $to[ $key ] . '"' );
                    unset( $to[ $key ] );
                }
            }

            return $to;
        }

        /**
         * Updates the status of the previously created models
         *
         * @param bool $success
         *
         * @return bool
         */
        public function updateModels( bool $success )
        {
            foreach ( $this->models as $model ) {
                $model->update( [
                    'status' => ( $success ) ? Sms::STATUS_SENT : Sms::STATUS_FAILED ,
                ] );
            }

            return $success;
        }

        /**
         * Saves the message to the database
         *
         * @throws Throwable
         * @return Model[]
         */
        public function save()
        {
            $models = [];

            foreach ( $this->to as $to ) {
                $receiver_id = null;
                $receiver_type = null;

                if ( MorphIdHandler::isMorphId( $to ) ) {
                    $to = MorphIdHandler::getModel( $to );
                }

                if ( $to instanceof NotifiableBySms ) {
                    $receiver_id = $to->id;
                    $receiver_type = get_class( $to );
                    $to = $to->getMobile();
                }

                if ( !is_string( $to ) ) {
                    info( '[SmsHandler] Invalid receiver given (not a NotifiableBySms): "' . $to . '"' );

                    continue;
                }

                $models[] = Sms::query()->create( [
                    'receiver_id'    => $receiver_id ,
                    'receiver_type'  => $receiver_type ,
                    'from'           => $this->from ,
                    'to'             => $to ,
                    'message'        => $this->body ,
                    'status'         => Sms::STATUS_OPEN ,
                    'reference_type' => ( $this->reference === null ) ? null : get_class( $this->reference ) ,
                    'reference_id'   => ( $this->reference === null ) ? null : $this->reference->id ,
                ] );
            }

            $this->models = $models;

            return $models;
        }
    }
