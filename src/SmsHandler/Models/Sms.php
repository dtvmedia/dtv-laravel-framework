<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SmsHandler\Models;

    use DTV\BaseHandler\Models\Model;
    use DTV\BaseHandler\Views\Components\Badge;
    use Illuminate\Database\Eloquent\Relations\MorphTo;

    /**
     * SMS Model
     *
     * @package   DTV\SmsHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Sms extends Model
    {
        /**
         * Status Constants
         */
        const STATUS_OPEN = 0;
        const STATUS_SENT = 1;
        const STATUS_FAILED = 2;

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'receiver_id' , 'receiver_type' , 'from' , 'to' , 'message' , 'status' , 'created_at' ,
            'reference_id' , 'reference_type'
        ];

        /**
         * The name of the related translation file
         * If null the getTranslationFile method will try to build it from the model name
         *
         * @var string
         */
        public static $translationFile = 'dtv.sms::sms';

        /**
         * Returns the related receiver
         *
         * @return MorphTo
         */
        public function receiver()
        {
            return $this->morphTo( 'receiver' );
        }

        /**
         * Returns the related reference
         *
         * @return MorphTo
         */
        public function reference()
        {
            return $this->morphTo( 'reference' );
        }

        /**
         * Returns the receiver name or number
         *
         * @return string
         */
        public function getReceiver()
        {
            if ( $this->receiver !== null ) {
                return $this->receiver->getModelValue();
            }

            return $this->to;
        }

        /**
         * Returns the translated status
         *
         * @return string
         */
        public function getStatus()
        {
            return self::trans( 'status.' . $this->status );
        }

        /**
         * Returns the translated status as status label
         *
         * @return Badge|string
         */
        public function getStatusLabel()
        {
            return match ($this->status) {
                Sms::STATUS_OPEN   => badge($this->getStatus(), 'info')->addClass('badge-md'),
                Sms::STATUS_SENT   => badge($this->getStatus(), 'success')->addClass('badge-md'),
                Sms::STATUS_FAILED => badge($this->getStatus(), 'warning')->addClass('badge-md'),
                default            => $this->getStatus(),
            };
        }

        /**
         * Returns an message excerpt
         *
         * @return string
         */
        public function getMessagePreview()
        {
            return str_excerpt( $this->message , 100 );
        }
    }
