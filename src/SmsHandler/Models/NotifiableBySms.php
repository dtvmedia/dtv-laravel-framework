<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SmsHandler\Models;

    /**
     * Notifiable By SMS Trait
     *
     * @package   DTV\SmsHandler\Models
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    trait NotifiableBySms
    {
        /**
         * Returns all sms of the current object
         *
         * @return \Illuminate\Database\Eloquent\Relations\MorphMany
         */
        public function sms()
        {
            return $this->morphMany( Sms::class , 'receiver' );
        }

        /**
         * Returns the mobile number
         *
         * @return string
         */
        public function getMobile()
        {
            return $this->mobile;
        }
    }