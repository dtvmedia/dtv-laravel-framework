<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SmsHandler;

    use DTV\SmsHandler\Contracts\NotifiableBySms;
    use DTV\SmsHandler\Contracts\ProviderContract;
    use DTV\SmsHandler\Jobs\SendSmsJob;
    use DTV\SmsHandler\Models\Sms;
    use Illuminate\Database\Eloquent\Model;
    use InvalidArgumentException;

    /**
     * SMS Handler
     *
     * @package   DTV\SmsHandler
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class SmsHandler
    {
        /**
         * Provider instance
         *
         * @var ProviderContract
         */
        protected ProviderContract $provider;

        /**
         * SmsHandler constructor.
         *
         * @param ProviderContract|null $provider
         */
        public function __construct( ProviderContract $provider = null )
        {
            if ( $provider === null ) {
                $providerClass = config( 'dtv.modules.sms.default_provider' );

                $provider = new $providerClass();

                if ( !$provider instanceof ProviderContract ) {
                    throw new InvalidArgumentException( 'Invalid provider class given!' );
                }
            }

            $this->provider = $provider;
        }

        /**
         * Adds an sms model
         *
         * @param Sms $sms
         *
         * @return $this
         */
        public function setModel( Sms $sms )
        {
            $this->provider->setModel( $sms );

            return $this;
        }

        /**
         * Sets the receivers of the message
         *
         * @param array|string|NotifiableBySms|NotifiableBySms[] $to
         *
         * @return $this
         */
        public function setTo( $to )
        {
            $this->provider->setTo( $to );

            return $this;
        }

        /**
         * Sets the message body
         *
         * @param string $body
         *
         * @return $this
         */
        public function setBody( string $body )
        {
            $this->provider->setBody( $body );

            return $this;
        }

        /**
         * Sets the sender number (if not set the global setting will be used)
         *
         * @param string $from
         *
         * @return $this
         */
        public function setFrom( string $from )
        {
            $this->provider->setFrom( $from );

            return $this;
        }

        /**
         * Sets the sms reference model
         *
         * @param Model|null $reference
         *
         * @return $this
         */
        public function setReference( ?Model $reference )
        {
            $this->provider->setReference( $reference );

            return $this;
        }

        /**
         * Sends the message
         *
         * @return $this
         */
        public function send()
        {
            if ( isDebugMode() ) {
                return $this;
            }

            $this->provider->send();

            return $this;
        }

        /**
         * Saves the message to the database
         *
         * @return Model[]
         */
        public function save()
        {
            return $this->provider->save();
        }

        /**
         * Saves the built message to the database and sends it
         *
         * @return Model[]
         */
        public function saveAndSend()
        {
            $models = $this->save();

            $this->send();

            return $models;
        }

        /**
         * Saves the built message to the database and queues it
         *
         * @return Model[]
         */
        public function saveAndQueue()
        {
            $models = $this->save();

            foreach ( $models as $model ) {
                SendSmsJob::dispatch( $model )->onQueue( config( 'dtv.modules.sms.send_queue' , 'default' ) );
            }

            return $models;
        }

        /**
         * Static Factory method
         *
         * @return SmsHandler
         */
        public static function create()
        {
            return new self();
        }

        /**
         * Creates an new sms handler from an sms model instance
         *
         * @param Sms $sms
         *
         * @return static
         */
        public static function instance( Sms $sms )
        {
            return static::create()
                ->setFrom( $sms->from )
                ->setTo( $sms->to )
                ->setBody( $sms->message )
                ->setModel( $sms );
        }
    }
