<?php

namespace DTV\SmsHandler;

use Illuminate\Support\Str;
use Propaganistas\LaravelPhone\PhoneNumber as BasePhoneNumber;

class PhoneNumber
{
    /**
     * Tries to normalize the format of the given phone number
     *
     * @param string $phone
     *
     * @return string
     */
    public static function normalize(string $phone)
    {
        // Replace two leading zero with a plus for better compatibility
        if (Str::startsWith($phone, '00')) {
            $phone = preg_replace('/^00/', '+', $phone);
        }

        // Todo hardcoded country code DE
        return (new BasePhoneNumber($phone, 'DE'))->formatE164();
    }
}
