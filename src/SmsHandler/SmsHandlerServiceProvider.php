<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SmsHandler;

    use DTV\BaseHandler\BaseServiceProvider;
    use DTV\BaseHandler\RoutingHandler;
    use DTV\SmsHandler\Controllers\SmsController;

    /**
     * SMS Handler Service Provider
     *
     * @package   DTV\SmsHandler
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class SmsHandlerServiceProvider extends BaseServiceProvider
    {
        /**
         *  Namespace for resources of the module
         *
         * @var string
         */
        protected $resourcesNamespace = 'dtv.sms';

        /**
         * Array of commans
         *
         * @var array
         */
        protected $commands = [];

        /**
         * Config path for the module enable switch
         *
         * @var string|null
         */
        public static $config = 'dtv.modules.sms.enabled';

        /**
         * Registers the Modules routes
         *
         * @throws \Exception
         */
        public function routes()
        {
            // SMS Routes
            $smsRoutes = new RoutingHandler( SmsController::class , 'sms' , 'sms.' , RoutingHandler::ACCESS_AUTH );
            $smsRoutes->get( '/getAutocompleteOptions' , 'getAutocompleteOptions' , 'getAutocompleteOptions' , 'can:sms.view' );
            $smsRoutes->get( '/' , 'index' , 'index' , 'can:sms.view' );
            $smsRoutes->get( '/create' , 'create' , 'create' , 'can:sms.view' );
            $smsRoutes->get( '/createBy/{reference?}' , 'createBy' , 'createBy' , 'can:sms.view' );
            $smsRoutes->post( '/create' , 'store' , 'create' , 'can:sms.view' );
            $smsRoutes->get( '/{id}/show' , 'show' , 'show' , 'can:sms.view' );
        }

        /**
         * Defines all the modules gates
         */
        public function gates()
        {
            // Todo gates
            gate()->define( 'sms.view' , function ( $user ) {
                return true;
            } );
        }
    }