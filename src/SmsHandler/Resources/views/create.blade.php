@extends( $layout )

@section('title')
    @lang('dtv.sms::sms.views.create')
@endsection

@section('buttons')
    <button type="submit" id="sms-submit" class="btn btn-save btn-{{ config( 'dtv.app.button_type' ) }}">
        <i class="far fa-fw fa-paper-plane " title=""></i> @lang('dtv.sms::sms.buttons.send')
    </button>
@endsection

@section('content')
    <style>
        .sms-input-addon .input-group-text {
            width: 90px;
            text-align: center;
            background: none;
            border: none;
            font-weight: bold;
            font-size: 18px;
            color:black;
        }

        .sms-input-addon i {
            margin-right: 5px;
        }

        .sms-text {
            min-height: 250px;
            font-size: 18px;
            border: none;
            border-radius: 0;
            padding: 15px;
        }

        .sms-header {
            background: rgb(255,255,255);
            background: linear-gradient(180deg, rgba(255,255,255,1) 0%, rgba(245,245,250,1) 33%, rgba(211,219,221,1) 100%);
            margin-top: -15px;
            padding-top: 15px;
            border-bottom: 2px solid #9b9b9b;
        }

        .sms-body > .col-12 {
            margin-bottom: -15px;
        }

        .sms-header .bootstrap-select {
            width: calc(100% - 70px) !important;
        }

        .sms-header .bootstrap-select .btn {
            border-top-left-radius: 0 !important;
            border-bottom-left-radius: 0 !important;
        }
        .sms-header input[type="file"] {
            height: auto;
        }

        .modal-footer {
            border-top: 2px solid #9b9b9b;
        }

        .sms-statusbar {
            background: rgb(229, 249, 255);
            font-size: 12px;
            color: #25354a;
            padding:3px 0;
            border-top: 1px solid rgb(158, 178, 219);
        }

        .input-group .form-taggable {
            position: relative;
            -webkit-box-flex: 1;
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
            width: 1%;
            margin-bottom: 0;
        }

        .taggle_list {
            margin: 0;
            width: 100%;
            display: block;
            padding: 0.2rem 0.25rem 1px;
            font-size: 1rem;
            line-height: 1.6;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        }

        .taggle_input {
            border: none;
            outline: none;
            font-size: 16px;
            font-weight: 300;
        }

        .taggle_list li {
            float: left;
            display: inline-block;
            white-space: nowrap;
            font-weight: 500;
        }

        .taggle_list .taggle {
            margin-right: 8px;
            background: #E2E1DF;
            padding: 3px 15px;
            border-radius: 15px;
            position: relative;
            cursor: pointer;
            margin-bottom: 2px;
        }

        .taggle_list::after {
            display: block;
            clear: both;
            content: "";
        }

        .taggle_list .taggle .close {
            font-size: 1.1rem;
            text-decoration: none;
            color: rgba(0, 0, 0, 0.8);
            border: 0;
            background: none;
            cursor: pointer;
            margin-left: 7px;
            margin-top: 3px;
        }

        .taggle_placeholder {
            position: absolute;
            color: #CCC;
            top: 7px;
            left: 15px;
            transition: opacity, .25s;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .taggle_input {
            padding: 4px;
            float: left;
            background: none;
            width: 100% !important;
            max-width: 100%;
        }

        .taggle_sizer {
            padding: 0;
            margin: 0;
            position: absolute;
            top: -500px;
            z-index: -1;
            visibility: hidden;
        }

    </style>
    <form method="post" id="new-sms" action="{{ route('sms.create') }}">
        @csrf()
        <input type="hidden" name="reference" value="{{ $reference ?? '' }}">
        <div class="row sms-header">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend sms-input-addon">
                                <span class="input-group-text"><i class="far fa-fw fa-user"></i> @lang('dtv.sms::sms.fields.to'):</span>
                            </div>

                            <div id="to" class="form-taggable clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row sms-body">
            <div class="col-12 p-0">
                <textarea class="form-control sms-text mb-0" id="text" name="text" placeholder="Text">{!! $text ?? '' !!}</textarea>
                <div class="container-fluid">
                    <div class="sms-statusbar row">
                        <div class="col-4">
                            @lang('dtv.sms::sms.fields.messages'): <span id="message-label">0</span>
                        </div>
                        <div class="col-4">
                            @lang('dtv.sms::sms.fields.chars'): <span id="chars-label">0</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
    <script>
        var DTV = DTV || {};

        /**
         * DTV Sms Class
         */
        DTV.Sms = class {
            /**
             * Constructor method
             */
            constructor() {
                this.preselected = @json( $to ?? [] );
                this.charsLabel = $( '#chars-label' );
                this.messagesLabel = $( '#message-label' );
                this.text = $( '#text' );
                this.taggle = null;

                this.init();
            }

            /**
             * Inits the sms create form
             */
            init() {
                // Init Taggle Input
                this.taggle = new Taggle( 'to', {
                    preserveCase: true,
                    hiddenInputName: 'to[]',
                    tags: Object.keys( this.preselected ),
                    tagFormatter: function ( li ) {
                        var text = li.querySelector( '.taggle_text' );

                        if ( this.preselected[ text.textContent ] ) {
                            text.textContent = this.preselected[ text.textContent ];
                        }

                        return li;
                    }.bind( this ),
                    onBeforeTagAdd: function ( event, tag ) {
                        console.log( event );
                        if ( event !== null && event.which === 13 ) {
                            const key = Object.keys( this.preselected ).find( key => this.preselected[ key ] === tag );

                            if ( key !== undefined ) {
                                this.taggle.add( key );
                                return false;
                            }
                        }
                    }.bind( this ),
                    saveOnBlur: true,
                } );
                var container = this.taggle.getContainer();

                $( this.taggle.getInput() ).autocomplete( {
                    source: url( '/sms/getAutocompleteOptions' ),
                    appendTo: container,
                    minLength: 3,
                    autoFocus: true,
                    delay: 500,
                    position: { at: "left bottom", of: container },
                    select: function ( event, data ) {
                        event.preventDefault();

                        if ( event.which === 13 ) {
                            return false;
                        }

                        //Add the tag if user clicks
                        this.preselected[ data.item.value ] = data.item.label;
                        this.taggle.add( data.item.value );
                    }.bind( this ),
                    focus: function ( event, data ) {
                        event.preventDefault();
                        this.preselected[ data.item.value ] = data.item.label;
                        this.taggle.getInput().value = data.item.label;
                    }.bind( this )
                } ).autocomplete( "instance" )._renderItem = function ( ul, item ) {
                    return $( '<li>' )
                        .append( '<div><div class="ui-menu-item-main">' + item.label + '</div><div class="ui-menu-item-second">' + item.mobile + '</div></div>' )
                        .appendTo( ul );
                };

                // Init Form
                new DTV.FormView(
                    document.querySelector( '#new-sms' ),
                    document.querySelector( '#sms-submit' )
                );

                // Keyup Event Handler
                $( 'body' ).on( 'keyup', '#text', this.update.bind( this ) );

                // Initial status bar update
                this.update();
            }

            /**
             * Updates the status bar values
             */
            update() {
                const length = this.text.val().length;
                const messages = Math.ceil( length / 160 );

                this.charsLabel.text( length );
                this.messagesLabel.text( messages );
            }
        };

        var sms = new DTV.Sms();
    </script>
@endpush