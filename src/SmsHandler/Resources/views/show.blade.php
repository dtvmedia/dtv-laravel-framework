@extends( $layout )

@section('title')
    @lang('dtv.sms::sms.views.show') {{ $sms->to }}
@endsection

@section('content')
    <style>
        #ajax_view_modal .modal-body > .row {
            background: rgb(255, 255, 255);
            background: linear-gradient(180deg, rgba(255, 255, 255, 1) 0%, rgba(245, 245, 250, 1) 33%, rgba(211, 219, 221, 1) 100%);
        }

        .sms-content {
            display: block;
            height: 300px;
            overflow: auto;
            margin: 0 -15px -15px -15px;
            border: none;
            border-top: 1px solid darkgray;
            background: white;
            width: calc(100% + 30px);
            padding: 20px;
        }

        .sms-header {
            margin-bottom: 15px;
        }

        .sms-header td, .sms-header th {
            padding: 1px;
        }
    </style>

    <div class="row">
        <div class="col-md-12">
            <table class="table-style-2 sms-header">
                <tr>
                    <th>@lang('dtv.sms::sms.fields.from')</th>
                    <td>
                        {{ $sms->from }}
                        <small class="float-right">
                            @lang('dtv.sms::sms.fields.created_at') {{ $sms->created_at }}
                        </small>
                    </td>
                </tr>
                <tr>
                    <th>@lang('dtv.sms::sms.fields.to')</th>
                    <td>
                        {{ $sms->to }}
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="sms-content">
        {{ $sms->message }}
    </div>
@endsection