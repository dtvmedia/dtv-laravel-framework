<?php

    return [

        'name' => 'SMS' ,

        'views' => [
            'index'  => 'SMS' ,
            'create' => 'Send SMS' ,
            'show'   => 'Message to' ,
        ] ,

        'buttons' => [
            'create' => 'Send SMS' ,
            'send'   => 'Send SMS' ,
            'show'   => 'Show message' ,
        ] ,

        'fields' => [
            'created_at' => 'Date' ,
            'receiver'   => 'Receiver' ,
            'message'    => 'Message' ,
            'messages'   => 'Messages' ,
            'chars'      => 'Characters' ,
            'costs'      => 'Potential costs' ,
            'status'     => 'Status' ,
            'from'       => 'From' ,
            'to'         => 'To' ,
            'reference'  => 'Reference',
        ] ,

        'status' => [
            \DTV\SmsHandler\Models\Sms::STATUS_OPEN   => 'Open' ,
            \DTV\SmsHandler\Models\Sms::STATUS_SENT   => 'Sent' ,
            \DTV\SmsHandler\Models\Sms::STATUS_FAILED => 'Failed' ,
        ] ,

        'messages' => [
            'sentTitle' => 'SMS sent successfully' ,
            'sentText'  => 'The SMS was sent successfully' ,
        ] ,

    ];