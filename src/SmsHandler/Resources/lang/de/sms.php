<?php

    return [

        'name' => 'SMS' ,

        'views' => [
            'index'  => 'SMS' ,
            'create' => 'SMS senden' ,
            'show'   => 'Nachricht an' ,
        ] ,

        'buttons' => [
            'create' => 'SMS senden' ,
            'send'   => 'SMS senden' ,
            'show'   => 'Nachricht anzeigen' ,
        ] ,

        'fields' => [
            'created_at' => 'Datum' ,
            'receiver'   => 'Empfänger' ,
            'message'    => 'Nachricht' ,
            'messages'   => 'Nachrichten' ,
            'chars'      => 'Zeichen' ,
            'costs'      => 'Potenzielle Kosten' ,
            'status'     => 'Status' ,
            'from'       => 'Von' ,
            'to'         => 'An' ,
            'reference'  => 'Referenz',
        ] ,

        'status' => [
            0 => 'Offen' ,
            1 => 'Gesendet' ,
            2 => 'Fehlgeschlagen' ,
        ] ,

        'messages' => [
            'sentTitle' => 'SMS erfolgreich gesendet' ,
            'sentText'  => 'Die SMS wurde erfolgreich gesendet' ,
        ]

    ];