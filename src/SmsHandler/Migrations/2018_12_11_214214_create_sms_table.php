<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Creates SMS Table Migration
     *
     * @package   -
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CreateSmsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create( 'sms' , function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->nullableMorphs( 'receiver' );
                $table->string( 'from' );
                $table->string( 'to' );
                $table->text( 'message' );
                $table->integer( 'status' )->unsigned()->default( \DTV\SmsHandler\Models\Sms::STATUS_OPEN );

                $table->timestamps();
                $table->softDeletes();
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists( 'sms' );
        }
    }
