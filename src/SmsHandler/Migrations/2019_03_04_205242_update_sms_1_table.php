<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Updates SMS Table Migration
     *
     * @package   -
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class UpdateSms1Table extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table( 'sms' , function ( Blueprint $table ) {
                $table->string( 'reference_type' )->nullable()->after( 'status' );
                $table->integer( 'reference_id' )->nullable()->unsigned()->after( 'reference_type' );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table( 'sms' , function ( Blueprint $table ) {
                $table->dropColumn( "reference_type" , "reference_id" );
            } );
        }
    }
