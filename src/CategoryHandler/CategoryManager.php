<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\CategoryHandler;

    use DTV\CategoryHandler\Models\Category;
    use DTV\Support\Contracts\ManagerContract;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use InvalidArgumentException;

    /**
     * Category Manager
     *
     * @package   DTV\CategoryHandler
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CategoryManager implements ManagerContract
    {
        /**
         * Array of registered categories
         *
         * @var array
         */
        protected array $categories = [];

        /**
         * Registers an category model via the name of the given category
         *
         * @param string $class
         */
        public function register(string $class): void
        {
            if (!is_a($class, Category::class, true)) {
                throw new InvalidArgumentException($class . ' is not a valid category model class!');
            }

            /** @var Category $model */
            $model = new $class;
            $categoryKey = $model->getCategoryName();

            if ($this->has($categoryKey)) {
                logger()->warning('The category type ID ' . $categoryKey . ' is registered twice!');
            }

            $this->categories[$categoryKey] = $class;
        }

        /**
         * Returns if an given category key is registered
         *
         * @param string $key
         *
         * @return bool
         */
        public function has(string $key): bool
        {
            return isset($this->categories[$key]);
        }

        /**
         * Returns an category model for the given category key if such exists
         *
         * @param string $key
         *
         * @return string|null
         */
        public function get(string $key): ?string
        {
            return $this->categories[$key] ?? null;
        }

        /**
         * Returns an category model for the given category key if such exists or throws a exception if not
         *
         * @param string $key
         *
         * @throws ModelNotFoundException
         *
         * @return string
         */
        public function getOrFail($key)
        {
            $model = $this->get($key);

            if ($model === null) {
                throw new ModelNotFoundException();
            }

            return $model;
        }

        /**
         * Returns the array with all registered categories
         *
         * @return array
         */
        public function all(): array
        {
            return $this->categories;
        }
    }
