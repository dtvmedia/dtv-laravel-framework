<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\CategoryHandler\Controllers;

    use DTV\BaseHandler\Controllers\Controller;
    use DTV\BaseHandler\JsonResponse;
    use DTV\BaseHandler\Views\Page;
    use DTV\CategoryHandler\Models\Category;
    use DTV\CategoryHandler\Services\Views\CategoryForm;
    use DTV\CategoryHandler\Services\Views\CategoryOverview;
    use Illuminate\Http\Request;
    use Throwable;

    /**
     * Category Controller
     *
     * @package   DTV\CategoryHandler\Controllers
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CategoryController extends Controller
    {
        /**
         * Shows the category overview page
         *
         * @param string|int  $type
         * @param string|null $filter
         *
         * @throws Throwable
         *
         * @return Page|string
         */
        public function index( $type , $filter = null )
        {
            $categoryClass = app( 'categories' )->getOrFail( $type );
            /** @var Category $categoryModel */
            $categoryModel = new $categoryClass();

            $query = match ($filter) {
                'inactive' => $categoryModel::query()->where('status', Category::STATUS_INACTIVE),
                default    => $categoryModel::query()->where('status', Category::STATUS_ACTIVE),
            };

            if ( $categoryModel->getConfig( 'create' ) === true ) {
                $this->addHeaderButton( $categoryModel::trans( 'buttons.create' ) , 'fa-plus' , 'categories.create' , [ 'type' => $type ] );
            }

            return $this->view( new CategoryOverview( $query ) );
        }

        /**
         * Shows and handles the category create form
         *
         * @param string|int $type
         * @param Request    $request
         *
         * @throws Throwable
         *
         * @return JsonResponse|Page|string
         */
        public function create( $type , Request $request )
        {
            $categoryClass = app( 'categories' )->getOrFail( $type );
            /** @var Category $categoryModel */
            $categoryModel = new $categoryClass();

            if ( $categoryModel->getConfig( 'create' ) === false ) {
                return $this->json()->warning( 'dtv.categories::categories.messages.actionNotAvailable' );
            }

            if ( $categoryModel->getConfig( 'limit' ) !== null && $categoryModel->newQuery()->count() >= $categoryModel->getConfig( 'limit' ) ) {
                return $this->json()->warning( 'dtv.categories::categories.messages.limitReached' , [ 'category' => $categoryModel::getModelName() ] );
            }

            return $this->form( $request , new CategoryForm( $categoryModel ) , function ( Request $request ) use ( $categoryModel ) {
                if ( $categoryModel->getConfig( 'unique' ) === true && $categoryModel->newQuery()->where( 'name' , $request->name )->count() > 0 ) {
                    return $this->json()->warning( 'dtv.categories::categories.messages.nameAlreadyExists' );
                }

                $categoryModel->newQuery()->create( $request->all() );

                return $this->json()
                    ->success( 'dtv.categories::categories.messages.created' , [ 'category' => $categoryModel::getModelName() ] )
                    ->reloadListView( escapeHtmlLabel( $categoryModel::getTranslationFile() . '.views.index' ) )
                    ->hideModal();
            } );
        }

        /**
         * Shows and handles the category edit form
         *
         * @param string|int $type
         * @param int        $id
         * @param Request    $request
         *
         * @throws Throwable
         *
         * @return JsonResponse|Page|string
         */
        public function edit( $type , $id , Request $request )
        {
            $categoryClass = app( 'categories' )->getOrFail( $type );
            /** @var Category $categoryModel */
            $categoryModel = $categoryClass::findOrFail( $id );

            if ( $categoryModel->getConfig( 'edit' ) === false ) {
                return $this->json()->warning( 'dtv.categories::categories.messages.actionNotAvailable' );
            }

            return $this->form( $request , new CategoryForm( $categoryModel ) , function ( Request $request ) use ( $categoryModel ) {
                if ( $categoryModel->getConfig( 'unique' ) === true &&
                     $categoryModel->newQuery()->where( 'name' , $request->name )->where( 'id' , '!=' , $categoryModel->id )->count() > 0 ) {
                    return $this->json()->warning( 'dtv.categories::categories.messages.nameAlreadyExists' );
                }

                $categoryModel->update($request->all());

                return $this->json()
                    ->success('dtv.categories::categories.messages.edited', ['category' => $categoryModel::getModelName()])
                    ->reloadListView(escapeHtmlLabel($categoryModel::getTranslationFile() . '.views.index'))
                    ->hideModal();
            } );
        }

        /**
         * Tries to delete an category model
         *
         * @param string|int $type
         * @param int        $id
         *
         * @throws Throwable
         * @return JsonResponse
         */
        public function delete( $type , $id )
        {
            $categoryClass = app( 'categories' )->getOrFail( $type );
            /** @var Category $categoryModel */
            $categoryModel = $categoryClass::findOrFail( $id );
            $checkRelations = $categoryModel->getConfig('deleteCheckRelations');

            return $this->deleteModel($categoryModel, $checkRelations, [], null, function ($categoryModel, JsonResponse $response) {
                return $response->hideModal()
                    ->reloadListView(escapeHtmlLabel($categoryModel::getTranslationFile() . '.views.index'));
            });
        }
    }
