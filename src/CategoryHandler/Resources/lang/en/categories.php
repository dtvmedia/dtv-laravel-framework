<?php

    return [

        'name' => 'Category' ,

        'views' => [
            ''
        ] ,

        'fields' => [
            'name'        => 'Name' ,
            'description' => 'Description' ,
            'status'      => 'Status' ,
        ] ,

        'status' => [
            \DTV\CategoryHandler\Models\Category::STATUS_INACTIVE => 'Inactive' ,
            \DTV\CategoryHandler\Models\Category::STATUS_ACTIVE   => 'Active' ,
        ] ,

        'messages' => [
            'createdTitle'            => ':category was created' ,
            'createdText'             => 'The :category was successfully created' ,
            'editedTitle'             => ':category was edited' ,
            'editedText'              => 'The :category was successfully edited' ,
            'actionNotAvailableTitle' => 'Action not available' ,
            'actionNotAvailableText'  => 'Fot the choosen object this action is not available' ,
            'limitReachedTitle'       => ':category Limit reached' ,
            'limitReachedText'        => 'The :category records limit was reached. You can not create more records.' ,
            'nameAlreadyExistsTitle'  => 'Name already exists' ,
            'nameAlreadyExistsText'   => 'There already exists an record with the given name' ,
        ]

    ];