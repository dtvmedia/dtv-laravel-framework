<?php

    return [

        'name' => 'Kategorie' ,

        'fields' => [
            'name'        => 'Name' ,
            'description' => 'Beschreibung' ,
            'status'      => 'Status' ,
        ] ,

        'status' => [
            0 => 'Inaktiv' ,
            1 => 'Aktiv' ,
        ] ,

        'messages' => [
            'createdTitle'            => ':category erstellt' ,
            'createdText'             => ':category wurde erfolgreich erstellt' ,
            'editedTitle'             => ':category bearbeitet' ,
            'editedText'              => ':category wurde erfolgreich bearbeitet' ,
            'actionNotAvailableTitle' => 'Aktion nicht verfügbar' ,
            'actionNotAvailableText'  => 'Für das gewählte Objekt ist diese Aktion nicht verfügbar' ,
            'limitReachedTitle'       => ':category Limit erreicht' ,
            'limitReachedText'        => 'Das :category Limit von Datensätzen wurde erreicht. Es können keine weiteren Datensätze hinzugefügt werden' ,
            'nameAlreadyExistsTitle'  => 'Name bereits vergeben' ,
            'nameAlreadyExistsText'   => 'Es existiert bereits ein Datensatz mit diesem Namen' ,
        ] ,

    ];