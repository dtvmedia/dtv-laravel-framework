<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\CategoryHandler\Services\Views;

    use DTV\BaseHandler\Views\ListView;
    use DTV\CategoryHandler\Models\Category;
    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\Relations\Relation;

    /**
     * Category Overview Class
     *
     * @package   DTV\CategoryHandler\Services\Views
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CategoryOverview extends ListView
    {
        /**
         * Category model
         *
         * @var Category|null
         */
        protected $model;

        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.categories::categories.views.index';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-cubes';

        /**
         * Translation file used for translating column names etc
         *
         * @var string
         */
        protected $transPath = 'dtv.categories::categories';

        /**
         * Order Column
         *
         * @var string
         */
        protected $orderColumn = 'name';

        /**
         * Order Direction
         *
         * @var string
         */
        protected $orderDirection = 'asc';

        /**
         * CategoryOverview constructor.
         *
         * @param null $data
         *
         * @throws \Exception
         */
        public function __construct( $data = null )
        {
            if ( $data instanceof Model ) {
                $model = $data;
            } elseif ( $data instanceof Builder || $data instanceof Relation ) {
                $model = $data->getModel();
            } else {
                throw new \Exception( 'Invalid value for $data' );
            }

            if ( !$model instanceof Category ) {
                throw new \Exception( 'The given model isn\'t an valid category model!' );
            }

            $this->model = $model;
            $this->transPath = $model::getTranslationFile();
            $this->label = $this->transPath . '.views.index';

            parent::__construct( $data );
        }

        /**
         * Adds the columns
         */
        protected function columns()
        {
            $this->addColumn( 'name' , 'name' , 'name' );

            if ( $this->model->getConfig( 'description' ) === true ) {
                $this->addColumn( 'description' , 'getDescription' , 'description' );
            }

            $this->addColumn( 'status' , 'getStatusLabel' , 'status' )->enableHtml();

            foreach ( $this->model->fields() as $field => $fieldConfig ) {
                if ( ( isset( $fieldConfig[ 'index' ] ) && $fieldConfig[ 'index' ] !== false ) || !isset( $fieldConfig[ 'index' ] ) ) {
                    if ( is_callable( $fieldConfig[ 'index' ] ) || is_string( $fieldConfig[ 'index' ] ) ) {
                        $this->addColumn( $field , $fieldConfig[ 'index' ] )->enableHtml();
                    } else {
                        $this->addColumn( $field );
                    }
                }
            }
        }

        /**
         * Adds the buttons for each record
         */
        protected function buttons()
        {
            if ( $this->model->getConfig( 'edit' ) === true ) {
                $this->addButton( 'edit' , 'fa-pencil' , 'categories.edit' )
                    ->setParameter( [ 'type' => $this->model->getCategoryName() ] );
            }
            if ( $this->model->getConfig( 'destroy' ) === true ) {
                $this->addConfirmButton( 'delete' , 'fa-times' , 'categories.delete' )
                    ->setParameter( [ 'type' => $this->model->getCategoryName() ] );
            }
        }
    }