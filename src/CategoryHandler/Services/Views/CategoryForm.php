<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\CategoryHandler\Services\Views;

    use DTV\BaseHandler\Views\FormInputs\Textarea;
    use DTV\BaseHandler\Views\FormView;
    use DTV\CategoryHandler\Models\Category;
    use Throwable;

    /**
     * Category Form Class
     *
     * @package   DTV\CategoryHandler\Services\Views
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CategoryForm extends FormView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.categories::categories.views.create';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-plus';

        /**
         * Translation file used for translating input labels etc
         *
         * @var string
         */
        protected $transPath = 'dtv.categories::categories';

        /**
         * CategoryForm constructor.
         *
         * @param Category $model
         */
        public function __construct(Category $model)
        {
            $mode = ($model->exists) ? 'edit' : 'create';

            $this->transPath = $model::getTranslationFile();
            $this->label = $this->transPath . '.views.' . $mode;

            parent::__construct($model);
        }

        /**
         * Adds the inputs to the form
         *
         * @throws Throwable
         */
        public function inputs()
        {
            $statusOptions = [
                Category::STATUS_ACTIVE   => option( 'dtv.categories::categories.status.' . Category::STATUS_ACTIVE , 'fa-check' ) ,
                Category::STATUS_INACTIVE => option( 'dtv.categories::categories.status.' . Category::STATUS_INACTIVE , 'fa-times' ) ,
            ];

            $this->addText('name')
                ->setValidationRule('required');
            $this->addSelect('status', $statusOptions)
                ->setValidationRule('required')
                ->setDefaultValue(true);
            if ($this->model->getConfig('description') === true) {
                $this->addTextarea('description')
                    ->setHeight(Textarea::DYNAMIC_HEIGHT);
            }

            $mode = $this->isUpdateMode() ? 'edit' : 'create';
            foreach ($this->model->fields() as $field => $fieldConfig) {
                if ((isset($fieldConfig[$mode]) && $fieldConfig[$mode] === true) || !isset($fieldConfig[$mode])) {
                    $input = $this->addDynamic($fieldConfig['type'], $field, null, [], value($fieldConfig['custom']['options'] ?? []));

                    if ($input instanceof Textarea) {
                        $input->setHeight($fieldConfig['custom']['height'] ?? Textarea::DYNAMIC_HEIGHT);
                    }
                }
            }
        }
    }
