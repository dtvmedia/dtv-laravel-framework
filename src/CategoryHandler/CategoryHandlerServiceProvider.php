<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\CategoryHandler;

    use DTV\BaseHandler\BaseServiceProvider;
    use DTV\BaseHandler\RoutingHandler;
    use DTV\CategoryHandler\Controllers\CategoryController;

    /**
     * Category Handler Service Provider
     *
     * @package   DTV\CategoryHandler
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CategoryHandlerServiceProvider extends BaseServiceProvider
    {
        /**
         *  Namespace for resources of the module
         *
         * @var string
         */
        protected $resourcesNamespace = 'dtv.categories';

        /**
         * Array of commans
         *
         * @var array
         */
        protected $commands = [];

        /**
         * Config path for the module enable switch
         *
         * @var string|null
         */
        public static $config = 'dtv.modules.categories.enabled';

        /**
         * Register the service provider.
         *
         * @return void
         */
        public function register()
        {
            parent::register();

            $this->app->singleton( 'categories' , function () {
                return new CategoryManager();
            } );
        }

        /**
         * Registers the Modules routes
         *
         * @throws \Exception
         */
        public function routes()
        {
            // Category Routes
            $categoryRoutes = new RoutingHandler( CategoryController::class , 'categories/{type}' , 'categories.' , RoutingHandler::ACCESS_AUTH );
            $categoryRoutes->any( '/create' , 'create' , 'create' , 'can:categories.change' );
            $categoryRoutes->any( '/edit/{id}' , 'edit' , 'edit' , 'can:categories.change' );
            $categoryRoutes->get( '/{filter?}' , 'index' , 'index' , 'can:categories.view' );
            $categoryRoutes->delete( '/delete/{id}' , 'delete' , 'delete' , 'can:categories.change' );
        }

        /**
         * Defines all the modules gates
         */
        public function gates()
        {
            gate()->define( 'categories.view' , function ( $user ) {
                return $user->hasPermission( [ 'categories.view' ] );
            } );
            gate()->define( 'categories.change' , function ( $user ) {
                return $user->hasPermission( [ 'categories.view' , 'categories.change' ] );
            } );
        }
    }