<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace  DTV\CategoryHandler\Models;

    use DTV\BaseHandler\Models\Model;

    /**
     * Category Field Model
     *
     * @package   DTV\CategoryHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CategoryField extends Model
    {
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'category_id' , 'name' , 'value'
        ];
    }