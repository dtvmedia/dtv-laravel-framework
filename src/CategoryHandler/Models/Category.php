<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\CategoryHandler\Models;

    use DTV\BaseHandler\Models\Model;
    use DTV\CategoryHandler\Models\Builders\CategoryBuilder;
    use Illuminate\Contracts\Support\Htmlable;
    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Database\Eloquent\Relations\HasMany;

    /**
     * Base Category Model
     *
     * @package   DTV\CategoryHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    abstract class Category extends Model
    {
        /**
         * Status constants
         */
        const STATUS_INACTIVE = 0;
        const STATUS_ACTIVE = 1;

        /**
         * The table associated with the model.
         *
         * @var string
         */
        protected $table = 'categories';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'category_type' , 'name' , 'description' , 'status'
        ];

        /**
         * Default Config array
         *
         * @var array
         */
        private array $defaultConfig = [
            'create'               => true ,
            'edit'                 => true ,
            'destroy'              => true ,
            'description'          => true ,
            'deleteCheckRelations' => [] ,
            'limit'                => null ,
            'unique'               => true ,
        ];

        /**
         * Config array for enabling/disabling the available actions/fields for this category model
         *
         * @var array
         */
        protected array $config = [];

        /**
         * The relations to eager load on every query.
         *
         * @var array
         */
        protected $with = [ 'categoryFields' ];

        /**
         * The name of the related translation file
         * If null the getTranslationFile method will try to build it from the model name
         *
         * @var string
         */
        protected static $translationFile = 'dtv.categories::categories';

        /**
         * Category type ID
         *
         * @var int
         */
        protected static $categoryType = null;

        /**
         * Category name for urls
         *
         * @var string
         */
        protected static $categoryName = null;

        /**
         * Category constructor
         *
         * @param array $attributes
         */
        public function __construct( array $attributes = [] )
        {
            // add automatically the category field keys to the fillable array
            $this->fillable = array_merge( $this->fillable , array_keys( $this->fields() ) );

            parent::__construct( $attributes );
        }

        /**
         * The "booting" method of the model.
         *
         * @return void
         */
        protected static function boot()
        {
            parent::boot();

            static::addGlobalScope( 'category' , function ( Builder $builder ) {
                $builder->where( 'category_type' , static::$categoryType );
            } );
        }

        /**
         * Returns all related category fields
         *
         * @return HasMany
         */
        public function categoryFields()
        {
            return $this->hasMany( CategoryField::class , 'category_id' )->withTrashed();
        }

        /**
         * Returns the category type id
         *
         * @return int
         */
        public function getCategoryType()
        {
            return static::$categoryType;
        }

        /**
         * Returns the category name
         *
         * @return string
         */
        public function getCategoryName()
        {
            return static::$categoryName ?? lcfirst( class_basename( static::class ) ) . 's';
        }

        /**
         * Should define the category fields in the derived models
         *
         * @return array
         */
        public function fields()
        {
            return [];
        }

        /**
         * Create a new Eloquent query builder for the model.
         *
         * @param \Illuminate\Database\Query\Builder $query
         *
         * @return CategoryBuilder
         */
        public function newEloquentBuilder( $query )
        {
            return new CategoryBuilder( $query );
        }

        /**
         * Magic get method which publishes the additional category fields as pseudo attributes
         *
         * @param string $key
         *
         * @return mixed
         */
        public function __get( $key )
        {
            $fields = $this->fields();

            // publishes the additional category fields as pseudo attributes
            if ( isset( $this->relations[ 'categoryFields' ] ) && isset( $fields[ $key ] ) ) {
                $fieldAttr = $this->relations[ 'categoryFields' ]->first( function ( $item ) use ( $key ) {
                    return $item->name == $key;
                } );

                if ( $fieldAttr instanceof CategoryField ) {
                    $value = $fieldAttr->value;
                } else {
                    $value = $fields[ $key ][ 'default' ] ?? null;
                }

                if ( !is_array( $fields[ $key ][ 'type' ] ) ) {
                    $value = cast( $fields[ $key ][ 'type' ] , $value );
                }

                // execute custom get mutator if available
                if ( isset( $fields[ $key ][ 'get_mutator' ] ) && is_callable( $fields[ $key ][ 'get_mutator' ] ) ) {
                    return $fields[ $key ][ 'get_mutator' ]( $value );
                }

                return $value;

            }

            return parent::__get( $key );
        }

        /**
         * Magic Isset Method which marks the addtional category fields as set
         *
         * @param string $key
         *
         * @return bool
         */
        public function __isset( $key )
        {
            // mark addtional category fields as set
            if ( array_key_exists( $key , $this->fields() ) ) {
                return true;
            }

            return parent::__isset( $key );
        }

        /**
         * Returns an category config value or default if no such exists
         *
         * @param string $config
         * @param mixed  $default
         *
         * @return bool|mixed
         */
        public function getConfig( string $config , $default = null )
        {
            if ( !isset( $this->config[ $config ] ) ) {
                return $default ?? $this->defaultConfig[ $config ];
            }

            return $this->config[ $config ];
        }

        /**
         * Returns the status as string
         *
         * @return string
         */
        public function getStatus()
        {
            return trans( 'dtv.categories::categories.status.' . $this->status );
        }

        /**
         * Returns an html status label
         *
         * @return Htmlable
         */
        public function getStatusLabel()
        {
            return match ($this->status) {
                self::STATUS_INACTIVE => badge(icon('fa-times', $this->getStatus())->showLabel(), 'danger'),
                self::STATUS_ACTIVE   => badge(icon('fa-check', $this->getStatus())->showLabel(), 'success'),
                default               => badge($this->getStatus(), 'default'),
            };
        }

        /**
         * Returns the category description or default if no such exists
         *
         * @param string $default
         *
         * @return string
         */
        public function getDescription( $default = '-' )
        {
            if ( empty( $this->description ) ) {
                return $default;
            }

            return str_excerpt( $this->description , 100 );
        }

        /**
         * Returns the Array of Select Options from a Builder/Model
         *
         * @param null          $builder  Builder instance
         * @param bool|mixed    $noOption If $noOption is something else then false then the value is used as key for the no option
         *                                If true is given then 0 will be used as key
         * @param callable|null $callable $callable Added callback function for sorting the query results
         *
         * @return array
         */
        public static function getSelectOptions( $builder = null , $noOption = false , callable $callable = null )
        {
            if ( $builder == null ) {
                $builder = static::query()->where( 'status' , self::STATUS_ACTIVE );
            }

            return parent::getSelectOptions( $builder , $noOption , $callable );
        }
    }
