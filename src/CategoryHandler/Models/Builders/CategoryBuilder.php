<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\CategoryHandler\Models\Builders;

    use Closure;
    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Database\Query\Builder as QueryBuilder;
    use DTV\CategoryHandler\Models\CategoryField;

    /**
     * Custom Category Builder
     *
     * @package   DTV\CategoryHandler\Models\Builders
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CategoryBuilder extends Builder
    {
        /**
         * Flag which indicates if the join was already run for this builder
         *
         * @var bool
         */
        protected $alreadyJoined = false;

        /**
         * Save a new model and return the instance.
         *
         * @param array $attributes
         *
         * @return \Illuminate\Database\Eloquent\Model|$this
         */
        public function create( array $attributes = [] )
        {
            // split attributes
            list( $basicAttributes , $attributes ) = $this->splitAttributes( $attributes );

            // create category
            $newModel = tap( $this->newModelInstance( $basicAttributes ) , function ( $instance ) {
                $instance->save();
            } );

            // create category fields
            foreach ( $this->model->fields() as $key => $value ) {
                $mutator = null;
                if ( isset( $value[ 'set_mutator' ] ) ) {
                    $mutator = $value[ 'set_mutator' ];
                }

                CategoryField::query()->create( [
                    'category_id' => $newModel->id ,
                    'name'        => $key ,
                    'value'       => array_key_exists( $key , $attributes ) ? with( $attributes[ $key ] , $mutator ) : '' ,
                ] );
            }

            return $newModel;
        }

        /**
         * Update a record in the database.
         *
         * @param array $values
         *
         * @return int
         */
        public function update( array $values )
        {
            // split attributes
            list( $basicAttributes , $attributes ) = $this->splitAttributes( $values );

            // update category
            $updated = $this->toBase()->update( $this->addUpdatedAtColumn( $basicAttributes ) );

            // update category fields
            foreach ( $this->model->fields() as $key => $value ) {
                if ( !isset( $attributes[ $key ] ) ) {
                    continue;
                }

                $mutator = null;
                if ( isset( $value[ 'set_mutator' ] ) ) {
                    $mutator = $value[ 'set_mutator' ];
                }

                $field = $this->model->categoryFields->where( 'name' , $key )->first();
                if ( $field === null ) {
                    CategoryField::query()->create( [
                        'category_id' => $this->model->id ,
                        'name'        => $key ,
                        'value'       => array_key_exists( $key , $attributes ) ? with( $attributes[ $key ] , $mutator ) : ''
                    ] );
                } else {
                    $field->update( [
                        'value' => array_key_exists( $key , $attributes ) ? with( $attributes[ $key ] , $mutator ) : ''
                    ] );
                }
            }

            return $updated;
        }

        /**
         * Add a basic where clause to the query.
         *
         * @param string|array|Closure $column
         * @param string               $operator
         * @param mixed                $value
         * @param string               $boolean
         *
         * @return $this|Builder
         */
        public function where( $column , $operator = null , $value = null , $boolean = 'and' )
        {
            // if we want to filter a "normal" column we still use the default where
            if ( !$column instanceof Closure && !array_key_exists( $column , $this->model->fields() ) ) {
                return parent::where( $this->prefixWithTable( $column ) , $operator , $value , $boolean );
            }

            $this->applyFieldsJoin();

            // if we have a closure given we still use the default where
            if ( $column instanceof Closure ) {
                return parent::where( $column , $operator , $value , $boolean );
            }

            // the where logic for the fields
            $this->query->where( function ( QueryBuilder $builder ) use ( $column , $value , $operator ) {
                $builder->where( 'category_fields.name' , '=' , $column );
                $builder->where( 'category_fields.value' , '=' , $value ?? $operator );
            } , null , null , $boolean );

            return $this;
        }

        /**
         * Add a "where in" clause to the query.
         *
         * @param string $column
         * @param mixed  $values
         * @param string $boolean
         * @param bool   $not
         *
         * @return $this|Builder
         */
        public function whereIn( $column , $values , $boolean = 'and' , $not = false )
        {
            // if we want to filter a "normal" column we still use the default whereIn
            if ( !array_key_exists( $column , $this->model->fields() ) ) {
                return parent::whereIn( $this->prefixWithTable( $column ) , $values , $boolean , $not );
            }

            $this->applyFieldsJoin();

            // the whereIn logic for the fields
            $this->query->where( function ( QueryBuilder $builder ) use ( $column , $values , $not ) {
                $builder->where( 'category_fields.name' , '=' , $column );
                $builder->whereIn( 'category_fields.value' , $values , 'and' , $not );
            } , null , null , $boolean );

            return $this;
        }

        /**
         * Prefixes an given column with the given table if necessary
         *
         * @param string $column
         * @param string $table
         *
         * @return string
         */
        protected function prefixWithTable( string $column , string $table = 'categories' )
        {
            if ( !str_contains( $column , '.' ) ) {
                return $table . '.' . $column;
            }

            return $column;
        }

        /**
         * Applies the join to the category fields table
         *
         * @return $this
         */
        protected function applyFieldsJoin()
        {
            // Here we ensure that the join and the select constraints are not called multiple times
            // But these still needs to be called in front of any closure scopes
            if ( $this->alreadyJoined === false ) {
                $this->select( 'categories.*' );
                $this->join( 'category_fields' , 'category_fields.category_id' , '=' , 'categories.id' );

                $this->alreadyJoined = true;
            }

            return $this;
        }

        /**
         * splits the attributes in base attributes and category fields attributes
         *
         * @param array $attributes
         *
         * @return array
         */
        protected function splitAttributes( array $attributes )
        {
            $basicAttributes[ 'category_type' ] = $this->model->getCategoryType();

            foreach ( [ 'name' , 'description' , 'status' , 'created_at' , 'updated_at' , 'deleted_at' ] as $column ) {
                if ( array_key_exists( $column , $attributes ) ) {
                    $basicAttributes[ $column ] = $attributes[ $column ];
                    unset( $attributes[ $column ] );
                }
            }

            return [
                $basicAttributes ,
                $attributes
            ];
        }
    }