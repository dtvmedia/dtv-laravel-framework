# CategoryHandler - DTV Laravel Framework

## Configs
- *enabled*: Package activation switch
- *routes_enabled*: Activation switch for the package routes seperated from the package switch itself

## Usage
1. Extend an model from the category class
2. The model needs an category type id ```protected $categoryType = 1;```
3. You need to register that category model
```app( 'categories' )->register( NewCategoryModel::class );```
4. The model needs an category name for the URL. This name is by default taken from the model name. In case you want to overwrite it us the category name property: ```protected $categoryName = 'categories';``` *(optional)*
5. You can define via the fields method additional category fields *(optional)*
6. You can enable/disable different actions/fields via the config array of the model *(optional)*
````php
        protected $config = [
            'create'               => true , // defines if objects of this model can be created
            'edit'                 => true , // defines if objects of this model can be edited
            'destroy'              => true , // defines if objects of this model can be deleted
            'description'          => true , // defines if the description field will be used for this model
            'deleteCheckRelations' => [] ,   // defines the relations which will be checked when deleting an model
            'limit'                => null , // defines the maximal allowed number of records of this model (null means no limitation)
            'unique'               => true , // defines if the object name needs to be unique within the model scope or not
        ];
````


### Category field definition
Custom category field will be defined via the fields method which can be overwritten from the base category model.
There you are supposed to define an array with category fields settings which gives you the following settings:

Option | Expected Datatype | Description 
--- | --- | --- 
type | string | Defines the category field datatype (see below) *(required)*
index | bool, string, callable *(default: false)* | Handles if the field will be displayed on the index page. False = doesn't show on index page, String and callables will be given to the $datapath parameter of the listview and can customize the displayment on the index page *(optional)*
create | bool *(default: false)* | Flag which handels if the category field will be displayed on the create form *(optional)*
edit | bool *(default: false)* |  Flag which handels if the category field will be displayed on the edit form *(optional)*
set_mutator | callable *(default: null)* | Function which can transform the input value before saving the value in the database *(optional)*
get_mutator | callable *(default: null)* | Function which can transform the database value during getter accesses *(optional)*
default | mixed *(default: null)* | Default value which will be used if no such value is defined in the database *(optional)*
custom| array *(default: null)* | More type dependant settings. *(optional)*
custom['height']| int *(default: null)* | Sets the height for textareas of text fields (only for type "text") *(optional)*
custom['options']| mixed *(default: null)* | Sets the options for some datatypes (only for the types "array", "model", "builder", "file"), this can also be a closure which returns the corresponding data object *(sometimes required)*

**Example**
````php
    /**
     * Defines the category fields
     *
     * @return array
     */
    public function fields()
    {
        return [
            'color' => [
                'type'        => 'color' ,
                'index'       => 'getColor' ,
                'create'      => true ,
                'edit'        => true ,
                'set_mutator' => null ,
                'get_mutator' => null ,
                'default'     => '#000000'
            ] ,
            'other_description' => [
                'type'        => 'text' ,
                'index'       => true ,
                'create'      => true ,
                'edit'        => true ,
                'set_mutator' => null ,
                'get_mutator' => null ,
                'default'     => '-' ,
                'custom'      => [
                    'height' => 150 ,
                ]
            ] ,
            'type' => [
                'type'        => 'array' ,
                'index'       => false ,
                'create'      => true ,
                'edit'        => true ,
                'set_mutator' => null ,
                'get_mutator' => null ,
                'custom'      => [
                    'options' => function() {
                        return [
                            'ubuntu'  => [ 'value' => 'Ubuntu' , 'icon' => 'fab fa-ubuntu fa-fw' ] ,
                            'windows' => [ 'value' => 'Windows' , 'icon' => 'fab fa-windows fa-fw' ] ,
                        ] ,
                    } ,
                ]
            ] ,
        ];
    }
````

### Available category field datatypes
Datatype | Realization 
--- | --- 
*string* | Simple text input with no special requirement
*text* | Renders as textarea
*int* | Text input where the input needs to be an valid integer
*bool* | Select input with yes and no options
*float* | Text input where the input needs to be an valid float or integer 
*percent* | Same as int with the inteval requirement 0 >= input >= 100 
*date* | Date input (browser native)
*time* | Time input (browser native)
*color* | Color input (browser native)
*password* | Password input _(Note this type is not officially supported for now)_
*file* | File input _(Note this type is not officially supported for now)_
*array* | You can pass an array with select options (with the custom options field) in order to realize an select (or the same as closure)
*model* | You can pass an model FQCN (with the custom options field) in order to realize an select with the select option from the specified model (or the same as closure)
*builder* | You can pass an builder instance (with the custom options field) in order to realize an select with the select option from the model of the specified builder. Furthermore the builder instance will be used as query base for the getSelectOptions function (or the same as closure).