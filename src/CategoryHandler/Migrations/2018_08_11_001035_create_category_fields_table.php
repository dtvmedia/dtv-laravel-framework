<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Creates Category fields Table Migration
     *
     * @package   -
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CreateCategoryFieldsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create( 'category_fields' , function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->integer( 'category_id' )->unsigned();
                $table->string( 'name' );
                $table->text( 'value' )->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->unique( [ 'category_id' , 'name' ] );
            } );

            Schema::table( 'category_fields' , function ( $table ) {
                $table->foreign( 'category_id' )->references( 'id' )->on( 'categories' );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists( 'category_fields' );
        }
    }