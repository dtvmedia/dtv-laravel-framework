<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Updates Category Fields Table Migration
     *
     * @package   -
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class UpdateCategoryFields2Table extends Migration
    {
        /**
         * Table name
         *
         * @var string
         */
        protected $table = 'category_fields';

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table( 'category_fields' , function ( Blueprint $table ) {
                DB::statement( 'ALTER TABLE `'.$this->table.'` CHANGE `value` `value` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;' );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table( 'category_fields' , function ( Blueprint $table ) {
                DB::statement( 'ALTER TABLE `'.$this->table.'` CHANGE `value` `value` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;' );
            } );
        }
    }