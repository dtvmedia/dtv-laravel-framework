<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Creates Categories Table Migration
     *
     * @package   -
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CreateCategoriesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create( 'categories' , function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->integer( 'category_type' )->unsigned();
                $table->string( 'name' );
                $table->string( 'description' )->nullable();
                $table->boolean( 'status' );
                $table->timestamps();
                $table->softDeletes();

                $table->index( [ 'category_type' ] );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists( 'categories' );
        }
    }