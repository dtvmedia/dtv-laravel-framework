<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Updates Categories Table Migration
     *
     * @package   -
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class UpdateCategories1Table extends Migration
    {
        /**
         * Table name
         *
         * @var string
         */
        protected $table = 'categories';

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table( $this->table , function ( Blueprint $table ) {
                DB::statement( 'ALTER TABLE `' . $this->table . '` CHANGE `description` `description` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;' );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table( $this->table , function ( Blueprint $table ) {
                DB::statement( 'ALTER TABLE `' . $this->table . '` CHANGE `description` `description` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;' );
            } );
        }
    }