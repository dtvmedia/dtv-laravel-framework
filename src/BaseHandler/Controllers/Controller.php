<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Controllers;

    use DTV\BaseHandler\Exceptions\CannotDeleteException;
    use DTV\BaseHandler\JsonResponse;
    use DTV\BaseHandler\Views\BaseFormView;
    use DTV\BaseHandler\Views\Components\Button;
    use DTV\BaseHandler\Views\Components\ConfirmButton;
    use DTV\BaseHandler\Views\CustomFormView;
    use DTV\BaseHandler\Views\FormView;
    use DTV\BaseHandler\Views\ListView;
    use DTV\BaseHandler\Views\Page;
    use DTV\BaseHandler\Views\View;
    use Closure;
    use Exception;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Http\Request;
    use Illuminate\Http\Response;
    use Illuminate\Routing\Controller as BaseController;
    use Illuminate\Foundation\Validation\ValidatesRequests;
    use InvalidArgumentException;
    use Throwable;

    /**
     * Base Controller of the Application
     *
     * @package   DTV\BaseHandler\Controllers
     * @copyright 2017 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Controller extends BaseController
    {
        use ValidatesRequests;

        /**
         * Header Button Array
         *
         * @var Button[]
         */
        private array $headerButtons = [];

        /**
         * Adds an Button to the page header
         *
         * @param string $label
         * @param string $icon
         * @param string $route
         * @param array  $parameter
         * @param bool   $confirm
         *
         * @throws Exception
         *
         * @return Button|ConfirmButton
         */
        public function addHeaderButton( $label , $icon , $route , $parameter = [] , $confirm = false )
        {
            if ( $confirm ) {
                $button = confirmButton( $label , $route , $parameter , $icon );
            } else {
                $button = button( $label , $route , $parameter , $icon )->useAjax();
            }

            $this->headerButtons[] = $button->setType( config( 'dtv.app.button_type' ) )->setSize( 'md' );

            return $button;
        }

        /**
         * Returns an new JsonResponse Object
         *
         * @return JsonResponse
         */
        public function json()
        {
            return new JsonResponse();
        }

        /**
         * Renders a FormBuilder Instance and handles the POST request
         *
         * @param Request      $request
         * @param BaseFormView $form
         * @param Closure      $closure
         *
         * @throws Throwable
         *
         * @return JsonResponse|Page|string
         */
        public function form( Request $request , BaseFormView $form , Closure $closure )
        {
            // Show the form
            if ( $request->method() == 'GET' ) {
                app( 'hooks' )->execute( 'headerButtons' , $this );

                if ( $form instanceof FormView ) {
                    return $this->showFormView( $form );
                } elseif ( $form instanceof CustomFormView ) {
                    return $this->showCustomFormView( $form );
                }

                throw new InvalidArgumentException( 'Invalid form object given' );
            }

            // POST Handling
            // Check if accessed via ajax when needed
            if ( $form->ajax() && !$request->ajax() ) {
                throw new Exception( 'Access via Ajax only' );
            }

            // Run request mutator & validate form
            $this->runPreValidationRequestMutator( $form , $request );
            $this->validateRequest( $form , $request );
            $this->runPostValidationRequestMutator( $form , $request );

            // Execute callback function and return its return value
            return $closure( $request );
        }

        /**
         * Shows the given form view
         *
         * @param FormView $form
         *
         * @throws Throwable
         * @return JsonResponse
         */
        private function showFormView( FormView $form )
        {
            return $this->json()
                ->modal(
                    $form->setLayout('dtv.base::layouts.form_modal_layout')->render(),
                    $form->getPageModalSize()
                );
        }

        /**
         * Shows the given custom form view
         *
         * @param CustomFormView $form
         *
         * @throws Exception
         * @return Page
         */
        private function showCustomFormView( CustomFormView $form )
        {
            $parameters = array_merge($form->getParameters(true), [
                'headerButtons' => $this->headerButtons,
            ]);

            return Page::create($form->getView(), $parameters)
                ->setMode($form->getPageMode(), $form->getPageModalSize());
        }

        /**
         * Validates the form request
         *
         * @param BaseFormView $form
         * @param Request      $request
         *
         * @throws Exception
         */
        private function validateRequest( BaseFormView $form , Request $request )
        {
            $only = null;
            if ( $form instanceof CustomFormView && $form->isUpdateMode() ) {
                $only = $request->keys();
            }

            $form->validate( $only );
        }

        /**
         * Runs the pre validation request mutators of all inputs of the form
         *
         * @param BaseFormView $form
         * @param Request      $request
         *
         * @throws Exception
         */
        private function runPreValidationRequestMutator( BaseFormView $form , Request $request )
        {
            foreach ( $form->getInputs() as $input ) {
                $input->preValidationSetMutator( $request );
            }
        }

        /**
         * Runs the post validation request mutators of all inputs of the form
         *
         * @param BaseFormView $form
         * @param Request      $request
         *
         * @throws Exception
         */
        private function runPostValidationRequestMutator( BaseFormView $form , Request $request )
        {
            foreach ( $form->getInputs() as $input ) {
                $input->postValidationSetMutator( $request );
            }
        }

        /**
         * Renders the given view as full page
         *
         * @param string|View $view
         * @param array       $data
         * @param string|null $layout
         *
         * @throws Throwable
         * @return Page|string
         */
        public function view( $view , $data = [] , $layout = null )
        {
            return $this->render( $view , $data , $layout);
        }

        /**
         * Renders the given view as modal
         *
         * @param string|View $view
         * @param array       $data
         * @param string      $size
         * @param string|null $layout
         *
         * @throws Throwable
         * @return JsonResponse|Page|Response|string
         */
        public function modal( $view , $data = [] , $size = 'modal-xl' , $layout = null )
        {
            return $this->render( $view , $data , $layout , Page::MODE_MODAL , $size );
        }

        /**
         * Renders the given view
         *
         * @param string|View $view
         * @param array       $data
         * @param string|null $layout
         * @param int         $mode
         * @param string|null $size
         *
         * @throws Throwable
         *
         * @return Page|Response|string|JsonResponse
         */
        private function render( $view , $data = [] , $layout = null , $mode = Page::MODE_PAGE , $size = null )
        {
            app( 'hooks' )->execute( 'headerButtons' , $this );

            if ( $layout === null ) {
                // load layout from config
                $layoutMode = ( $mode == Page::MODE_MODAL ) ? 'modal' : 'page';
                $layout = config( 'dtv.layouts.' . $layoutMode . '_layout' );
            }

            $data = array_merge( $data , [
                'headerButtons' => $this->headerButtons ,
                'view'          => $view ,
                'layout'        => $layout ,
            ] );

            // Single View Output
            if ( $view instanceof View ) {
                $request = request();

                if ( $view instanceof ListView && $view->hasUpdateRequested( $request ) ) {
                    return $view->toResponse( $request );
                }

                if ( $mode === Page::MODE_MODAL ) {
                    return $this->json()->modal( view( config( 'dtv.layouts.single_view_layout' ) , $data )->render() , $size );
                }

                return view( config( 'dtv.layouts.single_view_layout' ) , $data )->render();
            }

            // Page Builder
            return Page::create( $view , $data )->setMode( $mode , $size );
        }

        /**
         * Tries to delete an given model
         *
         * @param Model        $model           Model instance
         * @param array|string $checkRelations  Relation(s) which should be checked before. If there are any related objects the model can't be delete
         * @param array|string $deleteRelations Relation(s) which should be deleted too
         * @param Closure|null $customCheck     Custom optional check function. Should return true if the model can be deleted or false if not
         * @param Closure|null $successCallback Optional callback function which will be called if the deletion was successful
         *
         * @return JsonResponse
         */
        public function deleteModel(Model $model, array|string $checkRelations = [], array|string $deleteRelations = [], ?Closure $customCheck = null, ?Closure $successCallback = null)
        {
            $checkRelations = array_wrap( $checkRelations );
            $deleteRelations = array_wrap( $deleteRelations );

            if ( $model instanceof \DTV\BaseHandler\Models\Model ) {
                $objName = $model::getModelName();
            } else {
                $objName = class_basename( get_class( $model ) );
            }

            try {
                // Check the given relations. If there are any the model can't be deleted yet
                foreach ( $checkRelations as $checkRelation ) {
                    if ( isset( $model->$checkRelation ) && !$model->$checkRelation->isEmpty() ) {
                        throw new CannotDeleteException();
                    }
                }

                // if there is an custom check function define this function will also be checked
                if ( $customCheck !== null && is_callable( $customCheck ) ) {
                    if ( $customCheck( $model ) !== true ) {
                        throw new CannotDeleteException();
                    }
                }

                // Try to delete the object
                if ( !$model->delete() ) {
                    throw new CannotDeleteException();
                }
            } catch (CannotDeleteException) {
                // Something went wrong
                return $this->json()->hideModal()->warning('dtv.base::general.messages.deleteWarning', ['object' => $objName]);
            }

            // The model was successful delete. Now some related objects can be deleted aswell
            foreach ( $deleteRelations as $relation ) {
                if ( $model->$relation != null ) {
                    $model->$relation()->delete();
                }
            }

            $defaultResponse = $this->json()->success( 'dtv.base::general.messages.deleteSuccess' , [ 'object' => $objName ] )->reload();

            // if there is an success callback then execute it here
            if ( $successCallback !== null && is_callable( $successCallback ) ) {
                $response = $successCallback( $model , $defaultResponse );

                if ($response instanceof JsonResponse ) {
                    return $response;
                }
            }

            return $defaultResponse;
        }
    }
