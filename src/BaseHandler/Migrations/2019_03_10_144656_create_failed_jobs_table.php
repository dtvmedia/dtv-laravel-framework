<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Creates failed jobs Table Migration
     *
     * @package   -
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CreateFailedJobsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create( 'failed_jobs' , function ( Blueprint $table ) {
                $table->bigIncrements( 'id' );
                $table->text( 'connection' );
                $table->text( 'queue' );
                $table->longText( 'payload' );
                $table->longText( 'exception' );
                $table->timestamp( 'failed_at' )->useCurrent();
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists( 'failed_jobs' );
        }
    }
