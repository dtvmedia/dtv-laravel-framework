<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Creates jobs Table Migration
     *
     * @package   -
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CreateJobsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create( 'jobs' , function ( Blueprint $table ) {
                $table->bigIncrements( 'id' );
                $table->string( 'queue' )->index();
                $table->longText( 'payload' );
                $table->unsignedTinyInteger( 'attempts' );
                $table->unsignedInteger( 'reserved_at' )->nullable();
                $table->unsignedInteger( 'available_at' );
                $table->unsignedInteger( 'created_at' );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists( 'jobs' );
        }
    }
