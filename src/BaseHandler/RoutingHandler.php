<?php

/**
 * DTV Media Solutions
 *
 * PHP Version 7.0
 */

namespace DTV\BaseHandler;

use Closure;
use DTV\BaseHandler\Controllers\Controller;
use DTV\BaseHandler\Enums\HttpMethod;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use ReflectionClass;

/**
 * Routing Handler
 *
 * @package   DTV\BaseHandler
 * @copyright 2017 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 *
 * @method \Illuminate\Routing\Route get( $url , $function , $name , $middleware = [] , $access = null )
 * @method \Illuminate\Routing\Route post( $url , $function , $name , $middleware = [] , $access = null )
 * @method \Illuminate\Routing\Route any( $url , $function , $name , $middleware = [] , $access = null )
 * @method \Illuminate\Routing\Route put( $url , $function , $name , $middleware = [] , $access = null )
 * @method \Illuminate\Routing\Route delete( $url , $function , $name , $middleware = [] , $access = null )
 */
class RoutingHandler
{
    /**
     * Available Access Modes
     */
    public const ACCESS_ALL = 0;
    public const ACCESS_AUTH = 1;
    public const ACCESS_GUEST = 2;

    /**
     * Controller
     *
     * @var ReflectionClass
     */
    protected ReflectionClass $controller;

    /**
     * URL prefix
     *
     * @var string
     */
    protected string $prefix;

    /**
     * Name prefix for the routes
     *
     * @var string
     */
    protected string $name;

    /**
     * Access mode for the controller routes
     *
     * @var int
     */
    protected int $access;

    /**
     * Creates a new instance and sets the basic parameters
     *
     * @param string|Controller $controller Controller
     * @param string $prefix     URL prefix
     * @param string $name       Name prefix
     * @param int    $access     Access mode for the controller routes
     *
     * @throws Exception
     */
    public function __construct(Controller|string $controller, string $prefix = '', string $name = '', int $access = self::ACCESS_AUTH)
    {
        $this->controller = new ReflectionClass($controller);
        $this->prefix = $prefix;
        $this->name = !empty($name) ? Str::finish($name, '.') : '';
        $this->access = $access;
    }

    /**
     * Adds the basic routes
     *
     * @param bool      $delete Add delete routes
     * @param bool      $show   Add show detail route
     * @param bool|null $store  Add extra store route
     * @param bool|null $update Add extra update route
     *
     * @throws Exception
     */
    public function addBasicRoutes(bool $delete = true, bool $show = true, ?bool $store = false, ?bool $update = false)
    {
        Route::group([
            'middleware' => $this->getMiddleware(),
            'prefix'     => $this->prefix,
            'as'         => $this->name,
            'namespace'  => $this->controller->getNamespaceName(),
        ], function () use ($delete, $show, $store, $update) {
            // Index
            Route::get('/', ['uses' => $this->controller->getShortName() . '@index', 'as' => 'index']);

            // Create & Store
            if (!is_null($store)) {
                if ($store) {
                    Route::get('/create', ['uses' => $this->controller->getShortName() . '@create', 'as' => 'create']);
                    Route::post('/store', ['uses' => $this->controller->getShortName() . '@store', 'as' => 'store']);
                } else {
                    Route::any('/create', ['uses' => $this->controller->getShortName() . '@create', 'as' => 'create']);
                }
            }

            // Show
            if ($show) {
                Route::get('/{id}', ['uses' => $this->controller->getShortName() . '@show', 'as' => 'show']);
            }

            // Edit & Update
            if (!is_null($update)) {
                if ($update) {
                    Route::get('/{id}/edit', ['uses' => $this->controller->getShortName() . '@edit', 'as' => 'edit']);
                    Route::post('/{id}/update', ['uses' => $this->controller->getShortName() . '@update', 'as' => 'update']);
                } else {
                    Route::any('/{id}/edit', ['uses' => $this->controller->getShortName() . '@edit', 'as' => 'edit']);
                }
            }

            // Delete
            if ($delete) {
                Route::get('/{id}/destroy', ['uses' => $this->controller->getShortName() . '@destroy', 'as' => 'destroy']);
            }
        });
    }

    /**
     * Sets the name prefix for the routes
     *
     * @param string $name name prefix
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Sets the access mode for the controller routes
     *
     * @param int $access Access mode for the controller routes
     */
    public function setAccess(int $access)
    {
        $this->access = $access;
    }

    /**
     * Registers a new route
     *
     * @param string                  $url        Route URL
     * @param string|Closure|callable $function   Method of the controller
     * @param string                  $name       Name of the route
     * @param HttpMethod              $type       HTTP method
     * @param array                   $middleware More Middleware
     * @param int|null                $access     Access mode for the route
     *
     * @throws Exception
     * @return \Illuminate\Routing\Route
     */
    protected function addRoute(
        string $url,
        string|Closure|callable $function,
        string $name,
        HttpMethod $type = HttpMethod::GET,
        array $middleware = [],
        ?int $access = null
    ) {
        return Route::{$type->value}(
            $this->getUrl($url),
            $this->getFunction($function)
        )
            ->name($this->getName($name))
            ->middleware($this->getMiddleware($access, $middleware));
    }

    /**
     * Builds the full url with the given url postfix
     *
     * @param string $url
     *
     * @return string
     */
    private function getUrl(string $url): string
    {
        return rtrim($this->prefix, '/') . str_start($url, '/');
    }

    /**
     * Builds the full function call string with the given controller function name
     *
     * @param string|Closure|callable $function
     *
     * @return string|Closure|callable
     */
    private function getFunction(string|Closure|callable $function): string|Closure|callable
    {
        if (!is_string($function)) {
            return $function;
        }

        return str_finish($this->controller->getNamespaceName(), '\\') . $this->controller->getShortName() . '@' . $function;
    }

    /**
     * Builds the full route name with the given name postfix
     *
     * @param string $name
     *
     * @return string
     */
    private function getName(string $name): string
    {
        return Str::of($this->name)
            ->finish('.')
            ->append($name)
            ->ltrim('.')
            ->toString();
    }

    /**
     * Returns the corresponding middlewares for the set mode
     *
     * @param int|null $access     Access mode for a route
     * @param array    $middleware More middlewares
     *
     * @return array
     */
    protected function getMiddleware(?int $access = null, array $middleware = [])
    {
        if ($access === null) {
            $access = $this->access;
        }

        $baseMiddleware = match ($access) {
            self::ACCESS_ALL   => ['web'],
            self::ACCESS_GUEST => ['web', 'guest'],
            self::ACCESS_AUTH  => ['web', 'auth'],
            default            => [],
        };

        return array_merge($baseMiddleware, $middleware);
    }

    /**
     * Manages the access to the addRoute method via shortcuts
     *
     * @param string $type
     * @param array  $arguments
     *
     * @throws Exception
     * @return \Illuminate\Routing\Route
     */
    public function __call($type, $arguments)
    {
        $type = HttpMethod::from(Str::upper($type));

        if (count($arguments) < 3) {
            throw new Exception('Too less parameters for the addRoute method!');
        }

        return $this->addRoute(
            url: $arguments[0],
            function: $arguments[1],
            name: $arguments[2],
            type: $type,
            middleware: Arr::wrap($arguments[3] ?? []),
            access: $arguments[4] ?? null
        );
    }
}
