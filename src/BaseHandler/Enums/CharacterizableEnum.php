<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Enums;

    /**
     * Characterizable Enumeration Class
     *
     * @package   DTV\BaseHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CharacterizableEnum
    {
        /**
         * Sex constants
         */
        const SEX_UNKNOWN = 0;
        const SEX_MALE = 1;
        const SEX_FEMALE = 2;
        const SEX_DIVERSE = 3;
    }