<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Enums;

    /**
     * HTTP Method enum
     *
     * @package   DTV\BaseHandler\Models
     * @copyright 2024 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    enum HttpMethod: string
    {
        case ANY = 'ANY';
        case GET = 'GET';
        case POST = 'POST';
        case PUT = 'PUT';
        case DELETE = 'DELETE';
    }
