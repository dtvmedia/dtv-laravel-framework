<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler;

    use Closure;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Facades\Route;

    /**
     * Hook Handler
     *
     * @package   DTV\BaseHandler
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class HookHandler
    {
        /**
         * Array of defined hooks
         *
         * @var array
         */
        protected array $hooks = [];

        /**
         * Defines an hook for the given type, route and parameters combination
         *
         * @param string  $type
         * @param string  $route
         * @param array   $parameters
         * @param Closure $callback
         * @param int     $priority
         */
        public function define(string $type, string $route, array $parameters, Closure $callback, int $priority = 0)
        {
            $this->hooks[$type][$route][] = [
                'route'      => $route,
                'parameters' => $parameters,
                'priority'   => $priority,
                'callback'   => $callback,
            ];
        }

        /**
         * Executes all defines hooks for the given type and the current route and parameters
         *
         * @param string $type
         * @param mixed  ...$hookParams
         */
        public function execute(string $type, ...$hookParams)
        {
            $currentRoute = Route::getCurrentRoute();

            if (!isset($this->hooks[$type][$currentRoute->getName()])) {
                return;
            }

            $availableHooks = $this->hooks[$type][$currentRoute->getName()];
            // todo sort by prio

            foreach ($availableHooks as $hook) {
                foreach ($hook['parameters'] as $key => $param) {
                    $requestParam = $currentRoute->parameter($key);

                    if ($requestParam instanceof Model) {
                        $requestParam = $requestParam->id;
                    }

                    if ($requestParam !== $param) {
                        continue 2;
                    }
                }

                $hook['callback'](...$hookParams);
            }
        }
    }
