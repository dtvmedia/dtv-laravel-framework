<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Exceptions;

    /**
     * Cannot Delete Exception
     *
     * @package   DTV\BaseHandler\Exceptions
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CannotDeleteException extends \Exception
    {

    }