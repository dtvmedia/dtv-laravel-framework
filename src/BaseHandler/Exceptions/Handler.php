<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Exceptions;

    use Exception;
    use Illuminate\Auth\Access\AuthorizationException;
    use Illuminate\Auth\AuthenticationException;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
    use Illuminate\Http\JsonResponse;
    use Illuminate\Http\Request;
    use Illuminate\Http\Response;
    use Illuminate\Session\TokenMismatchException;
    use Illuminate\Validation\ValidationException;
    use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
    use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
    use Throwable;

    /**
     * Base Exception Handler
     *
     * @package   DTV\BaseHandler\Exceptions
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Handler extends ExceptionHandler
    {
        /**
         * The view which will be used to render exceptions
         *
         * @var string
         */
        protected string $exceptionView = 'views.your.exception.view';

        /**
         * A list of the exception types that are not reported.
         *
         * @var array
         */
        protected $dontReport = [
            ValidationException::class ,
            ModelNotFoundException::class ,
            NotFoundHttpException::class ,
        ];

        /**
         * A list of the inputs that are never flashed for validation exceptions.
         *
         * @var array
         */
        protected $dontFlash = [
            'password' ,
            'password_confirmation' ,
        ];

        /**
         * Latest exception cache
         *
         * @var Exception
         */
        protected Throwable $latestException;

        /**
         * Report or log an exception.
         * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
         *
         * @param Throwable $e
         *
         * @throws Throwable
         *
         * @return void
         */
        public function report(Throwable $e)
        {
            parent::report($e);
        }

        /**
         * Render an exception into an HTTP response.
         *
         * @param Request   $request
         * @param Throwable $e
         *
         * @throws Throwable
         *
         * @return SymfonyResponse
         */
        public function render($request, Throwable $e)
        {
            $this->latestException = $e;

            if (request()->ajax() && ($e instanceof AuthenticationException || $e instanceof TokenMismatchException)) {
                return json()->reload()->send(SymfonyResponse::HTTP_UNAUTHORIZED);
            }

            if ($e instanceof ValidationException) {
                return $this->exceptionResponse(
                    SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY,
                    'dtv.base::general.exceptions.validation',
                    'warning',
                    $e->validator->getMessageBag()->first()
                );
            }

            if ($e instanceof ModelNotFoundException) {
                return $this->exceptionResponse(SymfonyResponse::HTTP_NOT_FOUND, 'dtv.base::general.exceptions.modelNotFound');
            }

            if ($e instanceof NotFoundHttpException) {
                return $this->exceptionResponse(SymfonyResponse::HTTP_NOT_FOUND, 'dtv.base::general.exceptions.pageNotFound');
            }

            if ($e instanceof AuthorizationException) {
                return $this->exceptionResponse(
                    SymfonyResponse::HTTP_FORBIDDEN,
                    'dtv.base::general.exceptions.noPermissionsTitle',
                    'warning',
                    'dtv.base::general.exceptions.noPermissionsText'
                );
            }

            if ($request->ajax()) {
                return $this->generalJsonExceptionResponse($e);
            }

            return parent::render( $request , $e );
        }

        /**
         * Shows an exception screen with a corresponding message
         *
         * @param int    $code
         * @param string $message Exception message
         * @param string $type    Alert type
         * @param string $details
         *
         * @throws Throwable
         * @return JsonResponse|Response|SymfonyResponse
         */
        private function exceptionResponse( int $code , string $message , string $type = 'error' , $details = '' )
        {
            $message = trans( $message );
            $details = trans( $details );

            if ( request()->ajax() ) {
                return json()->message( $type , $message , $details , 5 )->send( $code );
            }

            if ( !view()->exists( $this->exceptionView ) ) {
                logger( 'Exception view "' . $this->exceptionView . '" does not exist!' );

                return parent::render( request() , $this->latestException );
            }

            return response()->view( $this->exceptionView , compact( 'message' , 'type' , 'details' ) , $code );
        }

        /**
         * Creates and returns an general JSON exception response
         *
         * @param Throwable $throwable
         *
         * @return JsonResponse
         */
        private function generalJsonExceptionResponse( Throwable $throwable )
        {
            $title = '[' . get_class( $throwable ) . '] ' . $throwable->getMessage();
            $text = $throwable->getFile() . ' (Line ' . $throwable->getLine() . ')';

            return json()->message( 'error' , $title , $text , 60 )->send( 500 );
        }
    }
