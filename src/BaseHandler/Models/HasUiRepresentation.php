<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Models;

    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Database\Eloquent\Model as BaseModel;
    use Illuminate\Database\Eloquent\Relations\Relation;

    /**
     * Addressable Model Extension Trait
     *
     * Managed Attributes:
     *  - street
     *  - house_number
     *  - zip_code
     *  - city
     *  - country
     *
     * @package   DTV\BaseHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    trait HasUiRepresentation
    {
        /**
         * The column used by the getSelectOptions Function for the options key
         *
         * @var string
         */
        protected static $optionKeyColumn = 'id';

        /**
         * The column used by the getSelectOptions Function for the options labels
         *
         * @var string
         */
        protected static $optionLabelColumn = 'name';

        /**
         * The Icon Class used by the getSelectOptions Function for the options labels
         *
         * @var string
         */
        protected static $optionLabelIcon = 'fa-angle-double-right';

        /**
         * The eager loading relations array for the get select options query
         *
         * @var array
         */
        protected static $optionQueryWith = [];

        /**
         * The name of the related translation file
         * If null the getTranslationFile method will try to build it from the model name
         *
         * @var string
         */
        protected static $translationFile = null;

        /**
         * Returns the related translation File
         *
         * @return string
         */
        public static function getTranslationFile()
        {
            if ( static::$translationFile !== null ) {
                return static::$translationFile;
            }

            return snake_case( str_plural( class_basename( static::class ) ) );
        }

        /**
         * Returns the Array of Select Options from a Builder/Model
         *
         * @param Builder|null  $builder  Builder instance
         * @param bool|mixed    $noOption If $noOption is something else then false then the value is used as key for the no option
         *                                If true is given then 0 will be used as key
         * @param callable|null $callable Added callback function for sorting the query results
         *
         * @return array
         */
        public static function getSelectOptions($builder = null, $noOption = false, callable $callable = null)
        {
            $options = [];
            $icon = static::$optionLabelIcon;

            if ($builder == null) {
                $builder = static::query();
            }

            if (!empty(static::$optionQueryWith)) {
                $builder->with(static::$optionQueryWith);
            }

            if ($noOption !== false) {
                $noOption = ($noOption === true) ? 0 : $noOption;
                $options[$noOption] = option(trans(static::getTranslationFile() . '.no_option'), 'fa-times');
            }

            $items = $builder->get();
            if ($callable !== null) {
                $items = $callable($items);
            }

            foreach ($items as $item) {
                $options[$item->{static::$optionKeyColumn}] = option(
                    $item->getModelValue(),
                    (method_exists(static::class, 'optionLabelIconCallback'))
                        ? static::optionLabelIconCallback($item)
                        : $icon
                );
            }

            return $options;
        }

        /**
         * Returns the translated name of the model
         *
         * @return string
         */
        public static function getModelName()
        {
            return trans( static::getTranslationFile() . '.name' );
        }

        /**
         * Model Translation helper
         *
         * @param string|null $id
         * @param array       $parameters
         * @param string|null $locale
         *
         * @return string
         */
        public static function trans( $id = null , $parameters = [] , $locale = null )
        {
            if ( $id != null ) {
                $id = '.' . $id;
            }

            return trans( static::getTranslationFile() . $id , $parameters , $locale );
        }

        /**
         * Returns an instance of the current model
         *
         * @param int|mixed $model
         * @param bool      $nullable
         *
         * @return BaseModel|null|static
         * @noinspection PhpUnhandledExceptionInspection
         * @noinspection PhpDocMissingThrowsInspection
         */
        public static function instance( $model , bool $nullable = true )
        {
            return model( $model , static::class , $nullable );
        }

        /**
         * Returns the value of the model regarding the defined option label column
         *
         * @return mixed
         */
        public function getModelValue()
        {
            $part = static::$optionLabelColumn;
            $context = $this;

            foreach ( explode( '.' , $part ) as $part ) {
                if ( method_exists( $context , $part ) ) {
                    $result = $context->$part();

                    if ( $result instanceof Relation ) {
                        $context = $context->$part;
                    } else {
                        $context = $result;
                    }
                } else {
                    return $context->$part;
                }
            }

            return $context;
        }

        /**
         * Returns a label with the models name and the models value
         *
         * @param bool $link
         *
         * @return string
         */
        public function getModelLabel( $link = false )
        {
            if ( $link ) {
                return '<a href="' . $this->getUrl() . '">[' . static::getModelName() . '] ' . $this->getModelValue() . '</a>';
            }

            return '[' . static::getModelName() . '] ' . $this->getModelValue();
        }

        /**
         * Returns the URL pointing to this model (needs to be overriden)
         *
         * @return string
         */
        public function getUrl()
        {
            return '#';
        }

    }
