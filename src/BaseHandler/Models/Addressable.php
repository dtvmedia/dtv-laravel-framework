<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Models;

    use DTV\GeoHandler\Models\Country;

    /**
     * Addressable Model Extension Trait
     *
     * Managed Attributes:
     *  - street
     *  - house_number
     *  - zip_code
     *  - city
     *  - country
     *
     * @package   DTV\BaseHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    trait Addressable
    {
        /**
         * Returns the street with the house number
         *
         * @return string
         */
        public function getAddress(): string
        {
            return trim( $this->street . ' ' . $this->house_number );
        }

        /**
         * Returns the full address with the country, zip code and city
         *
         * @param string $separator
         * @param bool   $withCountry
         *
         * @return string
         */
        public function getFullAddress( string $separator = ', ' , bool $withCountry = false ): string
        {
            $countryLabel = '';
            $address = $this->getAddress();
            $cityLabel = $this->zip_code . ' ' . $this->city;

            if ( $withCountry ) {
                $countryLabel = $this->country . '-';
            }

            if ( !empty( $cityLabel ) ) {
                $address .= $separator . $countryLabel . $cityLabel;
            }

            return $address;
        }

        /**
         * Returns the full country name
         *
         * @param string $default
         *
         * @return string
         */
        public function getCountry( string $default = '-' ): string
        {
            $country = Country::get( $this->country );

            if ( $country === null ) {
                return $default;
            }

            return $country;
        }
    }