<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Models;

    use DTV\Oxygen\Oxygen;

    /**
     * Session Model
     *
     * @package   DTV\BaseHandler\Models
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Session extends \Illuminate\Database\Eloquent\Model
    {
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'user_id' , 'ip_address' , 'user_agent' , 'payload' , 'last_activity'
        ];

        /**
         * The attributes that should be hidden for arrays.
         *
         * @var array
         */
        protected $hidden = [
            'payload'
        ];

        /**
         * Indicates if the IDs are auto-incrementing.
         *
         * @var bool
         */
        public $incrementing = false;

        /**
         * The "type" of the auto-incrementing ID.
         *
         * @var string
         */
        protected $keyType = 'string';

        /**
         * Indicates if the model should be timestamped.
         *
         * @var bool
         */
        public $timestamps = false;

        /**
         * Returns the related user
         *
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function user()
        {
            return $this->belongsTo( config( 'dtv.models.user' ) );
        }

        /**
         * Returns the name of the related user
         *
         * @return string
         */
        public function getUserName()
        {
            if ( $this->user === null ) {
                return $this->user_id ?? '-';
            }

            return $this->user->getDisplayName();
        }

        /**
         * Returns an oxygen object for the last activity timestamp
         *
         * @return Oxygen
         */
        public function getLastActivity()
        {
            return Oxygen::createFromTimestamp( $this->last_activity );
        }

        /**
         * Returns the last access url
         *
         * @return string
         */
        public function getLastAccessedUrl()
        {
            try {
                return unserialize( base64_decode( $this->payload ) )[ '_previous' ][ 'url' ] ?? '-';
            } catch ( \Throwable $e ) {
                return $e->getMessage();
            }
        }
    }
