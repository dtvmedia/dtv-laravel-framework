<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Models;

    use Database\Factories\UserFactory;
    use DTV\AclHandler\Models\HasRoles;
    use DTV\EmailHandler\Models\HasMailAccounts;
    use Illuminate\Auth\Authenticatable;
    use Illuminate\Auth\Passwords\CanResetPassword;
    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Foundation\Auth\Access\Authorizable;
    use Illuminate\Notifications\Notifiable;
    use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
    use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
    use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

    /**
     * User Model
     *
     * @package   DTV\BaseHandler\Models
     * @copyright 2017 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class User extends Model implements AuthenticatableContract , AuthorizableContract , CanResetPasswordContract
    {
        use HasRoles , Notifiable , Authenticatable , Authorizable , CanResetPassword , HasMailAccounts, HasFactory;

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'name' , 'password' , 'email' , 'first_name' , 'last_name' , 'last_login'
        ];

        /**
         * The attributes that should be hidden for arrays.
         *
         * @var array
         */
        protected $hidden = [
            'password' , 'remember_token' ,
        ];

        /**
         * The attributes that should be cast.
         *
         * @var array
         */
        protected $casts = [
            'last_login' => 'datetime',
        ];

        /**
         * The column used by the getSelectOptions Function for the options labels
         *
         * @var string
         */
        protected static $optionLabelColumn = 'getDisplayName';

        /**
         * The Icon Class used by the getSelectOptions Function for the options labels
         *
         * @var string
         */
        protected static $optionLabelIcon = 'fa-user';

        /**
         * Returns the shorter username of the user
         *
         * @return string
         */
        public function getUserName()
        {
            return $this->name;
        }

        /**
         * Returns the full name of the user
         *
         * @return string
         */
        public function getDisplayName()
        {
            return $this->first_name . ' ' . $this->last_name;
        }

        /**
         * Returns an user label with picture and display name as a link
         *
         * @param bool   $withPicture
         * @param int    $size
         * @param string $classes
         *
         * @return string
         */
        public function getDisplayNameLink( $withPicture = true , $size = 20 , $classes = 'mr-1 rounded-circle' )
        {
            $picture = ( $withPicture ) ? $this->getProfilePicture( $size , $classes ) : '';

            return '<a href="' . $this->getUrl() . '">' . $picture . $this->getDisplayName() . '</a>';
        }

        /**
         * Returns the users profile picture or an generic user icon if no such picture exists
         *
         * @param int    $size
         * @param string $classes
         *
         * @return string
         */
        public function getProfilePicture( $size = 20 , $classes = 'rounded-circle' )
        {
            $pictureUrl = $this->getProfilePictureUrl();

            if ( $pictureUrl === null ) {
                return icon( 'fa-user-circle' )->addAttribute( 'style' , 'font-size: ' . $size . 'px' )->useFixedWidth( false )
                    ->setIconBase( 'fal' )->addClass( $classes . ' fa-lg ' );
            }

            return '<img width="' . $size . '" height="' . $size . '" class="' . $classes . '" src="' . $pictureUrl . '">';
        }

        /**
         * Returns the url of the profile picture or null if no such exist
         *
         * @return string|null
         */
        public function getProfilePictureUrl()
        {
            return null;
        }

        /**
         * Returns whether or not this user is an superadmin
         *
         * @return bool
         */
        public function isSuperAdmin()
        {
            return false;
        }

        /**
         * Returns the Array of Select Options from a Builder/Model
         *
         * @param Builder|null $builder  Builder instance
         * @param bool|mixed   $noOption If $noOption is something else then false then the value is used as key for the no option
         *                               If true is given then 0 will be used as key
         * @param callable     $callable Added callback function for sorting the query results
         *
         * @return array
         */
        public static function getSelectOptions( $builder = null , $noOption = false , callable $callable = null )
        {
            if ( $builder === null ) {
                $builder = User::query();
            }

            $builder->whereNotIn( 'id' , explode( ',' , config( 'dtv.modules.admin.hidden_user_id' ) ) );

            return parent::getSelectOptions( $builder , $noOption , $callable );
        }

        protected static function newFactory()
        {
            return UserFactory::new();
        }
    }
