<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Models;

    use DTV\Oxygen\Oxygen;

    /**
     * Uses Oxygen Model Extension Trait
     *
     * @package   DTV\BaseHandler\Models
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    trait UsesOxygen
    {

        /**
         * Returns all relevant date fields for date mutation
         *
         * @return array
         */
        protected function getDateFields(): array
        {
            $registeredDates = ['created_at', 'updated_at', 'deleted_at'];

            foreach ($this->casts as $field => $cast) {
                if (in_array($cast, ['date', 'datetime'])) {
                    $registeredDates[] = $field;
                }
            }

            return $registeredDates;
        }

        /**
         * Overrides the has get mutator method and checks if a mutator is registered
         *
         * @param string $key
         *
         * @return bool
         */
        public function hasGetMutator($key)
        {
            if (method_exists($this, 'get' . studly_case($key) . 'Attribute')) {
                return parent::hasGetMutator($key);
            }

            if (in_array($key, $this->getDateFields())) {
                return true;
            }

            return false;
        }

        /**
         * Defines the date mutator for all the registered dates
         *
         * @param string $method
         * @param array  $parameters
         *
         * @return mixed
         */
        public function __call($method, $parameters)
        {
            foreach ($this->getDateFields() as $date) {
                if ($method == 'get' . studly_case($date) . 'Attribute') {
                    return Oxygen::parse($parameters[0]);
                }
            }

            return parent::__call($method, $parameters);
        }
    }
