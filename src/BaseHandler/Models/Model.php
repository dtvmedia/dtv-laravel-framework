<?php

    /** @noinspection PhpUnused */

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Models;

    use DTV\Support\MorphIdHandler;
    use Illuminate\Database\Eloquent\Model as BaseModel;
    use Illuminate\Database\Eloquent\SoftDeletes;

    /**
     * Base Model Class
     *
     * @package   DTV\BaseHandler\Models
     * @copyright 2017 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    abstract class Model extends BaseModel
    {
        use SoftDeletes;
        use HasEncryptedAttributes;
        use HasUiRepresentation;
        use UsesOxygen;

        /**
         * Returns whether or not the model was at least one time updated or not
         *
         * @return bool
         */
        public function wasAlreadyUpdated()
        {
            if ( $this->created_at->eq( $this->updated_at ) ) {
                return false;
            }

            return true;
        }

        /**
         * Returns the unique morph id
         *
         * @return string
         */
        public function getMorphId()
        {
            return MorphIdHandler::getId( static::class , $this->id );
        }
    }
