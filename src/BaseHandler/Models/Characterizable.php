<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Models;

    use DTV\BaseHandler\Enums\CharacterizableEnum;

    /**
     * Characterizable Model Extension Trait
     *
     * Managed Attributes:
     *  - sex
     *  - title
     *  - first_name
     *  - last_name
     *
     * @package   DTV\BaseHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    trait Characterizable
    {
        /**
         * Returns the full name of the person
         *
         * @return string
         */
        public function getDisplayName(): string
        {
            return $this->first_name . ' ' . $this->last_name;
        }

        /**
         * Returns the full name with salutation and title
         *
         * @return string
         */
        public function getFullName(): string
        {
            return trim( $this->getSalutation() . ' ' . $this->title . ' ' . $this->getDisplayName() );
        }

        /**
         * Returns the gender of the person
         *
         * @return array|\Illuminate\Contracts\Translation\Translator|null|string
         */
        public function getSex()
        {
            return trans( 'dtv.base::general.sex.' . $this->sex );
        }

        /**
         * Returns the salutation of the user
         *
         * @return string
         */
        public function getSalutation(): string
        {
            return trans( 'dtv.base::general.salutations.' . $this->sex );
        }

        /**
         * Returns an array with sex select options
         *
         * @return array
         */
        public static function getSexOptions()
        {
            $options = [];

            $options[ CharacterizableEnum::SEX_UNKNOWN ] = option( 'dtv.base::general.sex.' . CharacterizableEnum::SEX_UNKNOWN , 'fa-question' );
            $options[ CharacterizableEnum::SEX_MALE ] = option( 'dtv.base::general.sex.' . CharacterizableEnum::SEX_MALE , 'fa-male' );
            $options[ CharacterizableEnum::SEX_FEMALE ] = option( 'dtv.base::general.sex.' . CharacterizableEnum::SEX_FEMALE , 'fa-female' );

            if (config('dtv.app.gender_with_diverse', false) === true) {
                $options[ CharacterizableEnum::SEX_DIVERSE ] = option('dtv.base::general.sex.' . CharacterizableEnum::SEX_DIVERSE, 'fa-transgender');
            }

            return $options;
        }

        /**
         * Returns an array with salutation select options
         *
         * @return array
         */
        public static function getSalutationOptions()
        {
            $options = [];

            $options[ CharacterizableEnum::SEX_UNKNOWN ] = option( 'dtv.base::general.salutations.' . CharacterizableEnum::SEX_UNKNOWN , 'fa-chevron-double-right' );
            $options[ CharacterizableEnum::SEX_MALE ] = option( 'dtv.base::general.salutations.' . CharacterizableEnum::SEX_MALE , 'fa-chevron-double-right' );
            $options[ CharacterizableEnum::SEX_FEMALE ] = option( 'dtv.base::general.salutations.' . CharacterizableEnum::SEX_FEMALE , 'fa-chevron-double-right' );

            return $options;
        }
    }