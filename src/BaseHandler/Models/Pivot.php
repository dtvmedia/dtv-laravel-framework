<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Models;

    use Illuminate\Database\Eloquent\Relations\Concerns\AsPivot;

    /**
     * Base Pivot Model Class
     *
     * @package   DTV\BaseHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Pivot extends Model
    {
        use AsPivot;

        /**
         * The attributes that aren't mass assignable.
         *
         * @var array
         */
        protected $guarded = [];

        /**
         * Delete the model from the database.
         *
         * @return bool|null
         *
         * @throws \Exception
         */
        public function delete()
        {
            // Todo find a better solution
            $this->forceDeleting = true;

            return parent::delete();
        }
    }