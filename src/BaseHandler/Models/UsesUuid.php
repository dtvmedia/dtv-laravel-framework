<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Models;

    use Illuminate\Support\Str;

    /**
     * Uses UUID Model Extension Trait
     *
     * @see https://dev.to/wilburpowery/easily-use-uuids-in-laravel-45be
     *
     * Managed Attributes:
     *  - id (UUID)
     *
     * @package   DTV\BaseHandler\Models
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     * @deprecated
     */
    trait UsesUuid
    {
        /**
         * Boots the trait and registers the new auto key creation behaviour
         */
        protected static function bootUsesUuid()
        {
            static::creating( function ( $model ) {
                if ( !$model->getKey() ) {
                    $model->{$model->getKeyName()} = Str::uuid()->toString();
                }
            } );
        }

        /**
         * Get the value indicating whether the IDs are incrementing.
         *
         * @return bool
         */
        public function getIncrementing()
        {
            return false;
        }

        /**
         * Get the auto-incrementing key type.
         *
         * @return string
         */
        public function getKeyType()
        {
            return 'string';
        }
    }
