<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Models;

    use Illuminate\Support\Facades\Crypt;

    /**
     * HasEncryptedAttributes model Trait
     *
     * @package   DTV\BaseHandler\Models
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     * @deprecated
     */
    trait HasEncryptedAttributes
    {
        /**
         * Array of encrypted fields
         *
         * @var array
         */
        protected $encrypted = [];

        /**
         * Returns an attribute of the model
         *
         * @param string $key
         *
         * @return mixed
         */
        public function getAttribute( $key )
        {
            $value = parent::getAttribute( $key );

            if ( in_array( $key , $this->encrypted ) ) {
                $value = Crypt::decryptString( $value );
            }

            return $value;
        }

        /**
         * Sets the value of the given attribute for this model
         *
         * @param string $key
         * @param mixed  $value
         *
         * @return mixed
         */
        public function setAttribute( $key , $value )
        {
            if ( in_array( $key , $this->encrypted ) ) {
                $value = Crypt::encryptString( $value );
            }

            return parent::setAttribute( $key , $value );
        }
    }
