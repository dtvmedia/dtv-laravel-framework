<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler;

    use Carbon\Carbon;
    use DTV\BaseHandler\Views\ListView;
    use DTV\Oxygen\Oxygen;
    use Exception;
    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Contracts\Support\Responsable;
    use Illuminate\Http\Request;
    use Illuminate\Support\Traits\Conditionable;

    /**
     * Json Response Handler for AJAX Requests
     *
     * @package   DTV\BaseHandler
     * @copyright 2017 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class JsonResponse implements Responsable , Arrayable
    {
        use Conditionable;

        /**
         * Message Auto Timout constant
         */
        public const DEFAULT_TIMEOUT = -1;

        /**
         * Hide Modals constants
         */
        public const HIDE_MODAL_MAIN = 'main';
        public const HIDE_MODAL_CONFIRM = 'confirm';

        /**
         * Result Data Array
         *
         * @var array
         */
        protected array $data = [];

        /**
         * Adds an redirection using an route
         *
         * @param string $route
         * @param array  $parameter
         *
         * @return JsonResponse
         */
        public function redirect( $route , array $parameter = [] )
        {
            return $this->redirectTo( route( $route , $parameter ) );
        }

        /**
         * Adds an page reload statement
         *
         * @return JsonResponse
         */
        public function reload()
        {
            return $this->redirectTo( 'reload' );
        }

        /**
         * Adds an redirection
         *
         * @param string $to
         *
         * @return JsonResponse
         */
        public function redirectTo( $to )
        {
            $this->data[ 'redirect' ] = $to;

            // Remove reloadListView entries due to conflicting actions
            unset( $this->data[ 'reload_listviews' ] );

            return $this;
        }

        /**
         * Clears the redirect action
         *
         * @return $this
         */
        public function clearRedirect()
        {
            unset( $this->data[ 'redirect' ] );

            return $this;
        }

        /**
         * Adds an list view reload statement
         *
         * @param array|string $listviews
         *
         * @throws Exception
         * @return JsonResponse
         */
        public function reloadListView( $listviews )
        {
            $listviews = ( is_array( $listviews ) ) ? $listviews : func_get_args();

            foreach ( $listviews as $listview ) {
                if ( is_a( $listview , ListView::class , true ) ) {
                    /** @var ListView $view */
                    $view = new $listview();
                    $listview = $view->getLabel();
                }

                $this->data[ 'reload_listviews' ][] = $listview;
            }

            // Remove redirect entries due to conflicting actions
            $this->clearRedirect();

            return $this;
        }

        /**
         * Add a translated message
         *
         * @param string $mode
         * @param string $trans
         * @param array  $params
         * @param int    $time
         *
         * @return JsonResponse
         */
        public function transMessage( $mode , $trans , array $params = [] , int $time = self::DEFAULT_TIMEOUT )
        {
            $title = trans( $trans . 'Title' , $params );
            $text = trans( $trans . 'Text' , $params );

            return $this->message( $mode , $title , $text , $time );
        }

        /**
         * Adds an error message
         *
         * @param string $trans
         * @param array  $params
         * @param int    $time
         *
         * @return JsonResponse
         */
        public function error( $trans , array $params = [] , int $time = self::DEFAULT_TIMEOUT )
        {
            return $this->transMessage( 'error' , $trans , $params , $time );
        }

        /**
         * Adds an warning message
         *
         * @param string $trans
         * @param array  $params
         * @param int    $time
         *
         * @return JsonResponse
         */
        public function warning( $trans , array $params = [] , int $time = self::DEFAULT_TIMEOUT )
        {
            return $this->transMessage( 'warning' , $trans , $params , $time );
        }

        /**
         * Adds an success message
         *
         * @param string $trans
         * @param array  $params
         * @param int    $time
         *
         * @return JsonResponse
         */
        public function success( $trans , array $params = [] , int $time = self::DEFAULT_TIMEOUT )
        {
            return $this->transMessage( 'success' , $trans , $params , $time );
        }

        /**
         * Add a message
         *
         * @param string $mode
         * @param string $title
         * @param string $text
         * @param int    $time
         *
         * @return JsonResponse
         */
        public function message( $mode , $title , $text = '' , int $time = self::DEFAULT_TIMEOUT )
        {
            if ($time === self::DEFAULT_TIMEOUT) {
                $time = optional(config('dtv.app.message_timeout_defaults'))[ $mode ] ?? 5;
            }

            $this->data[ 'message' ][] = [
                'mode'  => ( $mode === 'error' ) ? 'danger' : $mode ,
                'title' => $title ,
                'text'  => $text ,
                'time'  => $time ,
            ];

            return $this;
        }

        /**
         * Adds an custom js event which will be fired off with the given data
         *
         * @param string $eventName
         * @param array  $data
         * @param string $key
         *
         * @return $this
         */
        public function fireEvent( string $eventName , array $data = [] , string $key = '*' )
        {
            $this->data[ 'events' ][] = [
                'name'      => $eventName ,
                'key'       => $key ,
                'parameter' => $data ,
            ];

            return $this;
        }

        /**
         * Sets the value of an given form input
         *
         * @param string      $name
         * @param string|null $value
         * @param bool        $triggerChange
         *
         * @return $this
         */
        public function setInput( string $name , ?string $value , bool $triggerChange = false )
        {
            $this->data[ 'inputs' ][ $name ] = [
                'value'   => $value ,
                'trigger' => $triggerChange ,
            ];

            return $this;
        }

        /**
         * Sets the value of an given form input
         *
         * @param string      $name
         * @param Carbon|null $value
         * @param bool        $triggerChange
         *
         * @return $this
         */
        public function setDate( string $name , ?Carbon $value , bool $triggerChange = false )
        {
            if ( $value instanceof Oxygen ) {
                $value = $value->toDefaultDateString();
            } elseif ( $value instanceof Carbon ) {
                $value = $value->toDateString();
            }

            return $this->setInput( $name , $value , $triggerChange );
        }

        /**
         * Sets the value of an given form checkbox
         *
         * @param string $name
         * @param bool   $value
         * @param bool   $triggerChange
         *
         * @return $this
         */
        public function setCheckbox( string $name , ?bool $value , bool $triggerChange = false )
        {
            if ( $value !== null ) {
                $value = intval( $value );
            }

            return $this->setInput( $name , $value , $triggerChange );
        }

        /**
         * Adds an hide modal statement
         *
         * @param string|false $hide
         *
         * @return JsonResponse
         */
        public function hideModal( $hide = self::HIDE_MODAL_MAIN )
        {
            $this->data[ 'modal' ][ 'hide' ] = $hide;

            return $this;
        }

        /**
         * Adds an modal
         *
         * @param string $html
         * @param string $size
         *
         * @return JsonResponse
         */
        public function modal( $html , $size = 'modal-md' )
        {
            $this->data[ 'modal' ][ 'html' ] = $html;
            $this->data[ 'modal' ][ 'size' ] = $size;

            return $this;
        }

        /**
         * Add an new Ajax Request (via Route and parameter) as Callback
         *
         * @param string $route
         * @param array  $parameter
         * @param string $method
         *
         * @return JsonResponse
         */
        public function newRequest( string $route , array $parameter = [] , string $method = 'GET' )
        {
            return $this->newRequestUrl( route( $route , $parameter ) , $method );
        }

        /**
         * Add an new Ajax Request (via URL) as Callback
         *
         * @param string $url
         * @param string $method
         *
         * @return JsonResponse
         */
        public function newRequestUrl( string $url , string $method = 'GET' )
        {
            $this->data[ 'newRequest' ][ 'method' ] = $method;
            $this->data[ 'newRequest' ][ 'url' ] = $url;

            return $this;
        }

        /**
         * Return all the Results as JSON
         *
         * @param int $status
         *
         * @return \Illuminate\Http\JsonResponse
         */
        public function send( int $status = 200 )
        {
            return response()->json( $this->data , $status );
        }

        /**
         * Create an HTTP response that represents the object.
         *
         * @param Request $request
         *
         * @return \Illuminate\Http\JsonResponse
         */
        public function toResponse( $request )
        {
            return $this->send();
        }

        /**
         * Get the instance as an array.
         *
         * @return array
         */
        public function toArray()
        {
            return $this->data;
        }
    }
