<?php

    return [

        'validation' => [
            'required' => 'This field is required' ,
            'numeric'  => 'This field must be a number' ,
            'string'   => 'This field must be a string' ,
            'min'      => [
                'string' => 'This field must be at least :MIN characters' ,
                'number' => 'This field must be at least :MIN' ,
            ] ,
            'max'      => [
                'string' => 'This field may not be greater than :MAX characters' ,
                'number' => 'This field may not be greater than :MAX' ,
            ] ,
        ] ,

        'select' => [
            'noneSelectedText' => 'Please choose' ,
            'noneResultsText'  => 'No results matched {0}' ,
            'selectAllText'    => 'Select all',
            'deselectAllText'  => 'Deselect all',
        ] ,

        'confirm' => [
            'delete'               => 'Delete' ,
            'execActionQuestion'   => 'Really do the following action?' ,
            'deleteObjectQuestion' => 'Are you sure you want to delete the selected object?' ,
        ]

    ];