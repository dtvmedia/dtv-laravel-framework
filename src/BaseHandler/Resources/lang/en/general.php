<?php

use DTV\BaseHandler\Enums\CharacterizableEnum;

return [

        'close' => 'Close' ,
        'save'  => 'Save' ,
        'back'  => 'Back' ,
        'error' => 'Error' ,
        'yes'   => 'Yes' ,
        'no'    => 'No' ,

        'sex' => [
            CharacterizableEnum::SEX_UNKNOWN => 'Unknown',
            CharacterizableEnum::SEX_MALE    => 'Male',
            CharacterizableEnum::SEX_FEMALE  => 'Female',
            CharacterizableEnum::SEX_DIVERSE => 'Diverse',
        ],

        'salutations' => [
            CharacterizableEnum::SEX_UNKNOWN => '',
            CharacterizableEnum::SEX_MALE    => 'Sir',
            CharacterizableEnum::SEX_FEMALE  => 'Madam',
            CharacterizableEnum::SEX_DIVERSE => '',
        ],

        'exceptions' => [
            'validation'         => 'Input error' ,
            'modelNotFound'      => 'Object not found' ,
            'pageNotFound'       => 'Page not found' ,
            'noPermissionsTitle' => 'No permission' ,
            'noPermissionsText'  => 'You have no permissions to view this page or execute this action' ,
        ] ,

        'messages' => [
            'deleteSuccessTitle' => ':object deleted' ,
            'deleteSuccessText'  => 'The :object was successfully deleted' ,
            'deleteWarningTitle' => ':object couldn\'t be deleted' ,
            'deleteWarningText'  => 'The :object couln\'t be deleted because of some related objects' ,
        ]

    ];