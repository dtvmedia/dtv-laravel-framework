<?php

    return [

        'actions'    => 'Actions' ,
        'search_for' => 'Search for' ,
        'no_results' => 'There are no records available...' ,

        'counter' => [
            'show'          => 'Show' ,
            'to'            => 'to' ,
            'from'          => 'from' ,
            'filtered_from' => 'filtered from'
        ] ,

        'pagination' => [
            'first' => 'First' ,
            'prev'  => 'Previous' ,
            'next'  => 'Next' ,
            'last'  => 'Last' ,
        ] ,

    ];