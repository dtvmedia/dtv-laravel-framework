<?php

    return [

        'validation' => [
            'required' => 'Dieses Feld muss ausgefüllt werden' ,
            'numeric'  => 'Dieses Feld darf nur numerische Eingaben enthalten' ,
            'string'   => 'Dieses Feld darf nur Zeichenfolgen enthalten' ,
            'min'      => [
                'string' => 'Die Eingabe dieses Feldes muss mindestens :MIN Zeichen lang sein' ,
                'number' => 'Der Wert dieses Feldes muss mindestens :MIN betragen' ,
            ] ,
            'max'      => [
                'string' => 'Die Eingabe dieses Feldes darf maximal :MAX Zeichen lang sein' ,
                'number' => 'Der Wert dieses Feldes darf maximal :MAX betragen' ,
            ] ,
        ] ,

        'select' => [
            'noneSelectedText' => 'Bitte wählen' ,
            'noneResultsText'  => 'Kein Ergebnis für {0} gefunden' ,
            'selectAllText'    => 'Alle auswählen',
            'deselectAllText'  => 'Alle abwählen',
        ] ,

        'confirm' => [
            'delete'               => 'Löschen' ,
            'execActionQuestion'   => 'Folgende Aktion wirklich ausführen?' ,
            'deleteObjectQuestion' => 'Möchten Sie wirklich das ausgewählte Objekt löschen?' ,
        ]

    ];