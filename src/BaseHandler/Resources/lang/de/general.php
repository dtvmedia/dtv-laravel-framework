<?php

use DTV\BaseHandler\Enums\CharacterizableEnum;

return [

        'close' => 'Schließen' ,
        'save'  => 'Speichern' ,
        'back'  => 'Zurück' ,
        'error' => 'Fehler' ,
        'yes'   => 'Ja' ,
        'no'    => 'Nein' ,

        'sex' => [
            CharacterizableEnum::SEX_UNKNOWN => 'Unbekannt',
            CharacterizableEnum::SEX_MALE    => 'Männlich',
            CharacterizableEnum::SEX_FEMALE  => 'Weiblich',
            CharacterizableEnum::SEX_DIVERSE => 'Divers',
        ],

        'salutations' => [
            CharacterizableEnum::SEX_UNKNOWN => '',
            CharacterizableEnum::SEX_MALE    => 'Herr',
            CharacterizableEnum::SEX_FEMALE  => 'Frau',
            CharacterizableEnum::SEX_DIVERSE => '',
        ],

        'exceptions' => [
            'validation'         => 'Eingabefehler' ,
            'modelNotFound'      => 'Objekt nicht gefunden' ,
            'pageNotFound'       => 'Seite nicht gefunden' ,
            'noPermissionsTitle' => 'Keine Berechtigung' ,
            'noPermissionsText'  => 'Sie haben keine Berechtigungen diese Seite zu sehen oder diese Aktion auszuführen' ,
        ] ,

        'messages' => [
            'deleteSuccessTitle' => ':object gelöscht' ,
            'deleteSuccessText'  => ':object wurde erfolgreich gelöscht' ,
            'deleteWarningTitle' => ':object nicht gelöscht' ,
            'deleteWarningText'  => ':object konnte aufgrund einiger Abhängigkeiten nicht gelöscht werden' ,
        ]

    ];