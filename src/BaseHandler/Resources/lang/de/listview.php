<?php

    return [

        'actions'    => 'Aktionen' ,
        'search_for' => 'Suche nach' ,
        'no_results' => 'Es sind momentan keine Datensätze verfügbar...' ,

        'counter' => [
            'show'          => 'Zeige' ,
            'to'            => 'bis' ,
            'from'          => 'von' ,
            'filtered_from' => 'gefiltert von'
        ] ,

        'pagination' => [
            'first' => 'Erste' ,
            'prev'  => 'Vorherige' ,
            'next'  => 'Nächste' ,
            'last'  => 'Letzte' ,
        ] ,

    ];