@if( $input->hasClientValidationsEnabled() ) <div class="has-feedback"> @endif

<input id="{{ $input->getId() }}" name="{{ $input->getName() }}" type="time"
       class="form-control form-timepicker {{ $input->getClassString() }}"
       @if( $input->step() !== null ) step="{{ $input->step() }}" @endif
       data-translation="{{ $input->getLabel() }}"
       @if( $input->hasClientValidationsEnabled() ) data-validation="{{ $input->getValidation() }}" @endif
       value="{{ $input->getValue() }}"
       @if( $input->required() == true ) required @endif
       @if( $input->readonly() == true ) readonly @endif
       @if( $input->disabled() == true ) disabled @endif
>

@if( $input->hasClientValidationsEnabled() ) <span class="feedback"></span></div> @endif