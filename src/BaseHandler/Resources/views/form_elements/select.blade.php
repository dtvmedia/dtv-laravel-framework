@if( $input->hasClientValidationsEnabled() ) <div class="has-feedback"> @endif

<select id="{{ $input->getId() }}" class="form-select show-tick input-max-width {{ $input->getClassString() }}"
        name="{{ $input->getName() }}@if( $input->multiple() == true )[]@endif"
        data-live-search="true"
        data-translation="{{ $input->getLabel() }}"
        data-width="100%"
        @if( $input->hasClientValidationsEnabled() ) data-validation="{{ $input->getValidation() }}" @endif
        @if( $input->multiple() == true ) multiple data-actions-box="true" @endif
        @if( $input->required() == true ) required @endif
        @if( $input->readonly() == true ) readonly @endif
        @if( $input->disabled() == true ) disabled @endif
>
    @foreach( $input->values() as $key => $value )
        <option value="{{ $key }}"
                @if( is_array( $input->getValue() ) )
                        @if( in_array( $key , $input->getValue() ) ) selected @endif
                @else
                        @if( strval($input->getValue()) === strval($key) ) selected @endif
                @endif
                @if( $value['icon'] != null ) data-icon="{{ $value['icon'] }}" @endif
                @if( $value['disabled'] === true ) disabled @endif >{{ $value['value'] }}</option>
    @endforeach
</select>

@if( $input->hasClientValidationsEnabled() ) <span class="feedback"></span></div> @endif