@if( $input->hasClientValidationsEnabled() ) <div class="has-feedback"> @endif

<div class="checkbox col-md-{{ $input->getSize() }}">
    <div class="custom-control custom-checkbox">
        <input type="checkbox" id="{{ $input->getId() }}" class="custom-control-input {{ $input->getClassString() }}"
               name="{{ $input->getName() }}"
               data-translation="{{ $input->getLabel() }}"
               @if( $input->hasClientValidationsEnabled() ) data-validation="{{ $input->getValidation() }}" @endif
               value="{{ $input->checkedValue() }}"
               @if( $input->required() == true ) required @endif
               @if( $input->readonly() == true ) readonly @endif
               @if( $input->disabled() == true ) disabled @endif
               @if( is_array( $input->getValue() ) )
                    @if( in_array( $input->checkedValue() , $input->getValue() ) ) checked @endif
               @else
                    @if( $input->getValue() == $input->checkedValue() ) checked @endif
               @endif
        >
        <label class="custom-control-label @if( $input->disabled() == true ) disabled @endif" for="{{ $input->getId() }}" >
            @if( $input->showLabel() === true )
                {{ $input->getLabel() }}
            @else
                &nbsp;
            @endif
        </label>
    </div>
</div>

@if( $input->hasClientValidationsEnabled() ) <span class="feedback"></span></div> @endif