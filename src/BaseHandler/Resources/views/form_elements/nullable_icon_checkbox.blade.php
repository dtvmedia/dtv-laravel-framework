@if( $input->hasClientValidationsEnabled() ) <div class="has-feedback"> @endif

    <input type="hidden" id="{{ $input->getId() }}"
           name="{{ $input->getName() }}"
           data-translation="{{ $input->getLabel() }}"
           @if( $input->hasClientValidationsEnabled() ) data-validation="{{ $input->getValidation() }}" @endif
           data-checked-value="{{ $input->checkedValue() }}"
           @if( $input->required() == true ) required @endif
           @if( $input->readonly() == true ) readonly @endif
           @if( $input->disabled() == true ) disabled @endif
           @if( is_array( $input->getValue() ) )
           @if( in_array( $input->checkedValue() , $input->getValue() ) ) checked @endif
           @else
           @if( $input->getValue() == $input->checkedValue() ) checked @endif
            @endif
    >
    <input type="checkbox" id="{{ $input->getId() }}_checkbox" data-value-id="{{ $input->getId() }}" name="{{ $input->getName() }}_checkbox" class="icon-checkbox checkbox-nullable {{ $input->getClassString() }}"
           @if( $input->required() == true ) required @endif
           @if( $input->readonly() == true ) readonly @endif
           @if( $input->disabled() == true ) disabled @endif
           @if( is_array( $input->getValue() ) )
                @if( in_array( $input->checkedValue() , $input->getValue() ) ) checked @endif
           @else
               @if( $input->getValue() == $input->checkedValue() ) checked @endif
               data-indeterminate="@if( $input->getValue() === null ){{'true'}}@else{{'false'}}@endif"
           @endif
    >{{-- Dont move on extra line because that causes an extra space between checkbox and label
--}}<label for="{{ $input->getId() }}_checkbox" @if( $input->disabled() == true ) class="disabled" @endif>
        @if( $input->showLabel() === true )
            {{ $input->getLabel() }}
        @endif
    </label>

    @if( $input->hasClientValidationsEnabled() ) <span class="feedback"></span></div> @endif