@if( $input->hasClientValidationsEnabled() ) <div class="has-feedback"> @endif

<textarea id="{{ $input->getId() }}" class="form-control {{ $input->getClassString() }}"
          name="{{ $input->getName() }}"
          data-translation="{{ $input->getLabel() }}"
          @if( $input->height() !== null && $input->height() !== \DTV\BaseHandler\Views\FormInputs\Textarea::DYNAMIC_HEIGHT )style="height: {{ $input->height() }}px" @endif
          @if( $input->hasClientValidationsEnabled() ) data-validation="{{ $input->getValidation() }}" @endif
          @if( $input->maxlength() !== null ) maxlength="{{ $input->maxlength() }}" @endif
          @if( $input->required() == true ) required @endif
          @if( $input->readonly() == true ) readonly @endif
          @if( $input->disabled() == true ) disabled @endif
          @if( $input->placeholder() !== null ) placeholder="{{ $input->placeholder() }}" @else placeholder="{{ $input->getLabel() }}" @endif
          @if( $input->hint() !== null ) data-toggle="popover" data-trigger="focus" data-placement="left" data-hint="{{ $input->hint() }}" @endif
>{{ $input->getValue() }}</textarea>

@if( $input->hasClientValidationsEnabled() ) <span class="feedback"></span></div> @endif