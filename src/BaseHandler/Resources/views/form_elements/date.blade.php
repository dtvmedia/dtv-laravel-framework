@if( $input->hasClientValidationsEnabled() ) <div class="has-feedback"> @endif

<input id="{{ $input->getId() }}" name="{{ $input->getName() }}" type="date"
       data-translation="{{ $input->getLabel() }}"
       class="form-control form-datepicker {{ $input->getClassString() }}"
       @if( $input->hasClientValidationsEnabled() ) data-validation="{{ $input->getValidation() }}" @endif
       value="{{ $input->getValue() }}"
       @if( $input->min() instanceof \DTV\Oxygen\Oxygen ) min="{{ $input->min()->format('Y-m-d') }}" @endif
       @if( $input->max() instanceof \DTV\Oxygen\Oxygen ) max="{{ $input->max()->format('Y-m-d') }}" @endif
       @if( $input->required() == true ) required @endif
       @if( $input->readonly() == true ) readonly @endif
       @if( $input->disabled() == true ) disabled @endif
>

@if( $input->hasClientValidationsEnabled() ) <span class="feedback"></span></div> @endif