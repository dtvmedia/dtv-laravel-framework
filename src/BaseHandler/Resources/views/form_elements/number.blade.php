@if( $input->hasClientValidationsEnabled() ) <div class="has-feedback"> @endif

<input id="{{ $input->getId() }}" class="form-control input-max-width {{ $input->getClassString() }}"
       type="number"
       name="{{ $input->getName() }}"
       value="{{ $input->getValue() }}"
       data-translation="{{ $input->getLabel() }}"
       @if( $input->min() !== null ) min="{{ $input->min() }}" @endif
       @if( $input->max() !== null ) max="{{ $input->max() }}" @endif
       @if( $input->step() !== null ) step="{{ $input->step() }}" @endif
       @if( $input->hasClientValidationsEnabled() ) data-validation="{{ $input->getValidation() }}" @endif
       @if( $input->maxlength() !== null ) maxlength="{{ $input->maxlength() }}" @endif
       @if( $input->required() == true ) required @endif
       @if( $input->readonly() == true ) readonly @endif
       @if( $input->disabled() == true ) disabled @endif
       @if( $input->placeholder() !== null ) placeholder="{{ $input->placeholder() }}" @else placeholder="{{ $input->getLabel() }}" @endif
       @if( $input->pattern() !== null ) pattern="{{ $input->pattern() }}" @endif
       @if( $input->hint() !== null ) data-toggle="popover" data-trigger="focus" data-placement="left" data-hint="{{ $input->hint() }}" @endif
/>

@if( $input->hasClientValidationsEnabled() ) <span class="feedback"></span></div> @endif