@if( $input->hasClientValidationsEnabled() ) <div class="has-feedback"> @endif

<input id="{{ $input->getId() }}" name="{{ $input->getName() }}" type="color"
       data-translation="{{ $input->getLabel() }}"
       class="form-control {{ $input->getClassString() }}"
       @if( $input->hasClientValidationsEnabled() ) data-validation="{{ $input->getValidation() }}" @endif
       value="{{ $input->getValue() }}"
       @if( $input->required() == true ) required @endif
       @if( $input->readonly() == true ) readonly @endif
       @if( $input->disabled() == true ) disabled @endif
>

@if( $input->hasClientValidationsEnabled() ) <span class="feedback"></span></div> @endif