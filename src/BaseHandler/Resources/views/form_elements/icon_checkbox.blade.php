@if( $input->hasClientValidationsEnabled() ) <div class="has-feedback"> @endif

<input type="checkbox" id="{{ $input->getId() }}" class="icon-checkbox {{ $input->getClassString() }}"
       name="{{ $input->getName() }}"
       data-translation="{{ $input->getLabel() }}"
       @if( $input->hasClientValidationsEnabled() ) data-validation="{{ $input->getValidation() }}" @endif
       value="{{ $input->checkedValue() }}"
       @if( $input->required() == true ) required @endif
       @if( $input->readonly() == true ) readonly @endif
       @if( $input->disabled() == true ) disabled @endif
        @if( is_array( $input->getValue() ) )
                @if( in_array( $input->checkedValue() , $input->getValue() ) ) checked @endif
        @else
                @if( $input->getValue() == $input->checkedValue() ) checked @endif
                @if( $input->getValue() === null ) data-indeterminate="true" @endif
        @endif
>{{-- Dont move on extra line because that causes an extra space between checkbox and label
--}}<label for="{{ $input->getId() }}" @if( $input->disabled() == true ) class="disabled" @endif>
        @if( $input->showLabel() === true )
                {{ $input->getLabel() }}
        @else
                &nbsp;
        @endif
</label>

@if( $input->hasClientValidationsEnabled() ) <span class="feedback"></span></div> @endif