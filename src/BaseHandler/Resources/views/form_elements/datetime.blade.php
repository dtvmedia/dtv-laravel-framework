<div class="form-group col-md-6">
    <label class="control-label" for="{{ $input->getId() }}">{{ $input->getLabel() }}</label>
    @if( $input->description() !== null ) <p>{!! $input->description() !!}</p> @endif

    @if( $input->hasClientValidationsEnabled() ) <div class="has-feedback"> @endif

        <input id="{{ $input->getId() }}" name="{{ $input->getName() }}" type="date"
               class="form-control form-datepicker {{ $input->getClassString() }}"
               data-translation="{{ $input->getLabel() }}"
               @if( $input->hasClientValidationsEnabled() ) data-validation="{{ $input->getValidation() }}" @endif value="{{ ( $input->getValue() instanceof \DTV\Oxygen\Oxygen ) ? $input->getValue()->toDefaultDateString() : $input->getValue() }}"
               @if( $input->min() instanceof \DTV\Oxygen\Oxygen ) min="{{ $input->min()->format('Y-m-d') }}" @endif
               @if( $input->max() instanceof \DTV\Oxygen\Oxygen ) max="{{ $input->max()->format('Y-m-d') }}" @endif
               @if( $input->required() == true ) required @endif
               @if( $input->readonly() == true ) readonly @endif
               @if( $input->disabled() == true ) disabled @endif
        >

    @if( $input->hasClientValidationsEnabled() ) <span class="feedback"></span></div> @endif
</div>

<div class="form-group col-md-6">
    <label class="control-label" for="{{ $input->getId() }}_time">&nbsp;</label>
    @if( $input->description() !== null ) <p>{!! $input->description() !!}</p> @endif

    @if( $input->hasClientValidationsEnabled() ) <div class="has-feedback"> @endif

        <input id="{{ $input->getId() }}_time" name="{{ $input->getName() }}_time" type="time"
               class="form-control form-timepicker {{ $input->getClassString() }}"
               data-translation="{{ $input->getLabel() }}"
               value="{{ ( $input->getValue() instanceof \DTV\Oxygen\Oxygen ) ? (($input->step() === 60) ? $input->getValue()->toShortTimeString() : $input->getValue()->toDefaultTimeString()) : $input->getValue() }}"
               @if( $input->hasClientValidationsEnabled() ) data-validation="{{ $input->getValidation() }}" @endif
               @if( $input->step() !== null ) step="{{ $input->step() }}" @else step="1" @endif
               @if( $input->required() == true ) required @endif
               @if( $input->readonly() == true ) readonly @endif
               @if( $input->disabled() == true ) disabled @endif
        >

    @if( $input->hasClientValidationsEnabled() ) <span class="feedback"></span></div> @endif
</div>
