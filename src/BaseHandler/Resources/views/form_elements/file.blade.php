@if( $input->hasClientValidationsEnabled() ) <div class="has-feedback"> @endif

    <input id="{{ $input->getId() }}" class="form-control input-max-width
       {{ $input->getClassString() }}"
           type="file"
           name="{{ $input->getName() }}"
           data-translation="{{ $input->getLabel() }}"
           @if( $input->hasClientValidationsEnabled() ) data-validation="{{ $input->getValidation() }}" @endif
           @if( $input->required() == true ) required @endif
           @if( $input->accept() !== null ) accept="{{ $input->accept() }}" @endif
           @if( $input->multiple() === true ) multiple @endif
    />

    @if( $input->hasClientValidationsEnabled() ) <span class="feedback"></span></div> @endif