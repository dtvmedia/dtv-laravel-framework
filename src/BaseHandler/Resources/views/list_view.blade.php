@section('view-title')
    @if( $list->show_title )
        {!! $list->icon !!} {{ $list->title }}
    @endif
@endsection

<div class="listview" id="{{ $list->label }}">
    <!-- Pre Table Widgets -->
    <div class="row">
        <div class="col-sm-6 text-left d-none d-sm-flex">
            <select class="form-control form-control-sm listview-pagelength-select" style="display: none;" autocomplete="off">
                <option>10</option>
                <option>20</option>
                <option>50</option>
                <option>100</option>
            </select>
        </div>
        <div class="col-sm-6 text-right">
            <input type="text" class="form-control form-control-sm listview-search" autocomplete="off" style="display: none;" placeholder="{{ trans('dtv.base::listview.search_for') }}"/>
        </div>
    </div>

    <!-- Table -->
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped listview-table">
                <thead>
                    <tr>
                        @foreach( $list->header as $key => $cell )
                            <th @isset( $list->columns[ $key ] ){!! $list->columns[ $key ]->getHtmlAttributes( true ) !!}@endisset>
                                {!! $cell !!}
                            </th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach( $list->data as $row )
                        <tr
                            @foreach( $row['lvMeta'] ?? [] as $key => $cell )
                                data-{{ $key }}="{{ $cell }}"
                            @endforeach
                        >
                            @foreach( $row as $key => $cell )
                                @if( $key == 'lvMeta' )
                                    @continue
                                @endif

                                <td @isset( $list->columns[ $loop->index - 1 ] ){!! $list->columns[ $loop->index - 1 ]->getHtmlAttributes() !!}@endisset>
                                    {!! $cell !!}
                                </td>
                            @endforeach
                        </tr>
                    @endforeach
                    <tr class="listview-no-results-row" style="display: {{ ( count( $list->data ) == 0 ) ? 'table-row' : 'none' }}">
                        <td colspan="{{ count( $list->header ) }}">
                            {{ trans('dtv.base::listview.no_results') }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <!-- Post Table Widgets -->
    <div class="row">
        <div class="col-sm-3 text-left d-none d-sm-flex">
            <span class="counter-wrapper" style="display: none;">
                {{ trans('dtv.base::listview.counter.show') }} <span class="count-from">{{ ( count( $list->data ) == 0 ) ? 0 : 1 }}</span>
                {{ trans('dtv.base::listview.counter.to') }} <span class="count-to">{{ count( $list->data ) }}</span>
                {{ trans('dtv.base::listview.counter.from') }} <span class="count-all">{{ $list->counts->all }}</span>
                <span class="count-filtered" style="display:none">({{ trans('dtv.base::listview.counter.filtered_from') }} <span class="count-filter-all">0</span>)</span>
            </span>
        </div>
        <div class="col-sm-6">
            <nav>
                <ul class="pagination pagination-sm justify-content-center" style="display: none;">
                    <li class="page-item listview-pagination-first disabled">
                        <a class="page-link" title="{{ trans('dtv.base::listview.pagination.first') }}">
                            <i class="far fa-fw fa-angle-double-left"></i>
                        </a>
                    </li>
                    <li class="page-item listview-pagination-prev disabled">
                        <a class="page-link" title="{{ trans('dtv.base::listview.pagination.prev') }}">
                            <i class="far fa-fw fa-angle-left"></i>
                        </a>
                    </li>
                    <li class="page-item listview-pagination-next {{ ( $list->counts->all <= 10 ) ? 'disabled' : '' }}">
                        <a class="page-link" title="{{ trans('dtv.base::listview.pagination.next') }}">
                            <i class="far fa-fw fa-angle-right"></i>
                        </a>
                    </li>
                    <li class="page-item listview-pagination-last {{ ( $list->counts->all <= 10 ) ? 'disabled' : '' }}">
                        <a class="page-link" title="{{ trans('dtv.base::listview.pagination.last') }}">
                            <i class="far fa-fw fa-angle-double-right"></i>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="col-sm-3 text-right d-none d-sm-flex">

        </div>
    </div>
</div>

@push('scripts')
    <script>
        $('#{{ $list->label }}').listview({
            ajax: '{{ $list->url }}',
            json: {
                data: @json($list->data),
                count_all: JSON.parse( {{ $list->count_all }} ),
                count_filtered: JSON.parse( {{ $list->count_all }} ),
            },
            showCounter: JSON.parse( {{ ($list->options->counter) ? 'true' : 'false' }} ),
            showPageLengthSelect: JSON.parse( {{ ($list->options->pageLengthSelect) ? 'true' : 'false' }} ),
            showSearchInput: JSON.parse( {{ ($list->options->searchInput) ? 'true' : 'false' }} ),
            showPagination: JSON.parse( {{ ($list->options->pagination) ? 'true' : 'false' }} ),
            enableSorting: JSON.parse( {{ ($list->options->sorting) ? 'true' : 'false' }} ),
            pageLength: JSON.parse( {{ $list->options->pageLength }} ),
            prevPageLength: JSON.parse( {{ $list->options->pageLength }} ),
            beforeRequest: function ( element , options ) {
                new DTV.LoadingScreen( element , true );
            },
            afterResponse: function ( element , vars , json ) {
                new DTV.LoadingScreen( element , false );
            },
            orderColumn: '{{ $list->sorting->column }}',
            orderDir: '{{ $list->sorting->direction }}',
        });
    </script>
@endpush