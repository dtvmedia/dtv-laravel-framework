@extends( $form->layout )

@section('view-title')
    @if( $form->show_title )
        {!! $form->icon !!} {{ $form->title }}
    @endif
@endsection

@section('view-content')
    <div class="row">
        {{ csrf_field() }}

        @foreach( $form->inputs as $input )
            @if( is_string( $input ) && $input == 'row' )
        </div><div class="row">
            @elseif( $input->getType() == 'custom' )
                <div class="col-md-{{ $input->getSize() }}">
                    {!! $input->html() !!}
                </div>
            @elseif( in_array( $input->getType(), [ 'checkbox', 'datetime', 'hidden' ] ) )
                {!! $input->render() !!}
            @else
                <div class="form-group col-md-{{ $input->getSize() }}">
                    <label class="control-label" for="{{ $input->getId() }}">{{ $input->getLabel() }}</label>
                    @if( $input->description() !== null ) <p>{!! $input->description() !!}</p> @endif

                    {!! $input->render() !!}
                </div>
            @endif
        @endforeach
    </div>
@endsection

@section('view-buttons')
    <button type="submit" class="btn btn-save btn-{{ config( 'dtv.app.button_type' ) }}">{!! icon('fa-save') !!} @lang('dtv.base::general.save')</button>
@endsection

@section('view-scripts')
    <script>
        @if( $form->ajax )
        new DTV.FormView( document.querySelector('#{{ $form->label }}'));
        @endif

        /**
         * Triggert einen Tooltip bei entsprechenden Elementen
         */
        $('[data-toggle="popover"]').popover({
            'html':true,
            'content':function () {
                return `<div class="row"><div class="col-md-1">{!! icon( 'fa-info-circle' , '' , 'fa-lg' ) !!} </div><div class="col-md-10"> ` + $(this).data('hint') + `</div></div>`;
            },
            'template': '<div class="popover form-hint" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
        });
    </script>
@endsection


