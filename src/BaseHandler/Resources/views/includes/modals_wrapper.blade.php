<!-- Ajax View Modals -->
<div class="modal fade" id="ajax_view_modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>

<!-- Ajax Confirm Modals -->
<div class="modal fade mt-5" id="confirm_prompt" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">##modal-title##</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">##modal-body##</div>
            <div class="modal-footer">
                <button data-ajax-action="#" data-ajax-method="DELETE" class="btn btn-save btn-{{ config( 'dtv.app.button_type' ) }}">##modal-button##</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('dtv.base::general.close') }}</button>
            </div>
        </div>
    </div>
</div>