{{-- Inits the JS App and exposes some data to the JS side of things --}}
<script>
    const App = new DTV.App();

    App.setBaseUrl( '{{ url('/') }}' );
    App.setDebugMode( {{ ( env( 'APP_DEBUG', false ) ) ? 'true' : 'false' }} );
    App.setLanguage( '{{ app()->getLocale() }}' );
    App.setTranslations( {!! json_encode( trans('dtv.base::javascript') ) !!} );
    App.setRoutes( @json( $routes ) );
    App.setMaintenanceMode( {{ app()->isDownForMaintenance() ? 'true' : 'false' }} );
    App.setAuthId({{ auth()->id() ?? 'null' }});

    App.init();
</script>