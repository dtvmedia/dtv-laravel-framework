<div class="modal-header">
    <h4 class="modal-title">
        {{-- Todo improve section uniformity --}}
        @yield('title')
        @yield('view-title')
    </h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
    @yield('content')
</div>
<div class="modal-footer">
    @yield('buttons')
    @if( isset($headerButtons) )
        @foreach( $headerButtons as $button )
            {!! $button !!}
        @endforeach
    @endif
    <button type="button" class="btn btn-default" data-dismiss="modal">@lang('dtv.base::general.close')</button>
</div>

@stack('scripts')