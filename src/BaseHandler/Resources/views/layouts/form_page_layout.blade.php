<form action="{{ $form->action }}" id="{{ $form->label }}" class="{{ $form->class }}" method="{{ $form->method }}">
    <div class="form-header">
        @yield('view-title')
    </div>
    <div class="form-body">
        @yield('view-content')
    </div>
    <div class="form-footer">
        @yield('view-buttons')
    </div>
</form>

@push('scripts')
    @yield('view-scripts')
@endpush


