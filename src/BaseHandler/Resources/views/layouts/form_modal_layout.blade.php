<form action="{{ $form->action }}" id="{{ $form->label }}" class="{{ $form->class }}" method="{{ $form->method }}">
    <div class="modal-header">
        <h4 class="modal-title">
            @yield('view-title')
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
        @yield('view-content')
    </div>
    <div class="modal-footer">
        @yield('view-buttons')
        <button type="button" class="btn btn-default" data-dismiss="modal">@lang('dtv.base::general.close')</button>
    </div>
</form>

@yield('view-scripts')