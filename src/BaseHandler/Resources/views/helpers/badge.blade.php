<span class="badge {{ $badge->getClassString() }}" title="{{ $badge->getTitle() }}"
    @foreach( $badge->getAttributes() as $attribute )
        {{ $attribute->key }}={{ $attribute->value }}
    @endforeach
>{!! $badge->getContent() !!}</span>