<div class="dropdown show {{ $dropdown->getWrapperClasses() }}"
    @foreach( $dropdown->getAttributes() as $attribute )
        {{ $attribute->key }}={{ $attribute->value }}
    @endforeach
>
    <a class="btn dropdown-toggle {{ $dropdown->getClasses() }}" href="#" data-toggle="dropdown">
        @if( $dropdown->hasIcon() )
            {!! $dropdown->getIcon()->render() !!}
        @endif
        {{ $dropdown->getLabel() }}
    </a>

    <div class="dropdown-menu">
        @foreach( $dropdown->getItems() as $button )
            <a class="dropdown-item" {{ $button->getLinkType() }}="{{ $button->getUrl() }}" title="{{ $button->getLabel() }}">
            @if( $button->hasIcon() )
            {!! $button->getIcon()->render() !!}
            @endif
            {{ $button->getLabel() }}
            </a>
        @endforeach
    </div>
</div>