<div class="btn-group"
    @foreach( $buttonGroup->getAttributes() as $attribute )
        {{ $attribute->key }}={{ $attribute->value }}
    @endforeach
>
    @foreach( $buttonGroup->getButtons() as $button )
        {!! $button->render() !!}
    @endforeach
</div>