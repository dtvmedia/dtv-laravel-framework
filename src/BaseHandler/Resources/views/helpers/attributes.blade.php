@foreach(($element?->getAttributes() ?? []) as $attribute)
    {{ $attribute->key }}="{{ $attribute->value }}"
@endforeach
