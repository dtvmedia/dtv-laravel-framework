<i class="{{ $icon->getClassString() }}" title="{{ $icon->getTitle() }}"
@foreach( $icon->getAttributes() as $attribute )
    {{ $attribute->key }}="{{ $attribute->value }}"
@endforeach
></i>
@if( $icon->getShowLabel() )
    {{ $icon->getTitle() }}
@endif