<button class="{{ $button->getClassString() }}" data-confirm-method="{{ $button->getMethod() }}"
        data-confirm-action="{{ $button->getUrl() }}"
        type="button"
        title="{{ $button->getLabel() }}"
        data-toggle="tooltip"
        data-placement="right"
        data-ajax="{{ ( $button->isAjax() ) ? '1' : '0' }}"
        @if( $button->hasMessage() ) data-confirm-message="{{ $button->getMessage() }}" @endif
        @if( $button->hasConfirmButtonLabel() ) data-confirm-button="{{ $button->getConfirmButtonLabel() }}" @endif
        @foreach( $button->getAttributes() as $attribute )
            {{ $attribute->key }}={{ $attribute->value }}
        @endforeach
>
    @if( $button->hasIcon() )
        {!! $button->getIcon()->render() !!}
    @endif
    @if( $button->getShowLabel() )
        {{ $button->getLabel() }}
    @endif
</button>