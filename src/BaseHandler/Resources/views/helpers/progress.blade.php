<div class="progress {{ $progress->getClasses() }}" @if( $progress->getHeight() !== null ) style="height: {{ $progress->getHeight() }}px;" @endif
    @foreach( $progress->getAttributes() as $attribute )
        {{ $attribute->key }}={{ $attribute->value }}
    @endforeach
>
    @foreach( $progress->getProgressbars() as $progressbar )
    <div class="progress-bar bg-{{ $progressbar['type'] }}" role="progressbar" style="width: {{ $progressbar['percent'] }}%; min-width: {{ $progressbar['min_width'] }}px;" aria-valuenow="{{ $progressbar['percent'] }}" aria-valuemin="0" aria-valuemax="100">{{ $progressbar['label'] }}</div>
    @endforeach
</div>