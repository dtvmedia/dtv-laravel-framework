<{{ $button->getHtmlBase() }}
        class="{{ $button->getClassString() }}"
@if( $button->getUrl() !== null )
{{ $button->getLinkType() }}="{{ $button->getUrl() }}"
@endif
@foreach( $button->getAttributes() as $attribute )
    {{ $attribute->key }}="{{ $attribute->value }}"
@endforeach
type="button"
title="{{ $button->getLabel() }}"
@if( !$button->getShowLabel() )
data-toggle="tooltip"
data-placement="right"
@endif
>
@if( $button->hasIcon() )
    {!! $button->getIcon()->render() !!}
@endif
@if( $button->getShowLabel() )
    {{ $button->getLabel() }}
@endif
</{{ $button->getHtmlBase() }}>
