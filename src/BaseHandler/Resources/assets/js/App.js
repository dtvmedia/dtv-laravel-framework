'use strict';

var DTV = DTV || {};

/**
 * App class
 *
 * @package   DTV
 * @copyright 2019 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
DTV.App = class {
    static get Selector() {
        return {
            MODAL: '#ajax_view_modal',
            AJAX_ACTION: '[data-ajax-action]',
            CONFIRM_ACTION: '[data-confirm-action]',
            OVERLAY: '.overlay',
        };
    }

    /**
     * Constructor
     */
    constructor() {
        // Singleton handling
        if ( !!DTV.App.instance ) {
            return DTV.App.instance;
        }

        DTV.App.instance = this;

        this.baseUrl = null;
        this.debugMode = true;
        this.maintenance = false;
        this.authId = null;
        this.language = 'en';
        this.translations = {};
        this.routes = {};
    }

    // Debug Mode

    setDebugMode( debugMode ) {
        this.debugMode = !!debugMode;

        return this;
    }

    isDebugMode() {
        return this.debugMode;
    }

    // Maintenance mode

    setMaintenanceMode( maintenance ) {
        this.maintenance = !!maintenance;

        if ( this.isMaintenanceMode() ) {
            new DTV.Message( 'danger', 'Maintenance mode', '' );
        }

        return this;
    }

    isMaintenanceMode() {
        return this.maintenance;
    }

    // Language & Translations

    setLanguage( language ) {
        if ( typeof language !== 'string' || language.length !== 2 ) {
            throw new Error( 'Invalid language code: ' + language );
        }

        this.language = language;

        return this;
    }

    getLanguage() {
        return this.language;
    }

    setTranslations( translations ) {
        this.translations = translations;

        return this;
    }

    translate( string, vars = {} ) {
        if ( typeof string !== 'string' ) {
            throw new Error( 'Invalid translation string: ' + string );
        }

        const parts = string.split( '.' );
        let lang = this.translations;

        for ( let i = 0; i < parts.length; i++ ) {
            if ( lang[ parts[ i ] ] ) {
                lang = lang[ parts[ i ] ];
            }
        }

        if ( typeof lang === 'string' ) {
            for ( let key in vars ) {
                if ( !vars.hasOwnProperty( key ) ) {
                    continue;
                }

                lang = lang.replace( ':' + key.toUpperCase(), vars[ key ] );
            }

            return lang;
        }

        return string;
    }

    // Route Handling

    setBaseUrl( url ) {
        this.baseUrl = url;

        return this;
    }

    setRoutes( routes ) {
        this.routes = routes;

        return this;
    }

    route( name, parameters = {} ) {
        if ( this.routes[ name ] === undefined ) {
            throw new Error( 'Invalid route name given. The route may not be exposed to javascript' );
        }

        let url = this.routes[ name ];

        // route parameter parsing
        url = url.replace( /{([a-zA-Z0-9_]+)}/gm, function ( match, parameter ) {
            parameter = parameter.replace( '?', '' );

            if ( parameters[ parameter ] === undefined ) {
                throw new Error( '"' + parameter + '" key is required for route "' + name + '"' );
            }

            const value = parameters[ parameter ];

            delete parameters[ parameter ];

            return value;
        } );

        // additional query parameter appending
        const otherParams = new URLSearchParams( parameters );
        if ( otherParams.toString().length > 0 ) {
            url = url + '?' + otherParams.toString();
        }

        return this.getUrl( url );
    }

    getUrl( url ) {
        if ( url === null ) {
            url = '';
        }

        // remove slashed at the beginning
        url = url.replace( /^\/+/g, '' );

        return this.baseUrl + '/' + url;
    }

    // Actions

    ajaxAction( element ) {
        const link = element;

        if ( matches( link, ':disabled' ) || link.classList.contains( 'disabled' ) ) {
            return false;
        }

        if ( link.hasAttribute( 'data-ajax-running' ) ) {
            return false;
        }

        const action = link.getAttribute( 'data-ajax-action' );
        let method = link.getAttribute( 'data-ajax-method' );
        let data = link.getAttribute( 'data-ajax-json' );

        link.setAttribute( 'data-ajax-running', true );

        if ( method != null ) {
            method = method.toUpperCase();
            if ( ![ 'GET', 'POST', 'PUT', 'DELETE' ].includes( method ) ) {
                method = 'GET';
            }
        } else {
            method = 'GET';
        }

        new DTV.Fetch( method, action, data )
            .finally( function () {
                setTimeout( function () {
                    link.removeAttribute( 'data-ajax-running' );
                }, 500 );
            } ).send();
    }

    confirmAction( element ) {
        const link = element;

        if ( matches( link, ':disabled' ) || link.classList.contains( 'disabled' ) ) {
            return false;
        }

        const action = link.getAttribute( 'data-confirm-action' );
        const ajaxRequest = link.getAttribute( 'data-ajax' ) || '1';
        const method = link.getAttribute( 'data-confirm-method' ) || 'DELETE';
        const title = link.getAttribute( 'data-confirm-title' ) || trans( 'confirm.execActionQuestion' );
        const message = link.getAttribute( 'data-confirm-message' ) || trans( 'confirm.deleteObjectQuestion' );
        const button = link.getAttribute( 'data-confirm-button' ) || trans( 'confirm.delete' );
        const json = link.getAttribute( 'data-ajax-json' ) || null;

        const prompt_modal = $( '#confirm_prompt' );
        const prompt_btn = prompt_modal.find( '.btn-save' );
        prompt_modal.find( '.modal-title' ).text( title );
        prompt_modal.find( '.modal-body' ).text( message );
        prompt_btn.text( button ).off( 'click' );

        if ( ajaxRequest === '1' ) {
            prompt_btn.attr( 'data-ajax-method', method ).attr( 'data-ajax-action', action ).removeAttr( 'href' );

            if ( json !== null ) {
                prompt_btn.attr( 'data-ajax-json', json );
            }
        } else {
            prompt_btn.attr( 'href', action ).removeAttr( 'data-ajax-action' );
            prompt_btn.on( 'click', function () {
                location.href = $( this ).attr( 'href' );
            } );
        }

        prompt_modal.modal();
    }

    // Auth

    setAuthId( id ) {
        this.authId = id;

        return this;
    }

    isLoggedIn() {
        return this.authId !== null;
    }

    // Event Handling

    _bindDebugEventHandler() {
        /**
         * Removes the form overlay when pressing 'x'. Especially usefull when an error occured while submitting the form and you're
         * stucked in the loading screen
         */
        document.addEventListener( 'keydown', function ( e ) {
            if ( e.key === 'x' ) {
                const overlay = document.querySelector( DTV.App.Selector.OVERLAY );

                if ( overlay !== null ) {
                    overlay.remove();
                }
            }
        } );
    }

    _bindActionEventHandler() {
        document.querySelector( 'body' ).addEventListener( 'click', function ( event ) {
            /**
             * Ajax Actions
             */
            const ajaxActionElement = event.target.closest( DTV.App.Selector.AJAX_ACTION );
            if ( ajaxActionElement !== null ) {
                this.ajaxAction( ajaxActionElement );
            }

            /**
             * Action Confirmation Prompt
             */
            const confirmActionElement = event.target.closest( DTV.App.Selector.CONFIRM_ACTION );
            if ( confirmActionElement !== null ) {
                this.confirmAction( confirmActionElement );
            }
        }.bind( this ) );
    }

    onLoaded( callback, thisPointer = null ) {
        if ( typeof callback !== 'function' ) {
            throw new Error( 'Invalid callback function' );
        }

        if ( thisPointer === null ) {
            thisPointer = this;
        }

        if ( document.readyState !== 'loading' ) {
            callback.call( thisPointer )
        } else if ( document.addEventListener ) {
            document.addEventListener( 'DOMContentLoaded', callback.bind( thisPointer ) );
        }
    }

    init() {
        this.onLoaded( function () {
            DTV.StoredResponseHandler.execAndClear();

            if ( this.isDebugMode() ) {
                this._bindDebugEventHandler();
            }

            this._bindActionEventHandler();

            // Todo Rework in vanilla js
            /**
             * Autofocus first Modal Input
             */
            $( 'body' ).on( 'shown.bs.modal', DTV.App.Selector.MODAL, function () {
                setTimeout( function () {
                    $( DTV.App.Selector.MODAL ).find( 'input:text:visible:first' ).focus();
                }, 50 );
            } );
        } );
    }
};