'use strict';

var DTV = DTV || {};

/**
 * Fetch Wrapper class
 *
 * @package   DTV
 * @copyright 2019 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
DTV.Fetch = class {
    /**
     * Fetch Constructor
     *
     * @param method
     * @param url
     * @param body
     */
    constructor( method, url, body = null ) {
        let params, searchParams;
        method = method.toUpperCase();

        if ( typeof body === 'string' ) {
            body = JSON.parse( body );
        }

        try {
            url = decodeURI( url );
        } catch ( e ) {
            console.error( e );
        }

        if ( [ 'GET', 'HEAD' ].includes( method ) ) {
            if ( url.includes( '?' ) ) {
                [ url, params ] = url.split( '?', 2 );
                searchParams = new URLSearchParams( params );
            } else {
                searchParams = new URLSearchParams();
            }

            for ( let key in body ) {
                if ( !body.hasOwnProperty( key ) ) {
                    continue;
                }

                if ( body[ key ] !== null ) {
                    if ( Array.isArray( body[ key ] ) ) {
                        body[ key ].forEach( function ( item ) {
                            searchParams.append( key + '[]', item );
                        } );
                    } else {
                        searchParams.append( key, body[ key ] );
                    }
                }
            }

            body = null;

            if ( searchParams.toString().length > 0 ) {
                url = url + '?' + searchParams.toString();
            }
        }

        this.url = url;
        this.method = method;
        this.body = null;
        this.thenHandler = DTV.Fetch.DEFAULTS.defaultThenHandler;
        this.catchHandler = DTV.Fetch.DEFAULTS.defaultCatchHandler;
        this.finallyHandler = DTV.Fetch.DEFAULTS.defaultFinallyHandler;
        this.header = clone( DTV.Fetch.DEFAULTS.defaultHeaders );

        if ( body instanceof FormData ) {
            this.body = body;
        } else if ( body === null ) {
            this.body = null;
        } else {
            this.body = JSON.stringify( body );
            this.header[ 'Content-Type' ] = 'application/json';
        }
    }

    /**
     * Sets the then/success handler
     *
     * @param handler
     *
     * @return {DTV.Fetch}
     */
    then( handler ) {
        this.thenHandler = handler;

        return this;
    }

    /**
     * Sets the catch/error handler
     *
     * @param handler
     *
     * @return {DTV.Fetch}
     */
    catch( handler ) {
        this.catchHandler = handler;

        return this;
    }

    /**
     * Sets the finally/always handler
     *
     * @param handler
     *
     * @return {DTV.Fetch}
     */
    finally( handler ) {
        this.finallyHandler = handler;

        return this;
    }

    /**
     * Sends the fetch request
     */
    send() {
        const request = fetch( this.url, {
            method: this.method,
            body: this.body,
            credentials: 'include',
            headers: this.header,
        } ).then( function ( response ) {
            if ( response.ok || [ 401, 404, 422 , 500 ].includes( response.status ) ) {
                if ( response.headers.get( 'Content-Type' ) === 'application/json' ) {
                    return response.json();
                } else {
                    return response.text();
                }
            }

            throw new Error( 'Error (' + response.status + '): ' + response.statusText );
        } );

        if ( this.thenHandler !== null ) {
            request.then( this.thenHandler )
        }

        if ( this.catchHandler !== null ) {
            request.catch( this.catchHandler )
        }

        if ( this.finallyHandler !== null ) {
            request.finally( this.finallyHandler )
        }
    }
};

DTV.Fetch.DEFAULTS = {
    defaultThenHandler: function ( json ) {
        if ( app().isDebugMode() ) {
            console.log( json );
        }

        new DTV.ResponseHandler( json );
    },
    defaultCatchHandler: function ( error ) {
        // Error Message
        new DTV.Message( 'danger', 'Error', error, 10 );
    },
    defaultFinallyHandler: null,
    defaultHeaders: {
        'X-CSRF-TOKEN': document.querySelector( 'meta[name="csrf-token"]' ).getAttribute( 'content' ),
        'X-Requested-With': 'XMLHttpRequest',
    }
};