'use strict';

var DTV = DTV || {};

/**
 * Data Element storage helper
 *
 * @package   DTV
 * @copyright 2019 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 * @type {{set(*, *, *): void, get(*=, *): (null|*), delete(*, *): (undefined)}}
 */
DTV.Data = (function () {
    const storeData = {};
    let id = 1;

    return {
        set( element, key, data ) {
            if ( typeof element.key === 'undefined' ) {
                element.key = {};
            }

            if ( typeof element.key[ key ] === 'undefined' ) {
                element.key[ key ] = id;
                id++
            }

            storeData[ element.key[ key ] ] = data
        },

        get( element, key ) {
            if ( !element || typeof element.key === 'undefined' || typeof element.key[ key ] === 'undefined' ) {
                return null
            }

            if ( typeof storeData[ element.key[ key ] ] !== 'undefined' ) {
                return storeData[ element.key[ key ] ]
            }

            return null
        },

        delete( element, key ) {
            if ( typeof element.key === 'undefined' || typeof element.key[ key ] === 'undefined' ) {
                return
            }

            if ( typeof storeData[ element.key[ key ] ] !== 'undefined' ) {
                delete storeData[ element.key[ key ] ];
                delete element.key
            }
        },

        all() {
            return storeData;
        }
    }
})();