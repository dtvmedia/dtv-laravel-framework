'use strict';

var DTV = DTV || {};

/**
 * Event Dispatcher class
 *
 * @package   DTV
 * @copyright 2019 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
DTV.EventDispatcher = class {
    /**
     * Constructor
     */
    constructor() {
        // Singleton handling
        if ( !!DTV.EventDispatcher.instance ) {
            return DTV.EventDispatcher.instance;
        }

        DTV.EventDispatcher.instance = this;

        this.events = {}
    }

    /**
     * Dispatches the given event
     *
     * @param eventName
     * @param data
     * @param key
     */
    dispatch( eventName, data , key = '*' ) {
        const hash = this._getKey( eventName , key );

        if ( this.events[ hash ] ) {
            this.events[ hash ].fire( data )
        } else {
            console.warn( 'No event handler found for: ' + hash );
        }
    }

    /**
     * Start listen event
     *
     * @param eventName
     * @param handler
     * @param key
     */
    on( eventName, handler, key = '*' ) {
        const hash = this._getKey( eventName , key );

        if ( !this.events[ hash ] ) {
            this.events[ hash ] = new DTV.Event( eventName )
        }

        this.events[ hash ].registerHandler( handler )
    }

    /**
     * Stop listen event
     *
     * @param eventName
     * @param key
     */
    off( eventName , key = '*' ) {
        const hash = this._getKey( eventName , key );

        if ( this.events[ hash ] ) {
            delete this.events[ hash ]
        }
    }

    /**
     * Returns the key for the given event and key name combination
     * @param eventName
     * @param key
     * @returns string
     * @private
     */
    _getKey( eventName , key ) {
        return eventName + '_' + key;
    }
};