'use strict';

var DTV = DTV || {};

/**
 * Validator class
 *
 * @package   DTV
 * @copyright 2019 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
DTV.Validator = class {
    /**
     * Validator Constructor
     *
     * @param scope
     */
    constructor( scope ) {
        if ( typeof scope === 'string' ) {
            scope = document.querySelector( scope );
        }

        this.scope = scope;
        this.inputs = this.scope.querySelectorAll( '[data-validation]' );
        this.valid = true;
        this.error = {
            message: '',
            vars: {}
        };

        this.initEventHandler();
    }

    /**
     * Inits the input event handlers
     */
    initEventHandler() {
        this.inputs.forEach( function ( input ) {
            if ( matches( input, 'select[data-validation]' ) ) {
                // selects
                input.addEventListener( 'change', function ( ev ) {
                    this.validateInput( ev.target );
                }.bind( this ) );
            } else if ( matches( input, '[data-validation]' ) ) {
                // other inputs
                input.addEventListener( 'blur', function ( ev ) {
                    this.validateInput( ev.target );
                }.bind( this ) );
            }
        }, this );
    }

    /**
     * Validates all validatable inputs inside the scope
     *
     * @return {boolean}
     */
    validate() {
        this.valid = true;

        this.inputs.forEach( function ( input ) {
            let validated = this.validateInput( input );

            if ( validated === false ) {
                this.valid = false;
            }
        }, this );

        return this.valid;
    }

    /**
     * Validates an specific input
     *
     * @param input
     *
     * @return {boolean}
     */
    validateInput( input ) {
        let rules = input.getAttribute( 'data-validation' ).split( '|' );
        let nullable = this.isNullable( rules );

        for ( let key in rules ) {
            let rule = this.getRule( rules[ key ] );
            let checkName = 'check' + rule.name.charAt( 0 ).toUpperCase() + rule.name.slice( 1 );
            let check = this[ checkName ];

            if ( typeof check !== 'function' ) {
                continue;
            } else {
                check = check.bind( this );
            }

            const value = new DTV.Input( input ).get();
            if ( nullable === true && (value === null || value === '') ) {
                // is valid --> success
            } else if ( check( input, value, rule.parameter, rules ) === false ) {
                this.setError( input, this.error.message, this.error.vars );

                return false;
            }
        }

        this.setSuccess( input );

        return true;
    }

    /**
     * Determines if the input is nullable or not
     *
     * @param rules
     * @return {boolean}
     */
    isNullable( rules ) {
        let nullable = true;

        for ( let key in rules ) {
            if ( !rules.hasOwnProperty( key ) ) {
                continue;
            }

            if ( rules[ key ] === 'required' ) {
                nullable = false;
                break;
            } else if ( rules[ key ] === 'nullable' ) {
                nullable = true;
                break;
            } else if ( rules[ key ] === 'optional' ) {
                nullable = true;
                break;
            }
        }

        return nullable;
    }

    /**
     * Splits the given string rule into rule name and parameters
     *
     * @param rule
     *
     * @return {{parameter: string | null, name: string}}
     */
    getRule( rule ) {
        const raw = rule.split( ':' );

        return {
            name: raw[ 0 ],
            parameter: raw[ 1 ] || null
        };
    }

    /**
     * Marks an input as successfully validated
     *
     * @param input
     */
    setSuccess( input ) {
        const parent = input.closest( '.has-feedback' );

        if ( parent !== null ) {
            const feedback = parent.querySelector( 'span.feedback' );

            if ( feedback !== null ) {
                feedback.textContent = '';
            }

            parent.classList.remove( 'has-error' );
            parent.classList.add( 'has-success' );
        }
    }

    /**
     * Marks as input as not validated and adds an error hint
     *
     * @param input
     * @param message
     * @param vars
     */
    setError( input, message, vars = {} ) {
        const parent = input.closest( '.has-feedback' );

        if ( parent !== null ) {
            const feedback = parent.querySelector( 'span.feedback' );

            if ( feedback !== null ) {
                feedback.textContent = trans( message, vars );
            }

            parent.classList.remove( 'has-success' );
            parent.classList.add( 'has-error' );
        }
    }

    /**
     * Sets the current error message
     *
     * @param message
     * @param vars
     */
    setCurrentError( message, vars = {} ) {
        this.error = {
            message: 'validation.' + message,
            vars: vars
        }
    }

    /**
     * Checks if the value matches the required rule
     *
     * @param input
     * @param value
     *
     * @return {boolean}
     */
    checkRequired( input, value ) {
        if ( value === '' || value === null ) {
            this.setCurrentError( 'required' );

            return false;
        }

        return true;
    }

    /**
     * Checks if the value matches the numeric rule
     *
     * @param input
     * @param value
     *
     * @return {boolean}
     */
    checkNumeric( input, value ) {
        if ( !isNumeric( value ) ) {
            this.setCurrentError( 'numeric' );

            return false;
        }

        return true;
    }

    /**
     * Checks if the value matches the string rule
     *
     * @param input
     * @param value
     *
     * @return {boolean}
     */
    checkString( input, value ) {
        if ( typeof value !== 'string' ) {
            this.setCurrentError( 'string' );

            return false;
        }

        return true;
    }

    /**
     * Dispatches the the validation request to the correct subcheck
     *
     * @param input
     * @param value
     * @param parameter
     * @param rules
     *
     * @return {boolean}
     */
    checkMin( input, value, parameter, rules ) {
        if ( !isNumeric( parameter ) ) {
            return true;
        }

        if ( input.getAttribute( 'type' ) !== 'text' && matches( input, 'textarea' ) === false ) {
            return true;
        }

        if ( rules.includes( 'numeric' ) ) {
            return this.checkMinNumeric( input, value, parameter );
        }

        return this.checkMinString( input, value, parameter );
    }

    /**
     * Checks if the value matches the min numeric rule with the given parameter
     *
     * @param input
     * @param value
     * @param parameter
     *
     * @return {boolean}
     */
    checkMinNumeric( input, value, parameter ) {
        if ( parseInt( value ) < parameter ) {
            this.setCurrentError( 'min.number', { "MIN": parameter } );

            return false;
        }

        return true;
    }

    /**
     * Checks if the value matches the min string rule with the given parameter
     *
     * @param input
     * @param value
     * @param parameter
     *
     * @return {boolean}
     */
    checkMinString( input, value, parameter ) {
        if ( value.length < parameter ) {
            this.setCurrentError( 'min.string', { "MIN": parameter } );

            return false;
        }

        return true;
    }

    /**
     * Dispatches the the validation request to the correct subcheck
     *
     * @param input
     * @param value
     * @param parameter
     * @param rules
     *
     * @return {boolean}
     */
    checkMax( input, value, parameter, rules ) {
        if ( !isNumeric( parameter ) ) {
            return true;
        }

        if ( input.getAttribute( 'type' ) !== 'text' && matches( input, 'textarea' ) === false ) {
            return true;
        }

        if ( rules.includes( 'numeric' ) ) {
            return this.checkMaxNumeric( input, value, parameter );
        }

        return this.checkMaxString( input, value, parameter );
    }

    /**
     * Checks if the value matches the max numeric rule with the given parameter
     *
     * @param input
     * @param value
     * @param parameter
     *
     * @return {boolean}
     */
    checkMaxNumeric( input, value, parameter ) {
        if ( parseInt( value ) > parameter ) {
            this.setCurrentError( 'max.number', { "MAX": parameter } );

            return false;
        }

        return true;
    }

    /**
     * Checks if the value matches the max string rule with the given parameter
     *
     * @param input
     * @param value
     * @param parameter
     *
     * @return {boolean}
     */
    checkMaxString( input, value, parameter ) {
        if ( value.length > parameter ) {
            this.setCurrentError( 'max.string', { "MAX": parameter } );

            return false;
        }

        return true;
    }
};