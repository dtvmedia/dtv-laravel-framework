'use strict';

var DTV = DTV || {};

/**
 * Response Handler class
 *
 * @package   DTV
 * @copyright 2019 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
DTV.ResponseHandler = class {
    /**
     * Constructor
     *
     * @param response
     */
    constructor( response ) {
        if ( this.handleRedirectsAndReloads( response ) === true ) {
            return;
        }

        this.handleNewRequests( response );
        this.handleMessages( response );
        this.handleModal( response );
        this.handleListViewReloads( response );
        this.handleEvents( response );
        this.handleInputs( response );
    }

    /**
     * Triggers an redirect or an reload with the given data
     *
     * @param data
     *
     * @return {boolean}
     */
    handleRedirectsAndReloads( data ) {
        if ( !data.hasOwnProperty( 'redirect' ) ) {
            return false;
        }

        if ( data.redirect === 'reload' ) {
            data.redirect = location.href.substring( 0, location.href.lastIndexOf( '#' ) );
        }

        this.handleMessages( data );

        DTV.StoredResponseHandler.store( data );

        location.href = data.redirect;

        if ( data.redirect.includes( '#' ) ) {
            // force refresh when using anchors
            location.reload( true );
        }

        return true;
    }

    /**
     * Triggers an new request with the given data
     *
     * @param data
     */
    handleNewRequests( data ) {
        if ( !data.hasOwnProperty( 'newRequest' ) ) {
            return;
        }

        const modal = $( '#ajax_view_modal' );
        const newRequest = new DTV.Fetch( data.newRequest.method, data.newRequest.url );

        // check if modal is already open
        if( modal.hasClass( 'show' ) ) {
            // close previous modal
            modal.modal( 'hide' );
            modal.one('hidden.bs.modal', function (e) {
                newRequest.send();
            })
        } else {
            newRequest.send();
        }
    }

    /**
     * Shows all given messages
     *
     * @param data
     */
    handleMessages( data ) {
        if ( !data.hasOwnProperty( 'message' ) ) {
            return;
        }

        data.message.forEach( function ( msg ) {
            new DTV.Message( msg.mode, msg.title, msg.text, msg.time );
        } );
    }

    /**
     * Handles all defines modal actions
     *
     * @param data
     */
    handleModal( data ) {
        if ( !data.hasOwnProperty( 'modal' ) ) {
            return;
        }

        const prompt_modal = $( '#confirm_prompt' );
        const modal = $( '#ajax_view_modal' );
        modal.find( '.modal-content' ).html( data.modal.html );

        // check if an confirm prompt modal is already open
        if ( prompt_modal.hasClass( 'show' ) ) {
            prompt_modal.modal( 'hide' )
        }

        // if the main modal is already open the show event is not called and therefore we need to manually trigger it here
        if ( modal.hasClass( 'show' ) ) {
            modal.trigger( 'show.bs.modal' );
        }

        if ( data.modal.size != null ) {
            modal.find( '.modal-dialog' ).removeClass( 'modal-xl modal-lg modal-sm modal-md' ).addClass( data.modal.size );
        }

        if ( data.modal.hide && data.modal.hide !== false ) {
            if ( data.modal.hide === 'main' ) {
                modal.modal( 'hide' )
            } else if ( data.modal.hide === 'confirm' ) {
                prompt_modal.modal( 'hide' )
            }
        } else {
            modal.modal();
            modal.trigger( 'shown.bs.modal' );
        }
    }

    /**
     * Reloads all defined listviews
     *
     * @param data
     */
    handleListViewReloads( data ) {
        if ( !data.hasOwnProperty( 'reload_listviews' ) ) {
            return;
        }

        data.reload_listviews.forEach( function ( listview ) {
            $( '#' + listview ).listview( 'reload' );
        } );
    }

    /**
     * Dispatches all given events
     *
     * @param data
     */
    handleEvents( data ) {
        if ( !data.hasOwnProperty( 'events' ) ) {
            return;
        }

        const dispatcher = new DTV.EventDispatcher();

        data.events.forEach( function ( event ) {
            dispatcher.dispatch( event.name, event.parameter , event.key );
        } );
    }

    /**
     * Handles all input updates
     *
     * @param data
     */
    handleInputs( data ) {
        if ( !data.hasOwnProperty( 'inputs' ) ) {
            return;
        }

        for ( let key in data.inputs ) {
            if ( !data.inputs.hasOwnProperty( key ) ) {
                continue;
            }

            let input = document.querySelector( '#' + key );
            const inputChanges = data.inputs[ key ];

            if ( input === null ) {
                continue;
            }

            new DTV.Input( input ).set( inputChanges.value , inputChanges.trigger );
        }
    }
};