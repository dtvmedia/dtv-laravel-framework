'use strict';

var DTV = DTV || {};

/**
 * Stored Response Handler class
 *
 * @package   DTV
 * @copyright 2019 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
DTV.StoredResponseHandler = class {
    /**
     * Stores an json response in the session storage
     *
     * @param response
     */
    static store( response ) {
        if ( response.hasOwnProperty( 'message' ) ) {
            sessionStorage.messages = JSON.stringify( response.message );
        }

        if ( response.hasOwnProperty( 'callback' ) ) {
            sessionStorage.callback = JSON.stringify( response.callback );
        }

        if ( response.hasOwnProperty( 'events' ) ) {
            sessionStorage.events = JSON.stringify( response.events );
        }

        if ( response.hasOwnProperty( 'newRequest' ) ) {
            sessionStorage.newRequest = JSON.stringify( response.newRequest );
        }
    }

    /**
     * Executes and clears an stored response
     */
    static execAndClear() {
        let data = {};

        // Show Session stored Messages
        if ( sessionStorage.hasOwnProperty( 'messages' ) ) {
            data.message = JSON.parse( sessionStorage.messages );
        }

        // Execute saved Callbacks
        if ( sessionStorage.hasOwnProperty( 'callback' ) ) {
            data.callback = JSON.parse( sessionStorage.callback );
        }

        // Fire of saved events
        if ( sessionStorage.hasOwnProperty( 'events' ) ) {
            data.events = JSON.parse( sessionStorage.events );
        }

        // Trigger for new request
        if ( sessionStorage.hasOwnProperty( 'newRequest' ) ) {
            data.newRequest = JSON.parse( sessionStorage.newRequest );
        }

        if ( Object.entries( data ).length > 0 ) {
            new DTV.ResponseHandler( data );
        }

        sessionStorage.clear();
    }
};