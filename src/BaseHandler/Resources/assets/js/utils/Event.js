'use strict';

var DTV = DTV || {};

/**
 * Event class
 *
 * @package   DTV
 * @copyright 2019 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
DTV.Event = class {
    /**
     * Constructor
     *
     * @param eventName
     */
    constructor( eventName ) {
        this.eventName = eventName;
        this.handlers = []
    }

    /**
     * Registers an event handler
     *
     * @param handler
     */
    registerHandler( handler ) {
        this.handlers.push( handler );
    }

    /**
     * Fire offs the event and executes all registered handlers
     *
     * @param data
     */
    fire( data ) {
        this.handlers.forEach( (handler) => {
            handler( data )
        } );
    }
};