/**
 * Framework helper functions
 *
 * @package   -
 * @copyright 2019 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */

/**
 * Returns the app instance
 *
 * @return {*}
 */
function app() {
    return new DTV.App();
}

/**
 * Returns the full url of the given relative path
 *
 * @param url
 *
 * @return {*}
 */
function url( url = null ) {
    return app().getUrl( url );
}

/**
 * Returns the full url by a given route name and its parameters
 *
 * @param name
 * @param parameters
 *
 * @return {*}
 */
function route( name, parameters = {} ) {
    return app().route( name, parameters );
}

/**
 * Tries to find the translation of a string
 *
 * @param string
 * @param vars
 *
 * @return {string|*}
 */
function trans( string, vars ) {
    return app().translate( string, vars );
}

/**
 * Creates an html element by an given template
 *
 * @param template
 * @param first
 *
 * @return {Element|Node|HTMLCollection}
 */
function template( template, first = true ) {
    const collection = new DOMParser().parseFromString( template, 'text/html' ).body.children;

    if ( first === true ) {
        return collection[ 0 ];
    }

    return collection;
}

/**
 * Simple is mobile device check function (looks for touch events)
 *
 * @return {boolean}
 */
function isMobileDevice() {
    return ('ontouchstart' in window || typeof navigator.msMaxTouchPoints !== 'undefined');
}

/**
 * Checks if an given object or element is from type jquery or not
 *
 * @param object
 *
 * @return {boolean}
 */
function isJquery( object ) {
    try {
        if ( object instanceof jQuery ) {
            return true;
        }
    } catch ( e ) {
    }

    return false;
}

/**
 * Checks if the given value is numeric or not
 *
 * @param value
 *
 * @return {boolean}
 */
function isNumeric( value ) {
    return !isNaN( parseFloat( value ) ) && isFinite( value )
}

/**
 * Checks if the given element is visible or not
 *
 * @param element
 *
 * @return {boolean}
 */
function isVisible( element ) {
    element = getElement( element );

    return !!(element.offsetWidth || element.offsetHeight || element.getClientRects().length);
}

/**
 * Checks if the given element matches with the selector
 *
 * @param element
 * @param selector
 *
 * @return {boolean | *}
 */
function matches( element, selector ) {
    element = getElement( element );

    if ( element === null || element === undefined ) {
        return false;
    }

    const func = element.matches || element.matchesSelector || element.msMatchesSelector || element.mozMatchesSelector || element.webkitMatchesSelector || element.oMatchesSelector;

    return func.call( element, selector );
}

/**
 * Checks if a given value is considered empty or not
 *
 * @param value
 *
 * @return {boolean}
 */
function empty( value ) {
    if ( value === null || value === false || value === undefined ) {
        return true;
    }

    if ( Array.isArray( value ) ) {
        return value.length === 0;
    }

    if ( typeof value === 'string' ) {
        return value.length === 0;
    }

    if ( typeof value === 'number' ) {
        return value === 0;
    }

    if ( typeof value === 'object' ) {
        return Object.keys( value ).length === 0;
    }

    return false;
}

/**
 * Clones an object or array in order to remove the call by reference behaviour
 *
 * @param object
 *
 * @returns {any}
 */
function clone( object ) {
    return JSON.parse( JSON.stringify( object ) );
}

/**
 * Returns an element
 *
 * @param element
 *
 * @return {null|Element|*}
 */
function getElement( element ) {
    if ( isJquery( element ) ) {
        return element[ 0 ];
    }

    if ( typeof element === 'string' ) {
        return document.querySelector( element );
    }

    if ( element instanceof Element ) {
        return element;
    }

    return null;
}

/**
 * Returns the base64 encoded SHA-256 hash of an given input
 * @param str
 * @param algorithm
 * @returns {Promise<string>}
 */
async function hash( str, algorithm = 'SHA-256' ) {
    const data = new TextEncoder().encode( str );
    const hashString = await crypto.subtle.digest( 'SHA-256', data );

    return btoa( String.fromCharCode( ...new Uint8Array( hashString ) ) );
}