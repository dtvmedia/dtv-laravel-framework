'use strict';

var DTV = DTV || {};

/**
 * CustomFormView class
 *
 * @package   DTV
 * @copyright 2019 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
DTV.CustomFormView = class extends DTV.BaseFormView {
    /**
     * Custom Form Constructor
     *
     * @param form
     * @param createMode
     * @param button
     */
    constructor( form, createMode = false, button = null ) {
        super( form );

        this.createMode = createMode;
        this.inputs = this.form.querySelectorAll( 'input, textarea, select' );
        this.button = getElement( button );
        this.valueCache = null;
        this.loadingScreen = null;

        // Init Form Inputs
        if ( this.createMode === true ) {
            this.initCreateMode();
        } else {
            this.initEditMode();
        }
    }

    /**
     * Inits the form inputs and binds the event handlers for the create mode
     */
    initCreateMode() {
        new DTV.FormView( this.form, this.button, true );
    }

    /**
     * On focus event handler for the edit mode
     *
     * @param e
     */
    onEditFocus( e ) {
        const input = e.target;

        DTV.CustomFormView.enableName( input );

        if ( matches( input, '[type="password"]' ) ) {
            input.value = '';
        }

        let value = input.value;

        if ( matches( input, 'select[multiple]' ) ) {
            value = Array.from( input.querySelectorAll( 'option:checked' ) ).map( el => el.value );
        }

        this.valueCache = value;
    }

    /**
     * On change event handler for the edit mode
     *
     * @param e
     */
    onEditChange( e ) {
        if ( e.detail === undefined || typeof e.detail !== 'object' ) {
            e.detail = {};
        }
        e.detail.auto = e.detail.auto ?? false;

        this.onEditFocus( e );

        const input = e.target;

        try {
            if ( input.classList.contains( 'checkbox-nullable' ) ) {
                this.submitForm( input.getAttribute( 'data-value-id' ), function ( data ) {
                    const textCheckbox = document.querySelector( '#' + input.getAttribute( 'data-value-id' ) );
                    data.delete( input.getAttribute( 'name' ) );
                    data.set( textCheckbox.getAttribute( 'data-name' ), textCheckbox.value );

                    return data;
                } , e.detail.auto );
            } else {
                this.submitForm( input.getAttribute( 'name' ) , null , e.detail.auto );
            }
        } finally {
            DTV.CustomFormView.disableName( input );
        }
    }

    /**
     * On blur event handler for the edit mode for checkboxes and file inputs
     *
     * @param e
     */
    onEditBlurAfterChange( e ) {
        const input = e.target;

        DTV.CustomFormView.disableName( input );
    }

    /**
     * On blur event handler for the edit mode
     *
     * @param e
     *
     * @return {boolean}
     */
    onEditBlur( e ) {
        if ( e.detail === undefined || typeof e.detail !== 'object' ) {
            e.detail = {};
        }
        e.detail.auto = e.detail.auto ?? false;

        const input = e.target;
        let value = input.value;

        if ( matches( input, 'select[multiple]' ) ) {
            value = Array.from( input.querySelectorAll( 'option:checked' ) ).map( el => el.value );
        }

        try {
            // client side input check if validations rules exists
            if ( matches( input, '[data-validation]' ) && this.validator.validateInput( input ) === false ) {
                console.log( 'VALIDATION FAIL' );
                throw false;
            }

            // Check if something changed
            if ( Array.isArray( value ) ) {
                // multiple select handling
                const valuesAreTheSame = (value.length === this.valueCache.length) && value.every( function ( element, index ) {
                    return element === this.valueCache[ index ];
                }.bind( this ) );

                if ( valuesAreTheSame === true ) {
                    console.log( 'SAME VALUE AS BEFORE' );
                    throw false;
                }
            } else {
                if ( this.valueCache === value ) {
                    console.log( 'SAME VALUE AS BEFORE' );
                    throw false;
                }
            }

            // Submit form
            this.submitForm( input.getAttribute( 'name' ) , null , e.detail.auto );
        } catch ( e ) {
            if ( e === false ) {
                if ( matches( input, '[type="password"]' ) && input.value === '' ) {
                    input.value = '••••••••••';
                }

                return false;
            }

            throw e;
        } finally {
            DTV.CustomFormView.disableName( input );
        }
    }

    /**
     * Submits the form
     *
     * @param changed
     * @param mutator
     * @param autoTriggered
     */
    submitForm( changed = null, mutator = null , autoTriggered = false ) {
        // Set Loading Screen
        this.loadingScreen = new DTV.LoadingScreen( this.form.parentElement, true );

        // Build Form Data
        let data = new FormData( this.form );
        data.set( '_changed', changed );
        data.set( '_auto_triggered', JSON.stringify( (autoTriggered === true) ) );

        if ( mutator !== null && typeof mutator === 'function' ) {
            data = mutator( data );
        }

        // Sends submit request
        new DTV.Fetch( this.method, this.action, data )
            .then( function ( response ) {
                this.loadingScreen.setStatus( false );
                new DTV.ResponseHandler( response );
            }.bind( this ) )
            .send();
    }

    /**
     * Inits the form inputs and binds the event handlers for the edit mode
     */
    initEditMode() {
        this.inputs.forEach( function ( input ) {
            DTV.CustomFormView.disableName( input );

            if ( $( input ).is( 'select' ) ) {
                $( input ).bind( 'show.bs.select', this.onEditFocus.bind( this ) );
                $( input ).bind( 'hidden.bs.select', this.onEditBlur.bind( this ) );
            } else if ( $( input ).is( '[type="checkbox"]' ) || $( input ).is( '[type="file"]' ) || $( input ).is( '[type="date"]' ) || $( input ).is( '[type="time"]' ) ) {
                $( input ).bind( 'change', this.onEditChange.bind( this ) );
                $( input ).bind( 'blur', this.onEditBlurAfterChange.bind( this ) );
            } else if ( $( input ).is( '[type="color"]' ) ) {
                $( input ).bind( 'focus', this.onEditFocus.bind( this ) );
                $( input ).bind( 'change', this.onEditBlur.bind( this ) );
            } else {
                $( input ).bind( 'focus', this.onEditFocus.bind( this ) );
                $( input ).bind( 'blur', this.onEditBlur.bind( this ) );
            }
        }.bind( this ) );

        this.preventSubmitOnEnter();
    };

    /**
     * Adds keypress event handlers to all inputs which prevent the auto submit when pressing enter
     */
    preventSubmitOnEnter() {
        this.inputs.forEach( function ( input ) {
            if ( matches( input, 'textarea' ) ) {
                return;
            }

            input.addEventListener( 'keydown', function ( e ) {
                if ( e.which === 13 ) {
                    e.preventDefault();
                    return false;
                }
            }.bind( this ) );
        }.bind( this ) );
    }

    /**
     * Enables the name of the given input in order to enable it form the submit
     *
     * @param input
     */
    static enableName( input ) {
        const name = input.getAttribute( 'data-name' );

        if ( matches( input, '[type="checkbox"]' ) && name.includes( '[]' ) ) {
            $( '[data-name="' + name + '"]' ).attr( "name", name );

            return;
        }

        input.setAttribute( "name", name );
    };

    /**
     * Disables the name of the given input in order to disable it form the submit
     *
     * @param input
     */
    static disableName( input ) {
        const name = input.getAttribute( 'name' ) || input.getAttribute( 'data-name' );

        if ( matches( input, '[type="checkbox"]' ) && name.includes( '[]' ) ) {
            $( '[name="' + name + '"]' ).removeAttr( "name" ).attr( "data-name", name );

            return;
        }

        if ( input.classList.contains( 'checkbox-nullable' ) ) {
            DTV.CustomFormView.disableName( document.querySelector( '#' + input.getAttribute( 'data-value-id' ) ) );
        }

        input.removeAttribute( 'name' );
        input.setAttribute( 'data-name', name );
    };
};
