'use strict';

var DTV = DTV || {};

/**
 * BaseFormView class
 *
 * @package   DTV
 * @copyright 2019 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
DTV.BaseFormView = class {
    /**
     * Base form view constructor
     * @param form
     * @param noInit
     */
    constructor( form, noInit = false ) {
        this.form = getElement( form );
        this.validator = new DTV.Validator( this.form );
        this.action = this.form.getAttribute( 'action' );
        this.method = this.form.getAttribute( 'method' );

        if ( noInit === false ) {
            this.initForms();
        }
    }

    /**
     * Inits all custom Form Elements
     */
    initForms() {
        this.initSelects();
        this.initDecimalInputs();
        this.initNullableIconCheckboxes();
        this.initDynamicTextareas();
    }

    /**
     * Inits Bootstrap Selects
     */
    initSelects() {
        $( 'select.form-select' ).selectpicker( {
            iconBase: 'fa',
            tickIcon: 'fa-check',
            size: 10,
            noneSelectedText: trans( 'select.noneSelectedText' ),
            noneResultsText: trans( 'select.noneResultsText' ),
            selectAllText: trans( 'select.selectAllText' ),
            deselectAllText: trans( 'select.deselectAllText' ),
        } );
    }

    /**
     * Inits Money Inputs
     */
    initDecimalInputs() {
        this.form.querySelectorAll( 'input[type="decimal"]' ).forEach( function ( input ) {
            new DTV.DecimalInput( input );
        } );
    }

    /**
     * Inits Nullable Icon checkboxes
     */
    initNullableIconCheckboxes() {
        this.form.querySelectorAll( 'input.checkbox-nullable' ).forEach( function ( input ) {
            input.addEventListener( 'change', function ( e ) {
                if ( e.detail !== undefined && e.detail.auto !== undefined && e.detail.auto === true ) {
                    // if the event was triggered automatically (e.g. via setInput) dont change the value
                    return;
                }

                const textCheckbox = document.querySelector( '#' + input.getAttribute( 'data-value-id' ) );

                // Value determination
                if ( input.getAttribute( 'data-indeterminate' ) === 'false' && input.checked === true ) {
                    textCheckbox.value = null;
                } else if ( input.checked === true ) {
                    textCheckbox.value = textCheckbox.getAttribute( 'data-checked-value' );
                } else {
                    textCheckbox.value = 0;
                }

                // 3 State Handling
                if ( this.getAttribute( 'data-indeterminate' ) === 'true' && this.checked === true ) {
                    this.setAttribute( 'data-indeterminate', 'false' );
                } else if ( this.getAttribute( 'data-indeterminate' ) === 'false' && this.checked === true ) {
                    this.checked = false;
                    this.indeterminate = true;
                    this.setAttribute( 'data-indeterminate', 'true' );
                }
            } );
        } );

        this.form.querySelectorAll( 'input[data-indeterminate="true"]' ).forEach( function ( input ) {
            input.indeterminate = true;
        } );
    }

    /**
     * Inits dynamic textareas
     */
    initDynamicTextareas() {
        const handler = function ( event ) {
            const obj = event.target;

            if ( isVisible( obj ) === false ) {
                setTimeout( function () {
                    handler( event )
                }, 100 );
            }

            let scroll;
            const modal = obj.closest( '.modal' );
            if ( modal === null ) {
                scroll = window.scrollY;
            } else {
                scroll = modal.scrollTop;
            }

            obj.style.height = "1px";
            obj.style.height = ( obj.scrollHeight ) + "px";

            if ( modal === null ) {
                window.scrollTo( 0, scroll );
            } else {
                modal.scrollTop = scroll;
            }
        };

        this.form.querySelectorAll( 'textarea.dynamic-height' ).forEach( function ( input ) {
            handler({target: input});
            input.removeEventListener( 'keyup', handler );
            input.removeEventListener( 'change', handler );
            input.addEventListener( 'keyup', handler );
            input.addEventListener( 'change', handler );
        } );
    }
};