'use strict';

var DTV = DTV || {};

/**
 * FormView class
 *
 * @package   DTV
 * @copyright 2019 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
DTV.FormView = class extends DTV.BaseFormView {
    /**
     * FormView Constructor
     *
     * @param form
     * @param button
     * @param noInit
     */
    constructor( form, button = null , noInit = false ) {
        super( form , noInit );

        this.inputs = this.form.querySelectorAll( 'input' );
        this.button = getElement( button ) || this.form.querySelector( 'button[type=submit]' );
        this.loadingScreen = null;

        this.initSubmitButton();
        this.preventSubmitOnEnter();
    }

    /**
     * Initializes the submit button
     */
    initSubmitButton() {
        if ( this.button.getAttribute( 'type' ) === 'submit' ) {
            this.button.setAttribute( 'type', 'button' );
        }

        this.button.addEventListener( 'click', this.onSubmitHandler.bind( this ) );
    }

    /**
     * Adds keypress event handlers to all inputs which prevent the auto submit when pressing enter
     */
    preventSubmitOnEnter() {
        this.inputs.forEach( function ( input ) {
            if ( matches( input, 'textarea' ) ) {
                return;
            }

            input.addEventListener( 'keydown', function ( e ) {
                if ( e.which === 13 ) {
                    e.preventDefault();
                    this.button.dispatchEvent( new MouseEvent( 'click', {
                        bubbles: true,
                        cancelable: true,
                        view: window
                    } ) );

                    return false;
                }
            }.bind( this ) );
        }.bind( this ) );
    }

    /**
     * On form submit event handler
     *
     * @return {boolean}
     */
    onSubmitHandler() {
        // Todo check if neccesarry
        // Pull the form object again cause in some circumstances (e.g. with VueJS the form is not up2date)
        this.form = document.querySelector( '#' + this.form.getAttribute( 'id' ) );

        // client side input check
        if ( this.validator.validate() === false ) {
            return false;
        }

        // Set Loading Screen
        this.loadingScreen = new DTV.LoadingScreen( this.form.parentElement, true );

        // Submit form
        new DTV.Fetch( this.method, this.action, new FormData( this.form ) )
            .then( function ( response ) {
                this.loadingScreen.setStatus( false );
                new DTV.ResponseHandler( response );
            }.bind( this ) )
            .send();
    }
};