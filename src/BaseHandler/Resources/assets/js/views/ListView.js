(function ( $ ) {
    /**
     * DTV List View jQuery Widget
     */
    $.widget( "dtv.listview", {
        /**
         * Widget Options
         */
        options: {
            // general options
            ajax: false,
            json: {
                data: [],
                count_all: 0,
                count_filtered: 0,
            },
            showCounter: true,
            showPageLengthSelect: true,
            showSearchInput: true,
            showPagination: true,
            enableSorting: true,
            onError: function onError( error ) {
                console.error( error );
            },
            beforeRequest: function beforeRequest( element, options ) {},
            afterResponse: function afterResponse( element, options, json ) {},
            noResultRowHtml: null,

            // List View Filter Vars
            page: 1,
            prevPageLength: 10,
            pageLength: 10,
            prevSearch: null,
            paginationItems: 10,
            search: null,
            orderColumn: 'id',
            orderDir: 'asc',
        },

        /**
         * List View Constructor
         * @private
         */
        _create: function () {
            const _this = this;

            // Setup Pagination
            if ( this.options.showPagination === true ) {
                this.element.find( '.pagination' ).css( 'display', 'flex' );
                this.element.find( '.listview-pagination-first' ).find( 'a' ).click( function () {
                    _this.options.page = 1;
                    _this._sendRequest();
                } );
                this.element.find( '.listview-pagination-prev' ).find( 'a' ).click( function () {
                    _this.options.page--;
                    _this._sendRequest();
                } );
                this.element.find( '.listview-pagination-next' ).find( 'a' ).click( function () {
                    _this.options.page++;
                    _this._sendRequest();
                } );
                this.element.find( '.listview-pagination-last' ).find( 'a' ).click( function () {
                    const all = _this.options.json.count_all;
                    _this.options.page = Math.ceil( all / _this.options.pageLength );
                    _this._sendRequest();
                } );
                this._updatePagination( this.options.json );
            }

            // Setup Sorting
            if ( this.options.enableSorting === true ) {
                const thead = this.element.find( 'thead' );

                thead.find('.sortable').click(function () {
                    const newSortColumn = $(this).data('field');

                    if( _this.options.orderColumn !== newSortColumn || _this.options.orderDir !== 'asc' ) {
                        _this.options.orderDir = 'asc';
                    } else {
                        _this.options.orderDir = 'desc';
                    }

                    _this.options.orderColumn = newSortColumn;
                    _this._sendRequest();
                });
            } else {
                this.element.find( '.sortable' ).removeClass( 'sortable' );
            }

            // Init Sorting indicators (always active)
            this._updateSorting();

            // Setup Page Length Select
            if ( this.options.showPageLengthSelect === true ) {
                this.element.find( '.listview-pagelength-select' ).val( this.options.pageLength ).css( 'display', 'inline-block' ).change( function () {
                    _this.options.pageLength = $( this ).val();
                    _this._sendRequest();
                } );
            }

            // Setup Search
            if ( this.options.showSearchInput === true ) {
                let search_request_wait_timer = null;
                this.element.find( '.listview-search' ).css( 'display', 'inline-block' ).keyup( function () {
                    _this.options.search = $( this ).val();

                    clearTimeout( search_request_wait_timer );
                    search_request_wait_timer = setTimeout( function () {
                        _this._sendRequest();
                    }, 1000 );
                } );
            }

            // Setup Count
            if ( this.options.showCounter === true ) {
                this.element.find( '.counter-wrapper' ).css( 'display', 'inline' );
            }

            // Get No Result Row Content
            if( this.options.noResultRowHtml === null ) {
                this.options.noResultRowHtml = this.element.find('.listview-no-results-row').html();
            }
        },

        /**
         * Resends the last request and reloads the listview
         */
        reload: function () {
            this._sendRequest();
        },

        /**
         * Sends an Ajax Request
         * @returns {boolean}
         */
        _sendRequest: function () {
            const _this = this;

            if ( this.options.ajax === false || this.options.ajax === null ) {
                return false;
            }

            // Before Request Callback
            this.options.beforeRequest( this.element, this.options );

            // Send Ajax Request
            new DTV.Fetch( 'GET', this.options.ajax, this._getFilters() )
                .then( function ( json ) {
                    // Save response data
                    _this.options.json = json;

                    // After Response Callback
                    _this.options.afterResponse( _this.element, _this.options, json );

                    // Redraw Table
                    _this._render( json );
                } ).catch( this.options.onError )
                .send();

            return true;
        },

        /**
         * Returns the object with the filter params
         * @returns {*}
         */
        _getFilters: function () {
            if ( this.options.search !== this.options.prevSearch ) {
                // if the search key differs from the previous one the page will be reseted
                this.options.prevSearch = this.options.search;
                this.options.page = 1;
            }

            if ( this.options.pageLength !== this.options.prevPageLength ) {
                // if the page length differs from the previous one the page will be recalculated
                const show = (this.options.page - 1) * this.options.prevPageLength + 1;

                this.options.page = Math.ceil( show / this.options.pageLength );
                this.options.prevPageLength = this.options.pageLength;
            }

            return {
                'lvName': this.element.attr( 'id' ),
                'lvPage': this.options.page,
                'lvPageLength': this.options.pageLength,
                'lvSearch': this.options.search,
                'lvOrderColumn': this.options.orderColumn,
                'lvOrderDirection': this.options.orderDir
            };
        },

        /**
         * Renders the given dataset and updates all widgets
         * @param json
         */
        _render: function ( json ) {
            // Update Table
            this._updateTable( json );

            // Update counts
            if ( this.options.showCounter === true ) {
                this._updateCounts( json );
            }

            // Update Pagination
            if ( this.options.showPagination === true ) {
                this._updatePagination( json );
            }

            // Update Sorting
            if( this.options.enableSorting === true ) {
                this._updateSorting();
            }
        },

        /**
         * Updates the table with the new dataset
         * @param json
         */
        _updateTable: function ( json ) {
            let i = 0;
            const thead = this.element.find( 'thead' );
            const tbody = this.element.find( 'tbody' );

            // get the header rows cells
            const headerCells = thead.find( 'th' );

            // Clear previous draw
            tbody.empty();

            // Add new dataset
            if ( json.data.length > 0 ) {
                json.data.forEach( function ( row ) {
                    i = 0;
                    let rowHtml = '<tr';

                    $.each( row.lvMeta, function ( index, cell ) {
                        rowHtml += ' data-' + index + '="' + cell + '" ';
                    } );
                    rowHtml += '>';

                    $.each( row, function ( index, cell ) {
                        if ( index === 'lvMeta' ) {
                            return;
                        }

                        let classes = headerCells.eq( i ).attr( 'class' );

                        classes = classes === undefined ? '' : 'class="' + classes + '"';
                        rowHtml += '<td ' + classes + '>' + cell + '</td>';

                        i++;
                    } );

                    tbody.append( rowHtml + '</tr>' );
                } );
            } else {
                // display no results hint row
                tbody.append( '<tr>' + this.options.noResultRowHtml + '</tr>' );
            }
        },

        /**
         * Update the row count display
         * @param json
         */
        _updateCounts: function ( json ) {
            const from = (this.options.page - 1) * this.options.pageLength;
            this.element.find( '.count-from' ).text( json.data.length === 0 ? 0 : from + 1 );
            this.element.find( '.count-to' ).text( from + json.data.length );

            if ( json.count_filtered === null ) {
                this.element.find( '.count-all' ).text( json.count_all );
                this.element.find( '.count-filtered' ).css( 'display', 'none' );
            } else {
                this.element.find( '.count-all' ).text( json.count_filtered );
                this.element.find( '.count-filter-all' ).text( json.count_all );
                this.element.find( '.count-filtered' ).css( 'display', 'inline' );
            }
        },

        /**
         * Update the pagination states
         * @param json
         */
        _updatePagination: function ( json ) {
            const from = (this.options.page - 1) * this.options.pageLength;
            const first = this.element.find( '.listview-pagination-first' );
            const prev = this.element.find( '.listview-pagination-prev' );
            const next = this.element.find( '.listview-pagination-next' );
            const last = this.element.find( '.listview-pagination-last' );

            if ( this.options.page <= 1 ) {
                prev.addClass( 'disabled' );
                first.addClass( 'disabled' );
            } else {
                prev.removeClass( 'disabled' );
                first.removeClass( 'disabled' );
            }

            const all = json.count_filtered === null ? json.count_all : json.count_filtered;
            if ( from + json.data.length >= all ) {
                next.addClass( 'disabled' );
                last.addClass( 'disabled' );
            } else {
                next.removeClass( 'disabled' );
                last.removeClass( 'disabled' );
            }

            this._generatePaginationItems( json );
        },

        /**
         * Generates the page pagination items
         * @param json
         * @private
         */
        _generatePaginationItems: function ( json ) {
            const page = this.options.page;
            const itemsCount = this.options.paginationItems - 4;
            const all = json.count_filtered === null ? json.count_all : json.count_filtered;
            const pages = Math.ceil( all / this.options.pageLength );

            let maxItemsLeft = Math.floor( (itemsCount - 1) / 2 );
            let maxItemsRight = itemsCount - 1 - maxItemsLeft;

            let itemsLeft = Math.min( page - 1, maxItemsLeft );
            if ( itemsLeft !== maxItemsLeft ) {
                maxItemsRight += maxItemsLeft - itemsLeft;
            }

            let itemsRight = Math.min( pages - page, maxItemsRight );
            if ( itemsRight !== maxItemsRight ) {
                maxItemsLeft += maxItemsRight - itemsRight;
            }
            itemsLeft = Math.min( page - 1, maxItemsLeft );

            let items = [];
            for ( let i = 1; page - i > 0 && i <= itemsLeft; i++ ) {
                items.push( page - i );

                if ( i > maxItemsLeft ) {
                    break;
                }
            }

            items = items.reverse();
            items.push( page );

            for ( let i = 1; page + i <= pages && i <= itemsRight; i++ ) {
                items.push( page + i );

                if ( i > maxItemsRight ) {
                    break;
                }
            }

            const _this = this;
            this.element.find( '.listview-pagination-page' ).remove();
            const next = this.element.find( '.listview-pagination-next' );

            items.forEach( function ( item ) {
                let paginationItem = $( '<li class="page-item listview-pagination-page ' + ((page === item) ? 'active' : '') + '"><a class="page-link" data-page="' + item + '">' + item + '</a></li>' );

                paginationItem.find( 'a' ).click( function () {
                    _this.options.page = $( this ).data( 'page' );
                    _this._sendRequest();
                } );

                next.before( paginationItem );
            } );
        },

        /**
         * Updates the sorting indicators in the table header
         * @private
         */
        _updateSorting: function () {
            const thead = this.element.find( 'thead' );

            // Remove previous sorting indicator
            thead.find( '[data-sort]' ).removeAttr( 'data-sort' );

            // add new sorting indicator
            thead.find( '.field-' + this.options.orderColumn.replace( '.', '_' ).toLowerCase() ).attr( 'data-sort', this.options.orderDir );
        }

    } );

}( jQuery ));