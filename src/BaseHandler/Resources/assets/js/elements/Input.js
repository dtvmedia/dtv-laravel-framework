'use strict';

var DTV = DTV || {};

/**
 * Input class
 *
 * @package   DTV
 * @copyright 2019 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
DTV.Input = class {
    /**
     * Data Key Getter
     *
     * @return {string}
     */
    static get DATA_KEY() {
        return 'DTV.Input';
    }

    /**
     * Constructor
     *
     * @param selector
     */
    constructor( selector ) {
        this.input = getElement( selector );

        if ( !this.isValidInput() ) {
            throw new Error( 'Invalid input "' + selector + '" given.' )
        }

        const instance = DTV.Input.instance( this.input );
        if ( instance !== null ) {
            return instance;
        }

        DTV.Data.set( this.input, this.constructor.DATA_KEY, this );
    }

    /**
     * Returns if the given input is valid or not
     *
     * @return {boolean}
     */
    isValidInput() {
        if ( this.input === null ) {
            return false;
        }

        return matches( this.input, 'input, textarea, select' );
    }

    /**
     * Returns if the given input is a select or not
     *
     * @return {boolean}
     */
    isSelect() {
        return matches( this.input, 'select' );
    }

    /**
     * Returns if the given input is a checkbox or not
     *
     * @return {boolean}
     */
    isCheckbox() {
        return matches( this.input, 'input[type="checkbox"]' );
    }

    /**
     * Returns if the given input is a nullable select or not
     *
     * @return {boolean}
     */
    isNullableCheckbox() {
        const checkbox = getElement( '#' + this.input.id + '_checkbox ' );

        return checkbox !== null && matches( this.input, 'input[type="hidden"]' )
    }

    /**
     * Returns if the given input is a decimal input or not
     *
     * @return {boolean}
     */
    isDecimal() {
        return matches( this.input, 'input[type="decimal"]' );
    }

    /**
     * Returns the current value of the input
     *
     * @return {[]|null|boolean|number|string}
     */
    get() {
        if ( this.isSelect() ) {
            return this._getSelect();
        } else if ( this.isCheckbox() ) {
            return this._getCheckbox();
        } else if ( this.isNullableCheckbox() ) {
            return this._getNullableCheckbox();
        } else if ( this.isDecimal() ) {
            return this._getDecimal();
        }

        return this._getDefault();
    }

    /**
     * The default getter
     * @return {*}
     * @private
     */
    _getDefault() {
        return this.input.value;
    }

    /**
     * The getter function for normal checkboxes (2 states)
     * @return {boolean}
     * @private
     */
    _getCheckbox() {
        return this.input.checked;
    }

    /**
     * The getter function for nullable checkboxes (3 states)
     * @return {null|boolean}
     * @private
     */
    _getNullableCheckbox() {
        const checkbox = getElement( '#' + this.input.id + '_checkbox ' );

        if ( checkbox.checked === false && checkbox.indeterminate === true ) {
            return null;
        }

        return checkbox.checked;
    }

    /**
     * The getter function for select
     * @return {null|[]|*}
     * @private
     */
    _getSelect() {
        if ( this.input.hasAttribute( 'multiple' ) ) {
            const items = [];

            for ( let item of this.input.selectedOptions ) {
                items.push( item.value );
            }

            return items;
        }

        const option = this.input.selectedOptions[ 0 ];

        if ( option === null || option === undefined ) {
            return null;
        }

        return option.value;
    }

    /**
     * The getter function for decimal inputs
     * @return {number}
     * @private
     */
    _getDecimal() {
        const decimalInput = DTV.DecimalInput.instance( this.input );

        if ( decimalInput === null ) {
            console.warn( 'No decimal input instance found for "#' + this.input.id + '"' );

            return 0;
        }

        return decimalInput.getValue();
    }

    /**
     * Sets the current value of the input
     * @param value
     * @param trigger
     */
    set( value, trigger = false ) {
        let element = null;

        if ( this.isSelect() ) {
            this._setSelect( value );
        } else if ( this.isCheckbox() ) {
            this._setCheckbox( value );
        } else if ( this.isNullableCheckbox() ) {
            element = getElement( '#' + this.input.id + '_checkbox ' );
            this._setNullableCheckbox( value );
        } else if ( this.isDecimal() ) {
            this._setDecimal( value );
        } else {
            this._setDefault( value );
        }

        if ( trigger ) {
            this.triggerChange( element );
        }
    }

    /**
     * The default setter function
     * @param value
     * @private
     */
    _setDefault( value ) {
        this.input.value = value;
    }

    /**
     * The setter function for normal checkboxes (2 states)
     * @param value
     * @private
     */
    _setCheckbox( value ) {
        this.input.checked = !!+value;
    }

    /**
     * The setter function for nullable checkboxes (3 states)
     * @param value
     * @private
     */
    _setNullableCheckbox( value ) {
        const checkbox = getElement( '#' + this.input.id + '_checkbox ' );


        if ( typeof value === 'boolean' ) {
            value = value ? this.input.getAttribute( 'data-checked-value' ) : '0';
        }

        this.input.value = value;

        if ( value === null || value.length === 0 ) {
            checkbox.checked = false;
            checkbox.indeterminate = true;
            checkbox.setAttribute( 'data-indeterminate', 'true' );
        } else {
            checkbox.checked = !!+value;
            checkbox.indeterminate = false;
            checkbox.setAttribute( 'data-indeterminate', 'false' );
        }
    }

    /**
     * The setter function for selects
     * @param value
     * @private
     */
    _setSelect( value ) {
        $( this.input ).selectpicker( 'val', value );

        // Normal select logic
        /*if ( !Array.isArray( value ) ) {
            value = [ value ];
        }

        for ( let i = 0, length = this.input.options.length; i < length; i++ ) {
            let option = this.input.options[ i ];

            if ( value.includes( option.value ) ) {
                option.selected = true;
            }
        }*/
    }

    /**
     * The setter function for decimal inputs
     * @param value
     * @private
     */
    _setDecimal( value ) {
        const decimalInput = DTV.DecimalInput.instance( this.input );

        if ( decimalInput === null ) {
            console.warn( 'No decimal input instance found for "#' + this.input.id + '"' );

            return;
        }

        decimalInput.setValue( value );
    }

    /**
     * Triggers an change event on the given element or the input
     * @param element
     */
    triggerChange( element = null ) {
        element = element || this.input;

        element.dispatchEvent( new CustomEvent( 'change', {
            detail: { auto: true }
        } ) );
    }

    /**
     * Get instance by element function
     *
     * @param element
     *
     * @return {*}
     */
    static instance( element ) {
        return DTV.Data.get( element, DTV.Input.DATA_KEY )
    }
};