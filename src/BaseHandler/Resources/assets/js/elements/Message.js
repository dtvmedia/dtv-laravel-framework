'use strict';

var DTV = DTV || {};

/**
 * Message class
 *
 * @package   DTV
 * @copyright 2019 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
DTV.Message = class {
    /**
     * Message constructor
     *
     * @param type
     * @param title
     * @param text
     * @param closeTime
     */
    constructor( type, title, text, closeTime = null ) {
        if ( type === 'error' ) {
            type = 'danger';
        }

        this.type = type;
        this.title = title;
        this.text = text;
        this.closeTime = closeTime;
        this.alertWrapper = document.querySelector( "#alerts" );

        this.createWrapperIfNotExists();

        // Create new alert and add it to the wrapper
        this.alert = this.createAlert();
        this.alertWrapper.append( this.alert );
    }

    /**
     * Creates the alert wrapper if it not exists
     */
    createWrapperIfNotExists() {
        if ( this.alertWrapper === null ) {
            this.alertWrapper = document.createElement( 'div' );
            this.alertWrapper.setAttribute( 'id', 'alerts' );

            document.querySelector( 'body' ).append( this.alertWrapper );
        }
    }

    /**
     * Creates an new alert html element
     *
     * @return {Element|Node|HTMLCollection}
     */
    createAlert() {
        const alert = template( `
             <div class="alert alert-${this.type}">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <h4>${this.title}</h4>
                <span>${this.text}</span>
             </div>
        ` );

        // Set Timeout for automatic close if needed
        if ( this.closeTime !== undefined && this.closeTime != null ) {
            setTimeout( this.close.bind( this ), this.closeTime * 1000 );
        }

        return alert;
    }

    /**
     * Closes the modal
     */
    close() {
        $( this.alert ).fadeOut( 1500, function () {
            this.remove();
        } );
    }
};