'use strict';

var DTV = DTV || {};

/**
 * Loading Screen class
 *
 * @package   DTV
 * @copyright 2019 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
DTV.LoadingScreen = class {
    /**
     * Data Key Getter
     *
     * @return {string}
     */
    static get DATA_KEY() {
        return 'DTV.LoadingScreen';
    }

    /**
     * LoadingScreen constructor
     *
     * @param element
     * @param status
     * @param options
     * @return {*}
     */
    constructor( element , status = true, options = {} ) {
        this.element = getElement( element );
        this.status = status;
        this.options = {
            icons: options.icons || DTV.LoadingScreen.DEFAULTS.icons,
        };

        const instance = DTV.LoadingScreen.instance( this.element );
        if ( instance !== null ) {
            instance.setStatus( status );

            return instance;
        }

        DTV.Data.set( this.element, this.constructor.DATA_KEY, this );

        this.setStatus( status );
    }

    /**
     * Sets the loading screen status
     *
     * @param status
     */
    setStatus( status ) {
        this._removeLoadingScreen();

        if ( status === true ) {
            this._addLoadingScreen();
        }
    }

    /**
     * Removes the loadings screen of the element
     */
    _removeLoadingScreen() {
        const oldOverlay = this.element.querySelector( '.overlay' );

        if ( oldOverlay !== null ) {
            oldOverlay.remove();
        }

        this.status = false;
    }

    /**
     * Adds the loading screen to the element
     */
    _addLoadingScreen() {
        this.element.style.position = 'relative';
        this.element.append( this._createLoadingScreen() );
        this.status = true;
    }

    /**
     * Creates the loading screen HTML Node
     *
     * @return {Element|Node}
     */
    _createLoadingScreen() {
        return template( `<div class="overlay"><i class="${this.options.icons}"></i></div>` );
    }

    /**
     * Get instance by element function
     *
     * @param element
     *
     * @return {*}
     */
    static instance( element ) {
        return DTV.Data.get( element, DTV.LoadingScreen.DATA_KEY )
    }
};

DTV.LoadingScreen.DEFAULTS = {
    icons: 'far fa-spinner fa-pulse',
};