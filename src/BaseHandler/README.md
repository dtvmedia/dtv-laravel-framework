# BaseHandler - DTV Laravel Framework

## FormView

### Change the wrapper layout
By default there are already two wrapper layouts for forms defined. One for modals and one basic layout. 
If you want to add a new layout you can easily create one. 
Your layouts should yield the following sections:

- form-header
- form-body
- form-footer
- form-scripts

If you have an form class then your need to set your wanted layout explicit:
````php
$form->setLayout( 'path.to.your.layout' );
````

~~If you dont want to set your layout everytime you can just extend the base form class to make your own and overwrite the default layout~~ 

## ListView
### General Hints
In order to work with the sorting functions its necessary that the given query builder doesn't include an orderBy rule.
The initial sorting is ascending by the id. In case you want to change this you can use the ``$orderColumn`` and ``$orderDir`` properties of the listview class.
Also notice that if you set multiple fields for an column, the first one is used for sorting the related column. 
If no field is given sorting is disabled for the corresponding column.

Another important information is that when using listviews in custom views you need to register them view the ``withView()`` function of the Page Builder. This way you the callbacks for the listview requests is automatically registered.

### Column Options

**setWidth(int|string $width)**

Sets the width of the column. Any given css width string is valid here or, if you just pass an integer here it is used as pixel value.

**setDisplayPoint(int $displayPoint)**

Sets the width below which the column won't be displayed. Especially useful for responsive table optimizations. The given value will be rounded to the next hundred pixel.

**addClass(string $class)**

Adds one or an string of multiple css classes to all the cells related to the column.

**enableHtml()**

Enabled the HTML output mode which basically means that the cell content will not be escaped. If you enable this mode be very careful when echoing content that is supplied by users of your application because of possible XSS attacks. 

### Meta Infos
In order to give the table rows (\<tr\> Elements) some custom data attributes we can define meta information which behave similar to the Columns. You can define an meta information using the addMeta() function:
````php
$this->addMeta('id')
````
This translates in the table html to following, in which 1 is the id of the current dataset:
````html
<tr data-id="1">
    ...
</tr>
````
In case you use custom array as raw data you can also define am datapath closure where you could customize the meta value as you wish.

## Asset Management
The simplest solution is to just publish the assets files of the framework with:
````
php artisan vendor:publish --force --tag=dtv.base-assets
````
At the moment you need to copy the package.json and the webpack.mix.js from the framework and change it for your needs in order to compile all assets.
We may simplify this step in the future.

## Hooks
You have the possibility to customize some things like buttons or listview columns from the app side via hooks.
Following hook types are currently defined:

- headerButtons (Parameters: Controller $controller )
- listView (Parameters: ListView $listview )

You can define an hook like that:
````
// Define Hooks
app( 'hooks' )->define( 'headerButtons' , 'test.show' , [] , function ( Controller $controller ) {
    $controller->addHeaderButton( 'test' , 'fa-star' , 'test.index' )->useHref();
} );
````
Beside the type, hooks are bound to an route (and optionally also to the route parameters) which means you can essentially define on which URL the hook should be activated.