<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Contracts;

    /**
     * Addressable Model Extension Interface
     *
     * @package   DTV\BaseHandler\Contracts
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    interface AddressableContract
    {
        /**
         * Returns the street with the house number
         *
         * @return string
         */
        public function getAddress(): string;

        /**
         * Returns the full address with the zip code and city
         *
         * @param string $separator
         * @param bool   $withCountry
         *
         * @return string
         */
        public function getFullAddress( string $separator = ', ' , bool $withCountry = false ): string;

        /**
         * Returns the full country name
         *
         * @param string $default
         *
         * @return string
         */
        public function getCountry( string $default = '-' ): string;
    }