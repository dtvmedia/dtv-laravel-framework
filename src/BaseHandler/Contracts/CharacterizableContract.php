<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Contracts;

    /**
     * Characterizable Model Extension Contract
     *
     * @package   DTV\BaseHandler\Contracts
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    interface CharacterizableContract
    {
        /**
         * Returns the full name of the person
         *
         * @return string
         */
        public function getDisplayName(): string;

        /**
         * Returns the full name with title
         *
         * @return string
         */
        public function getFullName(): string;

        /**
         * Returns the salutation of the user
         *
         * @return string
         */
        public function getSalutation(): string;
    }