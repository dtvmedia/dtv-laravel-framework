<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler;

    use Illuminate\Contracts\Container\BindingResolutionException;
    use Illuminate\Support\Facades\File;
    use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
    use ReflectionClass;

    /**
     * Base Service Provider
     *
     * @package   DTV\BaseHandler
     * @copyright 2017 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    abstract class BaseServiceProvider extends LaravelServiceProvider
    {
        /**
         *  Namespace for resources of the module
         *
         * @var string
         */
        protected $resourcesNamespace = '';

        /**
         * Array of commans
         *
         * @var array
         */
        protected $commands = [];

        /**
         * Array of widgets
         *
         * @var array
         */
        protected $widgets = [];

        /**
         * Module path
         *
         * @var string
         */
        protected $modulePath;

        /**
         * Registers the module
         *
         * @throws BindingResolutionException
         */
        public function register()
        {
            $this->getModulePath();
            $this->registerRoutes();
            $this->registerHelpers();
            $this->registerMigrations();
            $this->registerWidgets();
            $this->commands( $this->commands );
        }

        /**
         * Bootstrap any application services.
         *
         * @return void
         */
        public function boot()
        {
            $this->registerResourcePaths();
            $this->registerGates();
        }

        /**
         * Loads the module path
         */
        protected function getModulePath()
        {
            $this->modulePath = dirname( ( new ReflectionClass( static::class ) )->getFileName() );
        }

        /**
         * Registers the modules resource paths
         */
        protected function registerResourcePaths()
        {
            $viewPath = $this->modulePath . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'views';
            $langPath = $this->modulePath . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'lang';

            if ( File::exists( $viewPath ) === true ) {
                $this->loadViewsFrom( $viewPath , $this->resourcesNamespace );
                $this->publishes( [
                    $viewPath => resource_path( 'views/vendor/' . $this->resourcesNamespace ) ,
                ] , $this->resourcesNamespace );
            }

            if ( File::exists( $langPath ) === true ) {
                $this->loadTranslationsFrom( $langPath , $this->resourcesNamespace );
                $this->publishes( [
                    $langPath => resource_path( 'lang/vendor/' . $this->resourcesNamespace ) ,
                ] , $this->resourcesNamespace );
            }
        }

        /**
         * Loads the module routes
         */
        protected function registerRoutes()
        {
            $moduleName = str_replace( 'dtv.' , '' , $this->resourcesNamespace );

            if ( method_exists( $this , 'routes' ) && config( 'dtv.modules.' . $moduleName . '.routes_enabled' , false ) ) {
                $this->routes();
            }
        }

        /**
         * Loads the module helper function file if it exists
         */
        protected function registerHelpers()
        {
            $helper = $this->modulePath . DIRECTORY_SEPARATOR . 'helpers.php';

            if ( file_exists( $helper ) ) {
                require $helper;
            }
        }

        /**
         * Registers the modules migration files
         */
        protected function registerMigrations()
        {
            if ( file_exists( $this->modulePath . DIRECTORY_SEPARATOR . 'Migrations' ) ) {
                $this->loadMigrationsFrom( $this->modulePath . DIRECTORY_SEPARATOR . 'Migrations' );
            }
        }

        /**
         * Registers the modules gates
         */
        protected function registerGates()
        {
            if ( method_exists( $this , 'gates' ) ) {
                $this->gates();
            }
        }

        /**
         * Registers the modules widgets
         *
         * @throws BindingResolutionException
         */
        protected function registerWidgets()
        {
            foreach ( $this->widgets as $widget ) {
                $this->app->make( 'dashboards' )->register( $widget );
            }
        }
    }
