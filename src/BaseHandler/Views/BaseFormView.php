<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views;

    use BackedEnum;
    use DTV\BaseHandler\Views\FormInputs\Custom;
    use DTV\BaseHandler\Views\FormInputs\FormInput;
    use DTV\BaseHandler\Views\Traits\FormBuilder;
    use Exception;
    use Illuminate\Database\Eloquent\Collection;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Validation\Rules\Unique;
    use Throwable;

    /**
     * Base Form View Class
     *
     * @package   DTV\BaseHandler\Views
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    abstract class BaseFormView extends View
    {
        use FormBuilder;

        /**
         * Form inputs array
         *
         * @var FormInput[]
         */
        private array $inputs = [];

        /**
         * Form action url
         *
         * @var string
         */
        private string $action;

        /**
         * Form Request Method
         *
         * @var string
         */
        private string $method = 'POST';

        /**
         * Page modal size (only used if page mode is modal)
         *
         * @var string
         */
        protected $pageModalSize = Page::MODAL_SIZE_MD;

        /**
         * A switch which handles if the form request should use ajax or not
         *
         * @var bool
         */
        private bool $ajax = true;

        /**
         * Render layout view name
         *
         * @var string
         */
        protected $layout = '';

        /**
         * Form Data Model
         *
         * @var Model|null
         */
        protected $model = null;

        /**
         * View Icon Class
         *
         * @var string
         */
        protected $icon = '';

        /**
         * Flag which shows if the view was already built or not
         *
         * @var bool
         */
        private bool $built = false;

        /**
         * Form View constructor
         *
         * @param Model|null $model
         */
        function __construct( Model $model = null )
        {
            $this->model = $model;
            $this->action = request()->fullUrl();
        }

        /**
         * Sets an custom form action route
         *
         * @param string $route
         * @param array  $paramter
         *
         * @return $this
         */
        public function setAction( string $route , array $paramter = [] )
        {
            $this->action = route( $route , $paramter );

            return $this;
        }

        /**
         * Returns the form action
         *
         * @return string
         */
        public function getAction(): string
        {
            return $this->action;
        }

        /**
         * Tries to set a custom method (if whitelisted)
         *
         * @param string $method
         *
         * @return $this
         */
        public function setMethod( string $method )
        {
            $method = strtoupper( $method );

            if ( in_array( $method , [ 'GET' , 'POST' , 'PUT' , 'DELETE' ] ) ) {
                $this->method = $method;
            }

            return $this;
        }

        /**
         * Returns the form method
         *
         * @return string
         */
        public function getMethod(): string
        {
            return $this->method;
        }

        /**
         * Returns the page modal size of the form
         *
         * @return string
         */
        public function getPageModalSize(): string
        {
            return $this->pageModalSize;
        }

        /**
         * Sets if the form should use ajax to send the form data or an traditional request
         *
         * @param bool $ajax
         *
         * @return $this
         */
        public function useAjax( $ajax = true ): self
        {
            $this->ajax = boolval( $ajax );

            return $this;
        }

        /**
         * Returns the state of the ajax switch
         *
         * @return bool
         */
        public function ajax(): bool
        {
            return $this->ajax;
        }

        /**
         * Sets the render layout view
         *
         * @param string $layout
         *
         * @return $this
         */
        public function setLayout( string $layout )
        {
            $this->layout = $layout;

            return $this;
        }

        /**
         * Returns the render layout view
         *
         * @return string
         */
        public function getLayout(): string
        {
            return $this->layout;
        }

        /**
         * Tries to find the value of a element in the registed model if available, returns the default otherwise
         *
         * @param FormInput $input
         *
         * @see FormInput::setDefaultValue()
         * @throws Exception
         *
         * @return mixed
         */
        protected function getValue( FormInput $input )
        {
            if ( $this->model == null ) {
                return $input->getDefault();
            }

            $name = rtrim( $input->getName() , '[]' );

            try {
                $value = $this->model->{$name};
            } catch ( Throwable $e ) {
                throw new Exception( 'Property "' . $name . '" was not found in object of class ' . get_class( $this->model ) . ' (' . $e->getMessage() . ')' );
            }

            if ( $value === null ) {
                return $input->getDefault();
            }

            if ( $value instanceof Collection ) {
                $value = $value->pluck( 'id' )->toArray();
            }

            if ($value instanceof BackedEnum) {
                $value = $value->value;
            }

            return $input->getMutator( $value );
        }

        /**
         * Adds an FormInput object to the form
         *
         * @template Input of FormInput
         * @param Input $input
         *
         * @return Input
         */
        public function addInput( FormInput $input )
        {
            $this->inputs[] = $input;

            return $input;
        }

        /**
         * Checks some validation rules
         *
         * @param FormInput $formInput the form input to be validated
         *
         * @return string|array
         */
        protected function checkValidations( FormInput $formInput )
        {
            $validationRule = $formInput->getValidation();

            if ( $formInput instanceof Custom ) {
                foreach ( $validationRule as $name => $rules ) {
                    $validationRule[ $name ] = $this->rewriteValidationRules( $rules );
                }
            } else {
                $validationRule = $this->rewriteValidationRules( $validationRule );
            }

            return $validationRule;
        }

        /**
         * Checks and enhances some validation rules
         *
         * @param string|array $rules validation rules
         *
         * @return string
         */
        private function rewriteValidationRules( $rules )
        {
            if ( is_string( $rules ) ) {
                $rules = explode( '|' , $rules );
            }

            foreach ( $rules as $key => $rule ) {
                // Rewrite Unique Rule
                if ( $this->isUpdateMode() && ( $rule instanceof Unique || ( str_contains( $rule , 'unique' ) && substr_count( $rule , ',' ) == 1 ) ) ) {
                    $params = str_replace( 'unique:' , '' , (string) $rule );
                    [$table, ] = explode( ',' , $params , 2 );

                    if ( $this->model->getTable() == $table ) {
                        if ( $rule instanceof Unique ) {
                            $rule->ignore( $this->model->id );
                        } else {
                            $rules[ $key ] = $rule . ',' . $this->model->id;
                        }
                    }
                }
            }

            return implode( '|' , $rules );
        }

        /**
         * Returns the validations array of the form
         *
         * @param string|array $only Returns the validation rules only for the given inputs names
         *
         * @throws Exception
         *
         * @return array
         */
        public function getValidationRules( $only = null )
        {
            $rules = [];
            $only = array_wrap( $only );

            foreach ( $this->getInputs() as $input ) {
                if ( !empty( $input->getValidation() ) && $input->disabled() !== true ) {
                    if ( $input instanceof Custom ) {
                        // Custom HTML/Views/Elements Rules
                        foreach ( $this->checkValidations( $input ) as $key => $validation ) {
                            if ( !empty( $only ) && !in_array( $key , $only ) ) {
                                continue;
                            }

                            $rules[ $key ] = $validation;
                        }
                    } else {
                        // Normal Elements Rules with array input support
                        $name = rtrim( str_replace( ']' , '' , str_replace( '[' , '.' , $input->getName() ) ) , '.' );

                        if ( !empty( $only ) && !in_array( $name , $only ) ) {
                            continue;
                        }

                        $rules[ $name ] = $this->checkValidations( $input );
                    }
                }
            }

            return $rules;
        }

        /**
         * Tries to validate the form, throws a ValidationException if the validation fails
         *
         * @param string|array $only Validates only the given inputs
         *
         * @throws Exception
         */
        public function validate( $only = null )
        {
            request()->validate( $this->getValidationRules( $only ) );
        }

        /**
         * Returns wheter or not the form is in update mode
         *
         * @return bool
         */
        public function isUpdateMode(): bool
        {
            return !is_null( $this->model );
        }

        /**
         * Method which adds all Elements to the form
         *
         * @return void
         */
        abstract public function inputs();

        /**
         * Caches all Input IDs in order to have faster access via ID in custom form views
         *
         * @throws Exception
         */
        protected function cacheInputIds()
        {
            $this->inputs();

            $inputs = [];

            foreach ( $this->inputs as $input ) {
                if ( isset( $inputs[ $input->getId() ] ) ) {
                    throw new Exception( 'Input ' . $input->getId() . ' is already defined in this form' );
                }

                $inputs[ $input->getId() ] = $input;
            }

            $this->inputs = $inputs;
        }

        /**
         * Builds the FormView
         *
         * @throws Exception
         */
        protected function build()
        {
            if ($this->built) {
                return;
            }

            $this->cacheInputIds();
            $this->setIcon( $this->icon );
            $this->built = true;
        }

        /**
         * Returns an array of all input objects
         *
         * @throws Exception
         *
         * @return FormInput[]
         */
        public function getInputs()
        {
            $this->build();

            return $this->inputs;
        }
    }
