<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\Components;

    use Closure;
    use Illuminate\Database\Eloquent\Model;
    use Throwable;

    /**
     * HTML Element Bootstrap 4 List View Button Class
     *
     * @package   DTV\BaseHandler\Views\Components
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class ListViewButton extends Button
    {
        /**
         * Array of raw dynamic parameters
         *
         * @var array
         */
        protected array $rawDynamicParameter = [];

        /**
         *  Array of filled dynamic parameters (after binding a model)
         *
         * @var array
         */
        protected array $dynamicParameter = [];

        /**
         * An condition closure which will determine if the button will be rendered
         *
         * @var Closure|null
         */
        protected ?Closure $condition = null;

        /**
         * Flag which indicates whether or not the button will be rendered
         *
         * @var bool
         */
        protected bool $active = true;

        /**
         * Sets the condition closure
         *
         * @param Closure $condition
         *
         * @return $this
         */
        public function setCondition( Closure $condition ): self
        {
            $this->condition = $condition;

            return $this;
        }

        /**
         * Sets the fix parameters
         *
         * @param array $parameter
         *
         * @return $this
         */
        public function setParameter( array $parameter ): self
        {
            $this->parameter = $parameter;

            return $this;
        }

        /**
         * Sets the dynamic parameters
         *
         * @param string|array $parameter
         *
         * @return $this
         */
        public function setDynamicParameter( $parameter ): self
        {
            $this->rawDynamicParameter = ( is_array( $parameter ) ) ? $parameter : [ $parameter => $parameter ];

            return $this;
        }

        /**
         * Binds an model to the button used for evaluating the condition closure and filling the dynamic paramters
         *
         * @param Model $model
         */
        public function bindModel( Model $model )
        {
            // check if a check function is defined
            if ( $this->condition instanceof Closure ) {
                // call the check function to see if the row passes the check
                $this->active = boolval( ( $this->condition )( $model ) );
            }

            // Fill dynamic parameters
            foreach ( $this->rawDynamicParameter as $key => $param ) {
                if (str_contains($param, '.')) {
                    $steps = explode('.', $param);

                    $relModel = $model;
                    foreach ($steps as $step) {
                        $relModel = $relModel->$step;
                    }

                    $this->dynamicParameter[ $key ] = $relModel;
                } elseif( method_exists($model, $param) ) {
                    $this->dynamicParameter[ $key ] = $model->$param();
                } else {
                    $this->dynamicParameter[ $key ] = $model->$param;
                }
            }
        }

        /**
         * Returns the buttons url
         *
         * @return string
         */
        public function getUrl(): string
        {
            return route( $this->route , array_merge( $this->parameter , $this->dynamicParameter ) );
        }

        /**
         * Returns whether or not the current user is allowed to use this button
         *
         * @return bool
         */
        public function isAllowed(): bool
        {
            return can( $this->route , array_merge( $this->parameter , $this->dynamicParameter ) );
        }

        /**
         * Renders the button
         *
         * @throws Throwable
         *
         * @return string
         */
        public function render()
        {
            if ( $this->active === false || $this->isAllowed() === false ) {
                return '';
            }

            return parent::render();
        }
    }
