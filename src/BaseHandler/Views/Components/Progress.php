<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\Components;

    use DTV\BaseHandler\Views\HtmlElement;

    /**
     * HTML Element Bootstrap 4 Progress Class
     *
     * @package   DTV\BaseHandler\Views\Components
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Progress extends HtmlElement
    {
        /**
         * Array of progressbars
         *
         * @var array
         */
        protected $progressBars = [];

        /**
         * Available width in percent
         *
         * @var int
         */
        private $percentFree = 100;

        /**
         * Progressbar height in px
         *
         * @var int
         */
        protected $height = null;

        /**
         * CSS class array
         *
         * @var array
         */
        protected $classes = [];

        /**
         * Progress constructor
         *
         * @param float|null  $percent
         * @param string|null $type
         * @param string|null $label
         */
        public function __construct( $percent = null , $type = null , $label = null )
        {
            if ( $percent !== null ) {
                $this->addProgressbar( $percent , $type , $label );
            }
        }

        /**
         * Adds an css class to the main progress element
         *
         * @param string $class
         *
         * @return Progress
         */
        public function addClass( string $class ): self
        {
            $this->classes[] = $class;

            return $this;
        }

        /**
         * Returns the CSS classes string
         *
         * @return string
         */
        public function getClasses(): string
        {
            return implode( ' ' , $this->classes );
        }

        /**
         * Adds an progressbar
         *
         * @param float             $percent
         * @param string            $type
         * @param string|null|false $label
         *
         * @return Progress
         */
        public function addProgressbar( $percent , string $type = null , $label = null ): self
        {
            if ( $type == null ) {
                $type = 'primary';
            }

            $min_width = 0;
            $percent = min( max( 0 , $percent ) , 100 );

            $this->percentFree -= $percent;
            if ( $this->percentFree < 0 ) {
                $percent += $this->percentFree;
                $this->percentFree = 0;
            }

            if ( $label === false ) {
                $label = '';
            } elseif ( $label === null ) {
                $label = $percent . '%';
                $min_width = 22;
            }

            $this->progressBars[] = [
                'percent'   => $percent ,
                'type'      => $type ,
                'label'     => $label ,
                'min_width' => $min_width ,
            ];

            return $this;
        }

        /**
         * Returns the array of progressbars
         *
         * @return array
         */
        public function getProgressbars(): array
        {
            return $this->progressBars;
        }

        /**
         * Sets the progressbar height in px
         *
         * @param int $height
         *
         * @return Progress
         */
        public function setHeight( int $height ): self
        {
            $this->height = $height;

            return $this;
        }

        /**
         * Returns the progressbar height in px
         *
         * @return int
         */
        public function getHeight(): ?int
        {
            return $this->height;
        }

        /**
         * Renders the progressbar
         *
         * @return string
         */
        public function render(): string
        {
            return view( 'dtv.base::helpers.progress' , [ 'progress' => $this ] )->render();
        }
    }