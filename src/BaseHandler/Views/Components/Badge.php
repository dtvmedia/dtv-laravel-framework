<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\Components;

    use DTV\BaseHandler\Views\HtmlElement;

    /**
     * HTML Element Bootstrap 4 Badge Class
     *
     * @package   DTV\BaseHandler\Views\Components
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Badge extends HtmlElement
    {
        /**
         * Type of the badge
         *
         * @var string
         */
        protected $type;

        /**
         * HTML content of the badge
         *
         * @var string
         */
        protected $content;

        /**
         * Array of css classes
         *
         * @var array
         */
        protected $classes = [];

        /**
         * Badge constructor
         *
         * @param string $content Beware: content won't be escaped, so it is open to XSS attackes when using direct user input as content!
         * @param string $type
         */
        public function __construct( string $content , string $type = 'primary' )
        {
            $this->content = $content;
            $this->type = $type;
        }

        /**
         * Adds an css (or multiple space seperated css classes) to the badge
         *
         * @param string $class
         *
         * @return $this
         */
        public function addClass( string $class ): self
        {
            $this->classes[] = $class;

            return $this;
        }

        /**
         * Returns the html content of the badge
         *
         * @return string
         */
        public function getContent(): string
        {
            return $this->content;
        }

        /**
         * Returns the title of the badge
         *
         * @return string
         */
        public function getTitle(): string
        {
            return trim( strip_tags( $this->content ) );
        }

        /**
         * Returns the badge type
         *
         * @return string
         */
        public function getType(): string
        {
            return $this->type;
        }

        /**
         * Returns the string with all css classes
         *
         * @return string
         */
        public function getClassString(): string
        {
            return trim( 'badge-' . $this->getType() . ' ' . implode( ' ' , $this->classes ) );
        }

        /**
         * Renders the badge
         *
         * @throws \Throwable
         *
         * @return string
         */
        public function render()
        {
            return view( 'dtv.base::helpers.badge' , [ 'badge' => $this ] )->render();
        }
    }