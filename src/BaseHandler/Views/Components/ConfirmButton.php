<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\Components;

    use DTV\BaseHandler\Views\Traits\HasConfirmMessage;
    use DTV\BaseHandler\Views\Traits\HasIcon;

    /**
     * HTML Element Bootstrap 4 Confirm Button Class
     *
     * @package   DTV\BaseHandler\Views\Components
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class ConfirmButton extends BaseButton
    {
        use HasIcon , HasConfirmMessage;

        /**
         * Flag which handles if the buttons uses ajax or traditional requests
         *
         * @var bool
         */
        protected $ajax = true;

        /**
         * Renders the confirm button
         *
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
         */
        public function render()
        {
            if ( $this->isAllowed() === false ) {
                return '';
            }

            return view( 'dtv.base::helpers.button_confirm' , [ 'button' => $this ] );
        }
    }