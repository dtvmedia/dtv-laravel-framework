<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\Components;

    use DTV\BaseHandler\Views\HtmlElement;

    /**
     * HTML Element Bootstrap 4 Button Group Class
     *
     * @package   DTV\BaseHandler\Views\Components
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class ButtonGroup extends HtmlElement
    {
        /**
         * Array of all Buttons of the button group
         *
         * @var array|BaseButton[]
         */
        protected $buttons = [];

        /**
         * ButtonGroup constructor
         *
         * @param BaseButton[] ...$buttons
         */
        public function __construct( BaseButton ...$buttons )
        {
            foreach ( $buttons as $button ) {
                $this->addButton( $button );
            }
        }

        /**
         * Adds an button to the button group
         *
         * @param BaseButton $button
         *
         * @return $this
         */
        public function addButton( BaseButton $button )
        {
            $this->buttons[] = $button;

            return $this;
        }

        /**
         * Returns all registered buttons
         *
         * @return array|BaseButton[]
         */
        public function getButtons()
        {
            return $this->buttons;
        }

        /**
         * Renders the button group with all buttons
         *
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
         */
        public function render()
        {
            return view( 'dtv.base::helpers.button_group' , [ 'buttonGroup' => $this ] );
        }
    }