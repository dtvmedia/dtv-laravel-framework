<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\Components;

    use DTV\BaseHandler\Views\HtmlElement;

    /**
     * HTML Element Bootstrap 4 Base Button Class
     *
     * @package   DTV\BaseHandler\Views\Components
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    abstract class BaseButton extends HtmlElement
    {
        /**
         * Button Label
         *
         * @var string
         */
        protected $label;

        /**
         * The button route name
         *
         * @var string
         */
        protected $route;

        /**
         * Array of fixed route parameters
         *
         * @var array
         */
        protected $parameter = [];

        /**
         * Flag which handles if the buttons uses ajax or traditional requests
         *
         * @var bool
         */
        protected $ajax = false;

        /**
         * Flag which handles if the label will be shown
         *
         * @var bool
         */
        protected $showLabel = true;

        /**
         * Button Type class
         *
         * @var string
         */
        protected $type = 'outline-secondary';

        /**
         * Button size class
         *
         * @var string
         */
        protected $size = 'sm';

        /**
         * Additional CSS Classes
         *
         * @var string
         */
        protected $classes = '';

        /**
         * Render allowed flag
         *
         * @var bool
         */
        protected $allowed = true;

        /**
         * BaseButton constructor.
         *
         * @param string $label
         * @param string $route
         * @param array  $parameter
         */
        public function __construct( $label , $route , $parameter = [] )
        {
            $this->label = trans( $label );

            $this->setRouteAndParameter( $route , $parameter );
        }

        /**
         * Sets the route name and route parameters
         *
         * @param string $route
         * @param array  $parameter
         */
        protected function setRouteAndParameter( string $route , array $parameter = [] )
        {
            $this->route = $route;
            $this->parameter = $parameter;
        }

        /**
         * Sets the flag which handles if the label will be shown
         *
         * @param bool $hideLabel
         *
         * @return $this
         */
        public function hideLabel( $hideLabel = true )
        {
            $this->showLabel = !$hideLabel;

            return $this;
        }

        /**
         * Sets the flag which handles if the label will be shown
         *
         * @param bool $showLabel
         *
         * @return $this
         */
        public function showLabel( $showLabel = true )
        {
            $this->showLabel = $showLabel;

            return $this;
        }

        /**
         * Sets the size class of the button
         *
         * @param string $size
         *
         * @return $this
         */
        public function setSize( string $size ): self
        {
            $this->size = $size;

            return $this;
        }

        /**
         * Sets the type class of the button
         *
         * @param string $type
         *
         * @return $this
         */
        public function setType( string $type ): self
        {
            $this->type = $type;

            return $this;
        }

        /**
         * Sets additional classes for the button
         *
         * @param string $classes
         *
         * @return $this
         */
        public function setClasses( string $classes ): self
        {
            $this->classes = $classes;

            return $this;
        }

        /**
         * Returns the name of the underlaying html element
         *
         * @return string
         */
        public function getHtmlBase(): string
        {
            return ( $this->ajax ) ? 'button' : 'a';
        }

        /**
         * Returns whether or not this is a ajax button
         *
         * @return bool
         */
        public function isAjax(): bool
        {
            return $this->ajax;
        }

        /**
         * Returns an string with all css classes of the button
         *
         * @return string
         */
        public function getClassString(): string
        {
            return 'btn btn-' . $this->size . ' btn-' . $this->type . ' ' . $this->classes;
        }

        /**
         * Returns the buttons url
         *
         * @return string
         */
        public function getUrl(): ?string
        {
            return route( $this->route , $this->parameter );
        }

        /**
         * Returns whether or not the current user is allowed to use this button
         *
         * @return bool
         */
        public function isAllowed(): bool
        {
            return can( $this->route , $this->parameter );
        }

        /**
         * Returns the show label flag
         *
         * @return bool
         */
        public function getShowLabel(): bool
        {
            return $this->showLabel;
        }

        /**
         * Returns the button label
         *
         * @return string
         */
        public function getLabel(): string
        {
            return $this->label;
        }
    }