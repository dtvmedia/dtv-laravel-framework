<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\Components;

    use DTV\BaseHandler\Views\HtmlElement;

    /**
     * HTML Element Font Awesome 5 Icon Class
     *
     * @package   DTV\BaseHandler\Views\Components
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Icon extends HtmlElement
    {
        /**
         * Icon base css class
         *
         * @var string
         */
        protected $iconBase = 'far';

        /**
         * Icon fixed width css class
         *
         * @var string
         */
        protected $iconFixedWidth = 'fa-fw';

        /**
         * Icon css class
         *
         * @var string
         */
        protected $icon;

        /**
         * Icon title
         *
         * @var string
         */
        protected $title = '';

        /**
         * String of space seperated css classes
         *
         * @var string
         */
        protected $classes = '';

        /**
         * Flag which handles if the icon uses the fixed width css class
         *
         * @var bool
         */
        protected $useFixedWidth = true;

        /**
         * Flag which handles if the label will be shown
         *
         * @var bool
         */
        protected $showLabel = false;

        /**
         * Icon constructor
         *
         * @param string $icon
         * @param string $title
         * @param string $classes
         */
        public function __construct( string $icon , string $title = '' , string $classes = '' )
        {
            $this->icon = $icon;
            $this->title = $title;
            $this->classes = $classes;
        }

        /**
         * Function enables or disabled the fixed width switch
         *
         * @param bool $useFixedWidth
         *
         * @return $this
         */
        public function useFixedWidth( bool $useFixedWidth = true ): self
        {
            $this->useFixedWidth = $useFixedWidth;

            return $this;
        }

        /**
         * Function enables or disabled the show label switch
         *
         * @param bool $showLabel
         *
         * @return Icon
         */
        public function showLabel( bool $showLabel = true ): self
        {
            $this->showLabel = $showLabel;

            return $this;
        }

        /**
         * Adds an css class (or multiple space seperated classes) to the icon
         *
         * @param string $class
         *
         * @return Icon
         */
        public function addClass( string $class ): self
        {
            $this->classes = trim( $this->classes . ' ' . $class );

            return $this;
        }

        /**
         * Returns the space seperated css class string
         *
         * @return string
         */
        public function getClassString(): string
        {
            $string = $this->iconBase . ' ';

            if ( $this->useFixedWidth ) {
                $string .= $this->iconFixedWidth . ' ';
            }

            return $string . $this->icon . ' ' . $this->classes;
        }

        /**
         * Returns the icon title
         *
         * @return string
         */
        public function getTitle(): string
        {
            return $this->title;
        }

        /**
         * Returns whether or not the label will be shown
         *
         * @return bool
         */
        public function getShowLabel(): bool
        {
            return $this->showLabel;
        }

        /**
         * Sets the icon base css class
         *
         * @param string $iconBase
         *
         * @return $this
         */
        public function setIconBase( string $iconBase ): self
        {
            $this->iconBase = $iconBase;

            return $this;
        }

        /**
         * Renders the icon
         *
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
         */
        public function render()
        {
            return view( 'dtv.base::helpers.icon' , [ 'icon' => $this ] );
        }
    }