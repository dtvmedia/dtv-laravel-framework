<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\Components;

    use DTV\BaseHandler\Views\Traits\HasIcon;

    /**
     * HTML Element Bootstrap 4 Button Class
     *
     * @package   DTV\BaseHandler\Views\Components
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Button extends BaseButton
    {
        use HasIcon;

        /**
         * Enables the ajax link mode
         *
         * @param bool $ajax
         *
         * @return $this
         */
        public function useAjax( bool $ajax = true ): self
        {
            $this->ajax = $ajax;

            return $this;
        }

        /**
         * Enables the href link mode
         *
         * @param bool $href
         *
         * @return $this
         */
        public function useHref( bool $href = true ): self
        {
            $this->ajax = !$href;

            return $this;
        }

        /**
         * Returns the link type
         *
         * @return string
         */
        public function getLinkType(): string
        {
            return ( $this->ajax ) ? 'data-ajax-action' : 'href';
        }

        /**
         * Renders the button
         *
         * @throws \Throwable
         *
         * @return string
         */
        public function render()
        {
            if ( $this->isAllowed() === false ) {
                return '';
            }

            return view( 'dtv.base::helpers.button' , [ 'button' => $this ] )->render();
        }
    }