<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\Components;

    use DTV\BaseHandler\Views\HtmlElement;
    use DTV\BaseHandler\Views\Traits\HasIcon;
    use Exception;

    /**
     * HTML Element Bootstrap 4 Dropdown Class
     *
     * @package   DTV\BaseHandler\Views\Components
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Dropdown extends HtmlElement
    {
        use HasIcon;

        /**
         * The dropdown button label
         *
         * @var string
         */
        protected $label;

        /**
         * Array of dropdown buttons
         *
         * @var Button[]
         */
        protected $items = [];

        /**
         * Dropdown button type css class
         *
         * @var string
         */
        protected $type = 'btn-outline-secondary';

        /**
         * Dropdown button css class string
         *
         * @var string
         */
        protected $classes = '';

        /**
         * Dropdown wrapper css class string
         *
         * @var string
         */
        protected $wrapperClasses = '';

        /**
         * Dropdown constructor
         *
         * @param string           $label
         * @param string|Icon|null $icon
         *
         * @throws Exception
         */
        public function __construct( string $label , $icon )
        {
            $this->label = $label;
            $this->setIcon( $icon );
        }

        /**
         * Returns the dropdown label
         *
         * @return string
         */
        public function getLabel(): string
        {
            return $this->label;
        }

        /**
         * Sets the dropdown button type css class
         *
         * @param string $type
         *
         * @return $this
         */
        public function setType( string $type ): self
        {
            $this->type = 'btn-' . $type;

            return $this;
        }

        /**
         * Adds an css class (or multiple space seperated css classes) to the dropdown button
         *
         * @param string $classes
         *
         * @return $this
         */
        public function addClasses( string $classes ): self
        {
            $this->classes .= $classes;

            return $this;
        }

        /**
         * Returns an string of space seperated css classes for the dropdown button
         *
         * @return string
         */
        public function getClasses(): string
        {
            return $this->type . ' ' . $this->classes;
        }

        /**
         * Adds an css class (or multiple space seperated css classes) to the dropdown wrapper div
         *
         * @param string $wrapperClasses
         *
         * @return $this
         */
        public function setWrapperClasses( string $wrapperClasses ): self
        {
            $this->wrapperClasses = $wrapperClasses;

            return $this;
        }

        /**
         * Returns an string of space seperated css classes for the dropdown wrapper div
         *
         * @return string
         */
        public function getWrapperClasses(): string
        {
            return $this->wrapperClasses;
        }

        /**
         * Adds an Button to the dropdown
         *
         * @param Button $button
         *
         * @return $this
         */
        public function addItem( Button $button ): self
        {
            $this->items[] = $button;

            return $this;
        }

        /**
         * Returns an array of all added buttons
         *
         * @return Button[]
         */
        public function getItems(): array
        {
            return $this->items;
        }

        /**
         * Renders the dropdown
         *
         * @throws \Throwable
         *
         * @return string
         */
        public function render()
        {
            return view( 'dtv.base::helpers.dropdown' , [ 'dropdown' => $this ] )->render();
        }
    }
