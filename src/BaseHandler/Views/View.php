<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views;

    use DTV\BaseHandler\Views\Traits\HasIcon;
    use Exception;

    /**
     * Abstract Base View Class
     *
     * @package   DTV\BaseHandler\Views
     * @copyright 2017 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    abstract class View extends HtmlElement
    {
        use HasIcon;

        /**
         * View Label
         *
         * @var string
         */
        protected $label = '';

        /**
         * Switch for the usage of translations in views
         *
         * @var bool
         */
        protected $transEnabled = true;

        /**
         * Translation String Prefix
         *
         * @var string
         */
        protected $transPath = '';

        /**
         * Switch which handles if the title will be shown in page headers etc.
         *
         * @var bool
         */
        protected $showTitle = true;

        /**
         * View Translation Function Wrapper
         *
         * @param string $prefix
         * @param string $key
         * @param array  $replace
         *
         * @return string
         */
        public function trans(string $prefix, string $key, array $replace = []): string
        {
            if ($this->transEnabled) {
                return trans($this->transPath . str_finish('.' . $prefix, '.') . $key, $replace);
            }

            return $key;
        }

        /**
         * Sets the views label
         *
         * @param string $label
         *
         * @return $this
         */
        public function setLabel( string $label ): self
        {
            $this->label = $label;

            return $this;
        }

        /**
         * Returns the views label
         *
         * @throws Exception
         *
         * @return string
         */
        public function getLabel(): string
        {
            if ( empty( $this->label ) ) {
                throw new Exception( 'No label was defined for view "' . class_basename( static::class ) . '"' );
            }

            return escapeHtmlLabel( $this->label );
        }

        /**
         * Returns the md5 hash of the view label
         *
         * @throws Exception
         *
         * @return string
         */
        public function getHash(): string
        {
            return md5( $this->getLabel() );
        }

        /**
         * Returns the views title
         *
         * @return string
         */
        public function getTitle(): string
        {
            return trans( $this->label );
        }

        /**
         * Hides the title in page headers etc
         *
         * @return $this
         */
        public function hideTitle()
        {
            $this->showTitle = false;

            return $this;
        }

        /**
         * Shows the title in page headers etc
         *
         * @return $this
         */
        public function showTitle()
        {
            $this->showTitle = true;

            return $this;
        }

        /**
         * Retuns the show title switch
         *
         * @return bool
         */
        public function getShowTitle(): bool
        {
            return $this->showTitle;
        }
    }
