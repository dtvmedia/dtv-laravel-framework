<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\Traits;

    /**
     * Has Confirm Message Trait for Confirm Buttons
     *
     * @package   DTV\BaseHandler\Views\Traits
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    trait HasConfirmMessage
    {
        /**
         * Confirm message
         *
         * @var string
         */
        protected $message;

        /**
         * Confirm button label
         *
         * @var string
         */
        protected $confirmButtonLabel;

        /**
         * Action HTTP Method
         *
         * @var string
         */
        protected $method = 'DELETE';

        /**
         * Sets the confirm message
         *
         * @param string $message
         *
         * @return $this
         */
        public function setMessage( ?string $message ): self
        {
            if ( $message !== null ) {
                $this->message = trans( $message );
            }

            return $this;
        }

        /**
         * Returns the confirm message
         *
         * @return string
         */
        public function getMessage(): string
        {
            return $this->message;
        }

        /**
         * Return whether or not a confirm message is set
         *
         * @return bool
         */
        public function hasMessage(): bool
        {
            return ( $this->message == null ) ? false : true;
        }

        /**
         * Sets the confirm button label
         *
         * @param string $confirmButtonLabel
         *
         * @return $this
         */
        public function setConfirmButtonLabel( ?string $confirmButtonLabel ): self
        {
            if ( $confirmButtonLabel !== null ) {
                $this->confirmButtonLabel = trans( $confirmButtonLabel );
            }

            return $this;
        }

        /**
         * Returns the confirm button label
         *
         * @return string
         */
        public function getConfirmButtonLabel(): string
        {
            return $this->confirmButtonLabel;
        }

        /**
         * Returns whether or not a confirm button label is set
         *
         * @return bool
         */
        public function hasConfirmButtonLabel(): bool
        {
            return ( $this->confirmButtonLabel == null ) ? false : true;
        }

        /**
         * Sets the confirm action http method
         *
         * @param string $method
         *
         * @return $this
         */
        public function setMethod( string $method )
        {
            $method = strtoupper( $method );

            if ( !in_array( $method , [ 'GET' , 'POST' , 'PUT' , 'DELETE' , 'HEAD' , 'OPTIONS' ] ) ) {
                $method = 'POST';
            }

            $this->method = $method;

            return $this;
        }

        /**
         * Returns the confirm action http method
         *
         * @return string
         */
        public function getMethod()
        {
            return $this->method;
        }
    }