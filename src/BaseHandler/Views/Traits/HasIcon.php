<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\Traits;

    use DTV\BaseHandler\Views\Components\Icon;

    /**
     * Has Icon Trait
     *
     * @package   DTV\BaseHandler\Views\Traits
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    trait HasIcon
    {
        /**
         * Icon instance
         *
         * @var Icon|null
         */
        protected $icon = null;

        /**
         * Sets the Icon of an object
         *
         * @param Icon|string $icon Icon instance or an icon class name
         *
         * @return static
         */
        public function setIcon(Icon|string $icon): static
        {
            if (is_string($icon)) {
                $icon = new Icon($icon);
            }

            $this->icon = $icon;

            return $this;
        }

        /**
         * Returns whether or not an icon is set
         *
         * @return bool
         */
        public function hasIcon(): bool
        {
            return $this->icon !== null;
        }

        /**
         * Returns the icon or null if no such icon was set
         *
         * @return Icon|null
         */
        public function getIcon(): ?Icon
        {
            return $this->icon;
        }
    }
