<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\Traits;

    use DTV\BaseHandler\Views\FormInputs\Checkbox;
    use DTV\BaseHandler\Views\FormInputs\Color;
    use DTV\BaseHandler\Views\FormInputs\Custom;
    use DTV\BaseHandler\Views\FormInputs\Date;
    use DTV\BaseHandler\Views\FormInputs\Datetime;
    use DTV\BaseHandler\Views\FormInputs\Decimal;
    use DTV\BaseHandler\Views\FormInputs\File;
    use DTV\BaseHandler\Views\FormInputs\FormInput;
    use DTV\BaseHandler\Views\FormInputs\Hidden;
    use DTV\BaseHandler\Views\FormInputs\IconCheckbox;
    use DTV\BaseHandler\Views\FormInputs\Money;
    use DTV\BaseHandler\Views\FormInputs\NullableIconCheckbox;
    use DTV\BaseHandler\Views\FormInputs\Number;
    use DTV\BaseHandler\Views\FormInputs\Password;
    use DTV\BaseHandler\Views\FormInputs\Select;
    use DTV\BaseHandler\Views\FormInputs\Textarea;
    use DTV\BaseHandler\Views\FormInputs\Text;
    use DTV\BaseHandler\Views\FormInputs\Time;
    use Exception;
    use Throwable;

    /**
     * Form Builder Trait
     *
     * @package   DTV\BaseHandler\Views\Traits
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    trait FormBuilder
    {
        /**
         * Adds an text input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Text
         */
        public function addText($name, $label = null)
        {
            return $this->addInput(new Text($name, $label));
        }

        /**
         * Adds an select input with the given options to the form
         *
         * @param string $name
         * @param array  $values
         * @param string $label
         *
         * @return Select
         */
        public function addSelect($name, $values, $label = null)
        {
            return $this->addInput(new Select($name, $values, $label));
        }

        /**
         * Adds an textarea to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Textarea
         */
        public function addTextarea($name, $label = null)
        {
            return $this->addInput(new Textarea($name, $label));
        }

        /**
         * Adds an checkbox to the form
         *
         * @param string $name
         * @param string $label
         * @param string $checkedValue
         *
         * @return Checkbox
         */
        public function addCheckBox($name, $label = null, $checkedValue = '1')
        {
            return $this->addInput(new Checkbox($name, $label, $checkedValue));
        }

        /**
         * Adds an icon checkbox to the form
         *
         * @param string $name
         * @param string $label
         * @param string $checkedValue
         *
         * @return IconCheckbox
         */
        public function addIconCheckBox($name, $label = null, $checkedValue = '1')
        {
            return $this->addInput(new IconCheckbox($name, $label, $checkedValue));
        }

        /**
         * Adds an nullable icon checkbox to the form
         *
         * @param string     $name
         * @param string     $label
         * @param string|int $checkedValue
         *
         * @return NullableIconCheckbox
         */
        public function addNullableIconCheckBox($name, $label = null, $checkedValue = 1)
        {
            return $this->addInput(new NullableIconCheckbox($name, $label, $checkedValue));
        }

        /**
         * Adds an date input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Date
         */
        public function addDate($name, $label = null)
        {
            return $this->addInput(new Date($name, $label));
        }

        /**
         * Adds an time input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Time
         */
        public function addTime($name, $label = null)
        {
            return $this->addInput(new Time($name, $label));
        }

        /**
         * Adds an date time input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Datetime
         */
        public function addDateTime($name, $label = null)
        {
            return $this->addInput(new Datetime($name, $label));
        }

        /**
         * Adds an password input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Password
         */
        public function addPassword($name, $label = null)
        {
            return $this->addInput(new Password($name, $label));
        }

        /**
         * Adds an color input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Color
         */
        public function addColor($name, $label = null)
        {
            return $this->addInput(new Color($name, $label));
        }

        /**
         * Adds an hidden input to the form
         *
         * @param string $name
         *
         * @return Text
         */
        public function addHidden($name)
        {
            return $this->addInput(new Hidden($name));
        }

        /**
         * Adds an money input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Money
         */
        public function addMoney($name, $label = null)
        {
            return $this->addInput(new Money($name, $label));
        }

        /**
         * Adds an decimal input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Money
         */
        public function addDecimal($name, $label = null)
        {
            return $this->addInput(new Decimal($name, $label));
        }

        /**
         * Adds an number input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Number
         */
        public function addNumber($name, $label = null)
        {
            return $this->addInput(new Number($name, $label));
        }

        /**
         * Adds an file input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return File
         */
        public function addFile($name, $label = null)
        {
            return $this->addInput(new File($name, $label));
        }

        /**
         * Adds an dynamic by the given type specified input to the form
         *
         * @param string $type
         * @param string $name
         * @param string $label
         * @param array  $extraValidations
         * @param mixed  $options
         *
         * @throws Exception
         * @return Color|Date|Select|Text|Textarea|Time|File|Password
         */
        public function addDynamic(string $type, string $name, $label = null, array $extraValidations = [], $options = null)
        {
            if (in_array($type, ['array', 'model', 'builder']) && $options === null) {
                throw new Exception('Missing parameter $options for ' . $type . ' input for the field ' . $name);
            }

            switch ($type) {
                case 'int' :
                    // Int
                    return $this->addText($name, $label)
                        ->setValidationRule(array_merge($extraValidations, ['numeric', 'integer']));
                case 'percent' :
                    // Percent
                    return $this->addText($name, $label)
                        ->setValidationRule(array_merge($extraValidations, ['numeric', 'integer', 'min:0', 'max:100']));
                case 'bool' :
                    // Bool
                    return $this->addSelect($name, Select::getBoolSelectOptions(), $label)
                        ->setValidationRule(array_merge($extraValidations, ['boolean']));
                case 'float' :
                    // Float
                    return $this->addText($name, $label)
                        ->setValidationRule(array_merge($extraValidations, ['numeric', 'float']));
                case 'text' :
                    // Text
                    return $this->addTextArea($name, $label)
                        ->setValidationRule($extraValidations);
                case 'date' :
                    // Date
                    return $this->addDate($name, $label)
                        ->setValidationRule($extraValidations);
                case 'time' :
                    // Time
                    return $this->addTime($name, $label)
                        ->setValidationRule($extraValidations);
                case 'color' :
                    // Color
                    return $this->addColor($name, $label)
                        ->setValidationRule($extraValidations);
                case 'array':
                    // array
                    return $this->addSelect($name, $options, $label)
                        ->setValidationRule(array_merge($extraValidations));
                case 'model':
                    // Model
                    return $this->addSelect($name, $options::getSelectOptions(), $label)
                        ->setValidationRule(array_merge($extraValidations));
                case 'builder':
                    // Builder
                    return $this->addSelect($name, $options->getModel()::getSelectOptions($options), $label)
                        ->setValidationRule(array_merge($extraValidations));
                case 'file':
                    // File
                    $input = $this->addFile($name, $label)
                        ->setValidationRule(array_merge($extraValidations));

                    if (!empty($options)) {
                        $input->setAcceptFormats($options);
                    }

                    return $input;
                case 'password':
                    // Password
                    return $this->addPassword($name, $label)
                        ->setValidationRule(array_merge($extraValidations))
                        ->setSecure();
                default:
                    // String
                    return $this->addText($name, $label)
                        ->setValidationRule($extraValidations);
            }
        }

        /**
         * Adds some custom HTML to the form
         *
         * @param string $html
         *
         * @return Custom
         */
        public function addCustomHtml($html)
        {
            return $this->addInput(new Custom($html));
        }

        /**
         * Adds an custom View to the form
         *
         * @param string $view
         * @param array  $parameter
         *
         * @throws Throwable
         *
         * @return FormInput
         */
        public function addCustomView($view, array $parameter = [])
        {
            return $this->addCustomHtml(view($view, $parameter)->render());
        }
    }
