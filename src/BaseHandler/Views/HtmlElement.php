<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views;

    use Illuminate\Contracts\Support\Htmlable;

    /**
     * HTML Element Class
     *
     * @package   DTV\BaseHandler\Views
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    abstract class HtmlElement implements Htmlable
    {
        /**
         * Array of additional attributes
         *
         * @var array
         */
        public $attributes = [];

        /**
         * Renders the html element
         *
         * @return string
         */
        abstract public function render();

        /**
         * Returns the rendered html element
         *
         * @throws \Throwable
         *
         * @return string
         */
        public function toHtml()
        {
            return (string)$this->render();
        }

        /**
         * Returns the rendered html element
         *
         * @throws \Throwable
         *
         * @return string
         */
        public function __toString(): string
        {
            return $this->toHtml();
        }

        /**
         * Adds an attribute to the element
         *
         * @param string $attr
         * @param string $value
         *
         * @return $this
         */
        public function addAttribute( string $attr , string $value )
        {
            $this->attributes[] = (object)[
                'key'   => $attr ,
                'value' => $value ,
            ];

            return $this;
        }

        /**
         * Returns all additional attributes
         *
         * @return array
         */
        public function getAttributes(): array
        {
            return $this->attributes;
        }
    }