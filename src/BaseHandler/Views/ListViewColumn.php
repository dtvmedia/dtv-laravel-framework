<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views;

    /**
     * List View Column Class
     *
     * @package   DTV\BaseHandler\Views
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class ListViewColumn
    {
        /**
         * The columns name used for translation
         *
         * @var string
         */
        protected $name;

        /**
         * The datapath or closure which calculates the data of the column to be shown
         *
         * @var \Closure|null|string
         */
        protected $datapath;

        /**
         * The underlaying DB columns which should be used for search filters etc
         *
         * @var array
         */
        protected $fields;

        /**
         * The underlaying DB column used for sorting
         *
         * @var string|null
         */
        protected $sortingField;

        /**
         * An valid css width value
         *
         * @var string|null
         */
        protected $width;

        /**
         * Array of CSS classes
         *
         * @var array
         */
        protected $classes = [];

        /**
         * Flag which handles if html is in the cell content allowed or if it will escaped
         *
         * @var bool
         */
        protected $htmlEnabled = false;

        /**
         * Sorting enabled flag
         *
         * @var bool
         */
        protected $sortingEnabled = true;

        /**
         * Search enabled flag
         *
         * @var bool
         */
        protected $searchEnabled = true;

        /**
         * Creates a new column for an List View
         *
         * @param string          $name
         * @param string|\Closure $datapath
         * @param string|array    $fields
         */
        public function __construct( string $name , $datapath = null , $fields = [] )
        {
            $this->name = $name;
            $this->datapath = ( $datapath == null ) ? $name : $datapath;
            $this->fields = ( is_array( $fields ) ) ? $fields : [ $fields ];

            // if an array of fields is given then use the first one as sorting field
            if ( !empty( $this->fields ) ) {
                $this->sortingField = $this->fields[ 0 ];
            }
        }

        /**
         * Sets the column width
         *
         * @param string|int $width An valid css width value or just an integer which will be handeled as pixel value
         *
         * @return $this
         */
        public function setWidth( $width ): self
        {
            if ( is_numeric( $width ) ) {
                $this->width = intval( $width ) . 'px';
            } else {
                $this->width = $width;
            }

            return $this;
        }

        /**
         * Adds an CSS class to the columns cells
         *
         * @param string $class
         *
         * @return $this
         */
        public function addClass( string $class ): self
        {
            $this->classes[] = $class;

            return $this;
        }

        /**
         * Sets the Display Break Point (in px) where the column should not be displayed
         *
         * @param int $displayPoint
         *
         * @return $this
         */
        public function setDisplayPoint( int $displayPoint ): self
        {
            $displayPoint = putInRange( round( $displayPoint , - 2 ) , 300 , 1400 );

            $this->classes[ - 1 ] = 'hide-' . intval( $displayPoint );

            return $this;
        }

        /**
         * Disables sorting for this column
         *
         * @return $this
         */
        public function disabledSorting()
        {
            $this->sortingEnabled = false;
            $this->sortingField = null;

            return $this;
        }

        /**
         * Disables search for this column
         *
         * @return $this
         */
        public function disabledSearch()
        {
            $this->searchEnabled = false;

            return $this;
        }

        /**
         * Returns whether or not sorting is enabled for this column
         *
         * @return bool
         */
        public function isSortingEnabled()
        {
            return $this->sortingEnabled;
        }

        /**
         * Returns whether or not search is enabled for this column
         *
         * @return bool
         */
        public function isSearchEnabled()
        {
            return $this->searchEnabled;
        }

        /**
         * Enables the htmp output mode for the cells of the current column
         *
         * @return $this
         */
        public function enableHtml()
        {
            $this->htmlEnabled = true;

            return $this;
        }

        /**
         * Returns whether or not the html output is enabled
         *
         * @return bool
         */
        public function isHtmlEnabled()
        {
            return $this->htmlEnabled;
        }

        /**
         * Returns the columns name
         *
         * @return string
         */
        public function getName(): string
        {
            return $this->name;
        }

        /**
         * Returns the datapath or closure for the columns values
         *
         * @return \Closure|null|string
         */
        public function getDatapath()
        {
            return $this->datapath;
        }

        /**
         * Returns the underlaying DB columns
         *
         * @return array
         */
        public function getFields(): array
        {
            return $this->fields;
        }

        /**
         * Returns the underlaying DB column used for sorting
         *
         * @return string
         */
        public function getSortingField(): string
        {
            return $this->sortingField;
        }

        /**
         * Returns the css width value of the column
         *
         * @return null|string
         */
        public function getWidth(): ?string
        {
            return $this->width;
        }

        /**
         * Returns the array of all css classes
         *
         * @return array
         */
        public function getClasses(): array
        {
            return $this->classes;
        }

        /**
         * Returns an space seperated string with all css classes
         *
         * @return string
         */
        public function getClassString(): string
        {
            return implode( ' ' , $this->classes );
        }

        /**
         * Returns the additonal Html attributes for cells of this column
         *
         * @param bool $forHeader If true all attributes for the table header cells will be returned
         *
         * @return string
         */
        public function getHtmlAttributes( $forHeader = false ): string
        {
            $html = '';

            if ( $this->sortingField !== null ) {
                $this->addClass('sortable field-' . escapeHtmlLabel($this->sortingField));
            }

            if ( !empty( $this->classes ) ) {
                $html .= ' class="' . $this->getClassString() . '"';
            }

            if( $forHeader ) {
                if ( !empty( $this->width ) ) {
                    $html .= ' style="width: ' . $this->getWidth() . '"';
                }

                if( $this->sortingField !== null ) {
                    $html .= ' data-field="' . $this->getSortingField() . '"';
                }
            }

            return $html . ' ';
        }
    }