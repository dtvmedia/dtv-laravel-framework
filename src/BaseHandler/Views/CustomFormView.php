<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views;

    use DTV\BaseHandler\Views\FormInputs\FormInput;
    use Exception;
    use Illuminate\Database\Eloquent\Model;
    use Throwable;

    /**
     * Custom Form View Class
     *
     * @package   DTV\BaseHandler\Views
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    abstract class CustomFormView extends BaseFormView
    {
        /**
         * Render view name
         *
         * @var string
         */
        protected $view = null;

        /**
         * View parameter array
         *
         * @var array
         */
        protected $parameters = [];

        /**
         * Defines the key under which the model gets published to the view
         *
         * @var string
         */
        protected $modelViewKey = 'model';

        /**
         * Render layout view name
         *
         * @var string
         */
        protected $layout = null;

        /**
         * Page mode
         *
         * @var int
         */
        protected $pageMode = Page::MODE_PAGE;

        /**
         * Page modal size (only used if page mode is modal)
         *
         * @var string
         */
        protected $pageModalSize = Page::MODAL_SIZE_LG;

        /**
         * Flag which enable/disables the client validations for all form inputs
         *
         * @var bool
         */
        protected $enableClientValidations = true;

        /**
         * CustomFormView constructor.
         *
         * @param Model|null $model
         */
        public function __construct( ?Model $model = null )
        {
            parent::__construct( $model );

            if ( $this->layout === null ) {
                $this->layout = config( 'dtv.layouts.page_layout' );
            }
        }

        /**
         * Sets the render view name
         *
         * @param string $view
         *
         * @return $this
         */
        public function setView( string $view ): self
        {
            $this->view = $view;

            return $this;
        }

        /**
         * Returns the render view name
         *
         * @return string
         */
        public function getView(): string
        {
            return $this->view;
        }

        /**
         * Sets an view parameter (overwrites existing ones)
         *
         * @param string $key
         * @param mixed  $value
         *
         * @return CustomFormView
         */
        public function setParameter( string $key , $value ): self
        {
            $this->parameters[ $key ] = $value;

            return $this;
        }

        /**
         * Returns the view parameter array
         *
         * @param bool $build
         *
         * @throws Exception
         *
         * @return array
         */
        public function getParameters( bool $build = false ): array
        {
            if ( $build ) {
                $this->build();
            }

            $this->setParameter( 'form' , $this );
            $this->setParameter( 'label' , $this->getLabel() );
            $this->setParameter( 'title' , $this->getTitle() );
            $this->setParameter( 'icon' , $this->getIcon() );
            $this->setParameter( 'layout' , $this->getLayout() );
            $this->setParameter( $this->modelViewKey , $this->model );

            return $this->parameters;
        }

        /**
         * Returns whether a input given by its id exists
         *
         * @param string $id
         *
         * @throws Exception
         * @return bool
         */
        public function hasInput(string $id): bool
        {
            $input = $this->getInputs()[ $id ] ?? null;

            return $input !== null;
        }

        /**
         * Returns an specific input by its id
         *
         * @param string $id
         *
         * @throws Exception
         *
         * @return FormInput|null
         */
        public function getInput( string $id )
        {
            $input = $this->getInputs()[ $id ] ?? null;

            if ( $input === null ) {
                return null;
            }

            return $this->prepareInput( $input );
        }

        /**
         * Returns the page mode of the form
         *
         * @return int
         */
        public function getPageMode(): int
        {
            return $this->pageMode;
        }

        /**
         * Prepares and updates some properties of the input
         *
         * @param FormInput $input
         *
         * @throws Exception
         *
         * @return FormInput
         */
        private function prepareInput( FormInput $input )
        {
            // set correct label
            $input->setLabel( $this->trans( 'fields' , $input->getLabel() ) );

            // Disables client validations
            if ( $this->enableClientValidations === false ) {
                $input->disableClientValidations();
            }

            // Set correct default value
            if ( $this->isUpdateMode() ) {
                $default = $this->getValue( $input );

                $input->setDefaultValue( $default );
            }

            return $input;
        }

        /**
         * Renders the form as a modal
         *
         * @throws Throwable
         *
         * @return string
         */
        public function render()
        {
            $this->build();

            return view( $this->getView() , $this->getParameters() )->render();
        }
    }
