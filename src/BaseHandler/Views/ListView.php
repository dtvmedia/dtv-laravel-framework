<?php

    /** @noinspection PhpUnused */

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views;

    use Closure;
    use DTV\BaseHandler\Views\Components\ListViewButton;
    use DTV\BaseHandler\Views\Components\ListViewConfirmButton;
    use Exception;
    use Illuminate\Contracts\Database\Query\Expression;
    use Illuminate\Contracts\Support\Responsable;
    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\Relations\BelongsTo;
    use Illuminate\Database\Eloquent\Relations\HasOne;
    use Illuminate\Database\Eloquent\Relations\Relation;
    use Illuminate\Http\JsonResponse;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Http\Request;
    use Throwable;

    /**
     * List View Class
     *
     * @package   DTV\BaseHandler\Views
     * @copyright 2017 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    abstract class ListView extends View implements Responsable
    {
        /**
         * List View Data Query
         *
         * @var Builder|null
         */
        private $query;

        /**
         * Array of all registered ListViewMetas
         *
         * @var ListViewMeta[]
         */
        private array $metaColumns = [];

        /**
         * Array of all registered ListViewColumns
         *
         * @var ListViewColumn[]
         */
        private array $columns = [];

        /**
         * Array of all registered button with their checks
         *
         * @var ListViewButton[]
         */
        private array $buttons = [];

        /**
         * Number of all results overall
         *
         * @var int
         */
        private int $countOverall = 0;

        /**
         * Number of all results if search filters are active
         *
         * @var null|int
         */
        private ?int $countFiltered = null;

        /**
         * The current, already processed Dataset
         *
         * @var array
         */
        private array $data = [];

        /**
         * Current Page
         *
         * @var int
         */
        protected $page = 1;

        /**
         * Number of row of a page
         *
         * @var int
         */
        protected $pageLength = null;

        /**
         * Search String
         *
         * @var null|string
         */
        protected $search = null;

        /**
         * Order Column
         *
         * @var string
         */
        protected $orderColumn = 'id';

        /**
         * Order Direction
         *
         * @var string
         */
        protected $orderDirection = 'asc';

        /**
         * View Icon Class
         *
         * @var string
         */
        protected $icon = '';

        /**
         * Flag which shows if the view was already built or not
         *
         * @var bool
         */
        private bool $built = false;

        /**
         * Flag which decides whether or not the counter will be shown
         *
         * @var bool
         */
        protected $showCounter = true;

        /**
         * Flag which decides whether or not the page length select will be shown
         *
         * @var bool
         */
        protected $showPageLengthSelect = true;

        /**
         * Flag which decides whether or not the search input will be shown
         *
         * @var bool
         */
        protected $showSearchInput = true;

        /**
         * Flag which decides whether or not the pagination will be shown
         *
         * @var bool
         */
        protected $showPagination = true;

        /**
         * Flag which decides whether or not the sorting should be enabled
         *
         * @var bool
         */
        protected $enableSorting = true;

        /**
         * Closure which handles the fixed secondary order
         *
         * @var Closure|null
         */
        protected $secondaryOrder = null;

        /**
         * List View constructor
         *
         * @param Builder $query
         *
         * @throws Exception
         */
        public function __construct( $query = null )
        {
            $this->query = $query;

            if ( $this->pageLength === null ) {
                $this->pageLength = config( 'dtv.app.list_page_length' , 10 );
            }

            $this->setIcon( $this->icon );
        }

        /**
         * Hides the row counter info
         *
         * @param bool $hideCounter
         *
         * @return $this
         */
        public function hideCounter( bool $hideCounter = true )
        {
            $this->showCounter = !$hideCounter;

            return $this;
        }

        /**
         * Hides the page length select
         *
         * @param bool $hidePageLengthSelect
         *
         * @return $this
         */
        public function hidePageLengthSelect( bool $hidePageLengthSelect = true )
        {
            $this->showPageLengthSelect = !$hidePageLengthSelect;

            return $this;
        }

        /**
         * Hides the search input
         *
         * @param bool $hideSearchInput
         *
         * @return $this
         */
        public function hideSearchInput( bool $hideSearchInput = true )
        {
            $this->showSearchInput = !$hideSearchInput;

            return $this;
        }

        /**
         * Hides the pagination buttons
         *
         * @param bool $hidePagination
         *
         * @return $this
         */
        public function hidePagination( bool $hidePagination = true )
        {
            $this->showPagination = !$hidePagination;

            return $this;
        }

        /**
         * Disables the sorting functionality
         *
         * @param bool $disableSorting
         *
         * @return $this
         */
        public function disableSorting( bool $disableSorting = true )
        {
            $this->enableSorting = !$disableSorting;

            return $this;
        }

        /**
         * Adds an column for the List View
         *
         * @param string         $name
         * @param string|Closure $datapath
         * @param string|array   $fields
         *
         * @return ListViewColumn
         */
        public function addColumn( string $name , $datapath = null , $fields = [] )
        {
            $listViewColumn = new ListViewColumn( $name , $datapath , $fields );

            $this->columns[] = $listViewColumn;

            return $listViewColumn;
        }

        /**
         * Adds an meta info for the List View
         *
         * @param string         $name
         * @param string|Closure $datapath
         *
         * @return ListViewMeta
         */
        public function addMeta( string $name , $datapath = null )
        {
            $listViewMeta = new ListViewMeta( $name , $datapath );

            $this->metaColumns[] = $listViewMeta;

            return $listViewMeta;
        }

        /**
         * Adds an Button the the List View rows
         *
         * @param string $label
         * @param string $icon
         * @param string $route
         * @param array  $fixParameters
         *
         * @return ListViewButton
         */
        public function addButton( $label , $icon , $route , $fixParameters = [] )
        {
            $button = new ListViewButton( $this->trans( 'buttons' , $label ) , $route , $fixParameters );
            $button->setIcon( $icon );
            $button->setDynamicParameter( 'id' );
            $button->useAjax();
            $button->showLabel( false );

            $this->buttons[] = $button;

            return $button;
        }

        /**
         * Adds an Button the the List View rows
         *
         * @param string $label
         * @param string $icon
         * @param string $route
         * @param array  $fixParameters
         *
         * @return ListViewConfirmButton
         */
        public function addConfirmButton( $label , $icon , $route , $fixParameters = [] )
        {
            $button = new ListViewConfirmButton( $this->trans( 'buttons' , $label ) , $route , $fixParameters );
            $button->setIcon( $icon );
            $button->setDynamicParameter( 'id' );
            $button->useAjax();
            $button->showLabel( false );

            $this->buttons[] = $button;

            return $button;
        }

        /**
         * The function which should add the columns in the derived classes
         *
         * @return void
         */
        abstract protected function columns();

        /**
         * The function which should add the row buttons in the derived classes
         *
         * @return void
         */
        protected function buttons()
        {

        }

        /**
         * Handles the current request
         */
        private function handleRequest()
        {
            /** @var Request $request */
            $request = request();

            if ( $request->has( 'lvPageLength' ) ) {
                $this->pageLength = $request->get( 'lvPageLength' , 20 );
            }
            if ( $request->has( 'lvPage' ) ) {
                $this->page = $request->get( 'lvPage' , 1 );
            }
            if ( $request->has( 'lvSearch' ) ) {
                $this->search = $request->get( 'lvSearch' );
            }
            if ( $request->has( 'lvOrderColumn' ) ) {
                $this->orderColumn = $request->get( 'lvOrderColumn' , 'id' );
            }
            if ( $request->has( 'lvOrderDirection' ) ) {
                $this->orderDirection = $request->get( 'lvOrderDirection' , 'asc' );
            }
        }

        /**
         * Builds and executes the DB Query
         */
        private function setFilters()
        {
            // handle the current request
            $this->handleRequest();

            // get count of whole dataset
            $this->countOverall = ( clone $this->query )->count();

            // Apply query filters
            $this->setSearchFilters();
            $this->setOrderFilters();
            $this->setSecondaryOrderFilters();
            $this->setPaginationFilters();
        }

        /**
         * Applies the search filters
         */
        private function setSearchFilters()
        {
            if ( empty( $this->search ) ) {
                return;
            }

            $filtered = false;
            $this->query->where( function ( Builder $q ) use ( &$filtered ) {
                $applied = false;
                foreach ( $this->columns as $column ) {
                    if (!$column->isSearchEnabled()) {
                        continue;
                    }

                    if ( count( $column->getFields() ) > 0 ) {
                        $filtered = true;
                        $this->setColumnSearchFilters( $q , $column->getFields() , $applied );
                    }
                }
            } );

            // get whole count of filtered dataset
            if ($filtered === true) {
                $this->countFiltered = (clone $this->query)->count();
            }
        }

        /**
         * Applies the search filters for one specific column
         *
         * @param Builder $query
         * @param array   $fields
         * @param bool    $applied
         */
        private function setColumnSearchFilters( Builder $query , array $fields , bool &$applied )
        {
            $searchRelations = [];

            foreach ( $fields as $field ) {
                if ( str_contains( $field , '.' ) ) {
                    // save relation fields for later
                    [ $relation , $rest ] = explode( '.' , $field , 2 );
                    $searchRelations[ $relation ][] = $rest;
                } else {
                    // Simple column searchs can be executed here directly
                    if ( $applied === true ) {
                        $query->orWhere( $field , 'LIKE' , '%' . $this->search . '%' );
                    } else {
                        $query->where( $field , 'LIKE' , '%' . $this->search . '%' );
                        $applied = true;
                    }
                }
            }

            // Recursive relation search handling
            foreach ( $searchRelations as $key => $subColumns ) {
                $query->orWhereHas( $key , function ( Builder $q ) use ( $subColumns ) {
                    $q->where( function ( $q ) use ( $subColumns ) {
                        $applied = false;
                        $this->setColumnSearchFilters( $q , $subColumns , $applied );
                    } );
                } );
            }

            $applied = true;
        }

        /**
         * Applies the order query filters
         *
         * ToDo: at the moment we dont support multi level relation ordering and ordering with category fields
         */
        private function setOrderFilters()
        {
            $model = $this->query->getModel();
            $table = $model->getTable();

            $relation = $this->orderColumn;
            $rest = null;

            if ( str_contains( $this->orderColumn , '.' ) ) {
                [ $relation , $rest ] = explode( '.' , $this->orderColumn , 2 );
            }

            if ( !method_exists( $model , $relation ) ) {
                // if $part is no relation method we just try to order by it assuming its a simple column
                $this->query->orderBy( $relation , $this->orderDirection );

                return;
            }

            // Relation method handling
            $relationObj = $model->$relation();

            if ( !$relationObj instanceof BelongsTo && !$relationObj instanceof HasOne ) {
                // if the method return value is not a valid belongsTo or hasOne relation object we can't order at this point
                return;
            }

            $foreignKey = $relationObj->getForeignKeyName();
            $foreignTable = $relationObj->getQuery()->getModel()->getTable();

            $order = null;
            if ( $relationObj instanceof BelongsTo ) {
                $order = DB::raw( '(SELECT ' . $foreignTable . '.' . $rest . ' FROM ' . $foreignTable . ' WHERE ' . $foreignTable . '.id = ' . $table . '.' . $foreignKey . ')' );
            }

            if ( $relationObj instanceof HasOne ) {
                $order = DB::raw( '(SELECT `' . $foreignTable . '`.`' . $rest . '` FROM `' . $foreignTable . '` WHERE `' . $foreignTable . '`.`' . $foreignKey . '` = `' . $table . '`.`id`)' );
            }

            if ($order instanceof Expression) {
                $this->query->orderBy( $order , $this->orderDirection );
            }
        }

        /**
         * Applies the pagination query filters
         */
        private function setPaginationFilters()
        {
            $this->query->limit( $this->pageLength )->offset( $this->pageLength * ( $this->page - 1 ) );
        }

        /**
         * Applies an fixed secondary order
         */
        private function setSecondaryOrderFilters()
        {
            if ( $this->secondaryOrder instanceof Closure ) {
                $closure = $this->secondaryOrder;
                $closure( $this->query );
            }
        }

        /**
         * Executes the Query, processes the resulting dataset and build all buttons
         *
         * @throws Throwable
         */
        private function processDataset()
        {
            foreach ( $this->query->get() as $key => $row ) {
                // process meta infos
                $metaInfos = [];
                foreach ( $this->metaColumns as $meta ) {
                    $metaInfos[ $meta->getName() ] = static::getDataField( $row , $meta->getDatapath() );
                }
                $this->data[ $key ][ 'lvMeta' ] = $metaInfos;

                // process columns
                foreach ( $this->columns as $column ) {
                    $this->data[ $key ][ $column->getName() ] = static::getDataField( $row , $column->getDatapath() );

                    if ( !$column->isHtmlEnabled() ) {
                        $this->data[ $key ][ $column->getName() ] = e( $this->data[ $key ][ $column->getName() ] );
                    }
                }

                // process buttons
                if ( count( $this->buttons ) > 0 ) {
                    $this->data[ $key ][ 'lvButtons' ] = '<div class="btn-group">';

                    foreach ( $this->buttons as $button ) {
                        $this->data[ $key ][ 'lvButtons' ] .= $this->getButton( $row , $button );
                    }

                    $this->data[ $key ][ 'lvButtons' ] .= '</div>';
                }
            }
        }

        /**
         * Gets the date of a field
         *
         * @param Model          $row
         * @param string|Closure $datapath
         * @param string         $default
         *
         * @return string
         */
        public static function getDataField( $row , $datapath , $default = '-' )
        {
            $context = $row;
            $part = $datapath;

            if ( $datapath instanceof Closure ) {
                return (string) $datapath( $row );
            }

            foreach ( explode( '.' , $part ) as $part ) {
                if ( !is_object( $context ) ) {
                    break;
                }

                if ( method_exists( $context , $part ) ) {
                    $result = $context->$part();

                    if ( $result instanceof Relation ) {
                        $context = $context->$part ?? $default;
                    } else {
                        $context = $result;
                    }
                } else {
                    $result = $context->$part ?? $default;

                    if ( is_object( $result ) ) {
                        $context = $result;

                        continue;
                    }

                    return (string) $result;
                }
            }

            return (string) $context;
        }

        /**
         * Builds an Button
         *
         * @param Model          $row
         * @param ListViewButton $button
         *
         * @throws Throwable
         *
         * @return string
         */
        private function getButton( $row , $button )
        {
            $button->bindModel( $row );

            return $button->render();
        }

        /**
         * Returns the header row
         *
         * @return array
         */
        private function getHeaderRow()
        {
            $headerRow = [];

            foreach ( $this->columns as $column ) {
                $headerRow[] = $this->trans( 'fields' , $column->getName() );
            }

            if ( count( $this->buttons ) > 0 ) {
                $headerRow[] = trans( 'dtv.base::listview.actions' );
            }

            return $headerRow;
        }

        /**
         * Returns the base url for later ajax requests
         *
         * @return string
         */
        protected function getUrl()
        {
            return request()->url();
        }

        /**
         * Returns the row counts
         *
         * @return object
         */
        private function getCounts()
        {
            return (object) [
                'filtered' => $this->countFiltered ,
                'all'      => $this->countOverall ,
            ];
        }

        /**
         * Returns the number of all related rows
         *
         * @throws Throwable
         *
         * @return int
         */
        public function count()
        {
            $this->build();

            return $this->countOverall;
        }

        /**
         * Builds the ListView Response
         *
         * @throws Throwable
         */
        private function build()
        {
            if ($this->built === true) {
                return;
            }

            $this->columns();
            app( 'hooks' )->execute( 'listView' , $this );
            $this->buttons();

            $this->setFilters();
            $this->processDataset();
            $this->built = true;
        }

        /**
         * Checks whether or not this view has an update requested
         *
         * @param Request $request
         *
         * @throws Exception
         * @return bool
         */
        public function hasUpdateRequested( Request $request )
        {
            if ( $request->ajax() && $request->has( 'lvName' ) && $request->get( 'lvName' ) == $this->getLabel() ) {
                return true;
            }

            return false;
        }

        /**
         * Returns an json response with the filtered data
         *
         * @param Request $request
         *
         * @throws Throwable
         *
         * @return JsonResponse
         */
        public function toResponse( $request )
        {
            $this->build();

            return response()->json( [
                'data'           => $this->data ,
                'count_filtered' => $this->countFiltered ,
                'count_all'      => $this->countOverall ,
            ] );
        }

        /**
         * Renders the List View with the current dataset
         *
         * @throws Throwable
         *
         * @return string
         */
        public function render()
        {
            $this->build();

            $viewData = (object) [
                'label'          => $this->getLabel() ,
                'title'          => $this->getTitle() ,
                'icon'           => $this->getIcon() ,
                'show_title'     => $this->getShowTitle() ,
                'url'            => $this->getUrl() ,
                'header'         => $this->getHeaderRow() ,
                'data'           => $this->data ,
                'count_filtered' => $this->countFiltered ,
                'count_all'      => $this->countOverall ,
                'columns'        => $this->columns ,
                'counts'         => $this->getCounts() ,
                'options'        => (object) [
                    'counter'          => $this->showCounter ,
                    'pageLength'       => $this->pageLength ,
                    'pageLengthSelect' => $this->showPageLengthSelect ,
                    'searchInput'      => $this->showSearchInput ,
                    'pagination'       => $this->showPagination ,
                    'sorting'          => $this->enableSorting ,
                ] ,
                'sorting'        => (object) [
                    'column'    => $this->orderColumn ,
                    'direction' => $this->orderDirection ,
                ]
            ];

            return view( 'dtv.base::list_view' , [ 'list' => $viewData ] )->render();
        }
    }
