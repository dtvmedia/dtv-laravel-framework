<?php
/** @noinspection PhpUnused */

/**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views;

    use Illuminate\Contracts\Support\Responsable;
    use Illuminate\Http\Request;
    use Symfony\Component\HttpFoundation\Response;
    use Throwable;

    /**
     * Page Builder Class
     *
     * @package   DTV\BaseHandler\Views
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Page implements Responsable
    {
        /**
         * Page mode constants
         */
        const MODE_PAGE = 1;
        const MODE_MODAL = 2;

        /**
         * Modal size constants
         */
        const MODAL_SIZE_SM = 'modal-sm';
        const MODAL_SIZE_MD = 'modal-md';
        const MODAL_SIZE_LG = 'modal-lg';
        const MODAL_SIZE_XL = 'modal-xl';

        /**
         * View name path
         *
         * @var string
         */
        protected $view;

        /**
         * View parameters array
         *
         * @var array
         */
        protected $parameter = [];

        /**
         * Array of included subviews
         *
         * @var View[]
         */
        protected $includeViews = [];

        /**
         * The page layout mode
         *
         * @var int
         */
        protected $mode = null;

        /**
         * Modal size class
         *
         * @var string
         */
        protected $size = 'modal-lg';

        /**
         * Page constructor
         *
         * @param string $view
         * @param array  $parameter
         */
        public function __construct( string $view , array $parameter = [] )
        {
            $this->view = $view;
            $this->parameter = $parameter;
        }

        /**
         * Adds an variable with the given key as name to the view
         *
         * @param string $key
         * @param mixed  $value
         *
         * @return Page
         */
        public function with( string $key , $value ): self
        {
            $this->parameter[ $key ] = $value;

            return $this;
        }

        /**
         * Adds an subview with the given key as name to the view
         *
         * @param string $key
         * @param View   $view
         *
         * @return $this
         */
        public function withView( string $key , View $view ): self
        {
            $this->includeViews[ $key ] = $view;

            return $this;
        }

        /**
         * Adds an given array to the parameters array
         *
         * @param array $with
         *
         * @return Page
         */
        public function withArray( array $with ): self
        {
            $this->parameter = array_merge( $this->parameter , $with );

            return $this;
        }

        /**
         * Sets the layout mode and size (only for modal)
         *
         * @param int    $mode
         * @param string $size
         *
         * @return Page
         */
        public function setMode( int $mode , $size = null ): self
        {
            $this->mode = $mode;

            if ( $size !== null ) {
                $this->size = $size;
            }

            return $this;
        }

        /**
         * Renders the page
         *
         * @throws Throwable
         *
         * @return string|Response
         */
        public function render()
        {
            $request = request();

            // Check if any include views have some active response hooks which needs to be handled
            foreach ( $this->includeViews as $view ) {
                if ( $view instanceof ListView && $view->hasUpdateRequested( $request ) ) {
                    return $view->toResponse( $request );
                }
            }

            // add include views to the parameters
            foreach ( $this->includeViews as $key => $view ) {
                $this->parameter[ $key ] = $view;
            }


            if ( $this->mode === self::MODE_MODAL ) {
                return json()->modal( view( $this->view , $this->parameter )->render() , $this->size )->send();
            }

            return view( $this->view , $this->parameter )->render();
        }

        /**
         * Create an HTTP response that represents the object.
         *
         * @param Request $request
         *
         * @throws Throwable
         *
         * @return Response|string
         */
        public function toResponse( $request )
        {
            return $this->render();
        }

        /**
         * Factory method, creates an new page instance
         *
         * @param string $view
         * @param array  $parameter
         *
         * @return Page
         */
        public static function create( string $view , array $parameter = [] ): self
        {
            return new self( $view , $parameter );
        }
    }
