<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views;

    use DTV\BaseHandler\Views\FormInputs\Hidden;

    /**
     * Form Builder Class
     *
     * @package   DTV\BaseHandler\Views
     * @copyright 2017 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    abstract class FormView extends BaseFormView
    {
        /**
         * Extra form css classes
         *
         * @var string
         */
        private $class = '';

        /**
         * Render layout view name
         *
         * @var string
         */
        protected $layout = 'dtv.base::layouts.form_page_layout';

        /**
         * Input Size Overflow Counter
         *
         * @var int
         */
        private $overflowCounter = 0;

        /**
         * Sets extra CSS classes of the form
         *
         * @param string $class
         *
         * @return $this
         */
        public function setClass( $class )
        {
            $this->class = $class;

            return $this;
        }

        /**
         * Returns the extra css classes of the form
         *
         * @return string
         */
        public function getClass()
        {
            return $this->class;
        }

        /**
         * Processes the input data and returns all necessary stuff as an array for the view
         *
         * @throws \Exception
         *
         * @return array
         */
        public function processInputData()
        {
            $inputs = [];

            foreach ( $this->getInputs() as $input ) {
                // Handle rows
                if ( !$input instanceof Hidden ) {
                    $this->overflowCounter += $input->getSize();
                }

                if ( $this->overflowCounter > 12 ) {
                    $inputs[] = 'row';

                    $this->overflowCounter = $input->getSize();
                }

                // Apply form transformations
                $input->setLabel( $this->trans( 'fields' , $input->getLabel() ) );
                $input->setDefaultValue( $this->getValue( $input ) );
                $input->setValidationRule( $this->checkValidations( $input ) );

                // add input data to inputs array
                $inputs[] = $input;
            }

            return $inputs;
        }

        /**
         * Renders the form as a modal
         *
         * @throws \Throwable
         *
         * @return string
         */
        public function render()
        {
            $this->build();

            $viewData = (object) [
                'label'      => $this->getLabel() ,
                'title'      => $this->getTitle() ,
                'icon'       => $this->getIcon() ,
                'show_title' => $this->getShowTitle() ,
                'action'     => $this->getAction() ,
                'method'     => $this->getMethod() ,
                'class'      => $this->getClass() ,
                'ajax'       => $this->ajax() ,
                'layout'     => $this->getLayout() ,
                'inputs'     => $this->processInputData() ,
            ];

            return view( 'dtv.base::form_view' , [ 'form' => $viewData ] )->render();
        }
    }