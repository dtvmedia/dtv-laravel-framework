<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\FormInputs;

    /**
     * Custom Form Input Class
     *
     * @package   DTV\BaseHandler\Views\FormInputs
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Custom extends FormInput
    {
        /**
         * Array of validation rules for the custom inputs
         *
         * @var array
         */
        protected $validation = [];

        /**
         * Custom constructor.
         *
         * @param string $html
         */
        public function __construct( $html )
        {
            parent::__construct( md5($html) , '' );

            $this->parameters[ 'html' ] = $html;
        }

        /**
         * Sets the validation rules array for the custom view
         *
         * @param array $rules
         *
         * @throws \Exception
         *
         * @return $this|FormInput
         */
        public function setValidationRule( $rules )
        {
            if ( !is_array( $rules ) ) {
                throw new \Exception( 'An Custom view needs an array of validations rules where the keys are the input names.' );
            }

            return parent::setValidationRule( $rules );
        }

        /**
         * Renders the html element
         *
         * @return string
         */
        public function render()
        {
            return $this->parameters[ 'html' ];
        }
    }