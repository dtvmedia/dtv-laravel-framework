<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\FormInputs;

    /**
     * Text Form Input Class
     *
     * @package   DTV\BaseHandler\Views\FormInputs
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Text extends FormInput
    {
        /**
         * Renders the html element
         *
         * @throws \Throwable
         * @return string
         */
        public function render()
        {
            return view( 'dtv.base::form_elements.text' , [ 'input' => $this ] )->render();
        }
    }