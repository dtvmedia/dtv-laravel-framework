<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\FormInputs;

    /**
     * Hidden Form Input Class
     *
     * @package   DTV\BaseHandler\Views\FormInputs
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Hidden extends FormInput
    {
        /**
         * Hidden constructor
         *
         * @param string $name
         */
        public function __construct( string $name )
        {
            parent::__construct( $name , null );
        }

        /**
         * Renders the html element
         *
         * @throws \Throwable
         * @return string
         */
        public function render()
        {
            return view( 'dtv.base::form_elements.text' , [ 'input' => $this ] )->render();
        }
    }