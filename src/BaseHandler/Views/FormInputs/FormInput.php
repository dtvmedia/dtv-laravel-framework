<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\FormInputs;

    use DTV\BaseHandler\Views\BaseFormView;
    use DTV\BaseHandler\Views\HtmlElement;
    use Illuminate\Http\Request;
    use InvalidArgumentException;

    /**
     * Form Input Class
     *
     * @package   DTV\BaseHandler\Views\FormInputs
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     *
     * @method $this|string hint(?string $hint = null)
     * @method $this|int maxlength(?int $maxlength = null)
     * @method $this|bool readonly(?bool $readonly = null)
     * @method $this|bool disabled(?bool $disabled = null)
     * @method $this|string placeholder(?string $placeholder = null)
     */
    abstract class FormInput extends HtmlElement
    {
        /**
         * Input unique id
         *
         * @var string
         */
        protected $id;

        /**
         * Input name
         *
         * @var string
         */
        protected $name;

        /**
         * Input label
         *
         * @var string
         */
        protected $label;

        /**
         * Default value
         *
         * @var mixed
         */
        protected $default = '';

        /**
         * Validation rule
         *
         * @var string|array
         */
        protected $validation = '';

        /**
         * Column size of input
         *
         * @var int
         */
        protected $size = 6;

        /**
         * Array of additional parameters or attributes
         *
         * @var array
         */
        protected $parameters = [];

        /**
         * Array of CSS classes
         *
         * @var array
         */
        protected $classes = [];

        /**
         * Flag which enables/disables the client side check of the defined validations
         *
         * @var bool
         */
        protected $clientValidationEnabled = true;

        /**
         * FormInput constructor
         *
         * @param string $name
         * @param string $label
         */
        public function __construct( string $name , $label = null )
        {
            if ( $label == null ) {
                $label = $name;
            }

            $this->name = $name;
            $this->setLabel( $label );
        }

        /**
         * Returns the input name
         *
         * @return string
         */
        public function getName(): string
        {
            return $this->name;
        }

        /**
         * Sets the form input unique id
         *
         * @param string $id
         *
         * @return $this
         */
        public function setId( string $id ): self
        {
            $this->id = $id;

            return $this;
        }

        /**
         * Returns the form input unique id
         *
         * @return string
         */
        public function getId(): string
        {
            if ( $this->id === null ) {
                return $this->getName();
            }

            return $this->id;
        }

        /**
         * Sets the form input label
         *
         * @param string $label
         *
         * @return $this
         */
        public function setLabel( string $label ): self
        {
            $trans_label = trans( $label );

            $this->label = ( is_array( $trans_label ) ) ? $label : $trans_label;

            return $this;
        }

        /**
         * Returns the input label
         *
         * @return string
         */
        public function getLabel(): string
        {
            return $this->label;
        }

        /**
         * Returns the type of input
         *
         * @return string
         */
        public function getType(): string
        {
            return strtolower( class_basename( $this ) );
        }

        /**
         * Sets the default value of the input
         *
         * @param mixed $default
         *
         * @return $this
         * @see BaseFormView::getValue()
         *
         */
        public function setDefaultValue( $default ): self
        {
            $this->default = $this->getMutator( $default );

            return $this;
        }

        /**
         * Returns the inputs default value
         *
         * @return string
         */
        public function getDefault()
        {
            return $this->default;
        }

        /**
         * Returns the inputs default value (Alias method)
         *
         * @return string
         */
        public function getValue()
        {
            return $this->default;
        }

        /**
         * Sets the column size of the input
         *
         * @param int $size
         *
         * @return $this
         */
        public function setSize( int $size )
        {
            $this->size = $size;

            return $this;
        }

        /**
         * Returns the column size of the input
         *
         * @return int
         */
        public function getSize(): int
        {
            return $this->size;
        }

        /**
         * Sets the validation rule string or array
         *
         * @param string|array $rule
         *
         * @return $this
         */
        public function setValidationRule( $rule )
        {
            $this->validation = $rule;

            return $this;
        }

        /**
         * Returns the validation rule or rules array
         *
         * @return string|array
         */
        public function getValidation()
        {
            if ( !$this instanceof Custom && is_array( $this->validation ) ) {
                return implode( '|' , $this->validation );
            }

            return $this->validation;
        }

        /**
         * Returns the array of all registered additional parameters or attributes
         *
         * @return array
         */
        public function getParameters(): array
        {
            return $this->parameters;
        }

        /**
         * Returns whether or not a given parameter is set
         *
         * @param string $param
         *
         * @return bool
         */
        public function hasParameter( string $param ): bool
        {
            if ( isset( $this->parameters[ $param ] ) ) {
                return true;
            }

            return false;
        }

        /**
         * Adds an CSS class to the columns cells
         *
         * @param string $class
         *
         * @return $this
         */
        public function addClass( string $class ): self
        {
            $this->classes[] = $class;

            return $this;
        }

        /**
         * Returns the array of all css classes
         *
         * @return array
         */
        public function getClasses(): array
        {
            return $this->classes;
        }

        /**
         * Returns an space seperated string with all css classes
         *
         * @return string
         */
        public function getClassString(): string
        {
            return implode( ' ' , $this->getClasses() );
        }

        /**
         * Returns the form element data array
         *
         * @return array
         */
        public function getData(): array
        {
            return array_merge( $this->getParameters() , [
                'id'         => $this->getId() ,
                'name'       => $this->getName() ,
                'type'       => $this->getType() ,
                'label'      => $this->getLabel() ,
                'value'      => $this->getDefault() ,
                'validation' => $this->getValidation() ,
                'size'       => $this->getSize() ,
            ] );
        }

        /**
         * Disables the client side check of the defined validations
         *
         * @return $this
         */
        public function disableClientValidations()
        {
            $this->clientValidationEnabled = false;

            return $this;
        }

        /**
         * Returns whether or not the client side check of the defined validations is enabled
         *
         * @return bool
         */
        public function hasClientValidationsEnabled(): bool
        {
            return $this->clientValidationEnabled;
        }

        /**
         * The request set mutator which runs before the validation process
         *
         * @param Request $request
         */
        public function preValidationSetMutator( Request $request )
        {
            // needs to be overwritten...
        }

        /**
         * The request set mutator which runs after the validation process
         *
         * @param Request $request
         */
        public function postValidationSetMutator( Request $request )
        {
            // needs to be overwritten...
        }

        /**
         * The value get mutator which runs before passing a value to the html input
         *
         * @param $value
         *
         * @return mixed
         */
        public function getMutator( $value )
        {
            return $value;
        }

        /**
         * Call function which realizes some additional attribute setter
         *
         * @param string $name
         * @param array  $arguments
         *
         * @return $this
         */
        public function __call( $name , $arguments )
        {
            // Getter
            if ( count( $arguments ) === 0 ) {
                return $this->parameters[ $name ] ?? null;
            }

            // Setter
            if ( ( $name === 'hint' || $name === 'placeholder' ) && str_contains( $arguments[ 0 ] , '.' ) ) {
                $this->parameters[ $name ] = trans( $arguments[ 0 ] );
            } else {
                $this->parameters[ $name ] = $arguments[ 0 ];
            }

            return $this;
        }

        /**
         * Form Input factory method
         *
         * @return static
         */
        public static function create()
        {
            $arg_count = func_num_args();

            if ( $arg_count == 0 || $arg_count > 3 ) {
                throw new InvalidArgumentException( 'Invalid number of arguments given!' );
            }

            /** @phpstan-ignore-next-line  */
            return new static( ...func_get_args() );
        }
    }
