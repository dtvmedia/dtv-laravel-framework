<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\FormInputs;

    /**
     * File Form Input Class
     *
     * @package   DTV\BaseHandler\Views\FormInputs
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class File extends FormInput
    {
        /**
         * Input column size
         *
         * @var int
         */
        protected $size = 12;

        /**
         * Defines the accepted file formats
         *
         * @param string $formats
         *
         * @return $this
         */
        public function setAcceptFormats( string $formats )
        {
            $this->parameters[ 'accept' ] = $formats;

            return $this;
        }

        /**
         * Enables/disables the multiple select mode
         *
         * @param bool $multiple
         *
         * @return $this
         */
        public function setMultiple( bool $multiple = true )
        {
            $this->parameters[ 'multiple' ] = boolval( $multiple );

            return $this;
        }

        /**
         * Renders the html element
         *
         * @throws \Throwable
         * @return string
         */
        public function render()
        {
            return view( 'dtv.base::form_elements.file' , [ 'input' => $this ] )->render();
        }
    }