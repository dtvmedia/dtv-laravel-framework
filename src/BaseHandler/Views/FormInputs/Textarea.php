<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\FormInputs;

    /**
     * Textarea Form Input Class
     *
     * @package   DTV\BaseHandler\Views\FormInputs
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Textarea extends FormInput
    {
        const DYNAMIC_HEIGHT = - 1;

        /**
         * Input column size
         *
         * @var int
         */
        protected $size = 12;

        /**
         * Textarea constructor.
         *
         * @param string $name
         * @param null   $label
         */
        public function __construct( string $name , $label = null )
        {
            parent::__construct( $name , $label );

            $this->parameters[ 'height' ] = null;
        }

        /**
         * Sets the height in pixel
         *
         * @param int $pixel
         *
         * @return $this
         */
        public function setHeight( int $pixel )
        {
            if ( $pixel === static::DYNAMIC_HEIGHT ) {
                $this->classes[] = 'dynamic-height';
            } else {
                $this->parameters[ 'height' ] = $pixel;
            }

            return $this;
        }

        /**
         * Renders the html element
         *
         * @throws \Throwable
         * @return string
         */
        public function render()
        {
            return view( 'dtv.base::form_elements.textarea' , [ 'input' => $this ] )->render();
        }
    }