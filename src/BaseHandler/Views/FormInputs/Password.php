<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\FormInputs;

    /**
     * Password Form Input Class
     *
     * @package   DTV\BaseHandler\Views\FormInputs
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Password extends FormInput
    {
        /**
         * Array of additional parameters or attributes
         *
         * @var array
         */
        protected $parameters = [
            'secure' => false ,
        ];

        /**
         * Sets the secure mode for the password input
         *
         * @param bool $secure
         *
         * @return $this
         */
        public function setSecure( bool $secure = true ): self
        {
            $this->parameters[ 'secure' ] = $secure;

            return $this;
        }

        /**
         * Returns whether or not secure mode is enabled
         *
         * @return bool
         */
        public function isSecure(): bool
        {
            return $this->parameters[ 'secure' ];
        }

        /**
         * The value get mutator which runs before passing a value to the html input
         *
         * @param $value
         *
         * @return mixed
         */
        public function getMutator( $value )
        {
            if ( $this->isSecure() ) {
                return '••••••••••';
            }

            return $value;
        }

        /**
         * Renders the html element
         *
         * @throws \Throwable
         * @return string
         */
        public function render()
        {
            return view( 'dtv.base::form_elements.text' , [ 'input' => $this ] )->render();
        }
    }