<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\FormInputs;

    use DTV\Oxygen\Oxygen;

    /**
     * Date Form Input Class
     *
     * @package   DTV\BaseHandler\Views\FormInputs
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Date extends FormInput
    {
        /**
         * Sets the minimum valid date
         *
         * @param Oxygen|null $date
         *
         * @return $this
         */
        public function setMin( ?Oxygen $date ): self
        {
            $this->parameters[ 'min' ] = $date;

            return $this;
        }

        /**
         * Sets the maximum valid date
         *
         * @param Oxygen|null $date
         *
         * @return $this
         */
        public function setMax( ?Oxygen $date ): self
        {
            $this->parameters[ 'max' ] = $date;

            return $this;
        }

        /**
         * The value get mutator which runs before passing a value to the html input
         *
         * @param $value
         *
         * @return mixed
         */
        public function getMutator( $value )
        {
            if ( $value instanceof Oxygen ) {
                return $value->toDefaultDateString();
            }

            return $value;
        }

        /**
         * Renders the html element
         *
         * @throws \Throwable
         * @return string
         */
        public function render()
        {
            return view( 'dtv.base::form_elements.date' , [ 'input' => $this ] )->render();
        }
    }