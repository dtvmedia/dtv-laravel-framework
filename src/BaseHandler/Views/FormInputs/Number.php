<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\FormInputs;

    /**
     * Number Form Input Class
     *
     * @package   DTV\BaseHandler\Views\FormInputs
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Number extends FormInput
    {
        /**
         * Number constructor
         *
         * @param string $name
         * @param null   $label
         */
        public function __construct( string $name , $label = null )
        {
            parent::__construct( $name , $label );

            $this->parameters[ 'min' ] = null;
            $this->parameters[ 'max' ] = null;
            $this->parameters[ 'step' ] = null;
        }

        /**
         * Sets the minimum value
         *
         * @param float $min
         *
         * @return $this
         */
        public function setMin( float $min )
        {
            $this->parameters[ 'min' ] = $min;

            return $this;
        }

        /**
         * Sets the maximum value
         *
         * @param float $max
         *
         * @return $this
         */
        public function setMax( float $max )
        {
            $this->parameters[ 'max' ] = $max;

            return $this;
        }

        /**
         * Sets the step size
         *
         * @param float $step
         *
         * @return $this
         */
        public function setStep( float $step )
        {
            $this->parameters[ 'step' ] = $step;

            return $this;
        }

        /**
         * Renders the html element
         *
         * @throws \Throwable
         * @return string
         */
        public function render()
        {
            return view( 'dtv.base::form_elements.number' , [ 'input' => $this ] )->render();
        }
    }