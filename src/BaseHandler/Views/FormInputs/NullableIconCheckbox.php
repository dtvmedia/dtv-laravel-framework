<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\FormInputs;

    use Throwable;

    /**
     * Nullable Checkbox Form Input Class
     *
     * @package   DTV\BaseHandler\Views\FormInputs
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class NullableIconCheckbox extends Checkbox
    {
        /**
         * CSS icon class that will be used
         *
         * @var string
         */
        protected $iconClass = 'icon-checkbox-default';

        /**
         * Default value of checkboxes
         *
         * @var mixed
         */
        protected $default = null;

        /**
         * Checkbox constructor
         *
         * @param string $name
         * @param string $label
         * @param int    $checkedValue
         */
        public function __construct( $name , $label = null , $checkedValue = 1 )
        {
            parent::__construct( $name , $label , $checkedValue );
        }

        /**
         * Sets the icon class which will be used
         *
         * @param string $iconClass
         *
         * @return $this
         */
        public function setIconClass( string $iconClass ): self
        {
            $this->iconClass = $iconClass;

            return $this;
        }

        /**
         * Returns the array of all css classes
         *
         * @return array
         */
        public function getClasses(): array
        {
            return array_merge( parent::getClasses() , [ $this->iconClass ] );
        }

        /**
         * Renders the html element
         *
         * @throws Throwable
         * @return string
         */
        public function render()
        {
            return view( 'dtv.base::form_elements.nullable_icon_checkbox' , [ 'input' => $this ] )->render();
        }
    }
