<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\FormInputs;

    use DTV\Oxygen\Oxygen;
    use Illuminate\Http\Request;

    /**
     * Datetime Form Input Class
     *
     * @package   DTV\BaseHandler\Views\FormInputs
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Datetime extends FormInput
    {
        /**
         * Column size of input
         *
         * @var int
         */
        protected $size = 12;

        /**
         * Sets the minimum valid date
         *
         * @param Oxygen|null $date
         *
         * @return $this
         */
        public function setMin( ?Oxygen $date ): self
        {
            $this->parameters[ 'min' ] = $date;

            return $this;
        }

        /**
         * Sets the maximum valid date
         *
         * @param Oxygen|null $date
         *
         * @return $this
         */
        public function setMax( ?Oxygen $date ): self
        {
            $this->parameters[ 'max' ] = $date;

            return $this;
        }

        /**
         * Enables the second mode
         *
         * @param int $step
         *
         * @return $this
         */
        public function enableSeconds( int $step = 1 )
        {
            $this->parameters[ 'step' ] = $step;

            return $this;
        }

        /**
         * Disables the second mode
         *
         * @param int $step
         *
         * @return $this
         */
        public function disableSeconds( int $step = 60 )
        {
            $this->parameters[ 'step' ] = $step;

            return $this;
        }

        /**
         * The request set mutator which runs after the validation process
         *
         * @param Request $request
         */
        public function postValidationSetMutator( Request $request )
        {
            $name = $this->getName();

            if ( $request->has( [ $name , $name . '_time' ] ) && $request->$name !== null ) {
                $request->merge( [
                    $name => Oxygen::parse( $request->get( $name ) . ' ' . $request->get( $name . '_time' ) )
                ] );
            }
        }

        /**
         * Renders the html element
         *
         * @throws \Throwable
         * @return string
         */
        public function render()
        {
            return view( 'dtv.base::form_elements.datetime' , [ 'input' => $this ] )->render();
        }
    }
