<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\FormInputs;

    /**
     * Money Form Input Class
     *
     * @package   DTV\BaseHandler\Views\FormInputs
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Money extends Decimal
    {
        /**
         * ISO 4217 Currency Codes
         */
        const ISO_4217 = [
            'AFA' , 'AWG' , 'AUD' , 'ARS' , 'AZN' , 'BSD' , 'BDT' , 'BBD' , 'BYR' , 'BOB' , 'BRL' , 'GBP' , 'BGN' , 'KHR' , 'CAD' , 'KYD' , 'CLP' , 'CNY' ,
            'COP' , 'CRC' , 'HRK' , 'CPY' , 'CZK' , 'DKK' , 'DOP' , 'XCD' , 'EGP' , 'ERN' , 'EEK' , 'EUR' , 'GEL' , 'GHC' , 'GIP' , 'GTQ' , 'HNL' , 'HKD' ,
            'HUF' , 'ISK' , 'INR' , 'IDR' , 'ILS' , 'JMD' , 'JPY' , 'KZT' , 'KES' , 'KWD' , 'LVL' , 'LBP' , 'LTL' , 'MOP' , 'MKD' , 'MGA' , 'MYR' , 'MTL' ,
            'BAM' , 'MUR' , 'MXN' , 'MZM' , 'NPR' , 'ANG' , 'TWD' , 'NZD' , 'NIO' , 'NGN' , 'KPW' , 'NOK' , 'OMR' , 'PKR' , 'PYG' , 'PEN' , 'PHP' , 'QAR' ,
            'RON' , 'RUB' , 'SAR' , 'CSD' , 'SCR' , 'SGD' , 'SKK' , 'SIT' , 'ZAR' , 'KRW' , 'LKR' , 'SRD' , 'SEK' , 'CHF' , 'TZS' , 'THB' , 'TTD' , 'TRY' ,
            'AED' , 'USD' , 'UGX' , 'UAH' , 'UYU' , 'UZS' , 'VEB' , 'VND' , 'AMK' , 'ZWD' ,
        ];

        /**
         * Set the currency to be shown after the money value
         *
         * @param string $currency
         *
         * @throws \Exception
         * @return $this|Decimal
         */
        public function setUnit( string $currency )
        {
            if ( !in_array( $currency , static::ISO_4217 ) ) {
                throw new \Exception( 'Invalid currency code given. The currency needs to be a valid ISO 4271 code.' );
            }

            return parent::setUnit( $currency );
        }

        /**
         * Returns the type of input
         *
         * @return string
         */
        public function getType(): string
        {
            return 'decimal';
        }
    }