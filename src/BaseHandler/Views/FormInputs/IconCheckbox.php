<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\FormInputs;

    /**
     * Checkbox Form Input Class
     *
     * @package   DTV\BaseHandler\Views\FormInputs
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class IconCheckbox extends Checkbox
    {
        /**
         * CSS icon class that will be used
         *
         * @var string
         */
        protected $iconClass = 'icon-checkbox-default';
        
        /**
         * Sets the icon class which will be used
         *
         * @param string $iconClass
         *
         * @return $this
         */
        public function setIconClass( string $iconClass ): self
        {
            $this->iconClass = $iconClass;

            return $this;
        }

        /**
         * Returns the array of all css classes
         *
         * @return array
         */
        public function getClasses(): array
        {
            return array_merge( parent::getClasses() , [ $this->iconClass ] );
        }

        /**
         * Makes the icon checkbox nullable and sets the default value to null
         *
         * @return $this
         */
        public function nullable()
        {
            $this->setDefaultValue( null );

            return $this;
        }

        /**
         * Renders the html element
         *
         * @throws \Throwable
         * @return string
         */
        public function render()
        {
            return view( 'dtv.base::form_elements.icon_checkbox' , [ 'input' => $this ] )->render();
        }
    }