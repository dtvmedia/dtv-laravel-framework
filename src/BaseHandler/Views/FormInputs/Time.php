<?php

    /** @noinspection PhpUnused */

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\FormInputs;

    use DTV\Oxygen\Oxygen;
    use Throwable;

    /**
     * Time Form Input Class
     *
     * @package   DTV\BaseHandler\Views\FormInputs
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     *
     * @method int|null|self step($value = null)
     */
    class Time extends FormInput
    {
        /**
         * Time constructor.
         *
         * @param string $name
         * @param null   $label
         */
        public function __construct(string $name, $label = null)
        {
            $this->parameters[ 'step' ] = null;

            parent::__construct($name, $label);
        }

        /**
         * The value get mutator which runs before passing a value to the html input
         *
         * @param $value
         *
         * @return mixed
         */
        public function getMutator( $value )
        {
            if (empty($value)) {
                $value = null;
            }

            try {
                if (!$value instanceof Oxygen) {
                    $value = Oxygen::parse($value);
                }
            } catch (Throwable) {
                $value = Oxygen::parse(null);
            }

            if (is_int($this->step()) && $this->step() < 60) {
                return $value->toDefaultTimeString();
            }

            return $value->toShortTimeString();
        }

        /**
         * Enables the second mode
         *
         * @param int $step
         *
         * @return $this
         */
        public function enableSeconds( int $step = 1 )
        {
            $this->parameters[ 'step' ] = $step;

            return $this;
        }

        /**
         * Disables the second mode
         *
         * @param int $step
         *
         * @return $this
         */
        public function disableSeconds( int $step = 60 )
        {
            $this->parameters[ 'step' ] = $step;

            return $this;
        }

        /**
         * Renders the html element
         *
         * @throws Throwable
         * @return string
         */
        public function render()
        {
            return view( 'dtv.base::form_elements.time' , [ 'input' => $this ] )->render();
        }
    }
