<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\FormInputs;

    use Carbon\Carbon;

    /**
     * Select Form Input Class
     *
     * @package   DTV\BaseHandler\Views\FormInputs
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Select extends FormInput
    {
        /**
         * Select constructor
         *
         * @param string $name
         * @param string $label
         * @param array  $values
         */
        public function __construct( $name , array $values , $label = null )
        {
            parent::__construct( $name , $label ?? $name );

            $this->parameters[ 'values' ] = $values;
        }

        /**
         * Enables/disables the multiple select mode
         *
         * @param bool $multiple
         *
         * @return $this
         */
        public function setMultiple( bool $multiple = true )
        {
            $this->parameters[ 'multiple' ] = boolval( $multiple );

            return $this;
        }

        /**
         * The value get mutator which runs before passing a value to the html input
         *
         * @param $value
         *
         * @return mixed
         */
        public function getMutator( $value )
        {
            if ( is_bool( $value ) ) {
                // Boolean values won't work in html so we need to cast them to int at this point
                return (int) $value;
            }

            return $value;
        }

        /**
         * Renders the html element
         *
         * @throws \Throwable
         * @return string
         */
        public function render()
        {
            return view( 'dtv.base::form_elements.select' , [ 'input' => $this ] )->render();
        }

        /**
         * Returns the boolean select options
         *
         * @param bool $noOption Flag which enables/disables the no optio
         *
         * @return array
         */
        public static function getBoolSelectOptions(bool $noOption = false)
        {
            $options = [
                1 => option( 'dtv.base::general.yes' , 'fa-check' ) ,
                0 => option( 'dtv.base::general.no' , 'fa-times' ) ,
            ];

            if ($noOption === true) {
                $options = [-1 => option('-', 'fa-times')] + $options;
            }

            return $options;
        }

        /**
         * Returns the select options for selecting a year
         *
         * @param int      $years    Number of available years
         * @param int|null $to       Latest year available (default: current year)
         * @param bool     $noOption Flag which enables/disables the no option
         *
         * @return array
         */
        public static function getYearSelectOptions( int $years = 100 , int $to = null, bool $noOption = false)
        {
            $options = [];
            $to = $to ?? Carbon::today()->year;
            $from = $to - $years;

            if ($noOption === true) {
                $options[ 0 ] = option('-', 'fa-times');
            }

            for ( $i = $to ; $i > $from ; $i-- ) {
                $options[ $i ] = option( $i );
            }

            return $options;
        }
    }