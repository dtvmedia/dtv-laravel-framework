<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\FormInputs;

    use Illuminate\Http\Request;

    /**
     * Checkbox Form Input Class
     *
     * @package   DTV\BaseHandler\Views\FormInputs
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Checkbox extends FormInput
    {
        /**
         * Default value of checkboxes
         *
         * @var mixed
         */
        protected $default = false;

        /**
         * Checkbox constructor
         *
         * @param string $name
         * @param string $label
         * @param string $checkedValue
         */
        public function __construct( $name , $label = null , $checkedValue = '1' )
        {
            parent::__construct( $name , $label );

            $this->parameters[ 'showLabel' ] = true;
            $this->parameters[ 'checkedValue' ] = $checkedValue;
        }

        /**
         * Sets the negated show label flag
         *
         * @param bool $hideLabel
         *
         * @return $this
         */
        public function hideLabel( bool $hideLabel = true ): self
        {
            $this->parameters[ 'showLabel' ] = !$hideLabel;

            return $this;
        }

        /**
         * The request set mutator which runs after the validation process
         *
         * @param Request $request
         */
        public function postValidationSetMutator( Request $request )
        {
            $name = $this->getName();

            // if another input was changed ignore these rewrite rules
            if ( $request->has( '_changed' ) && $request->_changed !== $name ) {
                return;
            }

            // Rewrite the nullable case...
            if ( str_contains( $name , '[]' ) ) {
                // ...when using multiple checkboxes
                $name = rtrim( $name , '[]' );

                $request->merge( [
                    $name => $request->get( $name , [] )
                ] );
            } else {
                // ...when using normal checkboxes
                $request->merge( [
                    $name => $request->get( $name , 0 )
                ] );
            }
        }

        /**
         * Renders the html element
         *
         * @throws \Throwable
         * @return string
         */
        public function render()
        {
            return view( 'dtv.base::form_elements.checkbox' , [ 'input' => $this ] )->render();
        }
    }