<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views\FormInputs;

    use Illuminate\Http\Request;

    /**
     * Decimal Form Input Class
     *
     * @package   DTV\BaseHandler\Views\FormInputs
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Decimal extends FormInput
    {
        /**
         * Default value
         *
         * @var mixed
         */
        protected $default = 0.0;

        /**
         * Set the decimals to be shown of the decimal value
         *
         * @param int $decimals
         *
         * @return $this
         */
        public function setDecimals( int $decimals )
        {
            $this->parameters[ 'decimals' ] = $decimals;

            return $this;
        }

        /**
         * Set the unit to be shown after the decimal value
         *
         * @param string $unit
         *
         * @return $this
         */
        public function setUnit( string $unit )
        {
            $this->parameters[ 'unit' ] = $unit;

            return $this;
        }

        /**
         * The request set mutator which runs before the validation process
         *
         * @param Request $request
         */
        public function preValidationSetMutator( Request $request )
        {
            // Rewrites the decimal strings and merges them into the request
            $name = $this->getName();

            if ( $request->has( $name ) && $request->$name !== null ) {
                $request->merge( [
                    $name => static::parseValue( $request->$name )
                ] );
            }
        }

        /**
         * The value get mutator which runs before passing a value to the html input
         *
         * @param $value
         *
         * @return mixed
         */
        public function getMutator( $value )
        {
            if ( !empty( $value ) ) {
                return number_format( static::parseValue( $value ) , 2 , ',' , '' );
            }

            return $value;
        }

        /**
         * Renders the html element
         *
         * @throws \Throwable
         * @return string
         */
        public function render()
        {
            return view( 'dtv.base::form_elements.decimal' , [ 'input' => $this ] )->render();
        }

        /**
         * Parses an formatted decimal input string
         *
         * @param $value
         *
         * @return float
         */
        public static function parseValue( $value )
        {
            if ( empty( $value ) || is_float( $value ) ) {
                return $value;
            }

            return floatval( str_replace( ',' , '.' , preg_replace( '/[^0-9,-]+/' , '' , $value ) ) );
        }
    }