<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views;

    /**
     * List View Meta Class
     *
     * @package   DTV\BaseHandler\Views
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class ListViewMeta
    {
        /**
         * The columns name
         *
         * @var string
         */
        protected $name;

        /**
         * The datapath or closure which calculates the value of the meta field
         *
         * @var \Closure|null|string
         */
        protected $datapath;

        /**
         * Creates a new column for an List View
         *
         * @param string          $name
         * @param string|\Closure $datapath
         */
        public function __construct( string $name , $datapath = null )
        {
            $this->name = $name;
            $this->datapath = ( $datapath == null ) ? $name : $datapath;
        }

        /**
         * Returns the columns name
         *
         * @return string
         */
        public function getName(): string
        {
            return $this->name;
        }

        /**
         * Returns the datapath or closure for the columns values
         *
         * @return \Closure|null|string
         */
        public function getDatapath()
        {
            return $this->datapath;
        }
    }