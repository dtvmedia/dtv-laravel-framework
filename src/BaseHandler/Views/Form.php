<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler\Views;

    use DTV\BaseHandler\Views\FormInputs\Checkbox;
    use DTV\BaseHandler\Views\FormInputs\Color;
    use DTV\BaseHandler\Views\FormInputs\Date;
    use DTV\BaseHandler\Views\FormInputs\Datetime;
    use DTV\BaseHandler\Views\FormInputs\File;
    use DTV\BaseHandler\Views\FormInputs\IconCheckbox;
    use DTV\BaseHandler\Views\FormInputs\Money;
    use DTV\BaseHandler\Views\FormInputs\Number;
    use DTV\BaseHandler\Views\FormInputs\Password;
    use DTV\BaseHandler\Views\FormInputs\Select;
    use DTV\BaseHandler\Views\FormInputs\Text;
    use DTV\BaseHandler\Views\FormInputs\Textarea;
    use DTV\BaseHandler\Views\FormInputs\Time;

    /**
     * Form Class
     *
     * @package   DTV\BaseHandler\Views
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Form
    {
        /**
         * Adds an text input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Text
         */
        public static function text( $name , $label = null )
        {
            return new Text( $name , $label );
        }

        /**
         * Adds an select input with the given options to the form
         *
         * @param string $name
         * @param array  $values
         * @param string $label
         *
         * @return Select
         */
        public static function select( $name , $values , $label = null )
        {
            return new Select( $name , $values , $label );
        }

        /**
         * Adds an textarea to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Textarea
         */
        public static function textarea( $name , $label = null )
        {
            return new Textarea( $name , $label );
        }

        /**
         * Adds an checkbox to the form
         *
         * @param string $name
         * @param string $label
         * @param string $checkedValue
         *
         * @return Checkbox
         */
        public static function checkBox( $name , $label = null , $checkedValue = '1' )
        {
            return new Checkbox( $name , $label , $checkedValue );
        }

        /**
         * Adds an date input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Date
         */
        public static function date( $name , $label = null )
        {
            return new Date( $name , $label );
        }

        /**
         * Adds an time input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Time
         */
        public static function time( $name , $label = null )
        {
            return new Time( $name , $label );
        }

        /**
         * Adds an date time input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Datetime
         */
        public static function dateTime( $name , $label = null )
        {
            return new Datetime( $name , $label );
        }

        /**
         * Adds an password input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Password
         */
        public static function password( $name , $label = null )
        {
            return new Password( $name , $label );
        }

        /**
         * Adds an color input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Color
         */
        public static function color( $name , $label = null )
        {
            return new Color( $name , $label );
        }

        /**
         * Adds an icon checkbox input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return IconCheckbox
         */
        public static function iconCheckbox( $name , $label = null )
        {
            return new IconCheckbox( $name , $label );
        }

        /**
         * Adds an money input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Money
         */
        public static function money( $name , $label = null )
        {
            return new Money( $name , $label );
        }

        /**
         * Adds an number input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return Number
         */
        public static function number( $name , $label = null )
        {
            return new Number( $name , $label );
        }

        /**
         * Adds an file input to the form
         *
         * @param string $name
         * @param string $label
         *
         * @return File
         */
        public static function file( $name , $label = null )
        {
            return new File( $name , $label );
        }
    }