<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler;

    use Illuminate\Support\Facades\Blade;
    use Illuminate\Support\Facades\Validator;
    use Illuminate\Support\Facades\View;

    /**
     * Base Handler Service Provider
     *
     * @package   DTV\BaseHandler
     * @copyright 2017 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class BaseHandlerServiceProvider extends BaseServiceProvider
    {
        /**
         *  Namespace for resources of the module
         *
         * @var string
         */
        protected $resourcesNamespace = 'dtv.base';

        /**
         * Array of commans
         *
         * @var array
         */
        protected $commands = [];

        /**
         * Config path for the module enable switch
         *
         * @var string|null
         */
        public static $config = null;

        /**
         * Register the service provider.
         *
         * @return void
         */
        public function register()
        {
            parent::register();

            $this->app->singleton( 'hooks' , function () {
                return new HookHandler();
            } );
        }

        /**
         * Bootstrap any application services.
         *
         * @return void
         */
        public function boot()
        {
            parent::boot();

            $this->defineValidations();
            $this->defineBladeDirectives();
            $this->defineViewVars();

            $this->publishes( [
                __DIR__ . '/Resources/assets/' => resource_path( 'assets' ) ,
            ] , 'dtv.base-assets' );
        }

        /**
         * Defines all the modules gates
         */
        public function gates()
        {
            gate()->before( function ( $user ) {
                if ( $user->isSuperAdmin() ) {
                    return true;
                }

                return null;
            } );
        }

        /**
         * Defines additional validation rules
         */
        private function defineValidations()
        {
            Validator::extend( 'float' , function ( $attribute , $value , $parameters , $validator ) {
                return preg_match( '/^\d*(\.\d+)?$/' , $value );
            } );
        }

        /**
         * Defines additional blade directives
         */
        private function defineBladeDirectives()
        {
            Blade::directive( 'active' , function ( $expression ) {
                return "<?php if( isCurrentRoute( " . $expression . " )) { echo 'active'; } ?>";
            } );
        }

        /**
         * Defines the global and local view vars
         */
        private function defineViewVars()
        {
            // register globally shared view vars
            $currentVersion = version();
            View::share( 'app_version' , $currentVersion->version );
            View::share( 'app_version_date' , $currentVersion->last_changed );

            // register only locally shared view vars
            View::composer( 'dtv.base::includes.app_js_init' , AppJsInitComposer::class );
        }
    }