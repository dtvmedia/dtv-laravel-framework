<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\BaseHandler;

    use Illuminate\Routing\Route;
    use Illuminate\Support\Facades\Route as RouteFacade;
    use Illuminate\View\View;

    /**
     * App JS Init Composer Handler
     *
     * @package   DTV\BaseHandler
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class AppJsInitComposer
    {
        /**
         * Route whitelist config array
         *
         * @var \Illuminate\Support\Collection
         */
        protected $routeWhitelist;

        /**
         * AppJsInitComposer constructor.
         */
        public function __construct()
        {
            $this->routeWhitelist = collect( config( 'dtv.app.js_route_whitelist' , [] ) );
        }

        /**
         * Returns an array of whitelisted routes
         *
         * @return array
         */
        protected function getWhitelistedRoutes()
        {
            return $this->routeWhitelist->filter( function ( $value ) {
                return !str_contains( $value , '*' );
            } )->toArray();
        }

        /**
         * Returns an array of whitelisted route groups
         *
         * @return array
         */
        protected function getWhitelistedRouteGroups()
        {
            return $this->routeWhitelist->filter( function ( $value ) {
                return str_contains( $value , '*' );
            } )->map( function ( $value ) {
                return str_replace( '*' , '' , $value );
            } )->toArray();
        }

        /**
         * Returns an collection of all registered routes
         *
         * @return \Illuminate\Support\Collection
         */
        protected function getAllRoutes()
        {
            return collect( RouteFacade::getRoutes() );
        }

        /**
         * Returns all exposed routes
         *
         * @return \Illuminate\Support\Collection|array
         */
        protected function getExposedRoutes()
        {
            $routeWhitelist = $this->getWhitelistedRoutes();
            $routeGroupWhitelist = $this->getWhitelistedRouteGroups();

            if ( empty( $routeWhitelist ) && empty( $routeGroupWhitelist ) ) {
                return [];
            }

            return $this->getAllRoutes()
                ->filter( function ( Route $route ) use ( $routeWhitelist , $routeGroupWhitelist ) {
                    if ( in_array( $route->getName() , $routeWhitelist ) ) {
                        return true;
                    }

                    foreach ( $routeGroupWhitelist as $routeNamePrefix ) {
                        if ( starts_with( $route->getName() , $routeNamePrefix ) ) {
                            return true;
                        }
                    }

                    return false;
                } )->mapWithKeys( function ( Route $route ) {
                    return [ $route->getName() => $route->uri() ];
                } );
        }

        /**
         * Bind data to the view.
         *
         * @param View $view
         *
         * @return void
         */
        public function compose( View $view )
        {
            $view->with( 'routes' , $this->getExposedRoutes() );
        }
    }