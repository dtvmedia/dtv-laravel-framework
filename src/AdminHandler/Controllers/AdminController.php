<?php /** @noinspection PhpUnused */

/**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\AdminHandler\Controllers;

    use DTV\AdminHandler\Services\Handler\AdminAccessHandler;
    use DTV\AdminHandler\Services\Handler\QueueHandler;
    use DTV\AdminHandler\Services\Views\LoginForm;
    use DTV\AdminHandler\Services\Views\Sessions\SessionsOverview;
    use DTV\AdminHandler\Services\Views\Users\UsersOverview;
    use DTV\BaseHandler\Controllers\Controller;
    use DTV\BaseHandler\JsonResponse;
    use DTV\BaseHandler\Models\Session;
    use DTV\BaseHandler\Views\Page;
    use Exception;
    use Illuminate\Http\RedirectResponse;
    use Illuminate\Http\Request;
    use Illuminate\Routing\Redirector;
    use Illuminate\Support\Facades\Artisan;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Crypt;
    use Rap2hpoutre\LaravelLogViewer\LaravelLogViewer;
    use Symfony\Component\HttpFoundation\BinaryFileResponse;
    use Symfony\Component\Process\Exception\ProcessFailedException;
    use Symfony\Component\Process\Process;
    use Throwable;

    /**
     * Admin Controller
     *
     * @package   DTV\AdminHandler\Controllers
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class AdminController extends Controller
    {
        /**
         * Shows and handles the admin login page
         *
         * @param Request            $request
         * @param AdminAccessHandler $adminAccessHandler
         *
         * @throws Throwable
         * @return JsonResponse|Page|string
         */
        public function login( Request $request , AdminAccessHandler $adminAccessHandler )
        {
            return $this->form( $request , new LoginForm() , function () use ( $request , $adminAccessHandler ) {
                if ( $adminAccessHandler->login( $request ) === false ) {
                    return $this->json()->warning( 'dtv.admin::admin.messages.invalidCredentials' );
                }

                return $this->json()->redirect( 'admin.index' );
            } );
        }

        /**
         * Admin logout function
         *
         * @param AdminAccessHandler $adminAccessHandler
         *
         * @return RedirectResponse
         */
        public function logout( AdminAccessHandler $adminAccessHandler )
        {
            $adminAccessHandler->logout();

            return redirect()->route( 'admin.login' );
        }

        /**
         * Admin index dispatcher function
         *
         * @param AdminAccessHandler $adminAccessHandler
         *
         * @return RedirectResponse
         */
        public function index( AdminAccessHandler $adminAccessHandler )
        {
            if ( $adminAccessHandler->hasAccess() ) {
                return redirect()->route( 'admin.sessions.index' );
            }

            return redirect()->route( 'admin.login' );
        }

        /**
         * Shows the session overview page
         *
         * @throws Throwable
         */
        public function sessions()
        {
            $this->addHeaderButton( 'dtv.admin::sessions.buttons.clear' , 'fa-times' , 'admin.sessions.clear' , [] , true );

            return $this->view( 'dtv.admin::main' )
                ->withView( 'view' , new SessionsOverview( Session::query() ) );
        }

        /**
         * Clears all sessions beside the session of the current user
         *
         * @throws Exception
         * @return JsonResponse
         */
        public function sessionsClear()
        {
            foreach ( Session::all() as $session ) {
                if( $session->user_id === auth()->id() ) {
                    continue;
                }

                $session->delete();
            }

            return $this->json()
                ->reloadListView( SessionsOverview::class )
                ->hideModal();
        }

        /**
         * Shows the users overview page
         *
         * @throws Throwable
         * @return Page|string
         */
        public function users()
        {
            $model = config( 'dtv.models.user' );

            return $this->view( 'dtv.admin::main' )
                ->withView( 'view' , new UsersOverview( $model::query() ) );
        }

        /**
         * Login as the given user
         *
         * @param int $id
         *
         * @return RedirectResponse|Redirector
         */
        public function loginAsUser( $id )
        {
            $model = config( 'dtv.models.user' );
            $user = $model::query()->findOrFail( $id );

            Auth::loginUsingId( $user->id );

            return redirect( '/' );
        }

        /**
         * Shows the queue status overview
         *
         * @param QueueHandler $handler
         *
         * @throws Throwable
         * @return Page|string
         */
        public function queues( QueueHandler $handler )
        {
            $queues = $handler->getQueueStatus();
            $jobs = $handler->getJobs();
            $failedJobs = $handler->getFailedJobs();

            return $this->view('dtv.admin::queues', compact('queues', 'jobs', 'failedJobs'));
        }

        /**
         * Shows the versions overview page
         *
         * @throws Throwable
         * @return Page|string
         */
        public function versions()
        {
            $versions = [];

            // App
            $appVersion = version();
            $versions[ 'app' ] = [];
            $versions[ 'app' ][ 'Core' ] = $appVersion->version . ' (' . $appVersion->last_changed . ')';
            $versions[ 'app' ][ 'Laravel' ] = app()->version();

            // PHP
            $versions[ 'php' ] = [];
            $versions[ 'php' ][ 'Runtime' ] = phpversion();
            foreach (get_loaded_extensions() as $ext) {
                $versions[ 'php' ][ $ext ] = phpversion($ext);
            }

            // Composer packages
            $process = Process::fromShellCommandline('composer show -i', base_path());
            $process->run();

            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }

            $matches = [];
            $re = '/^([[0-9a-zA-Z-\/]+)[[:space:]]+(dev-[0-9a-zA-Z.-]+[[:space:]]?[0-9a-zA-Z.-]*|[0-9a-zA-Z.-]+)/m';
            preg_match_all($re, $process->getOutput(), $matches, PREG_SET_ORDER);

            foreach ($matches as $match) {
                $versions[ 'composer packages' ][ $match[ 1 ] ] = $match[ 2 ];
            }

            return $this->view('dtv.admin::versions', compact('versions'));
        }

        /**
         * Shows the maintenance overview page
         *
         * @throws Throwable
         * @return Page|string
         */
        public function maintenanceIndex()
        {
            return $this->view( 'dtv.admin::maintenance' );
        }

        /**
         * Deactivates the maintenance mode
         *
         * @return RedirectResponse
         */
        public function maintenanceUp()
        {
            Artisan::call( 'up' );

            return redirect()->route( 'admin.maintenance.index' );
        }

        /**
         * Activates the maintenance mode
         *
         * @param Request $request
         *
         * @return RedirectResponse
         */
        public function maintenanceDown( Request $request )
        {
            $params = [];

            if ( $request->filled( 'allowed_pw' ) ) {
                $params[ '--secret' ] = $request->allowed_pw;
            }

            Artisan::call( 'down' , $params );

            return redirect()->route( 'admin.maintenance.index' );
        }

        /**
         * Shows the log viewer page
         *
         * @param Request $request
         *
         * @throws Throwable
         * @return array|BinaryFileResponse|bool|JsonResponse|Page|RedirectResponse|string
         */
        public function logsIndex( Request $request )
        {
            $folderFiles = [];
            $logviewer = new LaravelLogViewer();

            if ( $request->input( 'f' ) ) {
                $logviewer->setFolder( Crypt::decrypt( $request->input( 'f' ) ) );
                $folderFiles = $logviewer->getFolderFiles( true );
            }

            if ( $request->input( 'l' ) ) {
                $logviewer->setFile( Crypt::decrypt( $request->input( 'l' ) ) );
            }

            $dispatchResponse = $this->logsDispatchRequest( $request , $logviewer );
            if ( $dispatchResponse ) {
                return $dispatchResponse;
            }

            $files = $logviewer->getFiles(true);
            rsort($files);

            $data = [
                'logs'           => $logviewer->all(),
                'folders'        => $logviewer->getFolders(),
                'current_folder' => $logviewer->getFolderName(),
                'folder_files'   => $folderFiles,
                'files'          => $files,
                'current_file'   => $logviewer->getFileName(),
                'standardFormat' => true,
            ];

            if ( $request->wantsJson() ) {
                return $data;
            }

            if ( is_array( $data[ 'logs' ] ) && !empty( $data[ 'logs' ] ) ) {
                $firstLog = reset( $data[ 'logs' ] );
                if ( !$firstLog[ 'context' ] && !$firstLog[ 'level' ] ) {
                    $data[ 'standardFormat' ] = false;
                }
            }

            return $this->view( 'dtv.admin::logs' , $data );
        }

        /**
         * Dispatches the log viewer action requests
         *
         * @param Request          $request
         * @param LaravelLogViewer $logviewer
         *
         * @throws Exception
         * @return bool|RedirectResponse|BinaryFileResponse
         */
        private function logsDispatchRequest( Request $request , LaravelLogViewer $logviewer )
        {
            if ( $request->input( 'f' ) ) {
                $logviewer->setFolder( Crypt::decrypt( $request->input( 'f' ) ) );
            }

            if ( $request->input( 'dl' ) ) {
                return response()->download( $logviewer->pathToLogFile( Crypt::decrypt( $request->input( 'dl' ) ) ) );
            } elseif ( $request->has( 'clean' ) ) {
                app( 'files' )->put( $logviewer->pathToLogFile( Crypt::decrypt( $request->input( 'clean' ) ) ) , '' );

                return redirect( $request->url() );
            } elseif ( $request->has( 'del' ) ) {
                app( 'files' )->delete( $logviewer->pathToLogFile( Crypt::decrypt( $request->input( 'del' ) ) ) );

                return redirect( $request->url() );
            } elseif ( $request->has( 'delall' ) ) {
                $files = ( $logviewer->getFolderName() )
                    ? $logviewer->getFolderFiles( true )
                    : $logviewer->getFiles( true );
                foreach ( $files as $file ) {
                    app( 'files' )->delete( $logviewer->pathToLogFile( $file ) );
                }

                return redirect( $request->url() );
            }

            return false;
        }
    }
