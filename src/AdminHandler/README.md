# AdminHandler - DTV Laravel Framework

## Requirements
- rap2hpoutre/laravel-log-viewer

## Configs
- *enabled*: Package activation switch
- *routes_enabled*: Activation switch for the package routes seperated from the package switch itself
- *access_password*: Admin backend password
- *access_user_id*: Allowed user ids which can access the admin backend login


## Usage
Just define the admin access password and the allowed user ids (comma seperation possible):
````
ADMIN_ACCESS_PASSWORD=12345
ADMIN_ACCESS_ID=1,2
````
If no password is provided the admin backend is not available.