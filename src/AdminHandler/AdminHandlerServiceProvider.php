<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\AdminHandler;

    use DTV\AdminHandler\Controllers\AdminController;
    use DTV\AdminHandler\Middlewares\AdminAccess;
    use DTV\AdminHandler\Middlewares\AllowedToEnterBackend;
    use DTV\BaseHandler\BaseServiceProvider;
    use DTV\BaseHandler\RoutingHandler;
    use Exception;
    use Illuminate\Contracts\Container\BindingResolutionException;
    use ReflectionException;

    /**
     * Admin Handler Service Provider
     *
     * @package   DTV\AdminHandler
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class AdminHandlerServiceProvider extends BaseServiceProvider
    {
        /**
         *  Namespace for resources of the module
         *
         * @var string
         */
        protected $resourcesNamespace = 'dtv.admin';

        /**
         * Array of commans
         *
         * @var array
         */
        protected $commands = [];

        /**
         * Config path for the module enable switch
         *
         * @var string|null
         */
        public static $config = 'dtv.modules.admin.enabled';

        /**
         * Register any application services.
         *
         * @throws ReflectionException
         * @throws BindingResolutionException
         * @return void
         */
        public function register()
        {
            parent::register();

            app( 'router' )->aliasMiddleware( 'admin-access' , AdminAccess::class );
            app( 'router' )->aliasMiddleware( 'admin-allowed' , AllowedToEnterBackend::class );
        }

        /**
         * Registers the Modules routes
         *
         * @throws Exception
         */
        public function routes()
        {
            $adminRoutes = new RoutingHandler( AdminController::class , 'admin' , 'admin' );
            $adminRoutes->get( '/' , 'index' , 'index' , [ 'admin-access' ] );
            $adminRoutes->any( '/login' , 'login' , 'login' , [ 'admin-allowed' ]);
            $adminRoutes->get( '/logout' , 'logout' , 'logout' );
            $adminRoutes->get( '/sessions/' , 'sessions' , 'sessions.index' , [ 'admin-access' ] );
            $adminRoutes->delete( '/sessions/clear' , 'sessionsClear' , 'sessions.clear' , [ 'admin-access' ] );
            $adminRoutes->get( '/users/' , 'users' , 'users.index' , [ 'admin-access' ] );
            $adminRoutes->get( '/users/{id}/login' , 'loginAsUser' , 'users.login' , [ 'admin-access' ] );
            $adminRoutes->get( '/versions' , 'versions' , 'versions' , [ 'admin-access' ] );
            $adminRoutes->get( '/queues' , 'queues' , 'queues' , [ 'admin-access' ] );
            $adminRoutes->get( '/maintenance' , 'maintenanceIndex' , 'maintenance.index' , [ 'admin-access' ] );
            $adminRoutes->get( '/maintenance/up' , 'maintenanceUp' , 'maintenance.up' , [ 'admin-access' ] );
            $adminRoutes->post( '/maintenance/down' , 'maintenanceDown' , 'maintenance.down' , [ 'admin-access' ] );
            $adminRoutes->get( '/logs' , 'logsIndex' , 'logs.index' , [ 'admin-access' ] );
        }

        /**
         * Defines all the modules gates
         */
        public function gates()
        {

        }
    }
