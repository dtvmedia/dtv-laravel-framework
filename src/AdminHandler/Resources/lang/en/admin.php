<?php

return [

    'views' => [
        'login'    => 'Admin Login' ,
        'versions' => 'Versions',
    ] ,

    'buttons' => [
        'sessions'    => 'Sessions' ,
        'users'       => 'Users' ,
        'queues'      => 'Queues & Jobs' ,
        'versions'    => 'Versions',
        'maintenance' => 'Maintenance' ,
        'telescope'   => 'Telescope' ,
        'logs'        => 'Log Viewer' ,
        'logout'      => 'Logout'
    ] ,

    'fields' => [
        'password' => 'Password',
        'module'   => 'Module',
        'version'  => 'Version',
    ] ,

    'messages' => [
        'invalidCredentialsTitle' => 'Invalid credentials' ,
        'invalidCredentialsText'  => 'The credentials you provided are invalid' ,
    ] ,

];
