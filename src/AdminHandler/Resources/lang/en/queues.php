<?php

    return [

        'views' => [
            'index'       => 'Queues & Jobs',
            'queues'      => 'Queues',
            'jobs'        => 'Jobs',
            'failed_jobs' => 'Failed jobs'
        ],

        'buttons' => [

        ],

        'fields' => [
            'name'         => 'Name',
            'status'       => 'Status',
            'id'           => 'ID',
            'queue'        => 'Queue',
            'attempts'     => 'Attempts',
            'reserved_at'  => 'Reserved at',
            'available_at' => 'Available at',
            'created_at'   => 'Created at',
            'connection'   => 'Connection',
            'exception'    => 'Exception',
            'failed_at'    => 'Failed at',
        ],

        'status' => [
            -1 => 'Not available',
            0  => 'Inactiv',
            1  => 'Activ',
        ],

        'messages' => [
            'noQueuesAvailable'     => 'No queues are defined for monitoring...',
            'noJobsAvailable'       => 'No jobs are currently queued...',
            'noFailedJobsAvailable' => 'There are no failed jobs currently - Yeah!',
        ],

    ];