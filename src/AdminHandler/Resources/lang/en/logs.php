<?php

    return [

        'views' => [
            'index'   => 'Log Viewer' ,
            'files'   => 'Log Files' ,
            'actions' => 'Actions' ,
        ] ,

        'buttons' => [
            'download'   => 'Download file' ,
            'clean'      => 'Clean file' ,
            'delete'     => 'Delete file' ,
            'delete_all' => 'Delete all files' ,
        ] ,

        'fields' => [
            'level'       => 'Level' ,
            'context'     => 'context' ,
            'date'        => 'Date' ,
            'line_number' => 'Line number' ,
            'content'     => 'Content' ,
        ] ,

        'messages' => [

        ] ,

    ];