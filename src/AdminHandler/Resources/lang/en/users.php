<?php

    return [

        'views' => [
            'index' => 'Users' ,
        ] ,

        'buttons' => [
            'login' => 'Login as user' ,
        ] ,

        'fields' => [
            'id'         => 'ID' ,
            'name'       => 'Name' ,
            'email'      => 'Email' ,
            'last_login' => 'Last login' ,
        ] ,

        'messages' => [

        ] ,

    ];