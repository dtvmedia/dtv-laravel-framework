<?php

    return [

        'views' => [
            'index' => 'Sessions' ,
        ] ,

        'buttons' => [

        ] ,

        'fields' => [
            'user_id'       => 'User' ,
            'ip_address'    => 'IP address' ,
            'user_agent'    => 'User agent' ,
            'payload'       => 'Payload' ,
            'last_activity' => 'Last activity' ,
            'last_accessed' => 'Last accessed' ,
        ] ,

        'messages' => [

        ] ,

    ];