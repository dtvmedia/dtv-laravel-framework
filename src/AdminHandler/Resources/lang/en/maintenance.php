<?php

    return [

        'views' => [
            'index' => 'Maintenance' ,
            'up'    => 'Deactivate maintenance mode' ,
            'down'  => 'Activate maintenance mode'
        ] ,

        'buttons' => [

        ] ,

        'fields' => [
            'allowed_pw' => 'Password' ,
            'default'    => 'Maintenance' ,
        ] ,

        'status' => [
            0 => 'offline' ,
            1 => 'online' ,
        ] ,

        'messages' => [
            'app_is'     => 'This application is currently ' ,
            'access_via' => 'Via :url you can bypass the maintenance mode.'
        ] ,

    ];