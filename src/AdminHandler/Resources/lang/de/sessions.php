<?php

    return [

        'views' => [
            'index' => 'Sessions' ,
        ] ,

        'buttons' => [
            'clear' => 'Clear Sessions'
        ] ,

        'fields' => [
            'user_id'       => 'Benutzer' ,
            'ip_address'    => 'IP Adresse' ,
            'user_agent'    => 'User Agent' ,
            'payload'       => 'Payload' ,
            'last_activity' => 'Letzte Aktivität' ,
            'last_accessed' => 'Zuletzt besucht' ,
        ] ,

        'messages' => [

        ] ,

    ];