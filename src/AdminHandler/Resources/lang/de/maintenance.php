<?php

    return [

        'views' => [
            'index' => 'Wartung' ,
            'up'    => 'Deaktiviere Wartungsmodus' ,
            'down'  => 'Aktiviere Wartungsmodus'
        ] ,

        'buttons' => [

        ] ,

        'fields' => [
            'allowed_pw' => 'Passwort' ,
            'default'    => 'Wartung' ,
        ] ,

        'status' => [
            0 => 'Offline' ,
            1 => 'Online' ,
        ] ,

        'messages' => [
            'app_is'     => 'Diese Anwendung ist zurzeit ' ,
            'access_via' => 'Über :url kann dann der Wartungsmodus umgangen werden.',
        ] ,

    ];