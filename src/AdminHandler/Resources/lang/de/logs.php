<?php

    return [

        'views' => [
            'index'   => 'Log Viewer' ,
            'files'   => 'Log Dateien' ,
            'actions' => 'Aktionen' ,
        ] ,

        'buttons' => [
            'download'   => 'Download Datei' ,
            'clean'      => 'Leere Datei' ,
            'delete'     => 'Lösche Datei' ,
            'delete_all' => 'Lösche alle Dateien' ,
        ] ,

        'fields' => [
            'level'       => 'Level' ,
            'context'     => 'Context' ,
            'date'        => 'Datum' ,
            'line_number' => 'Zeilennummer' ,
            'content'     => 'Inhalt' ,
        ] ,

        'messages' => [

        ] ,

    ];