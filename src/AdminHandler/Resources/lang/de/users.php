<?php

    return [

        'views' => [
            'index' => 'Benutzer' ,
        ] ,

        'buttons' => [
            'login' => 'Als Benutzer einloggen' ,
        ] ,

        'fields' => [
            'id'         => 'ID' ,
            'name'       => 'Name' ,
            'email'      => 'Email' ,
            'last_login' => 'Letzter Login' ,
        ] ,

        'messages' => [

        ] ,

    ];