<?php

return [

    'views' => [
        'login'    => 'Admin Login',
        'versions' => 'Versionen',
    ] ,

    'buttons' => [
        'sessions'    => 'Sessions' ,
        'users'       => 'Benutzer' ,
        'queues'      => 'Queues & Jobs' ,
        'versions'    => 'Versionen',
        'maintenance' => 'Wartung' ,
        'telescope'   => 'Telescope' ,
        'logs'        => 'Log Viewer' ,
        'logout'      => 'Logout'
    ] ,

    'fields' => [
        'password' => 'Passwort',
        'module'   => 'Modul',
        'version'  => 'Version',
    ] ,

    'messages' => [
        'invalidCredentialsTitle' => 'Ungültige Logindaten' ,
        'invalidCredentialsText'  => 'Die angegebenen Logindaten sind ungültig' ,
    ] ,

];
