<?php

    return [

        'views' => [
            'index'       => 'Queues & Jobs',
            'queues'      => 'Queues',
            'jobs'        => 'Jobs',
            'failed_jobs' => 'Failed jobs'
        ],

        'buttons' => [

        ],

        'fields' => [
            'name'         => 'Name',
            'status'       => 'Status',
            'id'           => 'ID',
            'queue'        => 'Queue',
            'attempts'     => 'Versuche',
            'reserved_at'  => 'Reserviert seit',
            'available_at' => 'Verfügbar ab',
            'created_at'   => 'Erstellt am',
            'connection'   => 'Verbindung',
            'exception'    => 'Fehlermeldung',
            'failed_at'    => 'Fehlerhaft am',
        ],

        'status' => [
            -1 => 'Nicht verfügbar',
            0  => 'Inaktiv',
            1  => 'Aktiv',
        ],

        'messages' => [
            'noQueuesAvailable'     => 'Es sind keine Queues zum monitoren definiert...',
            'noJobsAvailable'       => 'Es sind keine Jobs aktuell in der Queue...',
            'noFailedJobsAvailable' => 'Es gibt aktuell keine gefailten Jobs - Yeah!',
        ],

    ];