@extends( $layout )

@section('title')
    @lang('dtv.admin::maintenance.views.index')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            @include('dtv.admin::navigation')
        </div>
        <div class="col-md-9">
            <div class="p-5 text-center jumbotron-fluid ">
                <span class="display-4">
                    @lang('dtv.admin::maintenance.messages.app_is')<br>
                    @if( app()->isDownForMaintenance() )
                        <span class="text-danger font-weight-bold">
                            @lang('dtv.admin::maintenance.status.0')
                        </span>
                    @else
                        <span class="text-success font-weight-bold">
                            @lang('dtv.admin::maintenance.status.1')
                        </span>
                    @endif
                </span>

            </div>
            <hr class="my-4">

            <div class="p-5 text-center">
                @if( app()->isDownForMaintenance() )
                    <h3>@lang('dtv.admin::maintenance.views.up')</h3>

                    <a class="btn btn-success btn-lg mt-3" href="{{ route('admin.maintenance.up') }}">
                        @lang('dtv.admin::maintenance.views.up')
                    </a>
                @else
                    <h3>@lang('dtv.admin::maintenance.views.down')</h3>
                    <form action="{{ route('admin.maintenance.down') }}" method="POST" style="max-width: 400px" class="m-auto">
                        @csrf

                        <div class="form-group text-left">
                            <label for="allowed_pw" class="font-weight-bold">
                                @lang('dtv.admin::maintenance.fields.allowed_pw')
                            </label>
                            <input name="allowed_pw" id="allowed_pw" type="text" value="{{ str_random() }}" class="form-control">
                            <small>Über {{ url('/') }}/{@lang('dtv.admin::maintenance.fields.allowed_pw')} kann dann der Wartungsmodus umgangen werden.</small>
                        </div>

                        <button class="btn btn-danger btn-lg mt-3" type="submit">
                            @lang('dtv.admin::maintenance.views.down')
                        </button>
                    </form>

                @endif
            </div>
        </div>
    </div>
@endsection

