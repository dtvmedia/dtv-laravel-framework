@extends( $layout )

@section('title')
    @lang('dtv.admin::admin.views.versions')
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            @include('dtv.admin::navigation')
        </div>
        <div class="col-md-9">
            @foreach($versions as $category => $subversions)
                <h3>{{ strtoupper($category) }}</h3>
                <table class="table table-striped mb-4">
                    <thead>
                        <tr>
                            <th style="width: 33%;">@lang('dtv.admin::admin.fields.module')</th>
                            <th>@lang('dtv.admin::admin.fields.version')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($subversions as $module => $version)
                            <tr>
                                <td style="padding-block: 0.25rem;">{{ $module }}</td>
                                <td style="padding-block: 0.25rem;">{{ $version }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endforeach
        </div>
    </div>
@endsection
