<div class="list-group">
    <a href="{{ route( 'admin.sessions.index' ) }}" class="list-group-item list-group-item-action @active('admin.sessions.index')">
        <i class="far fa-fw fa-users"></i>
        @lang('dtv.admin::admin.buttons.sessions')
    </a>
    <a href="{{ route( 'admin.users.index' ) }}" class="list-group-item list-group-item-action @active('admin.users.index')">
        <i class="far fa-fw fa-user"></i>
        @lang('dtv.admin::admin.buttons.users')
    </a>
    <a href="{{ route( 'admin.queues' ) }}" class="list-group-item list-group-item-action @active('admin.queues')">
        <i class="far fa-fw fa-conveyor-belt-alt"></i>
        @lang('dtv.admin::admin.buttons.queues')
    </a>
    <a href="{{ route( 'admin.versions' ) }}" class="list-group-item list-group-item-action @active('admin.versions')">
        <i class="fab fa-fw fa-php"></i>
        @lang('dtv.admin::admin.buttons.versions')
    </a>
    <a href="{{ route( 'admin.maintenance.index' ) }}" class="list-group-item list-group-item-action @active('admin.maintenance.index')">
        <i class="far fa-fw fa-tools"></i>
        @lang('dtv.admin::admin.buttons.maintenance')
    </a>
    @if(\Illuminate\Support\Facades\Route::has('telescope'))
    <a href="{{ route( 'telescope' ) }}" class="list-group-item list-group-item-action">
        <i class="fab fa-fw fa-laravel"></i>
        @lang('dtv.admin::admin.buttons.telescope')
    </a>
    @endif
    <a href="{{ route( 'admin.logs.index' ) }}" class="list-group-item list-group-item-action @active('admin.logs.index')">
        <i class="far fa-fw fa-file-alt"></i>
        @lang('dtv.admin::admin.buttons.logs')
    </a>
    <a href="{{ route( 'admin.logout' ) }}" class="list-group-item list-group-item-action">
        <i class="far fa-fw fa-sign-out"></i>
        @lang('dtv.admin::admin.buttons.logout')
    </a>
</div>
