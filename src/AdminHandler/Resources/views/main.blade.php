@extends( $layout )

@section('title')
    {{ $view->getTitle() }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-3">
            @include('dtv.admin::navigation')
        </div>
        <div class="col-md-9">
            {!! $view->render() !!}
        </div>
    </div>
@endsection

