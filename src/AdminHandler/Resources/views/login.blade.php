@extends( $layout )

@section('title')
    @lang('dtv.admin::admin.views.login')
@endsection

@section('content')
    <form id="admin-login-form" action="{{ $form->getAction() }}" method="{{ $form->getMethod() }}">
        @csrf

        <div class="row">
            <div class="col-md-4 offset-md-4 pt-5 mb-5">
                <div class="mb-3">
                    <div class="form-control bg-secondary">
                        <i class="far fa-fw fa-user-circle"></i> {{ auth()->user()->getDisplayName() }}
                    </div>
                </div>
                <div class="mb-3">
                    {!! $form->getInput('password') !!}
                </div>

                <button type="submit" class="btn btn-{{ config('dtv.app.button_type') }} w-100 mt-3">
                    <i class="far fa-fw fa-save"></i> @lang('dtv.base::general.save')
                </button>
            </div>
        </div>

    </form>
@endsection

@push('scripts')
    <script>
        new DTV.CustomFormView( '#admin-login-form', true );

        setTimeout(function () {
            document.querySelector('#password').focus();
        },500);
    </script>
@endpush