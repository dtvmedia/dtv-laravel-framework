@extends( $layout )

@section('title')
    @lang('dtv.admin::logs.views.index')
@endsection

@section('content')
    <style>
        #table-log {
            font-size: 0.85rem;
        }

        .table-container {
            overflow-y: auto;
            height: 82vh;
        }

        .btn {
            font-size: 0.7rem;
        }

        .stack {
            font-size: 0.85em;
            word-break: keep-all;
            overflow-x: scroll;
            color: rgb(30, 30, 30);
        }

        .date {
            min-width: 75px;
        }

        .text {
            word-break: break-all;
        }

        a.llv-active {
            z-index: 2;
            background-color: #f5f5f5;
            border-color: #777;
        }

        .list-group-item {
            word-wrap: break-word;
        }

        .folder {
            padding-top: 15px;
        }

        .div-scroll {
            height: 80vh;
            overflow: hidden auto;
        }

        .nowrap {
            white-space: nowrap;
        }

        .table-fixed {
            table-layout: fixed;
        }
    </style>

    <div class="row">
        <div class="col-md-3">
            @include('dtv.admin::navigation')

            <h5 class="mt-4">@lang('dtv.admin::logs.views.files')</h5>
            <div class="list-group mb-3">
                @foreach($folders as $folder)
                    <div class="list-group-item">
                        <a class="list-group-item-action" href="?f={{ \Illuminate\Support\Facades\Crypt::encrypt($folder) }}">
                            <span class="fa fa-folder"></span> {{$folder}}
                        </a>
                        @if ($current_folder == $folder)
                            <div class="list-group folder">
                                @foreach($folder_files as $file)
                                    <a href="?l={{ \Illuminate\Support\Facades\Crypt::encrypt($file) }}&f={{ \Illuminate\Support\Facades\Crypt::encrypt($folder) }}"
                                       class="list-group-item-action list-group-item @if ($current_file == $file) llv-active @endif">
                                        {{$file}}
                                    </a>
                                @endforeach
                            </div>
                        @endif
                    </div>
                @endforeach
                @foreach($files as $file)
                    <a href="?l={{ \Illuminate\Support\Facades\Crypt::encrypt($file) }}"
                       class="list-group-item-action list-group-item @if ($current_file == $file) llv-active @endif">
                        <i class="far fa-fw fa-file-alt"></i>
                        {{$file}}
                    </a>
                @endforeach
            </div>

            <h5 class="mt-4">@lang('dtv.admin::logs.views.actions')</h5>
            <div class="list-group">
                @if($current_file)
                    <a class="list-group-item list-group-item-action"
                       href="?dl={{ \Illuminate\Support\Facades\Crypt::encrypt($current_folder ? $current_folder . "/" . $current_file : $current_file) }}{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                        <i class="far fa-fw fa-download "></i> @lang('dtv.admin::logs.buttons.download')
                    </a>
                    <a class="list-group-item list-group-item-action" id="clean-log"
                       href="?clean={{ \Illuminate\Support\Facades\Crypt::encrypt($current_folder ? $current_folder . "/" . $current_file : $current_file) }}{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                        <i class="far fa-fw fa-sync"></i> @lang('dtv.admin::logs.buttons.clean')
                    </a>
                    <a class="list-group-item list-group-item-action" id="delete-log"
                       href="?del={{ \Illuminate\Support\Facades\Crypt::encrypt($current_folder ? $current_folder . "/" . $current_file : $current_file) }}{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                        <i class="far fa-fw fa-trash"></i> @lang('dtv.admin::logs.buttons.delete')
                    </a>
                    @if(count($files) > 1)
                        <a class="list-group-item list-group-item-action" id="delete-all-log"
                           href="?delall=true{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
                            <i class="far fa-fw fa-trash-alt"></i> @lang('dtv.admin::logs.buttons.delete_all')
                        </a>
                    @endif
                @endif
            </div>
        </div>
        <div class="col-md-9">
            <div class="table-container">
                @if ($logs === null)
                    <div>
                        Log file >50M, please download it.
                    </div>
                @else
                    <table id="table-log" class="table table-striped table-fixed" data-ordering-index="{{ $standardFormat ? 2 : 0 }}">
                        <thead>
                            <tr>
                                @if ($standardFormat)
                                    <th style="width: 100px;">@lang('dtv.admin::logs.fields.level')</th>
                                    <th style="width: 100px;">@lang('dtv.admin::logs.fields.date')</th>
                                @else
                                    <th style="width: 100px;">@lang('dtv.admin::logs.fields.line_number')</th>
                                @endif
                                <th>@lang('dtv.admin::logs.fields.content')</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($logs as $key => $log)
                                <tr data-display="stack{{{$key}}}">
                                    @if ($standardFormat)
                                        <td class="nowrap">
                                            <small class="text-muted text-uppercase"><strong>@lang('dtv.admin::logs.fields.level'):</strong></small><br>
                                            <span class="text-{{{$log['level_class']}}}" title="@lang('dtv.admin::logs.fields.context'): {{$log['context']}}">
                                                <i class="fa fa-{{{$log['level_img']}}}"></i>&nbsp;&nbsp;{{$log['level']}}
                                            </span>
                                        </td>
                                    @endif
                                    <td class="date">{{{$log['date']}}}</td>
                                    <td class="text position-relative w-75">
                                        @if ($log['stack'])
                                            <button type="button"
                                                    class="float-right expand btn btn-outline-dark btn-sm mb-2 ml-2"
                                                    data-display="stack{{{$key}}}">
                                                <span class="fa fa-search"></span>
                                            </button>
                                        @endif
                                        {{{$log['text']}}}
                                        @if (isset($log['in_file']))
                                            <br/>{{{$log['in_file']}}}
                                        @endif
                                        @if ($log['stack'])
                                            <div class="w-100">
                                                <div class="stack nowrap mt-2" id="stack{{{$key}}}"
                                                     style="display: none;">
                                                    {!! nl2br(e(trim( $log['stack'] ))) !!}
                                                </div>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>

        app().onLoaded( function () {
            $( '.table-container tr' ).on( 'click', function () {
                $( '#' + $( this ).data( 'display' ) ).toggle();
            } );

            $( '#delete-log, #clean-log, #delete-all-log' ).click( function () {
                return confirm( 'Are you sure?' );
            } );
        } )
    </script>
@endpush

