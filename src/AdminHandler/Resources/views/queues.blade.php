@extends( $layout )

@section('title')
    @lang('dtv.admin::queues.views.index')
@endsection

@section('content')
    <style>
        .table-container {
            overflow-y: auto;
            font-size: 0.85rem;
        }

        .stack {
            font-size: 0.85em;
            word-break: keep-all;
            overflow-x: scroll;
            color: rgb(30, 30, 30);
        }

        .text {
            word-break: break-all;
        }

        .nowrap {
            white-space: nowrap;
        }

        .table-fixed {
            table-layout: fixed;
        }
    </style>
    <div class="row">
        <div class="col-md-3">
            @include('dtv.admin::navigation')
        </div>
        <div class="col-md-9">
            <h3>@lang('dtv.admin::queues.views.queues')</h3>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>@lang('dtv.admin::queues.fields.name')</th>
                        <th>@lang('dtv.admin::queues.fields.status')</th>
                    </tr>
                </thead>
                <tbody>
                    @if( count( $queues) === 0 )
                        <tr>
                            <td colspan="2">
                                <i>@lang('dtv.admin::queues.messages.noQueuesAvailable')</i>
                            </td>
                        </tr>
                    @else
                        @foreach( $queues as $queue )
                            <tr>
                                <td>{{ $queue['name'] }}</td>
                                <td>
                                    @if( $queue['status'] === true )
                                        {!! badge(icon('fa-check-circle',trans('dtv.admin::queues.status.1'))->setIconBase('fas')->showLabel(),'success')->addClass('badge-md') !!}
                                    @elseif( $queue['status'] === false )
                                        {!! badge(icon('fa-times-circle',trans('dtv.admin::queues.status.0'))->setIconBase('fas')->showLabel(),'danger')->addClass('badge-md') !!}
                                    @else
                                        {!! badge(icon('fa-question-circle',trans('dtv.admin::queues.status.-1'))->setIconBase('fas')->showLabel(),'warning')->addClass('badge-md') !!}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>

            <h3 class="mt-5">@lang('dtv.admin::queues.views.jobs')</h3>
            <table class="table table-striped table-container">
                <thead>
                    <tr>
                        <th>@lang('dtv.admin::queues.fields.id')</th>
                        <th>@lang('dtv.admin::queues.fields.queue')</th>
                        <th>@lang('dtv.admin::queues.fields.name')</th>
                        <th>@lang('dtv.admin::queues.fields.attempts')</th>
                        <th>@lang('dtv.admin::queues.fields.reserved_at')</th>
                        <th>@lang('dtv.admin::queues.fields.available_at')</th>
                        <th>@lang('dtv.admin::queues.fields.created_at')</th>
                    </tr>
                </thead>
                <tbody>
                    @if( count($jobs) === 0 )
                        <tr>
                            <td colspan="7">
                                <i>@lang('dtv.admin::queues.messages.noJobsAvailable')</i>
                            </td>
                        </tr>
                    @else
                        @foreach($jobs as $job)
                            <tr>
                                <td>{{ $job['id'] }}</td>
                                <td>{{ $job['queue'] }}</td>
                                <td>{{ $job['name'] }}</td>
                                <td>{{ $job['attempts'] }}</td>
                                <td>{{ $job['reserved_at']?->toDateTimeString() ?? '-' }}</td>
                                <td>{{ $job['available_at']?->toDateTimeString() ?? '-' }}</td>
                                <td>{{ $job['created_at']?->toDateTimeString() ?? '-' }}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>

            <h3 class="mt-5">@lang('dtv.admin::queues.views.failed_jobs')</h3>
            <table class="table table-striped table-container table-fixed">
                <thead>
                    <tr>
                        <th style="width: 40px;">@lang('dtv.admin::queues.fields.id')</th>
                        <th style="width: 100px;">@lang('dtv.admin::queues.fields.failed_at')</th>
                        <th style="width: 80px;">@lang('dtv.admin::queues.fields.queue') & @lang('dtv.admin::queues.fields.name')</th>
                        <th style="width: 160px;">@lang('dtv.admin::queues.fields.name')</th>
                        <th>@lang('dtv.admin::queues.fields.exception')</th>
                    </tr>
                </thead>
                <tbody>
                    @if( count($failedJobs) === 0 )
                        <tr>
                            <td colspan="6">
                                <i>@lang('dtv.admin::queues.messages.noFailedJobsAvailable')</i>
                            </td>
                        </tr>
                    @else
                        @foreach($failedJobs as $key => $job)
                            <tr data-display="stack{{{$key}}}">
                                <td>{{ $job['id'] }}</td>
                                <td>{{ $job['failed_at']?->toDateTimeString() ?? '-' }}</td>
                                <td>
                                    <small class="text-muted text-uppercase"><strong>Queue:</strong></small> {{ $job['queue'] }}<br>
                                    <small class="text-muted text-uppercase d-block mt-2"><strong>Conn.:</strong></small> {{ $job['connection'] }}
                                </td>
                                <td>{{ $job['name'] }}</td>
                                <td class="position-relative w-75">
                                    {{ $job['exception'] }}
                                    @if(isset($job['stacktrace']))
                                        <button type="button"
                                                class="float-right expand btn btn-outline-dark btn-sm mb-2 ml-2"
                                                data-display="stack{{{$key}}}">
                                            <span class="fa fa-search"></span>
                                        </button>
                                        <br>
                                        <div class="w-100">
                                            <div class="stack nowrap mt-2" id="stack{{{$key}}}" style="display: none;">
                                                {!! nl2br(e(trim( $job['stacktrace'] ))) !!}
                                            </div>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        app().onLoaded( function () {
            $( '.table-container tr' ).on( 'click', function () {
                $( '#' + $( this ).data( 'display' ) ).toggle();
            } );
        } )
    </script>
@endpush
