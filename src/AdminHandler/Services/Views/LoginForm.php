<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\AdminHandler\Services\Views;

    use DTV\BaseHandler\Views\CustomFormView;

    /**
     * Admin Login Form Class
     *
     * @package   DTV\AdminHandler\Services\Views
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class LoginForm extends CustomFormView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.admin::admin.views.login';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-plus';

        /**
         * Translation file used for translating input labels etc
         *
         * @var string
         */
        protected $transPath = 'dtv.admin::admin';

        /**
         * Render view name
         *
         * @var string
         */
        protected $view = 'dtv.admin::login';

        /**
         * Adds the inputs to the form
         *
         * @throws \Throwable
         */
        public function inputs()
        {
            $this->addPassword( 'password' )
                ->setValidationRule( 'required' );
        }
    }