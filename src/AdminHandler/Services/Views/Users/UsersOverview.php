<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\AdminHandler\Services\Views\Users;

    use DTV\BaseHandler\Models\User;
    use DTV\BaseHandler\Views\ListView;

    /**
     * Users Overview Class
     *
     * @package   DTV\AdminHandler\Services\Views\Users
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class UsersOverview extends ListView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.admin::users.views.index';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-users';

        /**
         * Translation file used for translating column names etc
         *
         * @var string
         */
        protected $transPath = 'dtv.admin::users';

        /**
         * Switch which handles if the title will be shown in page headers etc.
         *
         * @var bool
         */
        protected $showTitle = false;

        /**
         * Adds the columns
         */
        protected function columns()
        {
            $this->addColumn( 'id' , 'id' , 'id' );
            $this->addColumn( 'name' , function ( User $user ) {
                return $user->getProfilePicture() . ' ' . e( $user->getDisplayName() );
            } , [ 'first_name' , 'last_name' ] )->enableHtml();
            $this->addColumn( 'email' , 'email' , 'email' );
            $this->addColumn( 'last_login' , function ( User $user ) {
                return $user->last_login->diffForHumansHtml();
            } , 'last_login' )->enableHtml();
        }

        /**
         * Adds the buttons for each record
         */
        protected function buttons()
        {
            $this->addButton( 'login' , 'fa-sign-in' , 'admin.users.login' )
                ->setDynamicParameter( [ 'id' => 'id' ] )->useHref();
        }
    }