<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\AdminHandler\Services\Views\Sessions;

    use DTV\BaseHandler\Views\ListView;

    /**
     * Sessions Overview Class
     *
     * @package   DTV\AdminHandler\Services\Views\Sessions
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class SessionsOverview extends ListView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.admin::sessions.views.index';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-users';

        /**
         * Translation file used for translating column names etc
         *
         * @var string
         */
        protected $transPath = 'dtv.admin::sessions';

        /**
         * Number of row of a page
         *
         * @var int
         */
        protected $pageLength = 50;

        /**
         * Order Column
         *
         * @var string
         */
        protected $orderColumn = 'last_activity';

        /**
         * Order Direction
         *
         * @var string
         */
        protected $orderDirection = 'desc';

        /**
         * Switch which handles if the title will be shown in page headers etc.
         *
         * @var bool
         */
        protected $showTitle = false;

        /**
         * Adds the columns
         */
        protected function columns()
        {
            $this->addColumn( 'last_activity' , 'getLastActivity' , 'last_activity' );
            $this->addColumn( 'user_id' , 'getUserName' , 'user_id' );
            $this->addColumn( 'ip_address' , 'ip_address' , 'ip_address' );
            $this->addColumn( 'user_agent' , 'user_agent' , 'user_agent' );
            $this->addColumn( 'last_accessed' , 'getLastAccessedUrl' );
        }

        /**
         * Adds the buttons for each record
         */
        protected function buttons()
        {

        }
    }