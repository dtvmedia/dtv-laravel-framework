<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\AdminHandler\Services\Handler;

    use Exception;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Cookie;
    use Illuminate\Support\Facades\Hash;

    /**
     * Admin Access Handler
     *
     * @package   DTV\AdminHandler\Services\Handler
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class AdminAccessHandler
    {
        /**
         * Cookie name
         */
        const COOKIE_NAME = 'admin-token';

        /**
         * Sets the admin token cookie
         *
         * @param Request $request
         *
         * @throws Exception
         * @return bool
         */
        public function login(Request $request)
        {
            $adminAccessPassword = config('dtv.modules.admin.access_password');

            if ($adminAccessPassword === null) {
                throw new Exception('No admin access password is set!');
            }

            if ($request->password !== $adminAccessPassword) {
                return false;
            }

            cookie()->queue(static::COOKIE_NAME, bcrypt(auth()->id()), 20);

            return true;
        }

        /**
         * Removes the admin token cookie
         *
         * @return bool
         */
        public function logout()
        {
            Cookie::queue(Cookie::forget(static::COOKIE_NAME));

            return true;
        }

        /**
         * Checks if an correct admin token cookie is available
         *
         * @return bool
         */
        public function hasAccess()
        {
            return Hash::check(auth()->id(), Cookie::get(static::COOKIE_NAME));
        }

        /**
         * Checks if the current user is allowed to see the backend
         *
         * @return bool
         */
        public function isAllowedToAccess()
        {
            $allowedUsers = array_filter(explode(',', config('dtv.modules.admin.access_user_id')));

            if (empty($allowedUsers)) {
                return false;
            }

            return in_array(auth()->id(), $allowedUsers);
        }
    }
