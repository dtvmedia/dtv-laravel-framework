<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\AdminHandler\Services\Handler;

    use DTV\Oxygen\Oxygen;
    use Illuminate\Support\Facades\DB;
    use Symfony\Component\Process\Exception\ProcessFailedException;
    use Symfony\Component\Process\Process;

    /**
     * Queue Handler
     *
     * @package   DTV\AdminHandler\Services\Handler
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class QueueHandler
    {
        /**
         * Returns the status of the to be monitored queues
         *
         * @return array
         */
        public function getQueueStatus()
        {
            $queueData = [];
            $queues = config('dtv.modules.admin.monitor_queues', []);

            if (windows_os()) {
                return collect($queues)->map(function ($item) {
                    return [
                        'name'   => $item,
                        'status' => null,
                    ];
                })->toArray();
            }

            if (empty($queues)) {
                return [];
            }

            $process = Process::fromShellCommandline('ps -aux | grep queue:work');
            $process->run();

            // executes after the command finishes
            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }

            $runningQueues = [];
            $output = $process->getOutput();
            preg_match_all('/php .*artisan queue:work .*--queue=([a-zA-Z0-9_\-]*)/m', $output, $matches, PREG_SET_ORDER);
            foreach ($matches as $match) {
                $runningQueues[] = $match[1];
            }

            foreach ($queues as $queue) {
                $queueData[] = [
                    'name'   => $queue,
                    'status' => in_array($queue, $runningQueues),
                ];
            }

            return $queueData;
        }

        /**
         * Returns an array of currently queued jobs
         *
         * @return array
         */
        public function getJobs()
        {
            $jobs = [];

            foreach (DB::table('jobs')->get() as $job) {
                $jobs[] = [
                    'id'           => $job->id,
                    'queue'        => $job->queue,
                    'name'         => json_decode($job->payload)->displayName,
                    'attempts'     => $job->attempts,
                    'reserved_at'  => ($job->reserved_at === null) ? null : Oxygen::createFromTimestamp($job->reserved_at),
                    'available_at' => ($job->available_at === null) ? null : Oxygen::createFromTimestamp($job->available_at),
                    'created_at'   => ($job->created_at === null) ? null : Oxygen::createFromTimestamp($job->created_at),
                ];
            }

            return $jobs;
        }

        /**
         * Returns an array of failed jobs
         *
         * @return array
         */
        public function getFailedJobs()
        {
            $jobs = [];

            foreach (DB::table('failed_jobs')->get() as $job) {
                [$exception, $stacktrace] = explode('Stack trace:', $job->exception, 2);

                $jobs[] = [
                    'id'         => $job->id,
                    'queue'      => $job->queue,
                    'name'       => json_decode($job->payload)->displayName,
                    'connection' => $job->connection,
                    'exception'  => $exception,
                    'stacktrace' => $stacktrace,
                    'failed_at'  => ($job->failed_at === null) ? null : Oxygen::parse($job->failed_at),
                ];
            }

            return $jobs;
        }
    }
