<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\AdminHandler\Middlewares;

    use Closure;
    use DTV\AdminHandler\Services\Handler\AdminAccessHandler;
    use Illuminate\Http\Request;

    /**
     * Allowed To Enter Backend Middleware
     *
     * @package   DTV\AdminHandler\Middlewares
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class AllowedToEnterBackend
    {
        /**
         * Handle an incoming request.
         *
         * @param Request $request
         * @param Closure $next
         *
         * @return mixed
         */
        public function handle($request, Closure $next)
        {
            $adminAccessHandler = new AdminAccessHandler();

            if (!$adminAccessHandler->isAllowedToAccess()) {
                return redirect()->to('/');
            }

            return $next($request);
        }
    }
