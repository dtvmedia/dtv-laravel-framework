# ActivityHandler - DTV Laravel Framework

## Configs
- *enabled*: Package activation switch

## Usage
1. Add the ``DTV\ActivityHandler\SupportsActivities`` Trait to the model which should have activities
2. Log activities for the model like this
````php
$model->createActivity( 'comment' , 'test comment content' , [] )
````

