<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\ActivityHandler\Models;

    use DTV\BaseHandler\Models\Model;
    use Illuminate\Database\Eloquent\Relations\BelongsTo;
    use Illuminate\Database\Eloquent\Relations\MorphTo;

    /**
     * Activity Model
     *
     * @package   DTV\ActivityHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Activity extends Model
    {
        /**
         * The attributes that are mass assignable
         *
         * @var array
         */
        protected $fillable = [
            'type' , 'user_id' , 'text' , 'parameter' , 'reference_type' , 'reference_id' , 'route'
        ];

        /**
         * The name of the related translation file
         *
         * @var string
         */
        public static $translationFile = 'activities';

        /**
         * The attributes that should be cast to native types.
         *
         * @var array
         */
        protected $casts = [
            'parameter' => 'array' ,
        ];

        /**
         * Returns the related User
         *
         * @return BelongsTo
         */
        public function user(): BelongsTo
        {
            return $this->belongsTo(config('dtv.models.user'))->withDefault([
                'first_name' => 'System',
            ]);
        }

        /**
         * Returns the Reference Model of the Activity
         *
         * @return MorphTo
         */
        public function reference(): MorphTo
        {
            return $this->morphTo()->withTrashed();
        }
    }
