<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\ActivityHandler;

    use DTV\ActivityHandler\Models\Activity;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Facades\Route;

    /**
     * Activity Handler
     *
     * @package   DTV\ActivityHandler
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class ActivityHandler
    {
        /**
         * Creates an new activity
         *
         * @param string            $type
         * @param string            $text
         * @param array             $parameter
         * @param Model|string|null $reference
         * @param int|null          $reference_id
         * @param int|null          $user_id
         *
         * @return Activity
         */
        public function create(string $type, string $text = '', array $parameter = [], $reference = null, int $reference_id = null, int $user_id = null)
        {
            if ($reference instanceof Model) {
                $reference_type = get_class($reference);
                $reference_id = $reference->id;
            } elseif (is_string($reference) && is_int($reference_id)) {
                $reference_type = $reference;
            } else {
                $reference_type = null;
                $reference_id = null;
            }

            /** @var Activity */
            return Activity::query()->create([
                'type'           => $type,
                'user_id'        => $user_id ?? auth()->id(),
                'text'           => $text,
                'parameter'      => $parameter,
                'reference_type' => $reference_type,
                'reference_id'   => $reference_id,
                'route'          => Route::getCurrentRoute()?->getName() ?? '',
            ]);
        }
    }
