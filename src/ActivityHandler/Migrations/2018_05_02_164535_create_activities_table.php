<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateActivitiesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create( 'activities' , function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->integer( 'user_id' )->unsigned();
                $table->string( 'type' );
                $table->string( 'reference_type' )->nullable();
                $table->integer( 'reference_id' )->unsigned()->nullable();
                $table->text( 'text' );
                $table->text( 'parameter' );
                $table->string( 'route' );
                $table->timestamps();
                $table->softDeletes();
            } );

            Schema::table( 'activities' , function ( $table ) {
                $table->foreign( 'user_id' )->references( 'id' )->on( config( 'dtv.tables.user' )  );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists( 'activities' );
        }
    }
