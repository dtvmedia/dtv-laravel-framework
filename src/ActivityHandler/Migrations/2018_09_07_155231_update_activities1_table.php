<?php

    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class UpdateActivities1Table extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table( 'activities' , function ( Blueprint $table ) {
                DB::statement('ALTER TABLE activities MODIFY user_id INT(10) UNSIGNED;');
            } );
        }
    }
