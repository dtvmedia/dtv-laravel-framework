<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\ActivityHandler;

    /**
     * Supports Activities Trait for Models
     *
     * @package   DTV\ActivityHandler
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    trait SupportsActivities
    {
        /**
         * Creates an new activity for the current model
         *
         * @param string $type
         * @param string $text
         * @param array  $parameters
         *
         * @return mixed
         */
        public function createActivity(string $type, string $text = '', array $parameters = [])
        {
            return app(ActivityHandler::class)->create($type, $text, $parameters, $this);
        }
    }
