<?php

    return [

        // Date Formats
        'date'                     => 'YYYY-MM-DD' ,
        'date_with_day'            => 'ddd, YYYY-MM-DD' ,
        // Time Formats
        'time'                     => 'HH:mm:ss' ,
        'short_time'               => 'HH:mm' ,
        // Datetime Combination Formats
        'date_time'                => 'YYYY-MM-DD HH:mm:ss' ,
        'date_short_time'          => 'YYYY-MM-DD HH:mm' ,
        'date_time_with_day'       => 'ddd, YYYY-MM-DD HH:mm:ss' ,
        'date_short_time_with_day' => 'ddd, YYYY-MM-DD HH:mm' ,
        // To string format
        'to_string'                => 'ddd, YYYY-MM-DD HH:mm' ,

    ];
