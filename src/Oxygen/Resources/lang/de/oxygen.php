<?php

    return [

        // Date Formats
        'date'                     => 'DD.MM.YYYY' ,
        'date_with_day'            => 'dd, DD.MM.YYYY' ,
        // Time Formats
        'time'                     => 'HH:mm:ss' ,
        'short_time'               => 'HH:mm' ,
        // Datetime Combination Formats
        'date_time'                => 'DD.MM.YYYY HH:mm:ss' ,
        'date_short_time'          => 'DD.MM.YYYY HH:mm' ,
        'date_time_with_day'       => 'dd, DD.MM.YYYY HH:mm:ss' ,
        'date_short_time_with_day' => 'dd, DD.MM.YYYY HH:mm' ,
        // To string format
        'to_string'                => 'dd, DD.MM.YYYY HH:mm' ,
    ];
