# Oxygen - DTV Laravel Framework

Oxygen is an slightly improved wrapper for the Carbon DateTime class.

## Usage
### Date Mutator
If you're using the base model from this framework, all fields defined in the $dates property of the model will automatically mutated to Oxygen instances so make sure you define them properly.

### Change output formats
We decided to handle the output formats via the translation system due to different preferred formats for every country.
That gives us also the ability to publish these translations and customize them if necessary:
````
php artisan vendor:publish --tag=dtv.oxygen
````