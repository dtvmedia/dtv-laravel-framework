<?php

/**
 * DTV Media Solutions
 *
 * PHP Version 7.0
 */

/**
 * Helper Functions
 *
 * @package   DTV\SettingsHandler
 * @copyright 2018 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */

use DTV\Oxygen\Oxygen;

if (!function_exists('oxygen')) {
    /**
     * Parses the given time and returns an Oxygen object, Shortcut function
     *
     * @param DateTimeInterface|string|null $time
     * @param DateTimeZone|string|null      $timezone
     *
     * @return Oxygen
     */
    function oxygen(DateTimeInterface|string|null $time = null, DateTimeZone|string|null $timezone = null)
    {
        return Oxygen::parse($time, $timezone);
    }
}
