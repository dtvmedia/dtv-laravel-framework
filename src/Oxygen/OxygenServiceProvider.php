<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\Oxygen;

    use DTV\BaseHandler\BaseServiceProvider;

    /**
     * Oxygen Service Provider
     *
     * @package   DTV\Oxygen
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class OxygenServiceProvider extends BaseServiceProvider
    {
        /**
         *  Namespace for resources of the module
         *
         * @var string
         */
        protected $resourcesNamespace = 'dtv.oxygen';

        /**
         * Config path for the module enable switch
         *
         * @var string|null
         */
        public static $config = null;
    }