<?php /** @noinspection PhpUnused */

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\Oxygen;

    use Carbon\Carbon;
    use DateTimeZone;

    /**
     * Oxygen Date Class v2 - Carbon Extension
     *
     * @package   DTV\Oxygen
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Oxygen extends Carbon
    {
        /**
         * Flag which indicates if the object is considered null
         *
         * @var bool
         */
        protected bool $null = false;

        /**
         * Oxygen constructor
         *
         * @param string|null|bool         $time
         * @param DateTimeZone|string|null $tz
         */
        public function __construct($time = 'now', $tz = null)
        {
            // Null Handling
            $this->null = ($time === null);

            parent::__construct(($time === 'now') ? null : $time, $tz);

            if ($this->timestamp < -30610227208) { // 01-01-1000 00:00:00
                $this->null = true;
            }
        }

        /**
         * Get a Carbon instance for the current date and time.
         *
         * @param DateTimeZone|string|null $tz
         *
         * @return static
         */
        public static function now($tz = null): static
        {
            // sets time false and not null like carbon
            return new static('now', $tz);
        }

        /**
         * Returns the easter date of a given year
         *
         * @param int|null $year
         *
         * @return Oxygen
         */
        public static function easter(?int $year = null): self
        {
            if ($year == null) {
                $year = today()->year;
            }

            return static::createFromTimestamp(easter_date($year));
        }

        /**
         * Returns a time diff label with the actual date time as title
         *
         * @param string $default
         * @param bool   $absolute
         * @param bool   $short
         *
         * @return string
         */
        public function diffForHumansHtml(mixed $default = '-', bool $absolute = false, bool $short = false)
        {
            if ($this->isNull()) {
                return $default;
            }

            $diffString = parent::diffForHumans(null, $absolute, $short);

            return '<span class="date-ago" title="' . $this->toDateTimeString() . '">' . $diffString . '</span>';
        }

        /**
         * Overriden localized format function which checks if the the timestamp is null
         *
         * @param string $format
         * @param mixed  $default
         *
         * @return string
         */
        public function formatLocalized($format, $default = '-')
        {
            if ($this->isNull()) {
                return $default;
            }

            $allowedTransFormats = [
                'date',
                'date_with_day',
                'time',
                'short_time',
                'date_time',
                'date_time_with_day',
                'date_short_time',
                'date_short_time_with_day',
                'to_string',
            ];

            if (in_array($format, $allowedTransFormats)) {
                $format = trans('dtv.oxygen::oxygen.' . $format);
            }

            return $this->isoFormat($format);
        }

        /**
         * Returns whether or not the current is null
         *
         * @return bool
         */
        public function isNull(): bool
        {
            return $this->null;
        }

        /**
         * Returns whether or not the current is null
         *
         * @return bool
         */
        public function isNotNull(): bool
        {
            return !$this->null;
        }

        /**
         * Returns whether a given day is a holiday
         *
         * @param string|null $state 2-char state code
         *
         * @return bool
         */
        public function isHoliday(?string $state = null): bool
        {
            return (new Holidays())->isHoliday($this, $state);
        }

        /**
         * Returns whether or not a given date is an nation wide holiday
         *
         * @return bool
         */
        public function isNationwideHoliday(): bool
        {
            return (new Holidays())->isNationwideHoliday($this);
        }

        ///////////////////////////////////////////////////////////////////
        /////////////////////// STRING FORMATTING /////////////////////////
        ///////////////////////////////////////////////////////////////////

        /**
         * Returns the timestamp as default date time string
         *
         * @param mixed $default
         *
         * @return string
         */
        public function toDefaultString(mixed $default = '-'): string
        {
            if ($this->isNull()) {
                return $default;
            }

            return $this->format('Y-m-d H:i:s');
        }

        /**
         * Returns the timestamp as default date string
         *
         * @param mixed $default
         *
         * @return string
         */
        public function toDefaultDateString(mixed $default = '-'): string
        {
            if ($this->isNull()) {
                return $default;
            }

            return $this->format('Y-m-d');
        }

        /**
         * Returns the timestamp as default time string
         *
         * @param mixed $default
         *
         * @return string
         */
        public function toDefaultTimeString(mixed $default = '-'): string
        {
            if ($this->isNull()) {
                return $default;
            }

            return $this->format('H:i:s');
        }

        /**
         * Returns the timestamp as date string
         *
         * @return string
         */
        public function toDateString(): string
        {
            return $this->formatLocalized('date');
        }

        /**
         * Returns the timestamp as date with day string
         *
         * @return string
         */
        public function toDateWithDayString(): string
        {
            return $this->formatLocalized('date_with_day');
        }

        /**
         * Returns the timestamp as time string
         *
         * @param string $unitPrecision
         *
         * @return string
         */
        public function toTimeString($unitPrecision = 'second'): string
        {
            if (static::singularUnit($unitPrecision) === 'minute') {
                return $this->toShortTimeString();
            }

            return $this->formatLocalized('time');
        }

        /**
         * Returns the timestamp as short time string
         *
         * @return string
         */
        public function toShortTimeString(): string
        {
            return $this->formatLocalized('short_time');
        }

        /**
         * Returns the timestamp as date time string
         *
         * @param string $unitPrecision
         *
         * @return string
         */
        public function toDateTimeString($unitPrecision = 'second'): string
        {
            if (static::singularUnit($unitPrecision) === 'minute') {
                return $this->toDateShortTimeString();
            }

            return $this->formatLocalized('date_time');
        }

        /**
         * Returns the timestamp as date time with day string
         *
         * @return string
         */
        public function toDateTimeWithDayString(): string
        {
            return $this->formatLocalized('date_time_with_day');
        }

        /**
         * Returns the timestamp as date short time string
         *
         * @return string
         */
        public function toDateShortTimeString(): string
        {
            return $this->formatLocalized('date_short_time');
        }

        /**
         * Returns the timestamp as date short time with day string
         *
         * @return string
         */
        public function toDateShortTimeWithDayString(): string
        {
            return $this->formatLocalized('date_short_time_with_day');
        }

        /**
         * Returns the timestamp as string with the specified format
         *
         * @return string
         */
        public function __toString(): string
        {
            return $this->formatLocalized('to_string');
        }
    }
