<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\Oxygen;

    use Carbon\Carbon;
    use Exception;

    /**
     * German Holidays Class
     *
     * @package   DTV\Oxygen
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Holidays
    {
        /**
         * Array of all state
         */
        public const STATES_ALL = [ 'BW' , 'BY' , 'BE' , 'BB' , 'HB' , 'HH' , 'HE' , 'MV' , 'NI' , 'NW' , 'RP' , 'SL' , 'SN' , 'ST' , 'SH' , 'TH' ];

        /**
         * Holiday data array
         *
         * @var array
         */
        protected array $data = [];

        /**
         * Calculates all holidays of a given year
         *
         * @param int $year
         *
         * @return bool
         */
        protected function calcByYear(int $year): bool
        {
            if (isset($this->data[$year])) {
                return false;
            }

            $easter = Oxygen::easter($year);

            // Neujahr
            $this->data[$year][1][1] = [
                'name'   => 'Neujahr',
                'date'   => Oxygen::create($year, 1, 1),
                'states' => static::STATES_ALL,
            ];

            // Heilige Drei Könige
            $this->data[$year][1][6] = [
                'name'   => 'Heilige Drei Könige',
                'date'   => Oxygen::create($year, 1, 6),
                'states' => ['BW', 'BY', 'ST'],
            ];

            // Internationaler Frauentag
            $this->data[$year][3][8] = [
                'name'   => 'Internationaler Frauentag',
                'date'   => Oxygen::create($year, 3, 8),
                'states' => ['BE'],
            ];

            // Karfreitag
            $day = $easter->copy()->subDays(2);
            $this->data[$year][$day->month][$day->day] = [
                'name'   => 'Karfreitag',
                'date'   => $day,
                'states' => static::STATES_ALL,
            ];

            // Ostersonntag
            $this->data[$year][$easter->month][$easter->day] = [
                'name'   => 'Ostersonntag',
                'date'   => $easter,
                'states' => ['BB'],
            ];

            // Ostermontag
            $day = $easter->copy()->addDay();
            $this->data[$year][$day->month][$day->day] = [
                'name'   => 'Ostermontag',
                'date'   => $day,
                'states' => static::STATES_ALL,
            ];

            // Tag der Arbeit
            $this->data[$year][5][1] = [
                'name'   => 'Tag der Arbeit',
                'date'   => Oxygen::create($year, 5, 1),
                'states' => static::STATES_ALL,
            ];

            // Christi Himmelfahrt
            $day = $easter->copy()->addDays(39);
            $this->data[$year][$day->month][$day->day] = [
                'name'   => 'Christi Himmelfahrt',
                'date'   => $day,
                'states' => static::STATES_ALL,
            ];

            // Pfingstsonntag
            $day = $easter->copy()->addDays(49);
            $this->data[$year][$day->month][$day->day] = [
                'name'   => 'Pfingstsonntag',
                'date'   => $day,
                'states' => ['BB'],
            ];

            // Pfingstmontag
            $day = $easter->copy()->addDays(50);
            $this->data[$year][$day->month][$day->day] = [
                'name'   => 'Pfingstmontag',
                'date'   => $day,
                'states' => static::STATES_ALL,
            ];

            // Fronleichnam
            $day = $easter->copy()->addDays(60);
            $this->data[$year][$day->month][$day->day] = [
                'name'   => 'Fronleichnam',
                'date'   => $day,
                'states' => ['BW', 'BY', 'HE', 'NW', 'RP', 'SL'],
            ];

            // Mariä Himmelfahrt
            $this->data[$year][8][15] = [
                'name'   => 'Mariä Himmelfahrt',
                'date'   => Oxygen::create($year, 8, 15),
                'states' => ['BY', 'SL'],
            ];

            // Weltkindertag
            $this->data[$year][9][20] = [
                'name'   => 'Weltkindertag',
                'date'   => Oxygen::create($year, 9, 20),
                'states' => ['TH'],
            ];

            // Tag der Deutschen Einheit
            $this->data[$year][10][3] = [
                'name'   => 'Tag der Deutschen Einheit',
                'date'   => Oxygen::create($year, 10, 3),
                'states' => static::STATES_ALL,
            ];

            // Reformationstag
            $this->data[$year][10][31] = [
                'name'   => 'Reformationstag',
                'date'   => Oxygen::create($year, 10, 31),
                'states' => ['BB', 'HB', 'HH', 'MV', 'NI', 'SN', 'ST', 'SH', 'TH'],
            ];

            // Allerheiligen
            $this->data[$year][11][1] = [
                'name'   => 'Allerheiligen',
                'date'   => Oxygen::create($year, 11, 1),
                'states' => ['BW', 'BY', 'NW', 'RP', 'SL'],
            ];

            // Buß- und Bettag
            $nov23 = Oxygen::create($year, 11, 23);
            if ($nov23->dayOfWeekIso <= 3) {
                $day = $nov23->copy()->subDays($nov23->dayOfWeekIso + 7 - 3);
            } else {
                $day = $nov23->copy()->subDays($nov23->dayOfWeekIso - 3);
            }
            $this->data[$year][$day->month][$day->day] = [
                'name'   => 'Buß- und Bettag',
                'date'   => $day,
                'states' => ['SN'],
            ];

            // 1. Weihnachtstag
            $this->data[$year][12][25] = [
                'name'   => '1. Weihnachtstag',
                'date'   => Oxygen::create($year, 12, 25),
                'states' => static::STATES_ALL,
            ];

            // 2. Weihnachtstag
            $this->data[$year][12][26] = [
                'name'   => '2. Weihnachtstag',
                'date'   => Oxygen::create($year, 12, 26),
                'states' => static::STATES_ALL,
            ];

            return true;
        }

        /**
         * Function checks the given year
         *
         * @param int|null $year
         *
         * @throws Exception
         * @return int
         */
        protected function checkYear(?int $year = null): int
        {
            if ($year == null) {
                return today()->year;
            }

            if ($year < 1970 || $year > 2037) {
                throw new Exception('This function is only valid for years between 1970 and 2037 inclusive');
            }

            return $year;
        }

        /**
         * Returns whether a given day is a holiday
         *
         * @param Carbon      $date
         * @param string|null $state 2-char state code
         *
         * @return bool
         */
        public function isHoliday(Carbon $date, ?string $state = null): bool
        {
            $this->calcByYear($date->year);

            if (isset($this->data[$date->year][$date->month][$date->day])) {
                if ($state === null) {
                    return true;
                }

                if ($state === 'ALL') {
                    return count($this->data[$date->year][$date->month][$date->day]['states']) === 16;
                }

                return in_array($state, $this->data[$date->year][$date->month][$date->day]['states']);
            }

            return false;
        }

        /**
         * Returns whether or not a given date is an nation wide holiday
         *
         * @param Carbon $date
         *
         * @return bool
         */
        public function isNationwideHoliday(Carbon $date)
        {
            return $this->isHoliday($date, 'ALL');
        }

        /**
         * Returns all holidays for a given state
         *
         * @param string   $state 2-char state code
         * @param int|null $year
         *
         * @throws Exception
         * @return array
         */
        public function getByState(string $state, ?int $year = null): array
        {
            $holidays = [];
            $year = $this->checkYear($year);
            $this->calcByYear($year);

            foreach ($this->data[$year] as $month) {
                foreach ($month as $day) {
                    if (in_array($state, $day['states'])) {
                        $holidays[] = $day;
                    }
                }
            }

            return $holidays;
        }

        /**
         * Returns all holidays
         *
         * @param int|null $year
         *
         * @throws Exception
         * @return array
         */
        public function get(?int $year = null): array
        {
            $year = $this->checkYear($year);
            $this->calcByYear($year);

            return array_flatten($this->data[$year], 1);
        }
    }
