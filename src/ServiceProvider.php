<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV;

    use Carbon\Carbon;
    use DTV\AdminHandler\AdminHandlerServiceProvider;
    use DTV\ActivityHandler\ActivityHandlerServiceProvider;
    use DTV\BaseHandler\BaseHandlerServiceProvider;
    use DTV\BaseHandler\BaseServiceProvider;
    use DTV\AclHandler\AclHandlerServiceProvider;
    use DTV\CategoryHandler\CategoryHandlerServiceProvider;
    use DTV\CommentsHandler\CommentHandlerServiceProvider;
    use DTV\DashboardHandler\DashboardHandlerServiceProvider;
    use DTV\EmailHandler\EmailHandlerServicesProvider;
    use DTV\GeoHandler\GeoHandlerServiceProvider;
    use DTV\Oxygen\OxygenServiceProvider;
    use DTV\SettingsHandler\SettingsHandlerServiceProvider;
    use DTV\SmsHandler\SmsHandlerServiceProvider;
    use Illuminate\Contracts\Container\BindingResolutionException;

    /**
     * Service Provider
     *
     * @package   DTV
     * @copyright 2017 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class ServiceProvider extends BaseServiceProvider
    {
        /**
         * Namespace for resources of the module
         *
         * @var string
         */
        protected $resourcesNamespace = 'dtv';

        /**
         * Array of commans
         *
         * @var array
         */
        protected $commands = [];

        /**
         * Array of all to be registered providers
         *
         * @var array
         */
        protected array $providers = [
            BaseHandlerServiceProvider::class ,
            OxygenServiceProvider::class ,
            AclHandlerServiceProvider::class ,
            SettingsHandlerServiceProvider::class ,
            DashboardHandlerServiceProvider::class ,
            CategoryHandlerServiceProvider::class ,
            CommentHandlerServiceProvider::class ,
            EmailHandlerServicesProvider::class ,
            ActivityHandlerServiceProvider::class ,
            GeoHandlerServiceProvider::class ,
            SmsHandlerServiceProvider::class ,
            AdminHandlerServiceProvider::class ,
        ];

        /**
         * Bootstrap any application services.
         *
         * @return void
         */
        public function boot()
        {
            // Oxygen initialization
            $locale = config('app.locale');
            setlocale(LC_TIME, $locale, $locale . '_' . strtoupper($locale) . '.UTF-8');
            Carbon::setLocale($locale);

            // Config publishing
            $this->publishes([
                __DIR__ . '/../config/dtv.php' => config_path('dtv.php'),
            ], 'dtv-config');
        }

        /**
         * Registers the system services
         *
         * @throws BindingResolutionException
         */
        public function register()
        {
            parent::register();

            $this->mergeConfigFrom( __DIR__ . '/../config/dtv.php' , 'dtv' );

            $this->registerModules();
        }

        /**
         * Registers the modules
         */
        private function registerModules()
        {
            foreach ($this->providers as $provider) {
                if ($provider::$config === null || (config($provider::$config) === true)) {
                    $this->app->register($provider);
                }
            }
        }
    }
