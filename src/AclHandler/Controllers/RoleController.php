<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\AclHandler\Controllers;

    use DTV\AclHandler\Models\Role;
    use DTV\AclHandler\RoleHandler;
    use DTV\AclHandler\Services\Views\RoleOverview;
    use DTV\AclHandler\Services\Views\RolesForm;
    use DTV\BaseHandler\Controllers\Controller;
    use DTV\BaseHandler\JsonResponse;
    use DTV\BaseHandler\Views\Page;
    use Exception;
    use Illuminate\Http\Request;
    use Throwable;

    /**
     * Role Controller
     *
     * @package   DTV\AclHandler\Controllers
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class RoleController extends Controller
    {
        /**
         * Shows the role overview page
         *
         * @throws Throwable
         */
        public function index()
        {
            $this->addHeaderButton('dtv.acl::roles.buttons.create', 'fa-plus', 'roles.create');

            return $this->view(new RoleOverview(Role::query()));
        }

        /**
         * Shows and handles the create role form
         *
         * @param Request     $request
         * @param RoleHandler $roleHandler
         *
         * @throws Throwable
         *
         * @return JsonResponse|Page|string
         */
        public function create(Request $request, RoleHandler $roleHandler)
        {
            return $this->form($request, new RolesForm(), function (Request $request) use ($roleHandler) {
                $roleHandler->create(
                    $request->name,
                    $request->description,
                    $request->get('permissions', []),
                    $request->boolean('default')
                );

                return $this->json()
                    ->success( 'dtv.acl::roles.messages.created' )
                    ->reload();
            } );
        }

        /**
         * Shows and handles the edit role form
         *
         * @param Role        $role
         * @param Request     $request
         * @param RoleHandler $roleHandler
         *
         * @throws Throwable
         *
         * @return JsonResponse|Page|string
         */
        public function edit(Role $role, Request $request, RoleHandler $roleHandler)
        {
            return $this->form($request, new RolesForm($role), function (Request $request) use ($role, $roleHandler) {
                $roleHandler->edit($role, $request->all());

                return $this->json()
                    ->success('dtv.acl::roles.messages.edited')
                    ->reload();
            });
        }

        /**
         * Tries to delete the given role
         *
         * @param Role $role
         *
         * @throws Exception
         *
         * @return JsonResponse
         */
        public function delete(Role $role)
        {
            return $this->deleteModel($role, ['users']);
        }
    }
