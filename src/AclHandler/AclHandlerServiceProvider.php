<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\AclHandler;

    use DTV\AclHandler\Controllers\RoleController;
    use DTV\BaseHandler\BaseServiceProvider;
    use DTV\BaseHandler\RoutingHandler;
    use Exception;

    /**
     * ACL Handler Service Provider
     *
     * @package   DTV\CommentsHandler
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class AclHandlerServiceProvider extends BaseServiceProvider
    {
        /**
         *  Namespace for resources of the module
         *
         * @var string
         */
        protected $resourcesNamespace = 'dtv.acl';

        /**
         * Array of commans
         *
         * @var array
         */
        protected $commands = [];

        /**
         * Config path for the module enable switch
         *
         * @var string|null
         */
        public static $config = 'dtv.modules.acl.enabled';

        /**
         * Registers the Modules routes
         *
         * @throws Exception
         */
        public function routes()
        {
            $roleRoutes = new RoutingHandler( RoleController::class , 'roles' , 'roles' );
            $roleRoutes->get( '/' , 'index' , 'index' , [ 'can:roles.view' ] );
            $roleRoutes->any( '/create' , 'create' , 'create' , [ 'can:roles.change' ] );
            $roleRoutes->any( '/{role}/edit/' , 'edit' , 'edit' , [ 'can:roles.change' ] );
            $roleRoutes->delete( '/{role}/delete/' , 'delete' , 'delete' , [ 'can:roles.change' ] );
        }

        /**
         * Defines all the modules gates
         */
        public function gates()
        {
            gate()->define( 'roles.view' , function ( $user ) {
                return $user->hasPermission( [ 'roles.view' ] );
            } );
            gate()->define( 'roles.change' , function ( $user ) {
                return $user->hasPermission( [ 'roles.view' , 'roles.change' ] );
            } );
        }
    }
