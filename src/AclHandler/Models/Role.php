<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\AclHandler\Models;

    use Database\Factories\RoleFactory;
    use DTV\BaseHandler\Models\Model;
    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Database\Eloquent\Relations\BelongsToMany;

    /**
     * Role model
     *
     * @package   DTV\AclHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Role extends Model
    {
        use HasFactory;

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'name' , 'slug' , 'description' , 'permissions' , 'default'
        ];

        /**
         * The attributes that should be cast to native types.
         *
         * @var array
         */
        protected $casts = [
            'permissions' => 'array',
            'default'     => 'boolean',
        ];

        /**
         * The name of the related translation file
         * If null the getTranslationFile method will try to build it from the model name
         *
         * @var string
         */
        protected static $translationFile = 'dtv.acl::roles';

        /**
         * Returns all the related users
         *
         * @return BelongsToMany
         */
        public function users()
        {
            return $this->belongsToMany( config( 'dtv.models.user' ) );
        }

        /**
         * Returns the role name with the default icon
         *
         * @return string
         */
        public function getNameLabel()
        {
            return e( $this->name ) . ' ' . $this->getDefaultIcon();
        }

        /**
         * Returns an icon badge if the role is the default role
         *
         * @return string
         */
        public function getDefaultIcon()
        {
            if ($this->default === false) {
                return '';
            }

            return icon('fa-star', trans('dtv.acl::roles.fields.default'))
                ->addClass('text-warning')
                ->setIconBase('fas')
                ->useFixedWidth();
        }

        /**
         * Returns whether or not the role has given permissions
         *
         * @param array|string $permissions
         *
         * @return bool
         */
        public function hasPermission( $permissions ): bool
        {
            if ( is_string( $permissions ) ) {
                $permissions = [ $permissions ];
            }

            foreach ( $permissions as $permission ) {
                if ( !in_array( $permission , $this->permissions ) ) {
                    return false;
                }
            }

            return true;
        }

        protected static function newFactory()
        {
            return RoleFactory::new();
        }
    }
