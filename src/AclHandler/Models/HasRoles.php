<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\AclHandler\Models;

    use Illuminate\Database\Eloquent\Relations\BelongsToMany;

    /**
     * Has Roles Trait for the User model
     *
     * @package   DTV\AclHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    trait HasRoles
    {
        /**
         * Returns all the related roles
         *
         * @return BelongsToMany
         */
        public function roles(): BelongsToMany
        {
            return $this->belongsToMany( Role::class );
        }

        /**
         * Returns whether or not the model has the given permissions
         *
         * @param string|array $permissions
         *
         * @return bool
         */
        public function hasPermission( $permissions ): bool
        {
            if ( is_string( $permissions ) ) {
                $permissions = [ $permissions ];
            }

            // check if the permission is available in any role
            foreach ( $this->roles as $role ) {
                if ( $role->hasPermission( $permissions ) ) {
                    return true;
                }
            }

            return false;
        }
    }
