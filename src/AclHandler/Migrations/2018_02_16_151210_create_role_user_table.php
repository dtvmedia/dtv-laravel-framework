<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Creates Role User Table Migration
     *
     * @package   -
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CreateRoleUserTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create( 'role_user' , function ( Blueprint $table ) {
                $table->unsignedInteger( 'user_id' );
                $table->unsignedInteger( 'role_id' );
                $table->timestamps();

                $table->unique( [ 'user_id' , 'role_id' ] );
                $table->foreign( 'user_id' )->references( 'id' )->on( config( 'dtv.tables.user' )  )->onDelete( 'cascade' );
                $table->foreign( 'role_id' )->references( 'id' )->on( 'roles' )->onDelete( 'cascade' );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists( 'role_user' );
        }
    }
