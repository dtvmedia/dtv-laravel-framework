<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\AclHandler;

    use DTV\AclHandler\Models\Role;

    /**
     * Role Handler
     *
     * @package   DTV\AclHandler
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class RoleHandler
    {
        /**
         * Creates an new role
         *
         * @param string      $name
         * @param string|null $description
         * @param array       $permissions
         * @param bool        $default
         *
         * @return Role
         */
        public function create(string $name, ?string $description, array $permissions, bool $default = false)
        {
            $slug = str_slug( $name );

            if ($default) {
                Role::query()
                    ->where('default', true)
                    ->update(['default' => false]);
            }

            /** @var Role */
            return Role::query()->create(compact('name', 'slug', 'description', 'permissions', 'default'));
        }

        /**
         * Updates an role with the given attributes
         *
         * @param Role $role
         * @param array      $attributes
         *
         * @return bool
         */
        public function edit(Role $role, array $attributes = [])
        {
            $attributes['permissions'] = $attributes['permissions'] ?? [];

            if ($attributes['default'] ?? false == true) {
                Role::query()
                    ->where('default', true)
                    ->where('id', '!=', $role->id)
                    ->update(['default' => false]);
            }

            return $role->update($attributes);
        }

        /**
         * Returns all registered permissions in an grouped array
         *
         * @param string|null $extra_group
         *
         * @return array
         */
        public function getGroupedPermissions(?string $extra_group = null )
        {
            $groupedPermissions = [];
            $permissions = array_keys( gate()->abilities() );
            $extraGateGroups = config( 'dtv.modules.acl.gate_groups' );

            foreach ( $permissions as $permission ) {
                if ( $extra_group == null && starts_with( $permission , $extraGateGroups ) ) {
                    continue;
                } elseif ( $extra_group != null && !starts_with( $permission , $extra_group ) ) {
                    continue;
                }

                if ( str_contains( $permission , '.' ) ) {
                    [ $group , $perm ] = explode( '.' , $permission , 2 );
                    $groupedPermissions[ $group ][] = $group . '.' . $perm;
                } else {
                    $groupedPermissions[ 'general' ][] = $permission;
                }
            }

            return $groupedPermissions;
        }
    }
