<?php

    return [

        'name' => 'Rolle' ,

        'views' => [
            'index'  => 'Rollen' ,
            'create' => 'Rolle erstellen'
        ] ,

        'buttons' => [
            'create' => 'Rolle erstellen' ,
            'edit'   => 'Rolle bearbeiten' ,
            'delete' => 'Rolle löschen'
        ] ,

        'fields' => [
            'name'        => 'Name' ,
            'slug'        => 'Slug' ,
            'default'     => 'Standart' ,
            'description' => 'Beschreibung' ,
            'permissions' => 'Berechtigungen'
        ] ,

        'hints' => [
            'name_maxlength' => 'Rollennamen dürfen nicht länger als 60 Zeichen sein'
        ] ,

        'messages' => [
            'createdTitle' => 'Rolle erstellt' ,
            'createdText'  => 'Die Rolle wurde erfolgreich erstellt' ,
            'editedTitle'  => 'Rolle bearbeitet' ,
            'editedText'   => 'Die Rolle und die zugehörigen Berechtigungen wurden erfolgreich bearbeitet' ,
        ] ,
    ];