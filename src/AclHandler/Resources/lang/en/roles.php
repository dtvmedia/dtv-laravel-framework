<?php

    return [

        'name' => 'Role' ,

        'views' => [
            'index'  => 'Roles' ,
            'create' => 'Create Role'
        ] ,

        'buttons' => [
            'create' => 'Create Role' ,
            'edit'   => 'Edit Role' ,
            'delete' => 'Delete Role'
        ] ,

        'fields' => [
            'name'        => 'Name' ,
            'slug'        => 'Slug' ,
            'default'     => 'Default' ,
            'description' => 'Description' ,
            'permissions' => 'Permissions'
        ] ,

        'hints' => [
            'name_maxlength' => 'The role name must not be longer than 60 characters'
        ] ,

        'messages' => [
            'createdTitle' => 'Role created' ,
            'createdText'  => 'The role was successfully created' ,
            'editedTitle'  => 'Role updated' ,
            'editedText'   => 'Role and the related permissions were successfully updated' ,
        ] ,
    ];