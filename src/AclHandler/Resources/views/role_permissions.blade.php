<p class="mt-1 mb-2 control-label">{{ trans('dtv.acl::roles.fields.permissions') }}</p>
<div class="accordion" id="accordion">
    @foreach( $permissions as $group => $items )
        <div class="card">
            <div class="card-header p-1">
                <a data-toggle="collapse" data-parent="#accordion" class="clearfix" style="padding: 2px 5px;display: block; text-decoration: none" href="#group-{{ $group }}">
                    <span class="float-left font-weight-bold">&raquo; {{ trans( 'roles.groups.' . $group ) }}</span>
                    <span class="float-right mt-1" style="color:gray !important;font-size:12px;font-weight: normal">{{ count( $items ) }} {{ trans('dtv.acl::roles.fields.permissions') }}</span>
                </a>
            </div>
            <div id="group-{{ $group }}" class="collapse" data-parent="#accordion">
                <div class="card-body">
                    @foreach($items as $permission)
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="{{ $permission }}" name="permissions[]"
                                   value="{{ (( $group == 'general' ) ? str_replace( 'general.' , '' , $permission ) :$permission ) }}"
                                   @if( in_array( (( $group == 'general' ) ? str_replace( 'general.' , '' , $permission ) : $permission  ) , $selected)) checked @endif >
                            <label class="custom-control-label" for="{{ $permission }}">{{ trans( 'roles.permissions.' . $permission ) }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endforeach
</div>