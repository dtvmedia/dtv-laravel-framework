<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\AclHandler\Services\Views;

    use DTV\AclHandler\RoleHandler;
    use DTV\BaseHandler\Views\FormInputs\Select;
    use DTV\BaseHandler\Views\FormView;
    use Throwable;

    /**
     * Roles Form Class
     *
     * @package   DTV\AclHandler\Services\Views
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class RolesForm extends FormView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.acl::roles.views.create';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-plus';

        /**
         * Translation file used for translating input labels etc
         *
         * @var string
         */
        protected $transPath = 'dtv.acl::roles';

        /**
         * Adds the inputs to the form
         *
         * @throws Throwable
         */
        public function inputs()
        {
            $selected = [];
            $permissions = app( RoleHandler::class )->getGroupedPermissions();

            if ( $this->isUpdateMode() ) {
                $selected = $this->model->permissions ?? [];
            }

            $this->addText( 'name' )
                ->setValidationRule( 'required|string|max:60|unique:roles,name' )
                ->hint( 'dtv.acl::roles.hints.name_maxlength' );
            $this->addSelect( 'default' , Select::getBoolSelectOptions() )
                ->setDefaultValue( 0 )
                ->setValidationRule( 'required' );
            $this->addTextarea( 'description' )
                ->setValidationRule( 'nullable|string' );
            $this->addCustomView( 'dtv.acl::role_permissions' , compact( 'permissions' , 'selected' ) )
                ->setValidationRule( [ 'permissions' => 'nullable|array' ] )
                ->setSize( 12 );
        }
    }
