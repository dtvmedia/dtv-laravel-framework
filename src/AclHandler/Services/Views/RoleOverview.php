<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\AclHandler\Services\Views;

    use DTV\BaseHandler\Views\ListView;

    /**
     * Roles Overview Class
     *
     * @package   DTV\AclHandler\Services\Views
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class RoleOverview extends ListView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.acl::roles.views.index';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-users';

        /**
         * Translation file used for translating column names etc
         *
         * @var string
         */
        protected $transPath = 'dtv.acl::roles';

        /**
         * Adds the columns
         */
        protected function columns()
        {
            $this->addColumn( 'name' , 'getNameLabel' , 'name' )->enableHtml();
            $this->addColumn( 'description' );
        }

        /**
         * Adds the buttons for each record
         */
        protected function buttons()
        {
            $this->addButton( 'edit' , 'fa-pencil' , 'roles.edit' )
                ->setDynamicParameter( [ 'role' => 'id' ] );
            $this->addConfirmButton( 'delete' , 'fa-times' , 'roles.delete' )
                ->setDynamicParameter( [ 'role' => 'id' ] );
        }
    }