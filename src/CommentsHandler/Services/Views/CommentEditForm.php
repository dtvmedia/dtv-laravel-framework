<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\CommentsHandler\Services\Views;

    use DTV\BaseHandler\Views\FormView;

    /**
     * Comment Edit From Class
     *
     * @package   DTV\AclHandler\Services\Views
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CommentEditForm extends FormView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.comments::comments.views.edit';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-pencil';

        /**
         * Translation file used for translating input labels etc
         *
         * @var string
         */
        protected $transPath = 'dtv.comments::comments';

        /**
         * Adds the inputs to the form
         *
         * @throws \Throwable
         */
        public function inputs()
        {
            $this->addCustomView( config( 'dtv.modules.comments.textarea_view' ) , [
                'comment_name'  => 'comment' ,
                'comment_label' => trans( 'dtv.comments::comments.fields.comment' ) ,
                'comment_value' => ( $this->isUpdateMode() ) ? $this->model->comment : null ,
                'comment_modal' => true ,
            ] )->setValidationRule( [ 'comment' => 'required|string|min:5' ] )->setSize( 12 );
        }
    }