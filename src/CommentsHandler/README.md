# CommentHandler - DTV Laravel Framework

## Configs
- *enabled*: Package activation switch
- *routes_enabled*: Activation switch for the package routes seperated from the package switch itself
- *activities_enabled*: Activation switch for activities when adding an new comment (requires the ActivityHandler!)
- *textarea_view*: Custom textarea template name (if you wanna use some plugins for formatting etc.)
- *html*: Activation switch for enabling html comments. That means the output will not be escaped from our side. Make sure that the input is properly escaped, otherwise you open the door for XSS Attacks!


## Usage
1. Add the ``DTV\CommentsHandler\Models\Commentable`` Trait to the model which should have comments
2. Include the comment input form
````blade
 @include('dtv.comments::comments', [ 
    'comments' => $model->comments->sortByDesc('created_at') , 
    'model'    => $model ,
    'order'    => 'desc' ,
 ])
````

## View parameters
Parameter | Datatype | Description
--- | --- | --- 
comments | Collection | Collection of Comment Models  (required)
model | Model | The related model (default: null (readonly view))
order | string | 'asc' or 'desc' (default: asc)
commentModalMode | bool | Activates the redirection after edit (default: false) 
commentTitle | string | Defines the title of the view (default: 'dtv.comments::comments.views.index')
commentCreateTitle | string | Defines the title of the create section (default: 'dtv.comments::comments.views.create')
comment_label | string | Defines the label of the textarea (default: '')
comment_name | string | Defines the name of the textare (default: comment)
comment_value | string | Defines the value of the textarea (default '')