<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\CommentsHandler;

    use DTV\BaseHandler\BaseServiceProvider;
    use DTV\BaseHandler\RoutingHandler;
    use DTV\CommentsHandler\Controllers\CommentController;
    use DTV\CommentsHandler\Models\Comment;

    /**
     * Comment Handler Service Provider
     *
     * @package   DTV\CommentsHandler
     * @copyright 2017 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CommentHandlerServiceProvider extends BaseServiceProvider
    {
        /**
         *  Namespace for resources of the module
         *
         * @var string
         */
        protected $resourcesNamespace = 'dtv.comments';

        /**
         * Array of commans
         *
         * @var array
         */
        protected $commands = [];

        /**
         * Config path for the module enable switch
         *
         * @var string|null
         */
        public static $config = 'dtv.modules.comments.enabled';

        /**
         * Registers the Modules routes
         *
         * @throws \Exception
         */
        public function routes()
        {
            // Comments Routes
            $commentRoutes = new RoutingHandler( CommentController::class , 'comments' , 'comments.' , RoutingHandler::ACCESS_AUTH );
            $commentRoutes->post( '/create' , 'create' , 'create' , 'can:comments.create' );
            $commentRoutes->any( '/{comment}/edit/' , 'edit' , 'edit' , 'can:comments.edit,comment' );
            $commentRoutes->delete( '/{comment}/delete/' , 'delete' , 'delete' , 'can:comments.delete,comment' );
        }

        /**
         * Defines all the modules gates
         */
        public function gates() {
            gate()->define( 'comments.view' , function ( $user ) {
                return $user->hasPermission( [ 'comments.view' ] );
            } );
            gate()->define( 'comments.create' , function ( $user ) {
                return $user->hasPermission( [ 'comments.view' , 'comments.create' ] );
            } );
            gate()->define( 'comments.edit' , function ( $user , $comment ) {
                $comment = Comment::instance( $comment );

                return $comment->user_id === $user->id || $user->hasPermission( [ 'comments.view' , 'comments.edit' ] );
            } );
            gate()->define( 'comments.delete' , function ( $user , $comment ) {
                $comment = Comment::instance( $comment );

                return $comment->user_id === $user->id || $user->hasPermission( [ 'comments.view' , 'comments.delete' ] );
            } );
        }
    }