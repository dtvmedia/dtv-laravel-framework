<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\CommentsHandler\Controllers;

    use DTV\BaseHandler\Controllers\Controller;
    use DTV\BaseHandler\JsonResponse;
    use DTV\CommentsHandler\CommentHandler;
    use DTV\CommentsHandler\Models\Comment;
    use DTV\CommentsHandler\Services\Views\CommentEditForm;
    use DTV\Support\MorphIdHandler;
    use Illuminate\Http\Request;
    use Illuminate\Validation\ValidationException;

    /**
     * Comment Controller Class
     *
     * @package   DTV\CommentsHandler\Controllers
     * @copyright 2017 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CommentController extends Controller
    {
        /**
         * Adds an comment to an given reference object
         *
         * @param Request        $request
         * @param CommentHandler $commentHandler
         *
         * @throws ValidationException
         * @throws \Throwable
         * @return JsonResponse
         */
        public function create( Request $request , CommentHandler $commentHandler )
        {
            $this->validate( $request , [
                'comment'        => 'required|min:5' ,
                'reference_type' => 'required|string' ,
                'reference_id'   => 'required|integer' ,
            ] );

            $comment = $commentHandler->create( $request->comment , $request->reference_type , $request->reference_id );

            return $this->json()
                ->fireEvent( 'comment:created' , [
                    'html' => view( 'dtv.comments::comment' , [ 'comment' => $comment ] )->render()
                ] , MorphIdHandler::getId( $request->reference_type , $request->reference_id ) );
        }

        /**
         * Shows and handles the comment edit form
         *
         * @param Comment        $comment
         * @param Request        $request
         * @param CommentHandler $commentHandler
         *
         * @throws \Throwable
         *
         * @return JsonResponse|string
         */
        public function edit( Comment $comment , Request $request , CommentHandler $commentHandler )
        {
            $form = new CommentEditForm( $comment );

            return $this->form( $request , $form , function ( Request $request ) use ( $comment , $commentHandler ) {
                $commentHandler->edit( $comment , $request->comment );

                $response = $this->json();

                if ( $request->get( 'redirect' , '#' ) !== '#' ) {
                    $response->newRequestUrl( $request->get( 'redirect' , '#' ) );
                } else {
                    $response->hideModal()
                        ->fireEvent( 'comment:edited' , [
                            'id'   => $comment->id ,
                            'html' => view( 'dtv.comments::comment' , [ 'comment' => $comment ] )->render()
                        ] , MorphIdHandler::getId( $comment->reference_type , $comment->reference_id ) );
                }

                return $response;
            } );
        }

        /**
         * Tries to delete a comment
         *
         * @param Comment $comment
         *
         * @throws \Exception
         *
         * @return JsonResponse
         */
        public function delete( Comment $comment )
        {
            return $this->deleteModel( $comment , [] , [] , null , function ( Comment $comment , JsonResponse $response ) {
                return $response->clearRedirect()
                    ->hideModal( JsonResponse::HIDE_MODAL_CONFIRM )
                    ->fireEvent( 'comment:deleted' , [ 'id' => $comment->id ] , MorphIdHandler::getId( $comment->reference_type , $comment->reference_id ) );
            } );
        }
    }