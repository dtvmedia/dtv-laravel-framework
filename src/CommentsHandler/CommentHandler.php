<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\CommentsHandler;

    use DTV\ActivityHandler\ActivityHandler;
    use DTV\CommentsHandler\Models\Comment;

    /**
     * Comment Handler
     *
     * @package   DTV\CommentsHandler
     * @copyright 2017 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CommentHandler
    {
        /**
         * Creates an new comment
         *
         * @param string $comment
         * @param string $reference_type
         * @param int    $reference_id
         * @param bool   $createActivity
         *
         * @return $this|\Illuminate\Database\Eloquent\Model|Comment
         */
        public function create( string $comment , string $reference_type , int $reference_id , bool $createActivity = true )
        {
            if ( $createActivity && config( 'dtv.modules.comments.activities_enabled' , false ) === true ) {
                app( ActivityHandler::class )->create( 'comment' , $comment , [] , $reference_type , $reference_id );
            }

            return Comment::query()->create( [
                'user_id'        => auth()->id() ,
                'reference_type' => $reference_type ,
                'reference_id'   => $reference_id ,
                'comment'        => $comment
            ] );
        }

        /**
         * Updates the comment text of an given comment
         *
         * @param Comment $comment
         * @param string  $commentText
         *
         * @return bool
         */
        public function edit( Comment $comment , string $commentText )
        {
            return $comment->update( [
                'comment'      => $commentText ,
                'updated_from' => auth()->id() ,
            ] );
        }
    }