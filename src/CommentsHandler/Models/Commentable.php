<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\CommentsHandler\Models;

    /**
     * Commentable Trait
     *
     * @package   DTV\CommentsHandler\Models
     * @copyright 2017 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    trait Commentable
    {
        /**
         * Returns all related comments
         *
         * @return \Illuminate\Database\Eloquent\Relations\MorphMany
         */
        public function comments()
        {
            return $this->morphMany( Comment::class , 'reference' );
        }
    }