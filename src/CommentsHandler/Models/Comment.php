<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\CommentsHandler\Models;

    use DTV\BaseHandler\Models\Model;

    /**
     * Comment Model
     *
     * @package   DTV\CommentsHandler\Models
     * @copyright 2017 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Comment extends Model
    {
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'user_id' , 'reference_type' , 'reference_id' , 'comment' , 'updated_from'
        ];

        /**
         * The name of the related translation file
         * If null the getTranslationFile method will try to build it from the model name
         *
         * @var string
         */
        protected static $translationFile = 'dtv.comments::comments';

        /**
         * Returns the related user model
         *
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function user()
        {
            return $this->belongsTo( config( 'dtv.models.user' ) );
        }

        /**
         * Returns the related updated from user model
         *
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function updatedFrom()
        {
            return $this->belongsTo( config( 'dtv.models.user' ) , 'updated_from' );
        }
    }
