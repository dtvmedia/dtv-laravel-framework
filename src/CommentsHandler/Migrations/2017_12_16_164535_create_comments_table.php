<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Creates Comments Table Migration
     *
     * @package   -
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CreateCommentsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create( 'comments' , function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->integer( 'user_id' )->unsigned()->nullable();
                $table->string( 'reference_type' );
                $table->integer( 'reference_id' )->unsigned();
                $table->text( 'comment' );
                $table->timestamps();
                $table->softDeletes();
            } );

            Schema::table( 'comments' , function ( $table ) {
                $table->foreign( 'user_id' )->references( 'id' )->on( config( 'dtv.tables.user' ) );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists( 'comments' );
        }
    }
