<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Update Comments Table Migration
     *
     * @package   -
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class UpdateComments1Table extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table( 'comments' , function ( Blueprint $table ) {
                $table->integer( 'updated_from' )->unsigned()->nullable()->after( 'updated_at' );
                $table->foreign( 'updated_from' )->references( 'id' )->on( config( 'dtv.tables.user' ) );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table( 'comments' , function ( Blueprint $table ) {
                $table->dropColumn( 'updated_from' );
            } );
        }
    }
