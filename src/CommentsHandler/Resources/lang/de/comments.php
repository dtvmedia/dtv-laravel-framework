<?php

    return [

        'name'            => 'Kommentar' ,
        'no_comments'     => 'Bisher keine Kommentare...' ,
        'updated_this_at' => 'bearbeitet den Kommentar am' ,
        'wrote'           => 'schrieb' ,

        'views' => [
            'index'  => 'Kommentare' ,
            'create' => 'Kommentar hinzufügen' ,
            'edit'   => 'Kommentar bearbeiten'
        ] ,

        'buttons' => [
            'comment' => 'Kommentieren' ,
            'edit'    => 'Bearbeiten' ,
            'delete'  => 'Löschen' ,
        ] ,

        'fields' => [
            'comment' => 'Kommentar' ,
        ] ,

        'messages' => [
            'noEditPermissionsTitle' => 'Keine Berechtigung' ,
            'noEditPermissionsText'  => 'Sie dürfen nicht Kommentare anderer Nutzer bearbeiten' ,
        ]

    ];