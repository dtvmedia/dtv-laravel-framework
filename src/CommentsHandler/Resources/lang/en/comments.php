<?php

    return [

        'name'            => 'Comment' ,
        'no_comments'     => 'No comments here yet...' ,
        'updated_this_at' => 'updated this comment at' ,
        'wrote'           => 'wrote' ,

        'views' => [
            'index'  => 'Comments' ,
            'create' => 'Add comment' ,
            'edit'   => 'Edit comment'
        ] ,

        'buttons' => [
            'comment' => 'Comment' ,
            'edit'    => 'Edit' ,
            'delete'  => 'Delete' ,
        ] ,

        'fields' => [
            'comment' => 'Comment' ,
        ] ,

        'messages' => [
            'noEditPermissionsTitle' => 'No Permissions' ,
            'noEditPermissionsText'  => 'You have no permissions to edit comments of other users' ,
        ]

    ];