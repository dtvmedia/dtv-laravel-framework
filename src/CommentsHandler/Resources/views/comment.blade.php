<div class="comment" id="comment-{{ $comment->id }}">
    <div class="media">
        @if( $comment->user === null )
            <i class="fal fa-user-circle d-flex mr-3 mt-1" style="font-size: 40px;"></i>
        @else
            {!! $comment->user->getProfilePicture( 40 , 'fa-2x d-block mr-3 mt-1 rounded-circle' ) !!}
        @endif
        <div class="media-body">
            <span class="mt-0">
                <strong>
                    @if( $comment->user === null )
                        System
                    @else
                        {!! $comment->user->getDisplayNameLink( false ) !!}
                    @endif
                </strong>
                <small>
                    <span class="text-muted mr-2">
                        @lang('dtv.comments::comments.wrote')
                        @if( config('dtv.modules.comments.date_format') === null )
                            {!! $comment->created_at->diffForHumansHtml() !!}
                        @else
                            {{ $comment->created_at->formatLocalized( config('dtv.modules.comments.date_format') ) }}
                        @endif
                    </span>
                    @can( 'comments.edit' , $comment )
                        <button type="button" class="btn btn-link btn-inline btn-sm" data-ajax-action="{{ route( 'comments.edit', [ 'comment' => $comment->id , 'redirect' => (isset($commentModalMode)) ? url()->full() : '#' ] ) }}">
                            {!! icon( 'fa-pencil' ) !!} {{ trans('dtv.comments::comments.buttons.edit') }}
                        </button>
                    @endcan
                    @can( 'comments.delete' , $comment )
                        <button type="button" class="btn btn-link btn-inline btn-sm ml-2" data-confirm-action="{{ route( 'comments.delete', [ 'comment' => $comment->id ] ) }}">
                            {!! icon( 'fa-trash-alt' ) !!} {{ trans('dtv.comments::comments.buttons.delete') }}
                        </button>
                    @endcan
                </small>
            </span>
            <div class="mb-3 comment-content">
                <div style="color: black">
                    @if( config( 'dtv.modules.comments.html' ) === true )
                        {!! $comment->comment !!}
                    @else
                        {!! nl2br( e( $comment->comment ) ) !!}
                    @endif
                </div>

                @if( $comment->wasAlreadyUpdated() )
                    <div>
                        <p class="mt-2 text-muted" style="font-size:11px">
                            {!! icon('fa-edit') !!}
                            @if( $comment->updatedFrom === null )
                                System
                            @else
                                {!! $comment->updatedFrom->getDisplayNameLink( false ) !!}
                            @endif
                            @lang('dtv.comments::comments.updated_this_at')
                            @if( config('dtv.modules.comments.date_format') === null )
                                {{ $comment->updated_at->toDateTimeWithDayString() }}
                            @else
                                {{ $comment->updated_at->formatLocalized( config('dtv.modules.comments.date_format') ) }}
                            @endif
                        </p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>