@if( can( 'comments.create' ) && isset( $model ) )
    @php
        $commentKey = \DTV\Support\MorphIdHandler::getIdByModel( $model )
    @endphp
    <h3 class="mt-4">{{ $commentCreateTitle ?? trans( 'dtv.comments::comments.views.create' ) }}</h3>
    <form action="{{ route('comments.create') }}" id="comment_form_{{ $commentKey }}" method="POST">
        {{ csrf_field() }}

        <input type="hidden" name="reference_type" value="{{ get_class( $model ) }}"/>
        <input type="hidden" name="reference_id" value="{{ $model->id }}"/>

        @include( config( 'dtv.modules.comments.textarea_view' ) )
        <button type="submit" class="btn btn-{{ config( 'dtv.app.button_type' ) }}">
            {{ trans('dtv.comments::comments.buttons.comment') }}
        </button>
    </form>

    @push('scripts')
        <script>
            app().onLoaded( function () {
                new DTV.FormView( document.querySelector( '#comment_form_{{ $commentKey }}' ) );
            } );
        </script>
        <script>
            app().onLoaded( function () {
                const dispatcher = new DTV.EventDispatcher();

                dispatcher.off( 'comment:created' , '{{ $commentKey }}' );
                dispatcher.on( 'comment:created', function ( data ) {
                    const wrapper = document.querySelector( '#comment-wrapper-{{ $commentKey  }}' );
                    const commentsWrapper = wrapper.querySelector( '.comments' );
                    const noCommentsLine = wrapper.querySelector( '.no-comments' );

                    if ( commentsWrapper !== null && data.hasOwnProperty( 'html' ) ) {
                        const html = template( data.html );

                        if ( commentsWrapper.classList.contains( 'comment-order-desc' ) ) {
                            commentsWrapper.prepend( html );
                        } else {
                            commentsWrapper.append( html );
                        }

                        // handle "no comments" label
                        if ( noCommentsLine !== null ) {
                            noCommentsLine.classList.add( 'd-none' );
                        }

                        // clear input
                        const input = wrapper.querySelector( '#{{ $comment_name ?? 'comment'}}' );
                        if ( input !== null ) {
                            input.value = '';
                        }
                    }
                } , '{{ $commentKey }}' );

                dispatcher.off( 'comment:edited' , '{{ $commentKey }}' );
                dispatcher.on( 'comment:edited', function ( data ) {
                    if ( !data.hasOwnProperty( 'id' ) || !data.hasOwnProperty( 'html' ) ) {
                        return;
                    }

                    const comment = document.querySelector( '#comment-' + data.id );

                    if ( comment !== null ) {
                        comment.innerHTML = template( data.html ).innerHTML;
                    }
                } , '{{ $commentKey }}' );

                dispatcher.off( 'comment:deleted' , '{{ $commentKey }}' );
                dispatcher.on( 'comment:deleted', function ( data ) {
                    if ( !data.hasOwnProperty( 'id' ) ) {
                        return;
                    }

                    const wrapper = document.querySelector( '#comment-wrapper-{{ $commentKey  }}' );
                    const comment = wrapper.querySelector( '#comment-' + data.id );
                    const noCommentsLine = wrapper.querySelector( '.no-comments' );


                    if ( comment !== null ) {
                        comment.remove();

                        // handle "no comments" label
                        const commentCounter = wrapper.querySelectorAll('.comment').length;

                        if ( noCommentsLine !== null && commentCounter === 0 ) {
                            noCommentsLine.classList.remove( 'd-none' );
                        }
                    }
                } , '{{ $commentKey }}' );
            } );
        </script>
    @endpush
@endif