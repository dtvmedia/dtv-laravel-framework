{{-- No line break inside of textare or some spaces will be inside the form element by default --}}
<div class="form-group ">
    <label class="control-label {{ ( isset( $comment_label ) ? '' : 'd-none' ) }}" for="{{ $comment_name ?? 'comment'}}">{{ $comment_label ?? ''}}</label>
    <div class="has-feedback">
        <textarea style="height: 66px" id="{{ $comment_name ?? 'comment'}}" class="form-control dynamic-height" name="{{ $comment_name ?? 'comment'}}" data-validation="required|string|min:5|max:4000" required placeholder="">{{ $comment_value ?? ''}}</textarea>
        <span class="feedback"></span>
    </div>
</div>
