@can('comments.view')
    @if( isset( $comments ) )
        <div @isset( $model ) id="comment-wrapper-{{ \DTV\Support\MorphIdHandler::getIdByModel( $model ) }}" @endisset>
            <h3 class="mt-4">
                {{ $commentTitle ?? trans( 'dtv.comments::comments.views.index' ) }}
            </h3>
            <div class="comments comment-order-{{ $order ?? 'asc' }}">
                <p class="no-comments @if( count( $comments ) > 0 ) d-none @endif">@lang('dtv.comments::comments.no_comments')</p>

                @foreach( $comments as $comment )
                    @include('dtv.comments::comment')
                @endforeach
            </div>

            @if( isset( $model ) )
                @include( 'dtv.comments::comment_form', [ 'model' => $model ] )
            @endif
        </div>
    @endif
@endcan