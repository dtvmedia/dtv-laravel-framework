<?php

    return [

        'map' => [
            'mapNotAvailable'    => 'Karte nicht verfügbar' ,
            'invalidCoordinates' => 'Ungültige Koordinaten' ,
        ]

    ];