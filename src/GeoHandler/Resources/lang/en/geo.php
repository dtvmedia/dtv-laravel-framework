<?php

    return [

        'map' => [
            'mapNotAvailable'    => 'Map not available' ,
            'invalidCoordinates' => 'Invalid coordinates' ,
        ]

    ];