@section('view-title')
    {!! $map->icon !!} {{ $map->title }}
@endsection

@if( $map->valid )
    <div class="map google-maps-map" id="map_{{ $map->hash }}" style="width: {{ $map->width }}; height: {{ $map->height }}"></div>
@else
    <div class="map map-invalid" style="background:#e1e1e1; color:#aeaeae; display: flex; flex-direction: column; text-align:center; font-size:20px; justify-content: center;width: {{ $map->width }}; height: {{ $map->height }}">
        <i class="fas fa-exclamation-triangle fa-2x mb-3"></i><br>
        @lang('dtv.geo::geo.map.mapNotAvailable').<br>
        @lang('dtv.geo::geo.map.invalidCoordinates')!
    </div>
@endif

@push('scripts')
    @if( $map->valid )
        <script>
            var map;
            function initMap{{ $map->hash }}() {
                map = new google.maps.Map(document.getElementById('map_{{ $map->hash }}'), {
                    center: {lat: {{ $map->center->lat }}, lng: {{ $map->center->lon }}},
                    zoom: {{ $map->zoom }},
                    disableDefaultUI: true
                });

                var marker = new google.maps.Marker({
                    position: {lat: {{ $map->center->lat }}, lng: {{ $map->center->lon }}},
                    map: map
                });

            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key={{ $map->apikey }}&callback=initMap{{ $map->hash }}" async defer></script>
    @endif
@endpush