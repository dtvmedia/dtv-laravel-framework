@section('view-title')
    {!! $map->icon !!} {{ $map->title }}
@endsection

@if( $map->valid )
    <div class="position-relative" id="map_wrapper_{{ $map->hash }}">
        <div class="map here-api-map" id="map_{{ $map->hash }}" style="width: {{ $map->width }}; height: {{ $map->height }}"></div>
        <div class="map-overlay" id="map_overlay_{{ $map->hash }}" style="position: absolute;height: 100%;top: 0px;width: 100%;background: rgba(255,255,255,0.2);"></div>
    </div>
@else
    <div class="map map-invalid" style="background:#e1e1e1; color:#aeaeae; display: flex; flex-direction: column; text-align:center; font-size:20px; justify-content: center;width: {{ $map->width }}; height: {{ $map->height }}">
        <i class="fas fa-exclamation-triangle fa-2x mb-3"></i><br>
        @lang('dtv.geo::geo.map.mapNotAvailable').<br>
        @lang('dtv.geo::geo.map.invalidCoordinates')!
    </div>
@endif

@push('scripts')
    @if( $map->valid )
        <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-core.js"></script>
        <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-service.js"></script>
        <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-ui.js"></script>
        <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js"></script>
        <script>
            var platform = new H.service.Platform({
                apikey: '{{ $map->api_key }}'
            });

            // Instantiate (and display) a map object:
            var center = {
                lat: {{ $map->center->lat }},
                lng: {{ $map->center->lon }}
            };
            var map = new H.Map(
                document.getElementById('map_{{ $map->hash }}'),
                platform.createDefaultLayers().vector.normal.map,
                {
                    zoom: {{ $map->zoom }},
                    center: center,
                }
            );

            // Enable the event system on the map instance:
            var mapEvents = new H.mapevents.MapEvents( map );

            // Instantiate the default behavior, providing the mapEvents object:
            var behavior = new H.mapevents.Behavior( mapEvents );

            var marker = new H.map.Marker( center );
            map.addObject( marker );

            const Overlay = document.querySelector('#map_overlay_{{ $map->hash }}');
            const Wrapper = document.querySelector('#map_wrapper_{{ $map->hash }}');

            Overlay.addEventListener('click', function () {
                this.classList.add('d-none');
            });

            document.addEventListener('click', function () {
                Overlay.classList.remove('d-none');
            });

            Wrapper.addEventListener('click', function ( event ) {
                event.stopPropagation();
            });
        </script>

    @endif
@endpush
