<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\GeoHandler\Models;

    use Illuminate\Support\Facades\Lang;

    /**
     * Country Pseudo Model Class
     *
     * @package   DTV\GeoHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Country
    {
        /**
         * Returns an array of all countries
         *
         * @return array
         */
        public static function all(): array
        {
            return trans('dtv.geo::countries');
        }

        /**
         * Returns the country name for a given 2 digit country code
         *
         * @param string $countryCode
         *
         * @return string|null
         */
        public static function get(string $countryCode): ?string
        {
            if (Lang::has('dtv.geo::countries.' . $countryCode)) {
                return trans('dtv.geo::countries.' . $countryCode);
            }

            return null;
        }

        /**
         * Returns an array of select options with all countries
         *
         * @return array
         */
        public static function getSelectOptions(): array
        {
            $options = [];

            foreach (static::all() as $countryCode => $country) {
                $options[$countryCode] = option($country, 'fa-globe-africa');
            }

            return $options;
        }
    }
