<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\GeoHandler\Models;

    use Geocoder\Laravel\Facades\Geocoder;

    /**
     * Geocodable Model Extension Trait
     *
     * Requirements:
     *  - The toin0u/geocoder-laravel package needs to be installed
     *  - The model needs to use the Addressable trait
     *
     * Managed Attributes:
     *  - lat
     *  - lon
     *
     * @package   DTV\GeoHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    trait Geocodable
    {
        /**
         * Returns whethe or not the object is geocoded
         *
         * @return bool
         */
        public function isGeocoded(): bool
        {
            return (!empty($this->lat)
                    && !empty($this->lon)
                    && is_float($this->lat)
                    && is_float($this->lon));
        }

        /**
         * Returns the geo coordinates of the object or null if the object isn't geocoded yet
         *
         * @param bool $asArray
         *
         * @return array|object|null
         */
        public function getCoordinates(bool $asArray = false)
        {
            if (!$this->isGeocoded()) {
                return null;
            }

            $coords = [
                'lat' => $this->lat,
                'lon' => $this->lon,
            ];

            if ($asArray) {
                return $coords;
            }

            return (object)$coords;
        }

        /**
         * Geocodes the object
         *
         * @return bool
         */
        public function geocode(): bool
        {
            $query = $this->getFullAddress();
            $result = Geocoder::geocode($query)->get()->first();

            if ($result !== null) {
                $coords = $result->getCoordinates();

                if ($coords !== null) {
                    $this->update([
                        'lat' => $coords->getLatitude(),
                        'lon' => $coords->getLongitude(),
                    ]);

                    return true;
                }
            }

            return false;
        }
    }
