<?php

/**
 * DTV Media Solutions
 *
 * PHP Version 7.0
 */

namespace DTV\GeoHandler\Views;

use DTV\BaseHandler\Views\View;

/**
 * HERE API Map View Class
 *
 * @package   DTV\GeoHandler\Views
 * @copyright 2019 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
class HereApiMap extends View
{
    /**
     * View Label
     *
     * @var string
     */
    protected $label = 'dtv.geo::geo.views.here_api_map';

    /**
     * Switch for the usage of translations in views
     *
     * @var bool
     */
    protected $transEnabled = true;

    /**
     * Translation String Prefix
     *
     * @var string
     */
    protected $transPath = '';

    /**
     * Valid center coords flag
     *
     * @var bool
     */
    protected $valid = false;

    /**
     * Map center coordinates array
     *
     * @var array
     */
    protected $center;

    /**
     * Zoom level
     *
     * @var int
     */
    protected $zoom;

    /**
     * CSS height string
     *
     * @var string
     */
    protected $height;

    /**
     * CSS width string
     *
     * @var string
     */
    protected $width;

    /**
     * GoogleMapsMap constructor
     *
     * @param float  $centerLat
     * @param float  $centerLon
     * @param int    $zoom
     * @param string $height
     * @param string $width
     */
    public function __construct( $centerLat , $centerLon , int $zoom = 12 , string $height = '100%' , string $width = '100%' )
    {
        $this->center = [
            'lat' => $centerLat ,
            'lon' => $centerLon ,
        ];
        $this->zoom = $zoom;
        $this->height = $height;
        $this->width = $width;

        if ( !empty( $centerLat ) && is_float( $centerLat ) && !empty( $centerLon ) && is_float( $centerLon ) ) {
            $this->valid = true;
        }
    }

    /**
     * Renders the map
     *
     * @throws \Throwable
     *
     * @return string
     */
    public function render()
    {
        $viewData = (object) [
            'label'   => $this->getLabel() ,
            'title'   => $this->getTitle() ,
            'icon'    => $this->getIcon() ,
            'hash'    => $this->getHash() ,
            'valid'   => $this->valid ,
            'center'  => (object) $this->center ,
            'zoom'    => $this->zoom ,
            'width'   => $this->width ,
            'height'  => $this->height ,
            'api_key' => config( 'dtv.modules.geo.here_api_key' ) ,
        ];

        return view( 'dtv.geo::here_api_maps' , [ 'map' => $viewData ] )->render();
    }
}
