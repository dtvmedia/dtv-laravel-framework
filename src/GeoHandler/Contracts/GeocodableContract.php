<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\GeoHandler\Contracts;

    /**
     * Geocodable Model Extension Interface
     *
     * @package   DTV\GeoHandler\Contracts
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    interface GeocodableContract
    {
        /**
         * Returns whethe or not the object is geocoded
         *
         * @return bool
         */
        public function isGeocoded(): bool;

        /**
         * Returns the geo coordinates of the object or null if the object isn't geocoded yet
         *
         * @param bool $asArray
         *
         * @return array|object|null
         */
        public function getCoordinates(bool $asArray = false);

        /**
         * Geocodes the object
         */
        public function geocode();
    }
