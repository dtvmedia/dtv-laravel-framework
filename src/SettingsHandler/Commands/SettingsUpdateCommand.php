<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SettingsHandler\Commands;

    use DTV\SettingsHandler\Models\Setting;
    use Illuminate\Console\Command;

    /**
     * Settings Update Command
     *
     * @package   DTV\SettingsHandler\Commands
     * @copyright 2019 DTV Media Solutions (http://www.dtvmedia.de)
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class SettingsUpdateCommand extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'settings:update {--delete}';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Updates the settings options';

        /**
         * Execute the console command.
         */
        public function handle()
        {
            $dbSettings = Setting::all()->keyBy( 'key' );

            foreach ( settings()->getDefinitions() as $key => $definition ) {
                if ( !$dbSettings->has( $key ) ) {
                    settings()->set( $key , $definition->getDefault() );
                }

                $dbSettings->forget( $key );
            }

            foreach ( $dbSettings as $key => $setting ) {
                if ( $this->option( 'delete' ) === true ) {
                    logger()->info( 'Deleted not defined settings option "' . $key . '" in the database' );

                    $setting->forceDelete();
                } else {
                    logger()->info( 'Found not defined settings option "' . $key . '" in the database' );
                }
            }

            $this->info( 'Settings successfully updated!' );
        }
    }