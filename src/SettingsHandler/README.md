# SettingsHandler - DTV Laravel Framework

## Configs
- *enabled*: Package activation switch
- *routes_enabled*: Activation switch for the package routes seperated from the package switch itself

## Usage
It is recommened to use the settings() helper function for all interactions with the settings handler, because the settings handler is configured to run as a singleton. Thats important because internally all settings are cached after the first settings request.

The system primarly is designed for predefined settings, which means that you need to specify in your Service Providers _boot()_ method all settings options and then you can call the built-in command:``php artisan settings:update``. This command checks all setting definitions and adds them to the database (with the defined default or null) if they do not exist.

Furthermore the settings system is so designed that settings options always need to have a group. The full key of an settings group should have following format: _groupKey.settingsName_.

It is still possible to add not defined settings values to the database, but we do not recommend to do so.

### Define an settings group
Before defining settings option, you need to define an setting group. For example if you have an settings option _app.name_ you need to define an _app_ settings group.
````php
/**
 * Settings Define Group Parameters
 *
 * @param $groupKey Settings group key
 * @param $transKey Translation key prefix
 * @param $icon     Icon instance
 * @param $hidden   Flag which defines if the setting group should be hidden
 * @param $priority Priority number (lower is shown first)
 */
settings()->defineGroup( 'app' ,  'settings' , icon( 'fa-cogs' ) , false , 50 );
````

### Define an settings option
NOTE: it is not possible to define an settings option with a group key that was not defined before. In this case an exception is thrown!

````php
/**
 * Settings Define Parameters
 *
 * @param $key     Settings key
 * @param $default Default value in case the setting with the given key doesn't exists in the database
 */
settings()->define( 'app.color' , '#000000' )
    ->setType( SettingsOption::TYPE_COLOR ) // defines the datatype of the setting (default: "string")
    ->setHidden( false )                    // defines if the settings option should be hidden (default: false)
    ->setValidation( 'required' );          // custom validations (default: "required")
````

There are some special datatypes like "array" or "model" which require also the setOptions() method. The type of data you need to pass to this method is defined below in the data types table.

### Get an settings entry 
````php
/**
 * Settings Get Parameters
 *
 * @param $key     Settings key
 * @param $default Default value in case the setting with the given key doesn't exists
 */
settings( 'key' , 'default' );
// OR
settings()->get( 'key' , 'default' );
````

### Set an settings entry
````php
/**
 * Settings Set Parameters
 *
 * @param $key        Settings key
 * @param $value      Settings value
 */
settings()->set( 'key' , 'value' );
````

### Remove an entry
````php
/**
 * Settings Remove Parameters
 *
 * @param $key Settings key
 */
settings()->remove( 'key' );
````

### Available setting datatypes
Datatype | Realization | Options
--- | --- | --- 
*string* | Simple text input with no special requirement | 
*text* | Renders as textarea | 
*int* | Text input where the input needs to be an valid integer | 
*bool* | Select input with yes and no options | 
*float* | Text input where the input needs to be an valid float or integer |  
*percent* | Same as int with the inteval requirement 0 >= input >= 100 | 
*date* | Date input (browser native) | 
*time* | Time input (browser native) | 
*color* | Color input (browser native) | 
*password* | Password input, the submitted password will be stored encrypted in the database |  
*file* | File upload input (You need to link the storage public directory to be public available. The files will be uploaded to the settings subdirectory | You can specify an file type accept string
*array* | Select with some specific defined select options | You need to specify the select options array here 
*model* | Select with the getSelectOptions from the given model | You need to specify the FQCN here 
*builder* | Select with the getSelectOptions from the model of the given builder. Furthermore the builder will be used as base query for the getSelectOptions function | You need to specify an eloquent builder instance here 