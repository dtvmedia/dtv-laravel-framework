<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SettingsHandler;

    use Closure;
    use DTV\BaseHandler\Models\Model;
    use Exception;
    use Illuminate\Database\Eloquent\Builder;

    /**
     * Settings Option Class
     *
     * @package   DTV\SettingsHandler
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class SettingsOption
    {
        /**
         * Datatype constants
         */
        const TYPE_STRING = 'string';
        const TYPE_TEXT = 'text';
        const TYPE_INT = 'int';
        const TYPE_FLOAT = 'float';
        const TYPE_BOOL = 'bool';
        const TYPE_PERCENT = 'percent';
        const TYPE_DATE = 'date';
        const TYPE_TIME = 'time';
        const TYPE_COLOR = 'color';
        const TYPE_ARRAY = 'array';
        const TYPE_BUILDER = 'builder';
        const TYPE_MODEL = 'model';
        const TYPE_FILE = 'file';
        const TYPE_PASSWORD = 'password';

        /**
         * Settings key
         *
         * @var string
         */
        protected $key;

        /**
         * Settings default value
         *
         * @var mixed
         */
        protected $default = null;

        /**
         * Flag which defines if the setting option should be visible in the GUI
         *
         * @var bool
         */
        protected $hidden = false;

        /**
         * Settings datatype
         *
         * @var string
         */
        protected $type = 'string';

        /**
         * Settings validation rules array
         *
         * @var array
         */
        protected $validation = [ 'required' ];

        /**
         * Additional setting options
         *
         * @var mixed
         */
        protected $options = null;

        /**
         * SettingsOption constructor
         *
         * @param string $key
         * @param mixed  $default
         */
        public function __construct( string $key , $default = null )
        {
            $this->key = $key;
            $this->default = $default;
        }

        /**
         * Returns the settings key
         *
         * @return string
         */
        public function getKey()
        {
            return $this->key;
        }

        /**
         * Returns the group key
         *
         * @return string
         */
        public function getGroupKey()
        {
            [ $group ] = explode('.' , $this->getKey() , 2 );

            return $group;
        }

        public function getTransKey()
        {
            $group = settings()->getGroup( $this->getGroupKey() );

            return $group['transKey'] . '.' . $this->getKey();
        }

        public function getName()
        {
            return trans( $this->getTransKey() );
        }

        /**
         * Returns the settings default value
         *
         * @return mixed|null
         */
        public function getDefault()
        {
            return $this->default;
        }

        /**
         * Sets the hidden flag
         *
         * @param bool $hidden
         *
         * @return $this
         */
        public function setHidden( bool $hidden = true )
        {
            $this->hidden = $hidden;

            return $this;
        }

        /**
         * Returns whether or not the settings option is hidden
         *
         * @return bool
         */
        public function isHidden()
        {
            return $this->hidden;
        }

        /**
         * Sets the settings data type
         *
         * @param string $type
         *
         * @return $this
         */
        public function setType( string $type )
        {
            $this->type = $type;

            return $this;
        }

        /**
         * Returns the settings data type
         *
         * @return string
         */
        public function getType()
        {
            return $this->type;
        }

        /**
         * Sets the settings validation rule
         *
         * @param string|array $validation
         *
         * @return $this
         */
        public function setValidation( $validation )
        {
            if ( !is_array( $validation ) ) {
                $validation = explode( '|' , $validation );
            }

            $this->validation = $validation;

            return $this;
        }

        /**
         * Returns the settings validation rules array
         *
         * @return array
         */
        public function getValidation()
        {
            return $this->validation;
        }

        /**
         * Sets additional settings options
         *
         * @param $options
         *
         * @throws Exception
         * @return $this
         */
        public function setOptions( $options )
        {
            if ( !is_array( $options ) &&
                 !$options instanceof Builder &&
                 !is_a( $options , Model::class , true ) &&
                 !is_string( $options ) &&
                 !$options instanceof Closure
            ) {
                throw new Exception( 'Invalid data given for paramter $options!' );
            }

            $this->options = $options;

            return $this;
        }

        /**
         * Returns the additional settings options
         *
         * @return mixed
         */
        public function getOptions()
        {
            return $this->options;
        }
    }
