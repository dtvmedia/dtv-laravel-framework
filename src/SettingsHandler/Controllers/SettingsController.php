<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SettingsHandler\Controllers;

    use DTV\BaseHandler\Controllers\Controller;
    use DTV\SettingsHandler\Models\Setting;
    use DTV\SettingsHandler\Models\SettingsGroup;
    use DTV\SettingsHandler\Services\Views\Form;
    use DTV\SettingsHandler\SettingsOption;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Crypt;
    use Illuminate\Support\Facades\Storage;

    /**
     * Settings Controller Class
     *
     * @package   DTV\SettingsHandler\Controllers
     * @copyright 2018 DTV Media Solutions (http://www.dtvmedia.de)
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class SettingsController extends Controller
    {
        /**
         * Shows the Settings overview
         *
         * @throws \Throwable
         *
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
         */
        public function index()
        {
            $group = array_key_first( settings()->getGroups() );

            return redirect()->route( 'settings.edit' , [ 'group' => $group ] );
        }

        /**
         * Shows and handles the edit setting form
         *
         * @param string  $group   Setting group key
         * @param Request $request Request instance
         *
         * @throws \Exception
         *
         * @return \Illuminate\Http\JsonResponse|string
         */
        public function edit( $group , Request $request )
        {
            $groupData = settings()->getGroup( $group );
            $groupDefinitions = settings()->getDefinitionsByGroup( $group );
            $groupSettings = collect( settings()->getByGroup( $group ) )->mapWithKeys( function ( $value , $key ) {
                return [ escapeHtmlLabel( $key ) => $value ];
            } )->toArray();

            if ( $groupData[ 'hidden' ] === true ) {
                $groupDefinitions = [];
            }

            $form = ( new Form( new SettingsGroup( $groupSettings ) ) )->setDefinitions( $groupDefinitions );

            return $this->form( $request , $form , function ( Request $request ) use ( $group ) {
                $return = $this->json();

                $key = str_replace_first( $group . '_' , $group . '.' , $request->_changed );
                $definition = settings()->getDefinition( $key );
                $value = $request->{$request->_changed};

                switch ( $definition->getType() ) {
                    case SettingsOption::TYPE_FILE:
                        $file = $request->file( $request->_changed );
                        $filename = 'settings/' . $request->_changed . '/' . $file->getClientOriginalName();
                        Storage::disk( 'public' )->put( $filename , $file->get() );
                        $value = $filename;

                        $return->reload();
                        break;
                    case SettingsOption::TYPE_PASSWORD:
                        if ( empty( trim( $value ) ) ) {
                            $value = null;
                        } else {
                            $value = Crypt::encryptString( trim( $value ) );
                        }

                        break;
                    default:
                        break;
                }

                settings()->set( $key , $value );

                return $return->success( 'dtv.settings::settings.messages.edited' );
            } );
        }
    }
