<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SettingsHandler\Services\Views;

    use DTV\BaseHandler\Views\ListView;
    use DTV\SettingsHandler\Models\Setting;

    /**
     * Settings Overview Class
     *
     * @package   DTV\SettingsHandler\Services\Views
     * @copyright 2018 DTV Media Solutions (http://www.dtvmedia.de)
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Overview extends ListView
    {
        /**
         * View Label
         *
         * @var string
         */
        protected $label = 'dtv.settings::settings.views.index';

        /**
         * Translation String Prefix
         *
         * @var string
         */
        protected $transPath = 'dtv.settings::settings';

        /**
         * Adds the columns
         *
         * @return void
         */
        protected function columns()
        {
            $this->addColumn( 'key' , 'getKey' , 'key' );
            $this->addColumn( 'value' , 'getValue' , 'value' );
        }

        /**
         * Adds the row buttons
         *
         * @return void
         */
        protected function buttons()
        {
            $this->addButton( 'edit' , 'fa-pencil' , 'settings.edit' );
        }
    }