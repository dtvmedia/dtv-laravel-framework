<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SettingsHandler\Services\Views;

    use DTV\BaseHandler\Views\CustomFormView;
    use DTV\BaseHandler\Views\FormInputs\Textarea;
    use DTV\SettingsHandler\SettingsOption;
    use Exception;

    /**
     * Settings Form Class
     *
     * @package   DTV\SettingsHandler\Services\Views
     * @copyright 2018 DTV Media Solutions (http://www.dtvmedia.de)
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Form extends CustomFormView
    {
        /**
         * View Label
         *
         * @var string
         */
        protected $label = 'dtv.settings::settings.views.index';

        /**
         * View Icon Class
         *
         * @var string
         */
        protected $icon = 'fa-cog';

        /**
         * Translation String Prefix
         *
         * @var string
         */
        protected $transPath = 'dtv.settings::settings';

        protected $transEnabled = false;

        protected $view = 'dtv.settings::form';

        protected $modelViewKey = 'settings';

        /**
         * Group settings definitions
         *
         * @var array|SettingsOption[]
         */
        protected $definitions = [];

        /**
         *Adds all Elements to the form
         *
         * @throws Exception
         * @return void
         */
        public function inputs()
        {
            foreach ( $this->definitions as $definition ) {
                $input = $this->addDynamic(
                    $definition->getType() ,
                    escapeHtmlLabel( $definition->getKey() ) ,
                    trans($this->transPath . '.' . $definition->getKey()) ,
                    $definition->getValidation() ,
                    value($definition->getOptions())
                )->setSize( 12 );

                if ( $input instanceof Textarea ) {
                    $input->setHeight( 200 );
                }
            }
        }

        /**
         * Sets the group settings definitions
         *
         * @param array $definitions
         *
         * @return $this
         */
        public function setDefinitions( array $definitions )
        {
            $this->definitions = $definitions;

            $this->setParameter( 'definitions' , $definitions );

            if ( !empty( $definitions ) ) {
                $this->transPath = settings()->getGroup( array_first( $definitions )->getGroupKey() )['transKey'];
            }

            return $this;
        }
    }
