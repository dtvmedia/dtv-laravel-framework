<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Updates the Settings Table
     *
     * @package   -
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class UpdateSettingsTable3 extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table( 'settings' , function ( Blueprint $table ) {
                $table->dropColumn( 'hidden' );
                $table->dropColumn( 'type' );
                $table->dropColumn( 'validation' );
            } );

            DB::statement('ALTER TABLE `settings` CHANGE `value` `value` VARCHAR(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL;');
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table( 'settings' , function ( Blueprint $table ) {
                $table->boolean( 'hidden' )->default( false );
                $table->enum( 'type' , [ 'string' , 'int' , 'bool' , 'float' , 'text' , 'percent', 'date' ,'time' ,'color' ] )->after( 'hidden' );
                $table->string( 'validation' , 255 )->default( 'required' )->after( 'type' );
            } );

            DB::statement('ALTER TABLE `settings` CHANGE `value` `value` VARCHAR(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;');

        }
    }
