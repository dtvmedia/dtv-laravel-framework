<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Updates the Settings Table
     *
     * @package   -
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class UpdateSettingsTable1 extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table( 'settings' , function ( Blueprint $table ) {
                $table->enum( 'type' , [ 'string' , 'int' , 'bool' , 'float' , 'text' , 'percent' ] )->after( 'hidden' );
                $table->string( 'validation' , 255 )->default( 'required' )->after( 'type' );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table( 'settings' , function ( Blueprint $table ) {
                $table->dropColumn( 'type' );
                $table->dropColumn( 'validation' );
            } );
        }
    }
