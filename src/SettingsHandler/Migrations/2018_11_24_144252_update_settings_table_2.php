<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\DB;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Updates the Settings Table
     *
     * @package   -
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class UpdateSettingsTable2 extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            DB::statement( "ALTER TABLE `settings` MODIFY COLUMN `type` enum( 'string' , 'int' , 'bool' , 'float' , 'text' , 'percent' , 'date' ,'time' ,'color' ) NOT NULL AFTER `hidden`;" );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            DB::statement( "ALTER TABLE `settings` MODIFY COLUMN `type` enum( 'string' , 'int' , 'bool' , 'float' , 'text' , 'percent' ) NOT NULL AFTER `hidden`;" );
        }
    }
