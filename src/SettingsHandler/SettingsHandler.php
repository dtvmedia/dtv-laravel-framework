<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SettingsHandler;

    use DTV\BaseHandler\Views\Components\Icon;
    use DTV\SettingsHandler\Models\Setting;

    /**
     * Settings Handler
     *
     * @package   DTV\SettingsHandler
     * @copyright 2017 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class SettingsHandler
    {
        /**
         * Init Flag
         *
         * @var bool
         */
        private $init = false;

        /**
         * Settings Cache Array
         *
         * @var array|Setting[]
         */
        private $cache = [];

        /**
         * Array of all setting definition groups
         *
         * @var array
         */
        private $definitionGroups = [];

        /**
         * Array of all setting definitions
         *
         * @var array|SettingsOption[]
         */
        private $definitions = [];

        /**
         * Returns the value of the setting with the given key or $default if no such setting exists
         *
         * @param string $key
         * @param mixed  $default
         *
         * @return mixed|null
         */
        public function get( string $key , $default = null )
        {
            // Init Cache
            if ( $this->init === false ) {
                $this->loadAllInCache();
            }

            if ( !$this->cacheHas( $key ) ) {
                return $default ?? $this->getDefinition( $key )?->getDefault();
            }

            return $this->cache[ $key ]->getValue();
        }

        /**
         * Returns all settings values from a given group
         *
         * @param string $group
         *
         * @return array
         */
        public function getByGroup( string $group )
        {
            // Init Cache
            if ( $this->init === false ) {
                $this->loadAllInCache();
            }

            $groupSettings = [];

            foreach ( $this->cache as $key => $setting ) {
                if ( starts_with( $key , $group . '.' ) ) {
                    $groupSettings[ $key ] = $setting->getValue();
                }
            }

            return $groupSettings;
        }

        /**
         * Sets or create an setting option with the given key
         *
         * @param string $key
         * @param string $value
         *
         * @return $this|\Illuminate\Database\Eloquent\Model|null|static
         */
        public function set( string $key , $value )
        {
            if ( $this->cacheHas( $key ) ) {
                $setting = $this->cache[ $key ];
            } else {
                $setting = Setting::query()->where( 'key' , $key )->first();
            }

            // Reset init flag in order to force a cache refresh
            $this->init = false;

            if ( $setting !== null ) {
                // already exists -> update
                $setting->update( [ 'value' => $value , ] );

                return $setting;
            }

            // does not exist -> create new setting entry
            return Setting::query()->create( [
                'key'   => $key ,
                'value' => $value ,
            ] );
        }

        /**
         * Removes an given settings option
         *
         * @param string $key
         */
        public function remove( string $key )
        {
            // Delete in DB
            Setting::query()->where( 'key' , $key )->delete();

            // Delete in cache
            if ( $this->cacheHas( $key ) ) {
                unset( $this->cache[ $key ] );
            }
        }

        /**
         * Return whether or not the settings handler cache has a setting with the given key registered
         *
         * @param string $key
         *
         * @return bool
         */
        private function cacheHas( string $key )
        {
            return isset( $this->cache[ $key ] );
        }

        /**
         * Initializes the settings cache
         */
        private function loadAllInCache()
        {
            $settings = [];
            foreach ( Setting::query()->get() as $setting ) {
                $settings[ $setting->key ] = $setting;
            }

            $this->cache = $settings;
            $this->init = true;
        }

        /**
         * Defines an setting group
         *
         * @param string $groupKey
         * @param string $transKey
         * @param Icon   $icon
         * @param bool   $hidden
         * @param int    $priority
         */
        public function defineGroup( string $groupKey , string $transKey , Icon $icon , bool $hidden = false , int $priority = 50 )
        {
            $this->definitionGroups[ $groupKey ] = [
                'transKey' => $transKey ,
                'icon'     => $icon ,
                'hidden'   => $hidden ,
                'priority' => $priority ,
            ];
        }

        /**
         * Defines an settings option
         *
         * @param string $key
         * @param mixed  $default
         *
         * @return SettingsOption
         */
        public function define( string $key , $default = null )
        {
            $definition = new SettingsOption( $key , $default );

            list( $group , $rest ) = explode( '.' , $definition->getKey() , 2 );

            if ( !isset( $this->definitionGroups[ $group ] ) ) {
                throw new \Exception( 'Settings group "' . $group . '" not found. Define it first!' );
            }

            $this->definitions[ $key ] = $definition;

            return $definition;
        }

        /**
         * Returns the definition for a specific settings key
         *
         * @param string $key
         *
         * @return SettingsOption|null
         */
        public function getDefinition( string $key )
        {
            return $this->definitions[ $key ] ?? null;
        }

        /**
         * Returns all setting definitions as array
         *
         * @return array|SettingsOption[]
         */
        public function getDefinitions()
        {
            return $this->definitions;
        }

        /**
         * Returns all definitions by a group key
         *
         * @param string $group
         *
         * @return array|SettingsOption[]
         */
        public function getDefinitionsByGroup( string $group )
        {
            $definitions = [];

            foreach ( $this->getDefinitions() as $definition ) {
                if ( starts_with( $definition->getKey() , $group . '.' ) ) {
                    $definitions[ $definition->getKey() ] = $definition;
                }
            }

            return $definitions;
        }

        /**
         * Returns an registered setting groups by its group key
         *
         * @param string $groupKey
         *
         * @return array
         */
        public function getGroup( string $groupKey )
        {
            return $this->definitionGroups[ $groupKey ] ?? null;
        }

        /**
         * Returns all registered setting groups
         *
         * @return array
         */
        public function getGroups()
        {
            uasort( $this->definitionGroups , function ( array $a , array $b ) {
                return ( $a[ 'priority' ] <=> $b[ 'priority' ] );
            } );

            return $this->definitionGroups;
        }
    }
