<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SettingsHandler\Models;

    use DTV\BaseHandler\Models\Model;
    use DTV\SettingsHandler\SettingsOption;

    /**
     * Settings Group Fake Model
     *
     * @package   DTV\SettingsHandler\Models
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class SettingsGroup extends Model
    {
        /**
         * The attributes that aren't mass assignable.
         *
         * @var array
         */
        protected $guarded = [];

        public function getValueByDefinition( SettingsOption $option )
        {
            return $this->getAttribute( escapeHtmlLabel( $option->getKey() ) );
        }
    }
