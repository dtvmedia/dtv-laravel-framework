<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SettingsHandler\Models;

    use DTV\BaseHandler\Models\Model;
    use DTV\SettingsHandler\SettingsOption;
    use Illuminate\Support\Facades\Storage;

    /**
     * Settings Model
     *
     * @package   DTV\SettingsHandler\Models
     * @copyright 2017 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Setting extends Model
    {
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'key' , 'value' ,
        ];

        /**
         * The name of the related translation file
         * If null the getTranslationFile method will try to build it from the model name
         *
         * @var string
         */
        protected static $translationFile = 'dtv.settings::settings';

        /**
         * Returns the settings definition
         *
         * @return \DTV\SettingsHandler\SettingsOption|null
         */
        public function getDefinition()
        {
            return settings()->getDefinition( $this->key ) ?? new SettingsOption( $this->key );
        }

        /**
         * Returns the translated key
         *
         * @return mixed|string|\Symfony\Component\Translation\TranslatorInterface
         */
        public function getKey()
        {
            return trans( 'settings.keys.' . $this->key );
        }

        /**
         * Returns the settings value
         *
         * @return bool|\DTV\Oxygen\Oxygen|float|int|string
         */
        public function getValue()
        {
            if ( $this->getDefinition()->getType() === SettingsOption::TYPE_FILE ) {
                return config( 'app.url' ) . Storage::url( $this->value );
            }

            return cast( $this->getDefinition()->getType() , $this->value );
        }
    }
