<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\SettingsHandler;

    use DTV\BaseHandler\BaseServiceProvider;
    use DTV\BaseHandler\RoutingHandler;
    use DTV\SettingsHandler\Commands\SettingsUpdateCommand;
    use DTV\SettingsHandler\Controllers\SettingsController;

    /**
     * Settings Handler Service Provider
     *
     * @package   DTV\SettingsHandler
     * @copyright 2017 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class SettingsHandlerServiceProvider extends BaseServiceProvider
    {
        /**
         *  Namespace for resources of the module
         *
         * @var string
         */
        protected $resourcesNamespace = 'dtv.settings';

        /**
         * Array of commans
         *
         * @var array
         */
        protected $commands = [
            SettingsUpdateCommand::class ,
        ];

        /**
         * Config path for the module enable switch
         *
         * @var string|null
         */
        public static $config = 'dtv.modules.settings.enabled';

        /**
         * Register the service provider.
         *
         * @return void
         */
        public function register()
        {
            parent::register();

            $this->app->singleton( 'settings' , function () {
                return new SettingsHandler();
            } );
        }

        /**
         * Registers the Modules routes
         *
         * @throws \Exception
         */
        protected function routes()
        {
            // Settings
            $settingRoutes = new RoutingHandler( SettingsController::class , 'settings' , 'settings.' , RoutingHandler::ACCESS_AUTH );
            $settingRoutes->get( '/' , 'index' , 'index' , 'can:settings.view' );
            $settingRoutes->any( '/{group}' , 'edit' , 'edit' , 'can:settings.change' );
        }

        /**
         * Defines all the modules gates
         */
        public function gates()
        {
            gate()->define( 'settings.view' , function ( $user ) {
                return $user->hasPermission( [ 'settings.view' ] );
            } );
            gate()->define( 'settings.change' , function ( $user ) {
                return $user->hasPermission( [ 'settings.view' , 'settings.change' ] );
            } );
        }
    }