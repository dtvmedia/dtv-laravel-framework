<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    /**
     * Helper Functions
     *
     * @package   DTV\SettingsHandler
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */

    if ( !function_exists( 'settings' ) ) {
        /**
         * Returns the settings value or an settings handler instance if no parameters were given
         *
         * @param string $key
         * @param string $default
         *
         * @return \DTV\SettingsHandler\SettingsHandler|mixed|null
         */
        function settings( $key = null , $default = null )
        {
            if ( $key === null ) {
                return app( 'settings' );
            }

            return app()->make( 'settings' )->get( $key , $default );
        }
    }