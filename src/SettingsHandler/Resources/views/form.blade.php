@extends( $layout )

@section('title')
    {{ $form->getTitle() }}
@endsection

@push('styles')
    <style>
        .settings-file-wrapper {
            background: #dfdfdf;
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
        }

        .settings-file-preview {
            display: inline-block;
            margin: 15px;
            border: 1px dashed #b3b9bf;
        }

        .settings-table th {
            min-width: 20%;
            font-size: 16px;
            color: black;
            vertical-align: top;
        }

        .settings-table .settings-hint {
            margin-top: 2px;
            color: gray;
        }
    </style>
@endpush

@section('content')
    <form id="settings-form" class="fluent-form-view" action="{{ $form->getAction() }}" method="{{ $form->getMethod() }}">
        @csrf

        <div class="row">
            <div class="col-md-3">
                <div class="list-group">
                    @foreach( settings()->getGroups() as $groupKey => $group )
                        @if( $group['hidden'] === true )
                            @continue
                        @endif
                        <a href="{{ route( 'settings.edit' , [ 'group' => $groupKey ] ) }}" class="list-group-item list-group-item-action @active( 'settings.edit' , [ 'group' => $groupKey ])">
                            {!! $group['icon'] !!}
                            {{ trans( $group['transKey'] . '.' . $groupKey . '._group' ) }}
                        </a>
                    @endforeach
                </div>
            </div>
            <div class="col-md-9">
                <table class="table-style-2 settings-table">
                    @if( empty( $definitions ) )
                        <div class="alert alert-info">
                            Für diese Einstellungs Gruppe gibt es keine Optionen...
                        </div>
                    @endif
                    @foreach( $definitions as $definition )
                        @if( $definition->isHidden() )
                            @continue
                        @endif
                        <tr>
                            <th class="pb-3">
                                {{ $definition->getName() }}
                            </th>
                            <td class="pb-3">
                                @if( $definition->getType() === \DTV\SettingsHandler\SettingsOption::TYPE_FILE )
                                    <div class="settings-file-wrapper">
                                        <div class="settings-file-preview">
                                            <a href="{{ $settings->getValueByDefinition($definition) }}">
                                                @if( Illuminate\Support\Str::contains(strtolower($settings->getValueByDefinition($definition)),['.png','jpg','jpeg','.gif','.ico','.bmp']) )
                                                    <img alt="{{ basename($settings->getValueByDefinition($definition)) }}" src="{{ $settings->getValueByDefinition($definition) }}" style="max-height: 80px">
                                                @else
                                                    <i class="far fa-fw fa-file-alt"></i> {{ basename($settings->getValueByDefinition($definition)) }}
                                                @endif
                                            </a>
                                        </div>
                                        {!! $form->getInput( escapeHtmlLabel( $definition->getKey() ) ) !!}
                                    </div>
                                @else
                                    {!! $form->getInput( escapeHtmlLabel( $definition->getKey() ) ) !!}
                                @endif
                                @if( transExists( $definition->getTransKey() . '__hint' ) )
                                    <div class="settings-hint font-italic">
                                        @lang( $definition->getTransKey() . '__hint' )
                                    </div>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
    <script>
        new DTV.CustomFormView( '#settings-form' , false );
    </script>
@endpush
