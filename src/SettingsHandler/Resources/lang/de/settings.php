<?php

    return [

        'name' => 'Einstellung' ,

        'views' => [
            'index' => 'Einstellungen' ,
            'edit'  => 'Einstellung bearbeiten'
        ] ,

        'buttons' => [
            'edit' => 'Einstellung bearbeiten' ,
        ] ,

        'fields' => [
            'key'   => 'Einstellung' ,
            'value' => 'Wert' ,
        ] ,

        'messages' => [
            'editedTitle' => 'Einstellung bearbeitet' ,
            'editedText'  => 'Die Einstellung wurde erfolgreich bearbeitet' ,
        ]

    ];