<?php

    return [

        'name' => 'Setting' ,

        'views' => [
            'index' => 'Settings' ,
            'edit'  => 'Edit settings option'
        ] ,

        'buttons' => [
            'edit' => 'Edit settings option' ,
        ] ,

        'fields' => [
            'key'   => 'Setting' ,
            'value' => 'Value' ,
        ] ,

        'messages' => [
            'editedTitle' => 'Setting edited' ,
            'editedText'  => 'The setting was successfully edited' ,
        ]

    ];