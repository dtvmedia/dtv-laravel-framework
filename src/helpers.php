<?php

/**
 * DTV Media Solutions
 *
 * PHP Version 7.0
 */

use DTV\BaseHandler\JsonResponse;
use DTV\BaseHandler\Views\Components\Badge;
use DTV\BaseHandler\Views\Components\BaseButton;
use DTV\BaseHandler\Views\Components\Button;
use DTV\BaseHandler\Views\Components\ButtonGroup;
use DTV\BaseHandler\Views\Components\ConfirmButton;
use DTV\BaseHandler\Views\Components\Dropdown;
use DTV\BaseHandler\Views\Components\Icon;
use DTV\BaseHandler\Views\Components\Progress;
use DTV\Oxygen\Oxygen;
use DTV\Support\MorphIdHandler;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Route;

    /**
     * Helper Functions
     *
     * @package   DTV
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */

    if ( !function_exists( 'json' ) ) {
        /**
         * Returns an new JsonResponse Object
         *
         * @return JsonResponse
         */
        function json()
        {
            return new JsonResponse();
        }
    }

    if ( !function_exists( 'gate' ) ) {
        /**
         * Returns the Applications Gate instance
         *
         * @return Gate
         */
        function gate()
        {
            return app(Gate::class);
        }
    }

    if ( !function_exists( 'can' ) ) {
        /**
         * Checks if the current user is allowed to access a given route
         *
         * @param string $route
         * @param array  $parameters
         *
         * @return bool
         */
        function can(string $route, array $parameters = [])
        {
            // if the acl package is disabled all access is granted
            if ( config( 'dtv.modules.acl.enabled' , false ) === false ) {
                return true;
            }

            // search for an gate middleware for the given route
            $gateMiddleware = null;
            $route = Route::getRoutes()->getByName( $route );

            // if the route does not exists an error occured so we can return true here
            if ( $route === null ) {
                return true;
            }

            foreach ( $route->getAction()[ 'middleware' ] as $middleware ) {
                if (str_starts_with($middleware, 'can:')) {
                    $gateMiddleware = substr( $middleware , 4 );
                    break;
                }
            }

            // if no gate middlerware is defined for the given route the user is allowed to access the route
            if ( $gateMiddleware === null ) {
                return true;
            }

            // get ability and required parameters
            $can = explode( ',' , $gateMiddleware );
            $ability = $can[ 0 ];
            unset( $can[ 0 ] );

            // replace parameters
            foreach ( $can as $key => $param ) {
                $can[ $key ] = $parameters[ trim( $param ) ] ?? null;
            }

            // authorize
            return gate()->allows( $ability , $can );
        }
    }

    if ( !function_exists( 'option' ) ) {
        /**
         * Return a Select Option Dataset
         *
         * @param string $value
         * @param string $icon
         * @param bool   $disabled
         *
         * @return array
         */
        function option(string $value, string $icon = 'fa-angle-double-right', bool $disabled = false )
        {
            return [
                'value'    => (str_contains($value, '.')) ? trans($value) : $value,
                'icon'     => $icon . ' fa-fw',
                'disabled' => $disabled,
            ];
        }
    }

    if ( !function_exists( 'routeExists' ) ) {
        /**
         * Checks an returns whether or not a given route exists
         *
         * @param string $route
         *
         * @return bool
         */
        function routeExists(string $route)
        {
            return Route::has($route);
        }
    }

    if ( !function_exists( 'navigation' ) ) {
        /**
         * Builds the main navigation or subitems by a given input array
         *
         * @param array|null $navigation
         * @param bool       $maincall
         *
         * @return string
         */
        function navigation(array $navigation = null, bool $maincall = true)
        {
            if ( $navigation == null && $maincall) {
                $navigation = config( 'dtv.navigation' , [] );
            }

            $html = '';

            if ($maincall) {
                $html .= '<ul class="navbar-nav main-nav">';
            }

            foreach ( $navigation as $key => $item ) {
                if ( is_array( $item ) ) {
                    if ( array_key_exists( 'route' , $item ) && array_key_exists( 'parameter' , $item ) ) {
                        if ( !routeExists( $item[ 'route' ] ) || ( routeExists( $item[ 'route' ] ) && $item[ 'route' ] === 'categories.index' && !app( 'categories' )->has( $item[ 'parameter' ][ 'type' ] ?? null ) ) ) {
                            continue;
                        }

                        if ( $maincall ) {
                            $html .= '<li class="nav-item"><a class="nav-link" href="' . route( $item[ 'route' ] ,
                                    $item[ 'parameter' ] ) . '">' . trans( 'general.navigation.' . $key ) . '</a></li>';
                        } else {
                            $html .= '<a class="dropdown-item" href="' . route( $item[ 'route' ] ,
                                    $item[ 'parameter' ] ) . '" >' . trans( 'general.navigation.' . $key ) . '</a>';
                        }
                    } else {
                        $hasSubItems = false;
                        foreach ( $item as $subitem ) {
                            if ( !is_array( $subitem ) && $subitem != 'divider' ) {
                                if ( can( $subitem ) ) {
                                    $hasSubItems = true;
                                    break;
                                }
                            }
                        }

                        if (!$hasSubItems) {
                            continue;
                        }

                        $html .= '<li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">' . trans( 'general.navigation.' . $key ) . ' <span class="caret"></span></a>
                            <div class="dropdown-menu">';
                        $html .= navigation( $item , false ) . '</div>';
                    }
                } elseif ( $item == 'divider' ) {
                    $html .= '<div class="dropdown-divider"></div>';
                } elseif ( can( $item ) ) {
                    if ( !routeExists( $item ) ) {
                        continue;
                    }

                    if ( $maincall ) {
                        $html .= '<li class="nav-item"><a class="nav-link" href="' . route( $item ) . '">' . trans( 'general.navigation.' . $key ) . '</a></li>';
                    } else {
                        $html .= '<a class="dropdown-item" href="' . route( $item ) . '" >' . trans( 'general.navigation.' . $key ) . '</a>';
                    }
                }
            }

            if ($maincall) {
                $html .= '</ul>';
            }

            return $html;
        }
    }

    if ( !function_exists( 'version' ) ) {
        /**
         * Returns the applications version
         *
         * @param string|null $branch
         * @param mixed       $default
         *
         * @return stdClass
         */
        function version(?string $branch = null, mixed $default = null)
        {
            $config = config( 'dtv.app' );
            $last_changed = null;

            if ( $branch === null ) {
                $branch = $config[ 'git_branch' ];
            }

            try {
                $branch_ref_file = $config[ 'git_root' ] . $branch;
                $composer = json_decode( file_get_contents( app()->basePath() . DIRECTORY_SEPARATOR . 'composer.json' ) );

                if ( realpath( $branch_ref_file ) !== false ) {
                    $commit = substr( file_get_contents( $branch_ref_file ) , 0 , 6 );
                    $last_changed = date( "d.m.Y H:i:s" , filemtime( $branch_ref_file ) );
                } else {
                    $commit = '';
                }

                $version = rtrim( ( $composer->version ?? '1' ) . '.' . $commit , '.' ) . ' ' . $branch;
            } catch (Throwable) {
                if ( $default === null ) {
                    $version = app()->version();
                } else {
                    $version = $default;
                }
            }

            return (object) [
                'version'      => $version ,
                'last_changed' => $last_changed ,
            ];
        }
    }

    if ( !function_exists( 'escapeHtmlLabel' ) ) {
        /**
         * Escapes an given string as html label
         *
         * @param string $label
         * @param array  $disallowed
         *
         * @return string
         */
        function escapeHtmlLabel(string $label, array $disallowed = ['.', ':', ' ', '/'])
        {
            return str_replace($disallowed, '_', strtolower($label));
        }
    }

    if ( !function_exists( 'button' ) ) {
        /**
         * Returns the HTML of an Bootstrap Button
         *
         * @param string           $label
         * @param string           $route
         * @param array            $parameter
         * @param Icon|string|null $icon
         *
         * @throws Exception
         * @return Button
         */
        function button(string $label, string $route, array $parameter = [], Icon|string|null $icon = null)
        {
            return (new Button($label, $route, $parameter))->setIcon($icon);
        }
    }

    if ( !function_exists( 'confirmButton' ) ) {
        /**
         * Returns the HTML of an Bootstrap Confirm Button
         *
         * @param string           $label
         * @param string           $route
         * @param array            $parameter
         * @param Icon|string|null $icon
         * @param string|null      $confirmMessage
         * @param string|null      $confirmButton
         *
         * @throws Exception
         * @return ConfirmButton
         */
        function confirmButton(string $label, string $route, array $parameter, Icon|string|null $icon = null, ?string $confirmMessage = null, ?string $confirmButton = null)
        {
            return (new ConfirmButton($label, $route, $parameter))
                ->setIcon($icon)
                ->setMessage($confirmMessage)
                ->setConfirmButtonLabel($confirmButton);
        }
    }

    if ( !function_exists( 'buttonGroup' ) ) {
        /**
         * Returns the HTML of an Bootstrap Button Group with the given Buttons
         *
         * @param BaseButton ...$buttons
         *
         * @return ButtonGroup
         */
        function buttonGroup(BaseButton ...$buttons)
        {
            $group = new ButtonGroup();

            foreach ($buttons as $button) {
                $group->addButton($button);
            }

            return $group;
        }
    }

    if ( !function_exists( 'icon' ) ) {
        /**
         * Creates a FontAwesome Icon
         *
         * @param string $icon
         * @param string $title
         * @param string $classes
         *
         * @return Icon
         */
        function icon(string $icon, string $title = '', string $classes = '')
        {
            return new Icon($icon, $title, $classes);
        }
    }

    if ( !function_exists( 'badge' ) ) {
        /**
         * Creates the HTML for an Bootstrap Badge with the given parameters
         *
         * @param string $content Badge content
         * @param string $type    Badge type: primary, warning...
         *
         * @return Badge
         */
        function badge(string $content, string $type = 'primary' )
        {
            return new Badge($content, $type);
        }
    }

    if ( !function_exists( 'progressbar' ) ) {
        /**
         * Create the HTML for an Bootstrap Progress Bar with the given parameters
         *
         * @param float|int|null $progress
         * @param string|null    $type
         * @param string|null    $label
         *
         * @return Progress
         */
        function progressbar(float|int|null $progress = null, ?string $type = null, ?string $label = null )
        {
            return new Progress($progress, $type, $label);
        }
    }

    if ( !function_exists( 'dropdown' ) ) {
        /**
         * Create the HTML for an Bootstrap Dropdown with the given parameters
         *
         * @param string           $label
         * @param Icon|string|null $icon
         *
         * @throws Exception
         * @return Dropdown
         */
        function dropdown(string $label, Icon|string|null $icon )
        {
            return new Dropdown($label, $icon);
        }
    }

    if ( !function_exists( 'formatSeconds' ) ) {
        /**
         * Formats the given seconds in the H:i:s format
         *
         * @param int  $seconds
         * @param bool $withSeconds
         *
         * @return string
         */
        function formatSeconds(int $seconds, bool $withSeconds = true)
        {
            $h = floor($seconds / 3600);
            $i = ($seconds / 60) % 60;
            $s = $seconds % 60;

            if ($withSeconds) {
                return sprintf("%02d:%02d:%02d", $h, $i, $s);
            }

            return sprintf("%02d:%02d", $h, $i);
        }
    }

    if ( !function_exists( 'formatFileSize' ) ) {
        /**
         * Return the formatted file size
         *
         * @param int $bytes     File size in Bytes
         * @param int $precision Round Precision
         *
         * @return  string
         */
        function formatFileSize(int $bytes, int $precision = 2)
        {
            $units = ['B', 'KB', 'MB', 'GB', 'TB'];

            $bytes = max($bytes, 0);
            $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
            $pow = min($pow, count($units) - 1);
            $bytes /= pow(1024, $pow);

            return str_replace(',', '.', round($bytes, $precision)) . ' ' . $units[$pow];
        }
    }

    if ( !function_exists( 'formatColor' ) ) {
        /**
         * Returns an formatted color with an preview box
         *
         * @param string $color
         * @param bool   $showLabel
         * @param string $size
         *
         * @return string
         */
        function formatColor(string $color, bool $showLabel = true, string $size = '16px')
        {
            $html = '<div style="width: ' . $size . '; height: ' . $size . '; display:inline-block; background: ' . $color . '"></div>';

            if ($showLabel) {
                $html .= ' ' . $color;
            }

            return $html;
        }
    }

    if ( !function_exists( 'formatCurrency' ) ) {
        /**
         * Returns an float number formatted as currency (but without currency symbol)
         *
         * @param float $number
         * @param int   $decimals
         *
         * @return string
         */
        function formatCurrency(float $number, int $decimals = 2)
        {
            if (app()->getLocale() === 'de') {
                return number_format($number, $decimals, ',', '.');
            }

            return number_format($number, $decimals);
        }
    }

    if ( !function_exists( 'formatNumber' ) ) {
        /**
         * Returns an float number formatted as generic number
         *
         * @param float $number
         * @param int   $decimals
         * @param bool  $fixedDecimals
         *
         * @return string
         */
        function formatNumber(float $number, int $decimals = 2, bool $fixedDecimals = false)
        {
            if ( app()->getLocale() === 'de' ) {
                $decimalChar = ',';
                $thousandChar = '.';
            } else {
                $decimalChar = '.';
                $thousandChar = ',';
            }

            $number = number_format( $number , $decimals , $decimalChar , $thousandChar );

            if ( $fixedDecimals === false ) {
                $number = rtrim( rtrim( $number , '0' ) , $decimalChar );
            }

            return $number;
        }
    }

    if ( !function_exists( 'putInRange' ) ) {
        /**
         * Puts an value in the given range if the value is not already in it
         *
         * @param float $value
         * @param float $min
         * @param float $max
         *
         * @return float
         */
        function putInRange(float $value, float $min = 0.0, float $max = 100.0)
        {
            return min($max, max($min, $value));
        }
    }

    if ( !function_exists( 'transExists' ) ) {
        /**
         * Wrapper function for lang has which returns if there is an translation for a given translation key
         *
         * @param string $key
         *
         * @return bool
         */
        function transExists(string $key)
        {
            return Lang::has( $key );
        }
    }

    if ( !function_exists( 'model' ) ) {
        /**
         * Returns an object of modelClass (useful for method interfaces where id and object should be allowed for an parameter)
         *
         * @param Model|int $objectOrId
         * @param string    $modelClass
         * @param bool      $nullable
         *
         * @throws Exception
         *
         * @return Model|null
         */
        function model($objectOrId, string $modelClass, bool $nullable = false)
        {
            if ($objectOrId instanceof $modelClass) {
                return $objectOrId;
            }

            if ( !is_a( $modelClass , Model::class , true ) ) {
                throw new Exception( 'Given class is not a model!' );
            }

            $function = $nullable ? 'find' : 'findOrFail';

            return $modelClass::query()->$function( $objectOrId );
        }
    }

    if ( !function_exists( 'str_excerpt' ) ) {
        /**
         * Returns and excerpt of the given string with maximal $chars characters
         *
         * @param string $str
         * @param int    $chars
         * @param bool   $withTooltip
         *
         * @return string
         */
        function str_excerpt(string $str, int $chars = 30, bool $withTooltip = false)
        {
            $shortened = false;
            $excerpt = mb_substr( $str , 0 , $chars );

            if ( mb_strlen( $str ) > mb_strlen( $excerpt ) ) {
                $shortened = true;
                $excerpt .= '...';
            }

            if ( $withTooltip ) {
                $excerpt = e( $excerpt );

                if ( $shortened ) {
                    $excerpt = '<span data-toggle="tooltip" data-placement="right" title="" data-original-title="' . htmlspecialchars( $str ) . '">' . $excerpt . '</span>';
                }
            }

            return $excerpt;
        }
    }

    if ( !function_exists( 'cast' ) ) {
        /**
         * Casts an string value to the given type (types are compatible with the ones used in the settings and category handler)
         *
         * @param string      $type
         * @param string|null $value
         *
         * @return bool|Oxygen|float|int|string
         */
        function cast(string $type, ?string $value)
        {
            switch ( $type ) {
                case 'int':
                case 'percent':
                    return (int) $value;
                case 'bool':
                    return (bool) $value;
                case 'float':
                    return (float) $value;
                case 'date':
                case 'time':
                    return Oxygen::parse( $value );
                case 'password':
                    if ( empty( $value ) ) {
                        return null;
                    }

                    return Crypt::decryptString( $value );
                case 'array':
                case 'builder':
                case 'model':
                    if ( is_numeric( $value ) ) {
                        return (int) $value;
                    }

                    return $value;
                default:
                    return $value;
            }
        }
    }

    if ( !function_exists( 'isDebugMode' ) ) {
        /**
         * Returns whether or not the application is in debug mode
         *
         * @return bool
         */
        function isDebugMode()
        {
            return ( config( 'app.env' ) === 'debug' || config( 'app.debug' ) === true );
        }
    }

    if ( !function_exists( 'isCurrentRoute' ) ) {
        /**
         * Returns whether the given route is the current one or not
         *
         * @param string $route
         * @param mixed  $returnSuccess
         * @param array  $parameter
         *
         * @return bool
         */
        function isCurrentRoute(string $route, array $parameter = [], $returnSuccess = true)
        {
            if (Route::getCurrentRoute()?->getName() !== $route) {
                return false;
            }

            $routeParams = Route::getCurrentRoute()->parameters();
            foreach ($parameter as $key => $value) {
                if (!isset($routeParams[$key]) || $routeParams[$key] != $value) {
                    return false;
                }
            }

            return $returnSuccess;
        }
    }

    if ( !function_exists( 'morphId' ) ) {
        /**
         * Generates the morph id for the given model (Morph ID Helper)
         *
         * @param Model $model
         *
         * @return string
         */
        function morphId(Model $model)
        {
            return MorphIdHandler::getIdByModel($model);
        }
    }
