<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\Support;

    /**
     * Simple Rectangle class
     *
     * @package   DTV\Support
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Rectangle
    {
        /**
         * Width in px
         *
         * @var int
         */
        private int $width;

        /**
         * Height in px
         *
         * @var int
         */
        private int $height;

        /**
         * Rectangle constructor.
         *
         * @param int $w
         * @param int $h
         */
        public function __construct(int $w, int $h)
        {
            $this->width = $w;
            $this->height = $h;
        }

        /**
         * Returns the width
         *
         * @return int
         */
        public function getWidth(): int
        {
            return $this->width;
        }

        /**
         * Returns the height
         *
         * @return int
         */
        public function getHeight(): int
        {
            return $this->height;
        }
    }
