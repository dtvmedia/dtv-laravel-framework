<?php

/**
 * DTV Media Solutions
 *
 * PHP Version 7.0
 */

namespace DTV\Support;

use Exception;
use Illuminate\Database\Eloquent\Model;
use InvalidArgumentException;
use Throwable;

/**
 * Morph ID Handler
 *
 * @package   DTV\Support
 * @copyright 2018 DTV Media Solutions
 * @author    Dominique Heinelt <contact@dtvmedia.de>
 * @link      http://dtvmedia.de/
 */
class MorphIdHandler
{
    /**
     * Prefix constant
     */
    public static string $prefix = '_MIH';

    /**
     * Class mapping shortened morph IDs
     *
     * @var array
     */
    private static array $classMap = [];

    /**
     * Registers a class for the shortened version of morph ID
     *
     * @param string $key
     * @param string $modelClassName
     *
     * @return void
     */
    public static function registerShortClass(string $key, string $modelClassName): void
    {
        static::$classMap[$key] = $modelClassName;
    }

    /**
     * Clears the map for all shortened morph id classes
     *
     * @return void
     */
    public static function clearClassMap(): void
    {
        static::$classMap = [];
    }

    /**
     * Generates the morph id for the given model
     *
     * @param Model $model
     *
     * @return string
     */
    public static function getIdByModel(Model $model): string
    {
        return self::getId(get_class($model), $model->id);
    }

    /**
     * Generates the morph id for the given model name and id combination
     *
     * @param string $modelClassName
     * @param int    $modelId
     *
     * @return string
     */
    public static function getId(string $modelClassName, int $modelId): string
    {
        if (!is_a($modelClassName, Model::class, true)) {
            throw new InvalidArgumentException('Given class is not a model!');
        }
        if ($modelId < 1) {
            throw new InvalidArgumentException('Invalid model id given!');
        }

        $shortKey = array_search($modelClassName, static::$classMap);
        if ($shortKey !== false) {
            $modelClassName = $shortKey;
        }

        $chars = str_split($modelClassName . '-' . $modelId);
        foreach ($chars as $key => $char) {
            $chars[ $key ] = base_convert(str_pad(ord($char), 3, '3', STR_PAD_LEFT), 10, 36);
        }

        return static::$prefix . implode('', $chars);
    }

    /**
     * Returns the model by its morph id
     *
     * @param string $id
     * @param bool   $nullable
     *
     * @throws Throwable
     *
     * @return Model|null
     */
    public static function getModel(string $id, bool $nullable = true): ?Model
    {
        try {
            if (static::isMorphId($id) === false) {
                throw new Exception('Invalid morph id given: ' . $id);
            }

            $chars = str_split(str_replace(static::$prefix, '', $id), 2);

            foreach ($chars as $key => $char) {
                $part = (int)base_convert($char, 36, 10);

                if ($part > 300) {
                    $part -= 300;
                }

                $chars[ $key ] = chr($part);
            }

            $chars = implode('', $chars);

            [$modelClassName, $modelId] = explode('-', $chars, 2);

            if (isset(static::$classMap[ $modelClassName ])) {
                $modelClassName = static::$classMap[ $modelClassName ];
            }
        } catch (Throwable $exception) {
            logger()->error('MorphIdHandler error: cant decode model from: ' . $id);

            throw $exception;
        }

        if (!is_a($modelClassName, Model::class, true)) {
            throw new Exception('Decoded class is not a model!');
        }
        if ($modelId < 1) {
            throw new Exception('Decoded model id is invalid!');
        }

        $function = $nullable ? 'find' : 'findOrFail';

        return $modelClassName::query()->$function($modelId);
    }

    /**
     * Returns whether or not a given id is a potential morph id
     *
     * @param mixed $id
     *
     * @return bool
     */
    public static function isMorphId(mixed $id): bool
    {
        return is_string($id) && starts_with($id, static::$prefix);
    }
}
