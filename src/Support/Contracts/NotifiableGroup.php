<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\Support\Contracts;

    use Illuminate\Database\Eloquent\Relations\HasMany;

    /**
     * Notifiable Group Contract
     *
     * @package   DTV\Support\Contracts
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     *
     * @property Notifiable[] $notifiables
     */
    interface NotifiableGroup
    {
        /**
         * Returns all related Notifiable objects
         *
         * @return HasMany|Notifiable[]
         */
        public function notifiables();

        /**
         * Returns the unique notifiable id
         *
         * @return string
         */
        public function getMorphId();
    }
