<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\Support\Contracts;

    /**
     * Manager Contract
     *
     * @package   DTV\Support\Contracts
     * @copyright 2019 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    interface ManagerContract
    {
        /**
         * Registeres an given class
         *
         * @param string $class
         *
         * @return void
         */
        public function register(string $class): void;

        /**
         * Checks if an item with the given key is already registered
         *
         * @param string $key
         *
         * @return bool
         */
        public function has(string $key): bool;

        /**
         * Returns the item with the given key or null
         *
         * @param string $key
         *
         * @return mixed|null
         */
        public function get(string $key);

        /**
         * Returns an array with all registered items
         *
         * @return array
         */
        public function all(): array;
    }
