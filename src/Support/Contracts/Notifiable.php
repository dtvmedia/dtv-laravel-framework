<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\Support\Contracts;

    /**
     * Notifiable Contract
     *
     * @package   DTV\Support\Contracts
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    interface Notifiable
    {
        /**
         * Returns the unique notifiable id
         *
         * @return string
         */
        public function getMorphId();
    }