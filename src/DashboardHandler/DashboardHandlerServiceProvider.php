<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\DashboardHandler;

    use DTV\BaseHandler\BaseServiceProvider;
    use DTV\BaseHandler\RoutingHandler;
    use DTV\DashboardHandler\Controllers\DashboardController;
    use DTV\DashboardHandler\Controllers\WidgetController;
    use DTV\DashboardHandler\Models\Dashboard;
    use Exception;
    use Illuminate\Contracts\Container\BindingResolutionException;

    /**
     * Dashboard Handler Service Provider
     *
     * @package   DTV\DashboardHandler
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class DashboardHandlerServiceProvider extends BaseServiceProvider
    {
        /**
         *  Namespace for resources of the module
         *
         * @var string
         */
        protected $resourcesNamespace = 'dtv.dashboards';

        /**
         * Array of commans
         *
         * @var array
         */
        protected $commands = [];

        /**
         * Config path for the module enable switch
         *
         * @var string|null
         */
        public static $config = 'dtv.modules.dashboards.enabled';

        /**
         * Bootstrap any application services.
         *
         * @return void
         */
        public function boot()
        {
            $this->publishes( [
                __DIR__ . '/Resources/assets/' => resource_path( 'assets' ) ,
            ] , 'dtv.dashboard-assets' );

            parent::boot();
        }

        /**
         * Register the service provider.
         *
         * @throws BindingResolutionException
         * @return void
         */
        public function register()
        {
            parent::register();

            $this->app->singleton( 'widgets' , function () {
                return new WidgetManager();
            } );
        }

        /**
         * Registers the Modules routes
         *
         * @throws Exception
         */
        public function routes(): void
        {
            // Dashboard Routes
            $dashboardRoutes = new RoutingHandler( DashboardController::class , 'dashboards' , 'dashboards.' , RoutingHandler::ACCESS_AUTH );
            $dashboardRoutes->get( '/' , 'index' , 'index' , 'can:dashboards.view' );
            $dashboardRoutes->any( '/create' , 'create' , 'create' , 'can:dashboards.change' );
            $dashboardRoutes->get( '/{dashboard}' , 'show' , 'show' , 'can:dashboards.view,dashboard' );
            $dashboardRoutes->any( '/{dashboard}/edit' , 'edit' , 'edit' , 'can:dashboards.change,dashboard' );
            $dashboardRoutes->get( '/{dashboard}/share' , 'share' , 'share' , 'can:dashboards.change,dashboard' );
            $dashboardRoutes->get( '/{dashboard}/unshare' , 'unshare' , 'unshare' , 'can:dashboards.change,dashboard' );
            $dashboardRoutes->get( '/{dashboard}/markAsDefault' , 'markAsDefault' , 'markAsDefault' , 'can:dashboards.change,dashboard' );
            $dashboardRoutes->post( '/{dashboard}/update' , 'update' , 'update' , 'can:dashboards.change,dashboard' );
            $dashboardRoutes->delete( '/{dashboard}/delete/' , 'delete' , 'delete' , 'can:dashboards.change,dashboard' );

            // Widget Routes
            $widgetRoutes = new RoutingHandler( WidgetController::class , 'dashboards/{dashboard}/' , 'dashboards.widgets' , RoutingHandler::ACCESS_AUTH );
            $widgetRoutes->any( '/create' , 'create' , 'create' , 'can:dashboards.change,dashboard' );
            $widgetRoutes->any( '/{widget}/edit' , 'edit' , 'edit' , 'can:dashboards.change,dashboard' );
            $widgetRoutes->delete( '/{widget}/delete' , 'delete' , 'delete' , 'can:dashboards.change,dashboard' );
        }

        /**
         * Defines all the modules gates
         */
        public function gates(): void
        {
            gate()->define('dashboards.view', function ($user, $dashboard = null) {
                if ($dashboard !== null && !$dashboard instanceof Dashboard) {
                    /** @var Dashboard|null $dashboard */
                    $dashboard = Dashboard::query()->find($dashboard);

                    if ($dashboard === null) {
                        return false;
                    }
                }

                return $user->hasPermission(['dashboards.view']) && ($dashboard === null || $dashboard->user_id === $user->id);
            });
            gate()->define('dashboards.change', function ($user, $dashboard = null) {
                if ($dashboard !== null && !$dashboard instanceof Dashboard) {
                    /** @var Dashboard|null $dashboard */
                    $dashboard = Dashboard::query()->find($dashboard);

                    if ($dashboard === null) {
                        return false;
                    }
                }

                return $user->hasPermission(['dashboards.change']) && ($dashboard === null || $dashboard->user_id === $user->id);
            });
        }
    }
