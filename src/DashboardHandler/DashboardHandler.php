<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\DashboardHandler;

    use DTV\DashboardHandler\Models\Dashboard;
    use DTV\DashboardHandler\Models\Widget as WidgetModel;
    use Illuminate\Support\Facades\DB;

    /**
     * Dashboard Handler
     *
     * @package   DTV\DashboardHandler
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class DashboardHandler
    {
        /**
         * Return an array of all widget select options
         *
         * @return array
         */
        public static function getWidgetOptions()
        {
            $widgetOptions = [];

            foreach (app('widgets')->all() as $key => $widget) {
                $widgetClass = new $widget(new WidgetModel());
                if ($widgetClass->conditions()) {
                    $widgetOptions[$key] = option($widgetClass->getName(), 'fa-window');
                }
            }

            return $widgetOptions;
        }

        /**
         * Returns an array of layout options (not select syntax conform)
         *
         * @return array
         */
        public static function getLayoutOptions()
        {
            return config('dtv.modules.dashboards.layouts');
        }

        /**
         * Creates an new dashboard
         *
         * @param string   $name
         * @param int      $layout
         * @param boolean  $default
         * @param boolean  $shared
         * @param int      $template
         * @param int|null $user_id
         *
         * @return Dashboard
         */
        public function create($name, $layout, $default, $shared, $template = null, $user_id = null): Dashboard
        {
            $user_id = ($user_id === null) ? auth()->id() : $user_id;
            $layout = config('dtv.modules.dashboards.layouts.' . $layout);

            /** @var Dashboard $dashboard */
            $dashboard = Dashboard::query()->create([
                'user_id' => $user_id,
                'name'    => $name,
                'layout'  => $layout,
                'default' => $default,
                'shared'  => $shared,
            ]);

            // Use an shared dashboard as template
            if ($template !== null && $template != 0) {
                /** @var Dashboard $templateDashboard */
                $templateDashboard = Dashboard::query()->where('shared', 1)->findOrFail($template);

                foreach ($templateDashboard->widgets as $widget) {
                    $this->addWidget($dashboard, $widget->type);
                }
            }

            return $dashboard;
        }

        /**
         * Edits an given dashboard
         *
         * @param Dashboard $dashboard
         * @param string    $name
         * @param bool      $default
         *
         * @return Dashboard
         */
        public function edit(Dashboard $dashboard, string $name, bool $default): Dashboard
        {
            if ($default === true) {
                // unmark current default dashboard
                Dashboard::query()->where('user_id', $dashboard->user_id)->update(['default' => false]);
            }

            // set instead given dashboard as default
            $dashboard->update([
                'name'    => $name,
                'default' => $default,
            ]);

            return $dashboard;
        }

        /**
         * Shares an dashboard
         *
         * @param Dashboard $dashboard
         *
         * @return Dashboard
         */
        public function share(Dashboard $dashboard): Dashboard
        {
            $dashboard->update(['shared' => true]);

            return $dashboard;
        }

        /**
         * Unshares an dashboard
         *
         * @param Dashboard $dashboard
         *
         * @return Dashboard
         */
        public function unshare(Dashboard $dashboard): Dashboard
        {
            $dashboard->update(['shared' => false]);

            return $dashboard;
        }

        /**
         * Marks the given dashboard as favorite and unmarks the previous favorite
         *
         * @param Dashboard $dashboard
         *
         * @return Dashboard
         */
        public function markAsDefault(Dashboard $dashboard): Dashboard
        {
            // unmark current default dashboard
            Dashboard::query()->where('user_id', $dashboard->user_id)->update(['default' => false]);

            // set instead given dashboard as default
            $dashboard->update(['default' => true]);

            return $dashboard;
        }

        /**
         * Adds an widget to the given dashboard
         *
         * @param Dashboard $dashboard
         * @param string    $widget
         *
         * @return WidgetModel
         */
        public function addWidget(Dashboard $dashboard, string $widget): WidgetModel
        {
            $position = $this->getNextPosition($dashboard);

            /** @var WidgetModel */
            return WidgetModel::query()->create([
                'dashboard_id' => $dashboard->id,
                'type'         => $widget,
                'column'       => $position['column'],
                'row'          => $position['row'],
                'settings'     => [],
            ]);
        }

        /**
         * Updates the widget positions
         *
         * @param Dashboard $dashboard
         * @param array     $widgetPositions
         *
         * @return Dashboard
         */
        public function updatePositions(Dashboard $dashboard, array $widgetPositions): Dashboard
        {
            foreach ($widgetPositions as $colKey => $column) {
                foreach ($column as $rowKey => $widget_id) {
                    $dashboard->widgets->find($widget_id)?->update([
                        'column' => $colKey + 1,
                        'row'    => $rowKey + 1,
                    ]);
                }
            }

            return $dashboard;
        }

        /**
         * Calculates the position for the next widget of the given dashboard
         *
         * @param Dashboard $dashboard
         *
         * @return array
         */
        public function getNextPosition(Dashboard $dashboard): array
        {
            $maxWidgets = [];
            $maxWidgetsRaw = DB::table('dashboard_widgets')->where('dashboard_id', $dashboard->id)->groupBy('column')
                ->select([DB::raw('MAX(row) AS row'), 'column'])->get()->keyBy('column')->toArray();

            foreach ($dashboard->layout as $key => $col) {
                if (isset($maxWidgetsRaw[$key + 1])) {
                    $maxWidgets[$key + 1] = $maxWidgetsRaw[$key + 1]->row;
                } else {
                    $maxWidgets[$key + 1] = 0;
                }
            }

            return [
                'column' => array_first(array_keys($maxWidgets, min($maxWidgets))),
                'row'    => min($maxWidgets) + 1,
            ];
        }
    }
