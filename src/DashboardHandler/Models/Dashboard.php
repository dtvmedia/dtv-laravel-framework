<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\DashboardHandler\Models;

    use DTV\BaseHandler\Models\Model;
    use Illuminate\Database\Eloquent\Collection;
    use Illuminate\Database\Eloquent\Relations\BelongsTo;
    use Illuminate\Database\Eloquent\Relations\HasMany;

    /**
     * Dashboard Model
     *
     * @package   DTV\DashboardHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     *
     * @property Collection<Widget> $widgets
     */
    class Dashboard extends Model
    {
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'user_id' , 'name' , 'layout' , 'default' , 'shared'
        ];

        /**
         * The attributes that should be cast to native types.
         *
         * @var array
         */
        protected $casts = [
            'layout'  => 'array' ,
            'default' => 'boolean' ,
            'shared'  => 'boolean'
        ];

        /**
         * The name of the related translation file
         * If null the getTranslationFile method will try to build it from the model name
         *
         * @var string
         */
        protected static $translationFile = 'dtv.dashboards::dashboards';

        /**
         * Returns the related user model
         *
         * @return BelongsTo
         */
        public function user(): BelongsTo
        {
            return $this->belongsTo(config('dtv.models.user'));
        }

        /**
         * Returns all related widgets
         *
         * @return HasMany
         */
        public function widgets(): HasMany
        {
            return $this->hasMany(Widget::class);
        }
    }
