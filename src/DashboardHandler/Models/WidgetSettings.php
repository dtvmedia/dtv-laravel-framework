<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\DashboardHandler\Models;

    use DTV\BaseHandler\Models\Model;

    /**
     * Widget Settings Pseudo Model
     *
     * @package   DTV\DashboardHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class WidgetSettings extends Model
    {
        /**
         * WidgetSettings constructor.
         *
         * @param array $attributes
         */
        public function __construct( array $attributes = [] )
        {
            $this->fillable = array_keys( $attributes );

            parent::__construct( $attributes );
        }
    }
