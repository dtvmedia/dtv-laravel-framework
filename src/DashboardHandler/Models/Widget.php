<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\DashboardHandler\Models;

    use DTV\BaseHandler\Models\Model;
    use DTV\DashboardHandler\WidgetManager;
    use Illuminate\Database\Eloquent\Relations\BelongsTo;

    /**
     * Widget Model
     *
     * @package   DTV\DashboardHandler\Models
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class Widget extends Model
    {
        /**
         * The table associated with the model.
         *
         * @var string
         */
        protected $table = 'dashboard_widgets';

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'dashboard_id' , 'type' , 'column' , 'row' , 'settings'
        ];

        /**
         * The attributes that should be cast to native types.
         *
         * @var array
         */
        protected $casts = [
            'settings' => 'array' ,
        ];

        /**
         * The name of the related translation file
         * If null the getTranslationFile method will try to build it from the model name
         *
         * @var string
         */
        protected static $translationFile = 'dtv.dashboards::widgets';

        /**
         * Returns the related user model
         *
         * @return BelongsTo
         */
        public function dashboard(): BelongsTo
        {
            return $this->belongsTo(Dashboard::class);
        }

        /**
         * Returns the related widget class
         *
         * @return class-string
         */
        public function getClass()
        {
            /** @var WidgetManager $manager */
            $manager = app('widgets');

            return $manager->get($this->type);
        }
    }
