@extends( config('dtv.layouts.page_layout') )

@section('window-title')
    @if( $dashboard === null )
        @lang('dtv.dashboards::dashboards.views.noDashboard')
    @else
        {{ $dashboard->name }}
    @endif
@endsection

@section('title')
    @if( $dashboard === null )
        <i>@lang('dtv.dashboards::dashboards.views.noDashboard')</i>
    @else
        {{ $dashboard->name }}
        @if( $dashboard->default === true )
            <i class="fas fa-fw fa-star text-warning"></i>
        @endif
    @endif
@endsection

@section('content')
    <div class="row mb-3 cols-{{ count( $widgetData ) }}">
        @if( $dashboard === null )
            <div class="col" >
                <div class="empty-container">
                    <p>
                        <i class="fal fa-fw fa-2x fa-columns"></i><br>
                        @lang('dtv.dashboards::dashboards.messages.noDashboard')<br>
                        @lang('dtv.dashboards::dashboards.messages.addFirstDashboard')
                    </p>
                    {!! button( 'dtv.dashboards::dashboards.buttons.create' , 'dashboards.create' , [] , 'fa-plus' )->setType( config( 'dtv.app.button_type' ) )->setSize('lg')->useAjax() !!}
                </div>
            </div>
        @else
            @if( $dashboard->widgets->count() === 0 )
                <div class="col" >
                    <div class="empty-container">
                        <p>
                            <i class="fal fa-fw fa-2x fa-window"></i><br>
                            @lang('dtv.dashboards::dashboards.messages.noWidgets')<br>
                            @lang('dtv.dashboards::dashboards.messages.addFirstWidgets')
                        </p>
                        {!! button( 'dtv.dashboards::widgets.buttons.create' , 'dashboards.widgets.create' , [ 'dashboard' => $dashboard ] , 'fa-plus' )->setType( config( 'dtv.app.button_type' ) )->setSize('lg')->useAjax() !!}
                    </div>
                </div>
            @else
                @foreach( $widgetData as $key => $column )
                    <div class="widget-col" style="width: {{ $layoutData[ $key ] }}%;">
                        @foreach( $column as $widget )
                            @if( $widget->conditions() )
                                @include( 'dtv.dashboards::widget' )
                            @endif
                        @endforeach
                    </div>
                @endforeach
            @endif
        @endif
    </div>
@endsection

@section('buttons')
    @if( $dashboard !== null )
        {!! button( 'dtv.dashboards::widgets.buttons.create' , 'dashboards.widgets.create' , [ 'dashboard' => $dashboard ] , 'fa-plus' )->setType( config( 'dtv.app.button_type' ) )->setSize('md')->useAjax() !!}
    @endif
    <div class="dropdown d-inline-block">
        <button class="btn btn-md btn-{{ config( 'dtv.app.button_type' ) }}" data-toggle="dropdown">
            <i class="far fa-fw fa-cog"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-right">
            <h6 class="dropdown-header">Aktionen</h6>
            <button class="dropdown-item" data-ajax-action="{{ route( 'dashboards.create' ) }}">
                <i class="far fa-fw fa-plus"></i> @lang('dtv.dashboards::dashboards.buttons.create')
            </button>
            @if( $dashboard !== null )
                <button class="dropdown-item" data-ajax-action="{{ route( 'dashboards.edit' , [ 'dashboard' => $dashboard->id ] ) }}">
                    <i class="far fa-fw fa-pencil"></i> @lang('dtv.dashboards::dashboards.buttons.edit')
                </button>
                @if( $dashboard->shared === false )
                    <button class="dropdown-item" data-ajax-action="{{ route( 'dashboards.share' , [ 'dashboard' => $dashboard->id ] ) }}">
                        <i class="far fa-fw fa-share"></i> @lang('dtv.dashboards::dashboards.buttons.share')
                    </button>
                @else
                    <button class="dropdown-item" data-ajax-action="{{ route( 'dashboards.unshare' , [ 'dashboard' => $dashboard->id ] ) }}">
                        <i class="far fa-fw fa-ban"></i> @lang('dtv.dashboards::dashboards.buttons.unshare')
                    </button>
                @endif
                @if( $dashboard->default === false )
                    <button class="dropdown-item" data-ajax-action="{{ route( 'dashboards.markAsDefault' , [ 'dashboard' => $dashboard->id ] ) }}">
                        <i class="far fa-fw fa-star"></i> @lang('dtv.dashboards::dashboards.buttons.markAsDefault')
                    </button>
                @endif
                <button class="dropdown-item text-danger" data-confirm-method="delete" data-confirm-action="{{ route( 'dashboards.delete' , [ 'dashboard' => $dashboard->id ] ) }}">
                    <i class="far fa-fw fa-trash-alt"></i> @lang('dtv.dashboards::dashboards.buttons.delete')
                </button>
            @endif

            @if( !$otherDashboards->isEmpty() )
                <div class="dropdown-divider"></div>
                <h6 class="dropdown-header">Weitere Dashboards</h6>
                @foreach( $otherDashboards as $otherDashboard )
                    <a class="dropdown-item" href="{{ route( 'dashboards.show', [ 'dashboard' => $otherDashboard->id ] ) }}">
                        <i class="fas fa-fw fa-columns"></i> {{ $otherDashboard->name }}
                        @if( $otherDashboard->default === true )
                            <i class="fas fa-fw fa-star text-warning"></i>
                        @endif
                    </a>
                @endforeach
            @endif
        </div>
    </div>
@endsection

@push('scripts')
    @if( $dashboard !== null )
        <script>
            $( function () {
                $( ".widget-col" ).sortable( {
                    connectWith: ".widget-col",
                    handle: ".widget-mover",
                    cancel: ".widget-toggle",
                    placeholder: "widget-placeholder",
                    update: function ( event, ui ) {
                        if ( ui.sender ) {
                            return;
                        }

                        let widgets = {};
                        $( '.widget-col' ).each( function ( key ) {
                            widgets[ key ] = [];

                            $( this ).find( '.widget' ).each( function () {
                                widgets[ key ].push( $( this ).data( 'widget' ) );
                            } )
                        } );

                        new DTV.Fetch( 'POST', '{{ route( 'dashboards.update' , [ 'dashboard' => $dashboard->id ] ) }}', {widgets: widgets} )
                            .send();
                    }
                } );

                $( ".widget-toggle" ).on( "click", function () {
                    const Widget = $( this ).closest( ".widget" );
                    Widget.find( ".widget-content" ).toggle();
                    Widget.find( ".widget-minimize" ).toggle();
                    Widget.find( ".widget-maximize" ).toggle();
                } );
            } );
        </script>
    @endif
@endpush