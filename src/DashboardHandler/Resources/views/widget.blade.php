<div class="widget" data-widget="{{ $widget->id }}">
    <div class="widget-header">
        <span class="widget-heading">{{ $widget->getName() }}</span>
        <div class="widget-mover">

        </div>
        <div class="widget-options dropdown">
            <button class="btn btn-sm" type="button" data-toggle="dropdown">
                <i class="fas fa-ellipsis-h"></i>
            </button>

            <div class="dropdown-menu dropdown-menu-right">
                <button class="dropdown-item widget-toggle widget-minimize">
                    <i class="far fa-fw fa-minus"></i> @lang('dtv.dashboards::widgets.buttons.minimize')
                </button>
                <button class="dropdown-item widget-toggle widget-maximize" style="display: none">
                    <i class="far fa-fw fa-window-maximize"></i> @lang('dtv.dashboards::widgets.buttons.maximize')
                </button>
                @if( \DTV\DashboardHandler\Widget::hasSettings( $widget ) )
                    <button class="dropdown-item" data-ajax-action="{{ route('dashboards.widgets.edit',['dashboard' => $dashboard->id, 'widget' => $widget->id ]) }}">
                        <i class="far fa-fw fa-cog"></i> @lang('dtv.dashboards::widgets.buttons.settings')
                    </button>
                @endif
                <button class="dropdown-item" data-confirm-method="delete" data-confirm-action="{{ route('dashboards.widgets.delete',['dashboard' => $dashboard->id, 'widget' => $widget->id ]) }}">
                    <i class="far fa-fw fa-trash"></i> @lang('dtv.dashboards::widgets.buttons.remove')
                </button>
            </div>
        </div>
    </div>
    <div class="widget-content">
        @if( $widget instanceof \DTV\DashboardHandler\ListViewWidget )
            @if($widget::$useView === true)
                @include( $widget->getView(), $widget->handle() + ['widget_' . $widget->id => ${ 'widget_' . $widget->id } ] )
            @else
                {!! ${ 'widget_' . $widget->id } !!}
            @endif
        @else
            @include( $widget->getView(), $widget->handle() )
        @endif
    </div>
</div>