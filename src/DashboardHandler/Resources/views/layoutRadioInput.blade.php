<div class="form-group ">
    <label class="control-label" for="name">Layout</label>
    <div class="dashboard-layout-radio" id="layouts">
        @foreach( DTV\DashboardHandler\DashboardHandler::getLayoutOptions() as $key => $layout )
            <div class="dashboard-layout-item" data-value="{{ $key }}">
                @foreach( $layout as $col )
                    <div class="dashboard-layout-column" style="width: {{ $col }}%"></div>
                @endforeach
            </div>
        @endforeach
        <input type="hidden" name="layout" id="layout-input" value="">
    </div>
</div>

<script>
    /**
     * Layout Chooser Module
     */
    var LayoutChooser = {
        LayoutInput: $( '#layout-input' ),
        LayoutItems: $( '#layouts' ).find( '.dashboard-layout-item' ),

        /**
         * Init Function
         */
        init: function () {
            this.bindUIActions();
        },

        /**
         * UI Binding Function
         */
        bindUIActions: function () {
            this.LayoutItems.click( function () {
                LayoutChooser.LayoutItems.removeClass( 'dashboard-layout-item-active' );
                LayoutChooser.LayoutInput.val( $( this ).data( 'value' ) );
                $( this ).addClass( 'dashboard-layout-item-active' );
            } );
        }
    };

    /**
     * On Load Init Function
     */
    $( document ).ready( function () {
        LayoutChooser.init();
    } );
</script>
