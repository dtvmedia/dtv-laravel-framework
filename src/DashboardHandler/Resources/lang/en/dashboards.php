<?php

    return [

        'name'      => 'Dashboard' ,
        'no_option' => 'No template' ,

        'views' => [
            'index'       => 'Dashboard' ,
            'edit'        => 'Edit Dashboard' ,
            'create'      => 'Create Dashboard' ,
            'noDashboard' => 'No dashboard set up yet...'
        ] ,

        'buttons' => [
            'create'        => 'Create dashboard' ,
            'edit'          => 'Edit dashboard' ,
            'delete'        => 'Delete dashboard' ,
            'markAsDefault' => 'Mark as default' ,
            'share'         => 'Share dashboard' ,
            'unshare'       => 'Unshare dashboard'
        ] ,

        'fields' => [
            'name'     => 'Name' ,
            'default'  => 'Standard dashboard' ,
            'shared'   => 'Shared dashboard' ,
            'template' => 'Use shared dashboards' ,
            'layout'   => 'Layout' ,
            'widget'   => 'Widget'
        ] ,

        'messages' => [
            'noWidgets'             => 'This dashboard has currently no widgets.' ,
            'addFirstWidgets'       => 'Add now an first widget to this dashboard!' ,
            'noDashboard'           => 'You have currently no dashboard assigned.' ,
            'addFirstDashboard'     => 'Start now by setting up your first dashboard!' ,
            'createdTitle'          => 'Dashboard created' ,
            'createdText'           => 'Your new dashboard was successfully created' ,
            'editedTitle'           => 'Dashboard edited' ,
            'editedText'            => 'Your dashboard was successfully edited' ,
            'changedPositionsTitle' => 'Changed positions' ,
            'changedPositionsText'  => 'The widget positions were successfully updated' ,
            'sharedSuccessTitle'    => 'Shared dashboard' ,
            'sharedSuccessText'     => 'The dashboard was successfully shared. Now other user can use it as an template.' ,
            'unsharedSuccessTitle'  => 'Unshared dashboard' ,
            'unsharedSuccessText'   => 'The dashboard was successfully unshared. Other user can\'t use it as an template anymore.' ,
            'favoriteChangedTitle'  => 'Favorite dashboard changed' ,
            'favoriteChangedText'   => 'You successfully changed your favorite dashboard' ,
        ]

    ];