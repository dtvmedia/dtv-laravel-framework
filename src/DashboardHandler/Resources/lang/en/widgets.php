<?php

    return [

        'name' => 'Widget' ,

        'views' => [
            'create' => 'Add widget' ,
            'edit'   => 'Widget settings'
        ] ,

        'buttons' => [
            'create'   => 'Add widget' ,
            'edit'     => 'Edit widget' ,
            'delete'   => 'Delete widget' ,
            'maximize' => 'Maximize' ,
            'minimize' => 'Minimize' ,
            'settings' => 'Settings' ,
            'remove'   => 'Remove' ,
        ] ,

        'fields' => [

        ] ,

        'messages' => [
            'createdTitle'         => 'Widget added' ,
            'createdText'          => 'The chosen widget was successfully added' ,
            'settingsChangedTitle' => 'Widget settings changed' ,
            'settingsChangedText'  => 'The settings of the chosen widget were successfully changed'
        ]

    ];