<?php

    return [

        'name' => 'Widget' ,

        'views' => [
            'create' => 'Widget hinzufügen' ,
            'edit'   => 'Widget Einstellungen'
        ] ,

        'buttons' => [
            'create'   => 'Widget hinzufügen' ,
            'edit'     => 'Widget bearbeiten' ,
            'delete'   => 'Widget löschen' ,
            'maximize' => 'Maximieren' ,
            'minimize' => 'Minimieren' ,
            'settings' => 'Einstellungen' ,
            'remove'   => 'Entfernen' ,
        ] ,

        'fields' => [

        ] ,

        'messages' => [
            'createdTitle'         => 'Widget hinzugefügt' ,
            'createdText'          => 'Das gewählte Widget wurde erfolgreich hinzugefügt' ,
            'settingsChangedTitle' => 'Widget Einstellungen geändert' ,
            'settingsChangedText'  => 'Die Einstellungen des gewählten Widgets wurden erfolgreich geändert'
        ]

    ];