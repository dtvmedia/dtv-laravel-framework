<?php

    return [

        'name'      => 'Dashboard' ,
        'no_option' => 'Keine Vorlage' ,

        'views' => [
            'index'       => 'Dashboard' ,
            'edit'        => 'Dashboard bearbeiten' ,
            'create'      => 'Dashboard erstellen' ,
            'noDashboard' => 'Kein Dashboard verfügbar...'
        ] ,

        'buttons' => [
            'create'        => 'Dashboard erstellen' ,
            'edit'          => 'Dashboard bearbeiten' ,
            'delete'        => 'Dashboard löschen' ,
            'markAsDefault' => 'Als Standard markieren' ,
            'share'         => 'Dashboard teilen' ,
            'unshare'       => 'Teilen beenden'
        ] ,

        'fields' => [
            'name'     => 'Name' ,
            'default'  => 'Standard Dashboard' ,
            'shared'   => 'Geteiltes Dashboard' ,
            'template' => 'Dashboard als Vorlage' ,
            'layout'   => 'Layout' ,
            'widget'   => 'Widget'
        ] ,

        'messages' => [
            'noWidgets'             => 'Dieses Dashboard hat zurzeit keine Widgets.' ,
            'addFirstWidgets'       => 'Füge jetzt ein erstes Widget hinzu!' ,
            'noDashboard'           => 'Du hast momentan keine Dashboards verfügbar.' ,
            'addFirstDashboard'     => 'Starte jetzt und erstelle dein erstes Dashboard!' ,
            'createdTitle'          => 'Dashboard erstellt' ,
            'createdText'           => 'Dein neues Dashboard wurde erfolgreich erstellt' ,
            'editedTitle'           => 'Dashboard bearbeitet' ,
            'editedText'            => 'Dein Dashboard wurde erfolgreich bearbeitet' ,
            'changedPositionsTitle' => 'Positionen gespeichert' ,
            'changedPositionsText'  => 'Die Widget Positionen wurden erfolgreich gespeichert' ,
            'sharedSuccessTitle'    => 'Dashboard geteilt' ,
            'sharedSuccessText'     => 'Das Dashboard wurde erfolgreich geteilt. Andere Nutzer können dieses jetzt als Vorlage nutzen.' ,
            'unsharedSuccessTitle'  => 'Teilen des Dashboards gestoppt' ,
            'unsharedSuccessText'   => 'Das Teilen des Dashboards wurde beendet. Andere Nutzer können nun nicht mehr das Dashboard als Vorlage nutzen.' ,
            'favoriteChangedTitle'  => 'Standard Dashboard geändert' ,
            'favoriteChangedText'   => 'Du hast erfolgreich das Standard Dashboard geändert' ,
        ]

    ];