<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\DashboardHandler;

    use DTV\BaseHandler\Views\ListView;

    /**
     * Abstract List View Base Widget
     *
     * @package   DTV\DashboardHandler
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    abstract class ListViewWidget extends Widget
    {
        /**
         * Main widget function which should return an list view instance
         *
         * @return ListView
         */
        abstract public function listview(): ListView;

        /**
         * Main widget handle function which creates the widget's data array
         *
         * @return array
         */
        public final function handle()
        {
            return [ 'listview' => $this->listview() ];
        }
    }