<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Creates Dashboards Table Migration
     *
     * @package   -
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CreateDashboardsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create( 'dashboards' , function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->string( 'name' , 40 );
                $table->unsignedInteger( 'user_id' );
                $table->string( 'layout' , 20 );
                $table->boolean( 'default' )->default( false );
                $table->boolean( 'shared' )->default( false );
                $table->timestamps();
                $table->softDeletes();
            } );

            Schema::table( 'dashboards' , function ( $table ) {
                $table->foreign( 'user_id' )->references( 'id' )->on( config( 'dtv.tables.user' )  );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists( 'dashboards' );
        }
    }
