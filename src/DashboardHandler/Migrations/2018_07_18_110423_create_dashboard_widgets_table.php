<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    /**
     * Creates Dashboard Widgets Table Migration
     *
     * @package   -
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class CreateDashboardWidgetsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create( 'dashboard_widgets' , function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->unsignedInteger( 'dashboard_id' );
                $table->string( 'type' );
                $table->tinyInteger( 'column' );
                $table->tinyInteger( 'row' );
                $table->text( 'settings' );
                $table->timestamps();
                $table->softDeletes();
            } );

            Schema::table( 'dashboard_widgets' , function ( $table ) {
                $table->foreign( 'dashboard_id' )->references( 'id' )->on( 'dashboards' );
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists( 'dashboard_widgets' );
        }
    }
