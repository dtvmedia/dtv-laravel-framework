# DashboardHandler - DTV Laravel Framework

## Configs
- *enabled*: Package activation switch
- *routes_enabled*: Activation switch for the package routes seperated from the package switch itself
- *layouts*: Defines the available dashboard layouts with the column widths

## Usage
### Create an Widget
In order to create your own widget you need to create an class which extends from 
the abstract class ``DTV\DashboardHandler\Widget``. Following properties and method can be defined.

#### Property string $name
Required. Defines the name (translation path) of the widget

#### Property string $view
Required. Defines the view which will be used for rendering the widget

#### Property array $defaultSetttings
Optional. This property defines an array of default settings used for the widget. 
Users can override these default settings with their own custom changes via the settings form.

#### Method handle(): string
Required. This method is supposed the generate and return an data array which will then be passed to the widget view.

#### Method settings( WidgetEditForm , Widget ): FormView
Optional.  This method is supposed to generate the settings form. 
It should define the form elements and return the WidgetEditForm instance.
If no such mehtod is defined the widget has no settings form.

#### Method conditions(): bool
Optional. With this method some conditions for the availability of this widget can be defined.

### Add the widget
In order to make an widget available for use you need to register it in an ServiceProvider.
For managing all available widgets there is an singleton instance of ``\DTV\DashboardHandler\WidgetManager`` available.
This Manager class has an register function which publishes an widget. 
