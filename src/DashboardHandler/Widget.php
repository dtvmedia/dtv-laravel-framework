<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\DashboardHandler;

    use DTV\DashboardHandler\Models\Widget as WidgetModel;
    use DTV\DashboardHandler\Services\Views\WidgetEditForm;

    /**
     * Abstract Base Widget
     *
     * @package   DTV\DashboardHandler
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    abstract class Widget
    {
        /**
         * The underlying widget model
         *
         * @var WidgetModel
         */
        protected $model;

        /**
         * Widget name
         *
         * @var string
         */
        public static $name = 'dtv.dashboards::dashboards.widgets.new';

        /**
         * Widget view
         *
         * @var string
         */
        public static $view = '';

        /**
         * Flag which indicates if the view should be used or the listview should be rendered standalone
         *
         * @var bool
         */
        public static $useView = false;

        /**
         * Array of default settings for this widget
         *
         * @var array
         */
        public static $defaultSettings = [];

        /**
         * Widget constructor
         *
         * @param WidgetModel $model
         */
        public function __construct( WidgetModel $model )
        {
            $this->model = $model;
        }

        /**
         * Returns the underlying model
         *
         * @return WidgetModel
         */
        public function getModel(): WidgetModel
        {
            return $this->model;
        }

        /**
         * Returns the translated name of the widget
         *
         * @return string
         */
        public function getName(): string
        {
            return trans( static::$name );
        }

        /**
         * Returns the widget's view name
         *
         * @return string
         */
        public function getView(): string
        {
            return static::$view;
        }

        /**
         * Function which should define conditions for the widget availibilty
         *
         * @return bool
         */
        public function conditions()
        {
            return true;
        }

        /**
         * Main widget handle function which should create the widget's data array
         *
         * @return array
         */
        abstract public function handle();

        /**
         * Defines the widgets settings form
         *
         * @param WidgetEditForm $form
         * @param WidgetModel    $model
         *
         * @return WidgetEditForm
         */
        public static function settings( WidgetEditForm $form , WidgetModel $model )
        {
            return $form;
        }

        /**
         * Checks whether or not an widget has settings
         *
         * @param Widget $widget
         *
         * @throws \ReflectionException
         *
         * @return bool
         */
        public static function hasSettings( Widget $widget ): bool
        {
            $reflector = new \ReflectionMethod( $widget , 'settings' );

            return ( $reflector->getDeclaringClass()->getName() === get_class( $widget ) );
        }

        /**
         * Magic getter method which translates request to the underlying model
         *
         * @param string $name
         *
         * @return mixed
         */
        public function __get( string $name )
        {
            return $this->model->$name;
        }

        /**
         * Returns an settings value
         *
         * @param string $key
         *
         * @return mixed|null
         */
        public function setting( string $key )
        {
            return $this->model->settings[ $key ] ?? ( static::$defaultSettings[ $key ] ?? null );
        }
    }