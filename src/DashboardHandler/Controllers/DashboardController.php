<?php

    /** @noinspection PhpUnused */

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\DashboardHandler\Controllers;

    use DTV\BaseHandler\Controllers\Controller;
    use DTV\BaseHandler\JsonResponse;
    use DTV\BaseHandler\Views\Page;
    use DTV\DashboardHandler\DashboardHandler;
    use DTV\DashboardHandler\ListViewWidget;
    use DTV\DashboardHandler\Models\Dashboard;
    use DTV\DashboardHandler\Services\Views\DashboardCreateForm;
    use Exception;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\Request;
    use Throwable;

    /**
     * Dashboard Controller Class
     *
     * @package   DTV\DashboardHandler\Controllers
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class DashboardController extends Controller
    {
        /**
         * Shows the default dashboard
         *
         * @throws Throwable
         * @return string
         */
        public function index()
        {
            $defaultDashboard = Dashboard::query()
                ->where('user_id', auth()->id())
                ->where('default', 1)
                ->first();

            return $this->show($defaultDashboard);
        }

        /**
         * Creates an new dashboard
         *
         * @param Request          $request
         * @param DashboardHandler $dashboardHandler
         *
         * @throws Throwable
         * @return Page|JsonResponse|string
         */
        public function create(Request $request, DashboardHandler $dashboardHandler)
        {
            return $this->form($request, new DashboardCreateForm(), function (Request $request) use ($dashboardHandler) {
                $dashboard = $dashboardHandler->create(
                    $request->name,
                    $request->layout,
                    $request->get('default', false),
                    $request->get('shared', false),
                    $request->template
                );

                return $this->json()->success('dtv.dashboards::dashboards.messages.created')
                    ->redirect('dashboards.show', ['dashboard' => $dashboard->id]);
            });
        }

        /**
         * Shows and handles the edit dashboard form
         *
         * @param Dashboard        $dashboard
         * @param Request          $request
         * @param DashboardHandler $dashboardHandler
         *
         * @throws Throwable
         * @return JsonResponse|Page|string
         */
        public function edit(Dashboard $dashboard, Request $request, DashboardHandler $dashboardHandler)
        {
            $form = new DashboardCreateForm($dashboard);
            $form->setIcon('fa-pencil');
            $form->setLabel('dtv.dashboards::dashboards.views.edit');

            return $this->form($request, $form, function (Request $request) use ($dashboard, $dashboardHandler) {
                $dashboardHandler->edit(
                    $dashboard,
                    $request->name,
                    $request->boolean('default')
                );

                return $this->json()->success('dtv.dashboards::dashboards.messages.edited')->reload();
            });
        }

        /**
         * Shows an dashboards
         *
         * @param Dashboard|null $dashboard
         *
         * @throws Throwable
         * @return string
         */
        public function show(?Dashboard $dashboard = null)
        {
            $widgets = [];
            $layoutData = [];
            $listviewWidgets = [];
            $otherDashboards = Dashboard::query()->where('user_id', auth()->id());

            if ($dashboard !== null) {
                if ($dashboard->user_id !== auth()->id()) {
                    throw new ModelNotFoundException();
                }

                foreach ($dashboard->layout as $key => $col) {
                    $widgets[$key + 1] = [];
                    $layoutData[$key + 1] = $col;
                }

                foreach ($dashboard->widgets as $widget) {
                    $widgetClass = $widget->getClass();

                    if ($widgetClass !== null) {
                        $widgetObj = new $widgetClass($widget);
                        $widgets[$widget->column][$widget->row] = $widgetObj;

                        if ($widgetObj instanceof ListViewWidget) {
                            $listviewWidgets[] = $widgetObj;
                        }
                    }
                }

                foreach ($widgets as $col => $widget) {
                    ksort($widgets[$col]);
                }

                $otherDashboards->where('id', '!=', $dashboard->id);
            }

            $page = $this->view('dtv.dashboards::dashboards', [
                'dashboard'       => $dashboard,
                'widgetData'      => $widgets,
                'layoutData'      => $layoutData,
                'otherDashboards' => $otherDashboards->get(),
            ]);

            foreach ($listviewWidgets as $widget) {
                $page->withView('widget_' . $widget->id, $widget->listview());
            }

            return $page->render();
        }

        /**
         * Marks an given dashboard as favorite and unmarks the previous default
         *
         * @param int              $dashboard
         * @param DashboardHandler $dashboardHandler
         *
         * @return JsonResponse
         */
        public function markAsDefault($dashboard, DashboardHandler $dashboardHandler)
        {
            /** @var Dashboard $dashboard */
            $dashboard = Dashboard::query()
                ->where('user_id', auth()->id())
                ->findOrFail($dashboard);

            $dashboardHandler->markAsDefault($dashboard);

            return $this->json()
                ->success('dtv.dashboards::dashboards.messages.favoriteChanged')
                ->redirect('dashboards.index');
        }

        /**
         * Shared an dashboard which makes it usable as template for other users
         *
         * @param int              $dashboard
         * @param DashboardHandler $dashboardHandler
         *
         * @return JsonResponse
         */
        public function share($dashboard, DashboardHandler $dashboardHandler)
        {
            /** @var Dashboard $dashboard */
            $dashboard = Dashboard::query()
                ->where('user_id', auth()->id())
                ->findOrFail($dashboard);

            $dashboardHandler->share($dashboard);

            return $this->json()
                ->success('dtv.dashboards::dashboards.messages.sharedSuccess')
                ->redirect('dashboards.show', ['dashboard' => $dashboard->id]);
        }

        /**
         * Unshares an dashboard
         *
         * @param int              $dashboard
         * @param DashboardHandler $dashboardHandler
         *
         * @return JsonResponse
         */
        public function unshare($dashboard, DashboardHandler $dashboardHandler)
        {
            /** @var Dashboard $dashboard */
            $dashboard = Dashboard::query()
                ->where('user_id', auth()->id())
                ->findOrFail($dashboard);

            $dashboardHandler->unshare($dashboard);

            return $this->json()
                ->success('dtv.dashboards::dashboards.messages.unsharedSuccess')
                ->redirect('dashboards.show', ['dashboard' => $dashboard->id]);
        }

        /**
         * Updates the widget positions
         *
         * @param int              $dashboard
         * @param DashboardHandler $dashboardHandler
         * @param Request          $request
         *
         * @return JsonResponse
         */
        public function update($dashboard, DashboardHandler $dashboardHandler, Request $request)
        {
            /** @var Dashboard $dashboard */
            $dashboard = Dashboard::query()
                ->where('user_id', auth()->id())
                ->findOrFail($dashboard);

            $dashboardHandler->updatePositions($dashboard, $request->get('widgets'));

            return $this->json()->success('dtv.dashboards::dashboards.messages.changedPositions');
        }

        /**
         * Tries to delete a dashboard
         *
         * @param Dashboard $dashboard
         *
         * @throws Exception
         *
         * @return JsonResponse
         */
        public function delete(Dashboard $dashboard)
        {
            return $this->deleteModel($dashboard, [], ['widgets'], function (Dashboard $dashboard) {
                // users can only delete their own dashboards
                if ($dashboard->user_id !== auth()->id()) {
                    return false;
                }

                // default dashboard can't be deleted
                if ($dashboard->default === true) {
                    return false;
                }

                return true;
            });
        }
    }
