<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\DashboardHandler\Controllers;

    use DTV\BaseHandler\Controllers\Controller;
    use DTV\BaseHandler\JsonResponse;
    use DTV\BaseHandler\Views\Page;
    use DTV\DashboardHandler\DashboardHandler;
    use DTV\DashboardHandler\Models\Dashboard;
    use DTV\DashboardHandler\Models\Widget;
    use DTV\DashboardHandler\Models\WidgetSettings;
    use DTV\DashboardHandler\Services\Views\WidgetAddForm;
    use DTV\DashboardHandler\Services\Views\WidgetEditForm;
    use Exception;
    use Illuminate\Http\Request;
    use Throwable;

    /**
     * Widget Controller Class
     *
     * @package   DTV\DashboardHandler\Controllers
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class WidgetController extends Controller
    {
        /**
         * Adds an widget to an dashboard
         *
         * @param int              $dashboard
         * @param Request          $request
         * @param DashboardHandler $dashboardHandler
         *
         * @throws Throwable
         *
         * @return JsonResponse|Page|string
         */
        public function create($dashboard, Request $request, DashboardHandler $dashboardHandler)
        {
            /** @var Dashboard $dashboard */
            $dashboard = Dashboard::query()
                ->where('user_id', auth()->id())
                ->findOrFail($dashboard);

            return $this->form($request, new WidgetAddForm(), function (Request $request) use ($dashboard, $dashboardHandler) {
                $dashboardHandler->addWidget($dashboard, $request->widget);

                return $this->json()->success('dtv.dashboards::widgets.messages.created')->reload();
            });
        }

        /**
         * Edits the settings of an widget from the given dashboard
         *
         * @param int     $dashboard
         * @param int     $widget
         * @param Request $request
         *
         * @throws Throwable
         *
         * @return JsonResponse|Page|string
         */
        public function edit($dashboard, $widget, Request $request)
        {
            /** @var Dashboard $dashboard */
            $dashboard = Dashboard::query()
                ->where('user_id', auth()->id())
                ->findOrFail($dashboard);
            /** @var Widget $widget */
            $widget = $dashboard->widgets()
                ->findOrFail($widget);

            $widgetClass = $widget->getClass();
            $settings = new WidgetSettings( ( ( empty( $widget->settings ) ) ? $widgetClass::$defaultSettings : $widget->settings ) );

            $form = $widgetClass::settings( new WidgetEditForm( $settings ) , $widget );

            return $this->form( $request , $form , function ( Request $request ) use ( $widget ) {
                $widget->update( [ 'settings' => $request->except( '_token' ) ] );

                return $this->json()->success( 'dtv.dashboards::widgets.messages.settingsChanged' )->reload();
            } );
        }

        /**
         * Tries to delete a widget from a given dashboard
         *
         * @param int $dashboard
         * @param int $widget
         *
         * @throws Exception
         *
         * @return JsonResponse
         */
        public function delete( $dashboard , $widget )
        {
            /** @var Dashboard $dashboard */
            $dashboard = Dashboard::query()
                ->where('user_id', auth()->id())
                ->findOrFail($dashboard);
            $widget = $dashboard->widgets()
                ->findOrFail($widget);

            return $this->deleteModel($widget);
        }
    }
