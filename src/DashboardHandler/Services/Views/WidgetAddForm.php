<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\DashboardHandler\Services\Views;

    use DTV\BaseHandler\Views\FormView;
    use DTV\DashboardHandler\DashboardHandler;

    /**
     * Widget Add Form Class
     *
     * @package   DTV\DashboardHandler\Services\Views
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class WidgetAddForm extends FormView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.dashboards::widgets.views.create';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-plus';

        /**
         * Translation file used for translating input labels etc
         *
         * @var string
         */
        protected $transPath = 'dtv.dashboards::dashboards';

        /**
         * Adds the inputs to the form
         *
         * @throws \Throwable
         */
        public function inputs()
        {
            $this->addSelect( 'widget' , DashboardHandler::getWidgetOptions() )
                ->setValidationRule( 'required' )
                ->setSize( 12 );
        }
    }