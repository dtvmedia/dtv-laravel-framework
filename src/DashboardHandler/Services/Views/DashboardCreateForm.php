<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\DashboardHandler\Services\Views;

    use DTV\BaseHandler\Views\FormView;
    use DTV\DashboardHandler\DashboardHandler;
    use DTV\DashboardHandler\Models\Dashboard;

    /**
     * Dashboard Create Form Class
     *
     * @package   DTV\DashboardHandler\Services\Views
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class DashboardCreateForm extends FormView
    {
        /**
         * The views label
         *
         * @var string
         */
        protected $label = 'dtv.dashboards::dashboards.views.create';

        /**
         * The views icon
         *
         * @var string
         */
        protected $icon = 'fa-plus';

        /**
         * Translation file used for translating input labels etc
         *
         * @var string
         */
        protected $transPath = 'dtv.dashboards::dashboards';

        /**
         * Adds the inputs to the form
         *
         * @throws \Throwable
         */
        public function inputs()
        {
            $this->addText( 'name' )->setValidationRule( 'required' );

            if ( !$this->isUpdateMode() ) {
                $this->addSelect( 'template' , Dashboard::getSelectOptions( Dashboard::query()->where( 'shared' , 1 ) , true ) );
                $this->addCustomView( 'dtv.dashboards::layoutRadioInput' )->setSize( 12 )
                    ->setValidationRule( [ 'layout' => 'required' ] );
            }

            $this->addCheckBox( 'default' )->setDefaultValue( true )->setSize( 12 );

            if ( !$this->isUpdateMode() ) {
                $this->addCheckBox( 'shared' )->setDefaultValue( false )->setSize( 12 );
            }
        }
    }