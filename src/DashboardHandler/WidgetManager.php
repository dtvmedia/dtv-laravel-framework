<?php

    /**
     * DTV Media Solutions
     *
     * PHP Version 7.0
     */

    namespace DTV\DashboardHandler;

    use DTV\Support\Contracts\ManagerContract;
    use InvalidArgumentException;

    /**
     * Widget Manager
     *
     * @package   DTV\DashboardHandler
     * @copyright 2018 DTV Media Solutions
     * @author    Dominique Heinelt <contact@dtvmedia.de>
     * @link      http://dtvmedia.de/
     */
    class WidgetManager implements ManagerContract
    {
        /**
         * Array of registered widgets
         *
         * @var array
         */
        protected array $widgets = [];

        /**
         * Registers an widget
         *
         * @param string $class
         */
        public function register(string $class): void
        {
            if (!is_a($class, Widget::class, true)) {
                throw new InvalidArgumentException($class . ' is not a valid widget class!');
            }

            if ($this->has($class)) {
                logger()->warning('The widget ' . $class . ' is registered twice!');
            }

            $this->widgets[$class::$name] = $class;
        }

        /**
         * Returns if an given widget is already registered or not
         *
         * @param string $key
         *
         * @return bool
         */
        public function has(string $key): bool
        {
            return in_array($key, $this->widgets);
        }

        /**
         * Returns the widget class for a given name
         *
         * @param string $key
         *
         * @return string|null
         */
        public function get(string $key): ?string
        {
            return $this->widgets[$key] ?? null;
        }

        /**
         * Returns all registered widgets
         *
         * @return array
         */
        public function all(): array
        {
            return $this->widgets;
        }
    }
