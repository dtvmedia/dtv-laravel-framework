<?php

    namespace Tests;

    use Illuminate\Support\Facades\Hash;
    use Illuminate\Contracts\Console\Kernel;

    trait CreatesApplication
    {
        /**
         * Creates the application.
         *
         * @return \Illuminate\Foundation\Application
         */
        public function createApplication()
        {
            if ( !defined( 'DS' ) ) {
                define( 'DS' , DIRECTORY_SEPARATOR );
            }

            $app = require(__DIR__ . DS . '..' . DS . '.environment' . DS . 'bootstrap' . DS . 'app.php' );

            $app->make( Kernel::class )->bootstrap();

            Hash::driver( 'bcrypt' )->setRounds( 4 );

            return $app;
        }
    }
