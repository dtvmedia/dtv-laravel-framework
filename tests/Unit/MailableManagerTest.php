<?php

    namespace Tests\Unit;

    use DTV\EmailHandler\Layouts\TextMail;
    use DTV\EmailHandler\Services\MailableManager;
    use Tests\TestCase;

    class MailableManagerTest extends TestCase
    {
        public function testRegisterAndHas()
        {
            $handler = new MailableManager();
            $handler->register( TextMail::class );

            $this->assertTrue( $handler->has( 'text_mail' ) );
            $this->assertFalse( $handler->has( 'test' ) );

            $this->expectException( \InvalidArgumentException::class );
            $handler->register( MailableManagerTest::class );
        }

        public function testGetter()
        {
            $handler = new MailableManager();
            $handler->register( TextMail::class );

            $mailable = $handler->get( 'text_mail' );
            $this->assertSame( $mailable , TextMail::class );

            $mailable = $handler->get( 'test' );
            $this->assertNull( $mailable );

            $mailables = $handler->all();
            $this->assertIsArray( $mailables );
        }
    }