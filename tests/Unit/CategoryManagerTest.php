<?php

    namespace Tests\Unit;

    use DTV\CategoryHandler\CategoryManager;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Tests\Fakes\FakeCategory;
    use Tests\TestCase;

    class CategoryManagerTest extends TestCase
    {
        public function testRegisterAndHas()
        {
            $handler = new CategoryManager();
            $handler->register( FakeCategory::class );

            $this->assertTrue( $handler->has( 'fakes' ) );
            $this->assertFalse( $handler->has( 'test' ) );

            $this->expectException( \InvalidArgumentException::class );
            $handler->register( CategoryManagerTest::class );
        }

        public function testGetter()
        {
            $handler = new CategoryManager();
            $handler->register( FakeCategory::class );

            $model = $handler->get( 'fakes' );
            $this->assertSame( $model , FakeCategory::class );

            $model = $handler->get( 'test' );
            $this->assertNull( $model );

            $model = $handler->getOrFail( 'fakes' );
            $this->assertSame( $model , FakeCategory::class );

            $this->expectException( ModelNotFoundException::class );
            $handler->getOrFail( 'test' );

            $models = $handler->all();
            $this->assertIsArray( $models );
        }
    }