<?php

    namespace Tests\Unit;

    use DTV\SettingsHandler\Models\Setting;
    use DTV\Support\MorphIdHandler;
    use Tests\TestCase;

    class MorphIdHandlerTest extends TestCase
    {
        public function testMorphIdCreation()
        {
            $model = new Setting();
            $model->id = 56;

            $this->assertSame( MorphIdHandler::getIdByModel( $model ) , '_MIHa8aoaqawan2t38382x322v37acb1322s302t36awah332s2t3037awan2t38382x322v9l9t9u' );
        }

        public function testExceptionIfInvalidMorphIdIsGiven()
        {
            $this->expectException(\Exception::class);

            MorphIdHandler::getModel('_MIHa8aoah6df45hdh22v9l9t9u');
        }

        public function testShortenedMorphIdEncoding()
        {
            $model = new Setting();
            $model->id = 56;

            MorphIdHandler::clearClassMap();
            MorphIdHandler::registerShortClass('s', Setting::class);

            $this->assertSame('_MIH379l9t9u', MorphIdHandler::getIdByModel( $model ));
        }

        public function testShortenedMorphIdDecoding()
        {
            MorphIdHandler::clearClassMap();
            MorphIdHandler::registerShortClass('s', Setting::class);

            $this->assertSame(null, MorphIdHandler::getModel('_MIH379l9t9u'));
        }

        public function testExceptionIfInvalidShortenedMorphIdIsGiven()
        {
            MorphIdHandler::clearClassMap();
            $this->expectException(\Exception::class);

            MorphIdHandler::getModel('_MIH379l9t9u');
        }
    }
