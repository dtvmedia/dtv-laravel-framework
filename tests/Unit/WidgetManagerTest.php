<?php

    namespace Tests\Unit;

    use DTV\DashboardHandler\WidgetManager;
    use Tests\Fakes\FakeWidget;
    use Tests\TestCase;

    class WidgetManagerTest extends TestCase
    {
        public function testRegisterAndHas()
        {
            $handler = new WidgetManager();
            $handler->register( FakeWidget::class );

            $this->assertTrue( $handler->has( FakeWidget::class ) );
            $this->assertFalse( $handler->has( WidgetManagerTest::class ) );

            $this->expectException( \InvalidArgumentException::class );
            $handler->register( WidgetManagerTest::class );
        }

        public function testGetter()
        {
            $handler = new WidgetManager();
            $handler->register( FakeWidget::class );

            $widgets = $handler->all();
            $this->assertIsArray( $widgets );
            $this->assertNotEmpty( $widgets );

            $widget = $handler->get( 'fake_widget' );
            $this->assertSame( $widget , FakeWidget::class );

            $widget = $handler->get( 'test' );
            $this->assertNull( $widget );

            $widgets = $handler->all();
            $this->assertIsArray( $widgets );
        }
    }