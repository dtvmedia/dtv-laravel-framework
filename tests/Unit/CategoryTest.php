<?php

    namespace Tests\Unit;

    use DTV\CategoryHandler\Models\Builders\CategoryBuilder;
    use Tests\Fakes\FakeCategory;
    use Tests\TestCase;

    class CategoryTest extends TestCase
    {
        public function testCategoryBuilder()
        {
            $category = FakeCategory::query()->create( [
                'name'        => 'Test' ,
                'description' => '-' ,
                'status'      => true ,
                'test_field'  => true ,
            ] );

            $this->assertIsInt( $category->id );
            $this->assertDatabaseHas( 'category_fields' , [
                'name'  => 'test_field' ,
                'value' => '1' ,
            ] );

            $category->update( [
                'name'       => 'Test2' ,
                'test_field' => false ,
            ] );

            $this->assertTrue( true );

            $category = FakeCategory::query()->findOrFail( $category->id );

            $this->assertIsInt( $category->id );
            $this->assertFalse( $category->test_field );
            $this->assertSame( $category->name , 'Test2' );

            $results = FakeCategory::query()->where( 'test_field' , false )->get()->keyBy( 'name' );

            $this->assertTrue( $results->has( 'Test2' ) );

            $results = FakeCategory::query()->where( function ( CategoryBuilder $builder ) {
                $builder->where( 'test_field' , false )->orWhere( 'test_field' , true );
            } )->get()->keyBy( 'name' );

            $this->assertTrue( $results->has( 'Test2' ) );

            $results = FakeCategory::query()->whereIn( 'test_field' , [ false , true ] )->get()->keyBy( 'name' );

            $this->assertTrue( $results->has( 'Test2' ) );
        }
    }