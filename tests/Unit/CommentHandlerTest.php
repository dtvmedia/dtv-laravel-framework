<?php

    namespace Tests\Unit;

    use DTV\CategoryHandler\CategoryManager;
    use DTV\CommentsHandler\CommentHandler;
    use DTV\CommentsHandler\Models\Comment;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Tests\Fakes\FakeCategory;
    use Tests\TestCase;

    class CommentHandlerTest extends TestCase
    {
        public function testCreateAndEdit()
        {
            $handler = new CommentHandler();
            $comment = $handler->create( 'Thats an test comment' , FakeCategory::class , 1 );

            $this->assertInstanceOf( Comment::class , $comment );
            $this->assertDatabaseHas( 'comments' , [
                'user_id'        => null ,
                'reference_type' => FakeCategory::class ,
                'reference_id'   => 1 ,
                'comment'        => 'Thats an test comment'
            ] );

            $handler->edit( $comment , 'Thats an test comment2' );

            $this->assertDatabaseHas( 'comments' , [
                'comment' => 'Thats an test comment2'
            ] );
        }
    }