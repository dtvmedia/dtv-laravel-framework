<?php

    namespace Tests\Unit;

    use DTV\ActivityHandler\ActivityHandler;
    use Illuminate\Foundation\Testing\RefreshDatabase;
    use Tests\TestCase;

    class ActivityHandlerTest extends TestCase
    {
        use RefreshDatabase;

        public function testCreate()
        {
            $handler = new ActivityHandler();
            $activity = $handler->create( 'test.activity' , 'Thats the :key text' , [ 'key' => 'value' ] );

            $this->assertDatabaseHas( 'activities' , [
                'type'           => 'test.activity' ,
                'user_id'        => null ,
                'text'           => 'Thats the :key text' ,
                'parameter'      => '{"key":"value"}' ,
                'reference_type' => null ,
                'reference_id'   => null ,
                'route'          => '' ,
            ] );
            $this->assertIsInt( $activity->id );
        }
    }