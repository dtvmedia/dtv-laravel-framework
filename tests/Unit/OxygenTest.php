<?php

    namespace Tests\Unit;

    use DTV\Oxygen\Oxygen;
    use Illuminate\Support\Facades\Lang;
    use Tests\TestCase;

    class OxygenTest extends TestCase
    {
        public function testStaticNowInstantiation()
        {
            $now = Oxygen::now();

            $this->assertInstanceOf( Oxygen::class , $now );
            $this->assertFalse( $now->isNull() );
        }

        public function testIsNullMethod()
        {
            $isNull = new Oxygen( null );
            $isNotNull = Oxygen::now();

            $this->assertTrue( $isNull->isNull() );
            $this->assertFalse( $isNotNull->isNull() );
        }

        public function testToDateStringMethod()
        {
            $date = Oxygen::create( 2018 , 3 , 15 );

            $this->assertSame( $date->toDateString() , '2018-03-15' );
        }

        public function testEnglishFormatterMethods()
        {
            Lang::setLocale('en');
            $date = Oxygen::create(2018, 3, 15, 12, 13, 14);

            $this->assertSame($date->toDateString(), '2018-03-15');
            $this->assertSame($date->toDateWithDayString(), 'Thu, 2018-03-15');
            $this->assertSame($date->toTimeString(), '12:13:14');
            $this->assertSame($date->toShortTimeString(), '12:13');
            $this->assertSame($date->toDateTimeString(), '2018-03-15 12:13:14');
            $this->assertSame($date->toDateTimeWithDayString(), 'Thu, 2018-03-15 12:13:14');
            $this->assertSame($date->toDateShortTimeString(), '2018-03-15 12:13');
            $this->assertSame($date->toDateShortTimeWithDayString(), 'Thu, 2018-03-15 12:13');
        }

        public function testGermanFormatterMethods()
        {
            Lang::setLocale('de');
            Oxygen::setLocale('de');

            $date = Oxygen::create(2018, 3, 15, 12, 13, 14);

            $this->assertSame($date->toDateString(), '15.03.2018');
            $this->assertSame($date->toDateWithDayString(), 'Do, 15.03.2018');
            $this->assertSame($date->toTimeString(), '12:13:14');
            $this->assertSame($date->toShortTimeString(), '12:13');
            $this->assertSame($date->toDateTimeString(), '15.03.2018 12:13:14');
            $this->assertSame($date->toDateTimeWithDayString(), 'Do, 15.03.2018 12:13:14');
            $this->assertSame($date->toDateShortTimeString(), '15.03.2018 12:13');
            $this->assertSame($date->toDateShortTimeWithDayString(), 'Do, 15.03.2018 12:13');
        }
    }
