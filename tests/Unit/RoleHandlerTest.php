<?php

    namespace Tests\Unit;

    use DTV\AclHandler\RoleHandler;
    use Tests\TestCase;

    class RoleHandlerTest extends TestCase
    {
        public function testCreateAndUpdate()
        {
            $handler = new RoleHandler();
            $role = $handler->create( 'Test' , '-' , [ 'roles.view' ] , false );
            $handler->create( 'Another Test' , '-' , [] , true );

            $this->assertDatabaseHas( 'roles' , [
                'name'        => 'Test' ,
                'default'     => false ,
                'permissions' => '["roles.view"]' ,
            ] );
            $this->assertDatabaseHas( 'roles' , [
                'name'    => 'Another Test' ,
                'default' => true ,
            ] );
            $this->assertIsInt( $role->id );

            $handler->edit( $role , [
                'name'    => 'Test2' ,
                'default' => true
            ] );

            $this->assertTrue( $role->default );
            $this->assertDatabaseMissing( 'roles' , [
                'name'    => 'Another Test' ,
                'default' => true ,
            ] );
        }

        public function testGroupedPermissions()
        {
            $handler = new RoleHandler();
            $permissions = $handler->getGroupedPermissions();

            $this->assertIsArray( $permissions );
            $this->assertArrayNotHasKey( 'general' , $permissions );
            $this->assertArrayHasKey( 'roles' , $permissions );
            $this->assertIsArray( $permissions[ 'roles' ] );
            $this->assertContains( 'roles.view' , $permissions[ 'roles' ] );
        }
    }