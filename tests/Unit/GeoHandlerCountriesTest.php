<?php

    namespace Tests\Unit;

    use DTV\GeoHandler\Models\Country;
    use Tests\TestCase;

    class GeoHandlerCountriesTest extends TestCase
    {
        public function testAllMethod()
        {
            $countries = Country::all();

            $this->assertIsArray( $countries );
            $this->assertNotEmpty( $countries );
        }

        public function testGetMethod()
        {
            config()->set( 'app.locale' , 'en' );

            $this->assertEquals( Country::get( 'DE' ) , 'Germany' );
            $this->assertNull( Country::get( 'AB123' ) );
        }

        public function testGetSelectOptionsMethod()
        {
            config()->set( 'app.locale' , 'en' );

            $this->assertIsArray( Country::getSelectOptions() );
        }
    }