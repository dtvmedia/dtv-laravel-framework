<?php

    namespace Tests\Unit;

    use Carbon\Carbon;
    use DTV\BaseHandler\JsonResponse;
    use Illuminate\Support\Facades\Crypt;
    use Tests\TestCase;

    class HelpersTest extends TestCase
    {
        public function testJsonHelper()
        {
            $this->assertInstanceOf( JsonResponse::class , json() );
        }

        public function testGateHelper()
        {
            $this->assertInstanceOf( \Illuminate\Contracts\Auth\Access\Gate::class , gate() );
        }

        public function testOptionHelper()
        {
            $option = option( 'test' );

            $this->assertIsArray( $option );
            $this->assertArrayHasKey( 'value' , $option );
            $this->assertArrayHasKey( 'icon' , $option );
            $this->assertSame( 'test' , $option[ 'value' ] );
            $this->assertSame( 'fa-angle-double-right fa-fw' , $option[ 'icon' ] );
        }

        public function testRouteExistsHelper()
        {
            $this->assertTrue( routeExists( 'roles.index' ) );
            $this->assertFalse( routeExists( 'not.a.valid.route' ) );
        }

        public function testFormatSecondsHelper()
        {
            $this->assertSame( formatSeconds( 0 ) , '00:00:00' );
            $this->assertSame( formatSeconds( 60 ) , '00:01:00' );
            $this->assertSame( formatSeconds( 3600 ) , '01:00:00' );
            $this->assertSame( formatSeconds( 43823 ) , '12:10:23' );
            $this->assertSame( formatSeconds( 86400 ) , '24:00:00' );
            $this->assertSame( formatSeconds( 90000 ) , '25:00:00' );

            $this->assertSame( formatSeconds( 0 , false ) , '00:00' );
            $this->assertSame( formatSeconds( 30 , false ) , '00:00' );
            $this->assertSame( formatSeconds( 60 , false ) , '00:01' );
            $this->assertSame( formatSeconds( 3600 , false ) , '01:00' );
            $this->assertSame( formatSeconds( 43823 , false ) , '12:10' );
            $this->assertSame( formatSeconds( 86400 , false ) , '24:00' );
            $this->assertSame( formatSeconds( 90000 , false ) , '25:00' );
        }

        public function testFormatFilesizeHelper()
        {
            $this->assertSame( formatFileSize( 0 ) , '0 B' );
            $this->assertSame( formatFileSize( 512 ) , '512 B' );
            $this->assertSame( formatFileSize( 1024 ) , '1 KB' );
            $this->assertSame( formatFileSize( 2048 ) , '2 KB' );
            $this->assertSame( formatFileSize( 2500 ) , '2.44 KB' );
            $this->assertSame( formatFileSize( 1048576 ) , '1 MB' );
            $this->assertSame( formatFileSize( 5242880 ) , '5 MB' );
            $this->assertSame( formatFileSize( 1073741824 ) , '1 GB' );
            $this->assertSame( formatFileSize( 536870912000 ) , '500 GB' );
            $this->assertSame( formatFileSize( 1073741824000 ) , '1000 GB' );
            $this->assertSame( formatFileSize( 1099511627776 ) , '1 TB' );
        }

        public function testFormatCurrencyHelper()
        {
            $this->assertSame( formatCurrency( 0 ) , '0.00' );
            $this->assertSame( formatCurrency( 100 ) , '100.00' );
            $this->assertSame( formatCurrency( 1000 ) , '1,000.00' );
            $this->assertSame( formatCurrency( -1000 ) , '-1,000.00' );
            $this->assertSame( formatCurrency( 1234.56 ) , '1,234.56' );
            $this->assertSame( formatCurrency( 1234.5678 ) , '1,234.57' );
            $this->assertSame( formatCurrency( 1234.5678 , 4 ) , '1,234.5678' );
            $this->assertSame( formatCurrency( 1234.5678 , 0 ) , '1,235' );
        }

        public function testFormatNumberHelper()
        {
            $this->assertSame( formatNumber( 0 ) , '0' );
            $this->assertSame( formatNumber( 1000 ) , '1,000' );
            $this->assertSame( formatNumber( -1000.5 ) , '-1,000.5' );
            $this->assertSame( formatNumber( 1234.5678 ) , '1,234.57' );
            $this->assertSame( formatNumber( 1234.5678 , 4 ) , '1,234.5678' );
            $this->assertSame( formatNumber( 1234.5678 , 0 ) , '1,235' );

            $this->assertSame( formatNumber( 0 , 2 , true ) , '0.00' );
            $this->assertSame( formatNumber( 1000 , 2 , true ) , '1,000.00' );
            $this->assertSame( formatNumber( -1000 , 2 , true ) , '-1,000.00' );
            $this->assertSame( formatNumber( 1234.5678 , 2 , true ) , '1,234.57' );
            $this->assertSame( formatNumber( 1234.5678 , 4 , true ) , '1,234.5678' );
            $this->assertSame( formatNumber( 1234.5678 , 0 , true ) , '1,235' );
        }

        public function testPutInRangeHelper()
        {
            $this->assertSame( putInRange( 0 ) , 0.0 );
            $this->assertSame( putInRange( 50 ) , 50.0 );
            $this->assertSame( putInRange( 100 ) , 100.0 );
            $this->assertSame( putInRange( -100 ) , 0.0 );
            $this->assertSame( putInRange( 150 ) , 100.0 );
            $this->assertSame( putInRange( 150 , -100 , 200 ) , 150.0 );
            $this->assertSame( putInRange( -150 , -100 , 200 ) , -100.0 );
            $this->assertSame( putInRange( 250 , -100 , 200 ) , 200.0 );
        }

        public function testCastHelper()
        {
            $this->assertSame( cast( 'string' , null ) , null );
            $this->assertSame( cast( 'string' , '0.5' ) , '0.5' );
            $this->assertSame( cast( 'int' , '1.7' ) , 1 );
            $this->assertSame( cast( 'percent' , '45' ) , 45 );
            $this->assertSame( cast( 'float' , '0.5' ) , 0.5 );
            $this->assertSame( cast( 'bool' , '1' ) , true );
            $this->assertSame( cast( 'bool' , '0' ) , false );
            $this->assertEquals( cast( 'date' , '2019-10-21' ) , Carbon::create( 2019 , 10 , 21 ) );
            $this->assertEquals( cast( 'time' , '14:09:32' ) , Carbon::createFromTime( 14 , 9 , 32 ) );
            $this->assertSame( cast( 'password' , '' ) , null );
            $this->assertSame( cast( 'password' , Crypt::encryptString( 'test' ) ) , 'test' );
        }
    }