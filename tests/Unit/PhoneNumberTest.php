<?php

    use DTV\Oxygen\Oxygen;
use DTV\SmsHandler\PhoneNumber;
use Illuminate\Support\Facades\Lang;
    use Tests\TestCase;

    class PhoneNumberTest extends TestCase
    {
        public function testNormalize()
        {
            $this->assertEquals('+33678233644', PhoneNumber::normalize('0033 - 67 82 33 644'));
            $this->assertEquals('+393404122924', PhoneNumber::normalize('00393404122924'));
            $this->assertEquals('+436604252760', PhoneNumber::normalize('0043 660 42 52 760'));
            $this->assertEquals('+436604252760', PhoneNumber::normalize('+436604252760'));
            $this->assertEquals('+491724552633', PhoneNumber::normalize('0172 - 455 26 33'));
            $this->assertEquals('+4930814522450', PhoneNumber::normalize('030 - 81 45 22 450'));
        }
    }
