<?php

    namespace Tests;

    use DTV\AclHandler\Models\Role;
    use DTV\BaseHandler\Models\User;
    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    trait AuthenticatesUser
    {
        protected $user;

        public function getUser()
        {
            if ( $this->user !== null ) {
                return $this->user;
            }

            try {
                return $this->getUserWithPermissions();
            } catch ( ModelNotFoundException $exception ) {
                return $this->createNewUserAndRole();
            }
        }

        public function getAuthenticatedUser()
        {
            $user = $this->getUser();

            if ( auth()->check() === false ) {
                auth()->loginUsingId( $user->id );
            }

            return $user;
        }

        protected function createNewUserAndRole()
        {
            $this->user = User::factory()->create();
            $role = Role::factory()->create();
            $this->user->roles()->attach( $role->id );

            return $this->user;
        }

        protected function getUserWithPermissions()
        {
            $this->user = User::query()->whereHas( 'roles' , function ( Builder $builder ) {
                $builder->where( 'permissions' , json_encode( array_keys( gate()->abilities() ) ) );
            } )->firstOrFail();

            return $this->user;
        }
    }
