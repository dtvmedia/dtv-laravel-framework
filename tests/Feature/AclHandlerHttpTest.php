<?php

    namespace Tests\Feature;

    use DTV\AclHandler\Models\Role;
    use Tests\AuthenticatesUser;
    use Tests\TestCase;

    class AclHandlerHttpTest extends TestCase
    {
        use AuthenticatesUser;

        public function testAclBasicPages()
        {
            $this->actingAs( $this->getAuthenticatedUser() , 'web' )
                ->get( route( 'roles.index' ) )
                ->assertStatus( 200 );

            $this->actingAs( $this->getAuthenticatedUser() , 'web' )
                ->get( route( 'roles.create' ) )
                ->assertStatus( 200 );
        }

        public function testAclCreateAndDeleteWorkflow()
        {
            $name = 'HTTP Test ' . str_random( 6 );

            // Create
            $this->actingAs( $this->getAuthenticatedUser() , 'web' )
                ->withHeader( 'X-Requested-With' , 'XMLHttpRequest' )
                ->post( route( 'roles.create' ), [
                    'name'        => $name ,
                    'default'     => 0 ,
                    'description' => '-' ,
                    'permissions' => [
                        'roles.view'
                    ] ,
                ] )
                ->assertStatus( 200 )
                ->assertJsonFragment( [ 'mode' => 'success' ] );

            // Edit
            $role = Role::query()->where( 'name' , $name )->firstOrFail();
            $this->actingAs( $this->getAuthenticatedUser() , 'web' )
                ->get( route( 'roles.edit' , [ 'role' => $role->id ] ) )
                ->assertStatus( 200 );

            // Delete
            $this->actingAs( $this->getAuthenticatedUser() , 'web' )
                ->delete( route( 'roles.delete' , [ 'role' => $role->id ] ) )
                ->assertStatus( 200 )
                ->assertJsonFragment( [ 'mode' => 'success' ] );
        }
    }
