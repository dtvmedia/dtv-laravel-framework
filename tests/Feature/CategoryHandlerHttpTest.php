<?php

    namespace Tests\Feature;

    use Tests\AuthenticatesUser;
    use Tests\Fakes\FakeCategory;
    use Tests\TestCase;

    class CategoryHandlerHttpTest extends TestCase
    {
        use AuthenticatesUser;

        protected function getCategory()
        {
            $category = new FakeCategory();

            $this->app->make( 'categories' )->register( FakeCategory::class );

            return $category;
        }

        public function testCategoryBasicPages()
        {
            $category = $this->getCategory();

            $this->actingAs( $this->getAuthenticatedUser() , 'web' )
                ->get( route( 'categories.index' , [ 'type' => $category->getCategoryName() ] ) )
                ->assertStatus( 200 );

            $this->actingAs( $this->getAuthenticatedUser() , 'web' )
                ->get( route( 'categories.create' , [ 'type' => $category->getCategoryName() ] ) )
                ->assertStatus( 200 );
        }

        public function testCategoryCreateAndDeleteWorkflow()
        {
            $category = $this->getCategory();
            $name = 'Fake Category ' . str_random( 6 );

            // Create
            $this->actingAs( $this->getAuthenticatedUser() , 'web' )
                ->withHeader( 'X-Requested-With' , 'XMLHttpRequest' )
                ->post( route( 'categories.create' , [ 'type' => $category->getCategoryName() ] ) , [
                    'name'        => $name ,
                    'description' => '-' ,
                    'status'      => true ,
                    'test_field'  => false ,
                ] )
                ->assertStatus( 200 )
                ->assertJsonFragment( [ 'mode' => 'success' ] );

            // Edit
            $category = FakeCategory::query()->where( 'name' , $name )->firstOrFail();
            $this->actingAs( $this->getAuthenticatedUser() , 'web' )
                ->get( route( 'categories.edit' , [ 'type' => $category->getCategoryName() , 'id' => $category->id ] ) )
                ->assertStatus( 200 );

            // Delete
            $this->actingAs( $this->getAuthenticatedUser() , 'web' )
                ->delete( route( 'categories.delete' , [ 'type' => $category->getCategoryName() , 'id' => $category->id ] ) )
                ->assertStatus( 200 )
                ->assertJsonFragment( [ 'mode' => 'success' ] );
        }
    }