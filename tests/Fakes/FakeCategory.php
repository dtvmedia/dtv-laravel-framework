<?php

    namespace Tests\Fakes;

    use DTV\CategoryHandler\Models\Category;

    class FakeCategory extends Category
    {
        /**
         * Category type ID
         *
         * @var int
         */
        protected static $categoryType = 1;

        /**
         * Category name for urls
         *
         * @var string
         */
        protected static $categoryName = 'fakes';

        /**
         * Defines the category fields
         *
         * @return array
         */
        public function fields()
        {
            return [
                'test_field' => [
                    'type'        => 'bool' ,
                    'index'       => true ,
                    'create'      => true ,
                    'edit'        => true ,
                    'set_mutator' => null ,
                    'get_mutator' => null ,
                ] ,
            ];
        }
    }