<?php

    namespace Tests\Fakes;

    use DTV\DashboardHandler\Widget;

    class FakeWidget extends Widget
    {
        /**
         * Widget name
         *
         * @var string
         */
        public static $name = 'fake_widget';

        /**
         * Main widget handle function which should create the widget's data array
         *
         * @return array
         */
        public function handle()
        {
            return [];
        }
    }