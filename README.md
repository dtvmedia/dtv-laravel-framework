![DTV Laravel Framework](banner.png)


# DTV Laravel Framework

A Laravel extension framework, which offers especially many helper functions and classes for content and frontend stuff.

## Features & Modules 
- View Classes (ListView, FormView)
- Base Classes (Controller, Model, User Model, Exception Handler)
- Util Classes (JsonResponse, RoutingHandler, MorphIdHandler...)
- Oxygen (Carbon Wrapper)
- Helper functions
- Submodules
    - ACL Module (Roles using Laravel Gates)
    - Activity Module
    - Admin Module
    - Category Module
    - Comments Module
    - Dashboard Module
    - Email Module (BETA)
    - Geo Module (ALPHA)
    - Settings Module
    - SMS Module (ALPHA) 

## Installation
- Add the ```\DTV\ServiceProvider``` class to the array of service providers in your app.php
  In case you use package auto discovery you can skip this step.
- Publish the ```dtv.php``` config if needed (optional). 
  There you can enable/disable different modules.
  Notice if you want use the single view support you need to configure a wrapper view for that. In this case publishing the config is required.
  A basic single view wrapper would look like this:
````blade
@extends( 'layouts.app' )

@section('title')
    @yield('view-title')
@endsection

@section('content')
    {!! $view !!}
@endsection
````
- Publish all javascript and css assets. The assets will be copied to the ```Resources/assets``` directory.
- Add them to your ```webpack.mix.js``` file 
```javascript
let mix = require( 'laravel-mix' );

mix.babel([
    'src/BaseHandler/Resources/assets/js/helpers.js',
    'src/BaseHandler/Resources/assets/js/App.js',
    'src/BaseHandler/Resources/assets/js/views/*.js',
    'src/BaseHandler/Resources/assets/js/elements/*.js',
    'src/BaseHandler/Resources/assets/js/utils/*.js',
], '.environment/public/js/dtv-framework.js');
mix.styles([
    'src/BaseHandler/Resources/assets/css/dtv-framework.css',
    'src/BaseHandler/Resources/assets/css/dtv-listview.css',
    'src/DashboardHandler/Resources/assets/css/dtv-dashboard.css'
], '.environment/public/css/dtv-framework.css');
```
- Extend all Controller from the BaseController Class
- Extend all Models from the Base Model Class
- Extend the User Model from the Base User Model Class
- Extend the Exception Handler from the Base Exception Handler Class and add the view property
- Switch all route definitions to the RoutingHandler (optional)
- Include the modal_wrapper and app_js_init blade views into your main view in order to enable the javascript extensions.

### Possible Features in the future
- Documentation Page
- Timezone support (see https://github.com/rweber7912/timezoned)
- FileHandler (files relation, HasFiles Trait)

### Problems
View nesting -> https://github.com/Te-cho/compile-blades

## phpDoc 
php phpDocumentor.phar -d ./src -t docs/api -defaultpackagename=DTV

## Suggestion
For most problems there are already exist many great open source packages.
Here we just want to list some problems and packages we tried and which convinced us.

### PDF Creation
**barryvdh/laravel-dompdf**

Really cool package for generating pdf which works by converting HTML to an PDF. 
Its just an Laravel wrapper package of ``dompdf/dompdf``

### Geocoding
**toin0u/geocoder-laravel**

A great geocoding handler packages which supports many providers and http clients. 
Its just an Laravel wrapper package of ``geocoder-php/Geocoder``

**geocoder-php/nominatim-provider**

One Provider for this package is Nominatim, which is the Geocoder from the Open Street Map Project. 
A big advantage is that you can use this provider whitout any licence key or similar.

**geocoder-php/here-provider**

Another Provider for this package is the HERE API Provider. 
With an registered account you can get your API credentials and are able to send up to 250k Geocoding requests per month for free.