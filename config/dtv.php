<?php

    return [

        /**
         * App
         */
        'app'        => [
            'provider'            => App\Providers\AppServiceProvider::class ,
            'git_root'            => __DIR__ . '\..\.git\refs\heads\\' ,
            'git_branch'          => env( 'GIT_BRANCH' , 'master' ) ,
            'button_type'         => 'success' ,
            'message_timeout_defaults' => [
                'success' => 5,
                'warning' => 5,
                'error'   => 10,
            ],
            'list_page_length'    => 10 ,
            'js_route_whitelist'  => [],
            'gender_with_diverse' => false,
        ] ,

        /**
         * Navigation
         */
        'navigation' => [
            'Home' => 'home' ,
        ] ,

        /**
         * Models & Tables
         */
        'models'     => [
            'user' => DTV\BaseHandler\Models\User::class ,
        ] ,
        'tables'     => [
            'user' => 'users' ,
        ] ,

        /**
         * Layouts
         */
        'layouts'    => [
            'single_view_layout' => 'layouts.single_view_wrapper' ,
            'page_layout'        => 'layouts.app' ,
            'modal_layout'       => 'dtv.base::layouts.general_modal_layout' ,
        ] ,

        /**
         * Module Settings
         */
        'modules'    => [
            'acl' => [
                'enabled'        => true ,
                'routes_enabled' => true ,
                'gate_groups'    => []
            ] ,

            'activities' => [
                'enabled' => true ,
            ] ,

            'admin' => [
                'enabled'         => true ,
                'routes_enabled'  => true ,
                'access_password' => env( 'ADMIN_ACCESS_PASSWORD') ,
                'access_user_id'  => env( 'ADMIN_ACCESS_ID' , 1 ) ,
                'hidden_user_id'  => env( 'ADMIN_HIDDEN_ID' , 1 ) ,
                'monitor_queues'  => [ 'default' ] ,
            ] ,

            'categories' => [
                'enabled'        => true ,
                'routes_enabled' => true ,
            ] ,

            'comments' => [
                'enabled'            => true ,
                'routes_enabled'     => true ,
                'activities_enabled' => true ,
                'textarea_view'      => 'dtv.comments::comment_textarea' ,
                'html'               => false ,
                'date_format'        => null ,
            ] ,

            'dashboards' => [
                'enabled'        => true ,
                'routes_enabled' => true ,
                'layouts'        => [
                    0 => [ '100' ] ,
                    1 => [ '50' , '50' ] ,
                    2 => [ '40' , '60' ] ,
                    3 => [ '60' , '40' ] ,
                    4 => [ '33' , '33' , '33' ]
                ]
            ] ,

            'emails' => [
                'enabled'             => true ,
                'routes_enabled'      => true ,
                'debug'               => [
                    'host'       => env( 'MAIL_DEBUG_HOST' , 'debugmail.io' ) ,
                    'port'       => env( 'MAIL_DEBUG_PORT' , 25 ) ,
                    'username'   => env( 'MAIL_DEBUG_USERNAME' ) ,
                    'password'   => env( 'MAIL_DEBUG_PASSWORD' ) ,
                    'encryption' => env( 'MAIL_DEBUG_ENCRYPTION' ) ,
                ] ,
                'search'              => [] ,
                'layout_color'        => 'deepskyblue' ,
                'default_layout'      => DTV\EmailHandler\Layouts\HtmlMail::class ,
                'send_queue'          => 'default' ,
                'main_out_account_id' => env('MAIL_MAIN_OUT_ACCOUNT_ID') ,
            ] ,

            'geo' => [
                'enabled'         => true,
                'google_maps_key' => env('GOOGLE_MAPS_KEY'),
                'here_api_key'    => env('HERE_API_KEY'),
            ],

            'settings' => [
                'enabled'        => true ,
                'routes_enabled' => true ,
            ] ,

            'sms' => [
                'enabled'          => true ,
                'routes_enabled'   => true ,
                'default_provider' => DTV\SmsHandler\Providers\CmProvider::class ,
                'search'           => [] ,
                'send_queue'       => 'default' ,
            ] ,
        ]

    ];
