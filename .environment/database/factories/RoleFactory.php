<?php

namespace Database\Factories;

use DTV\AclHandler\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Role::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->text( 20 );

        return [
            'name'        => $name ,
            'slug'        => escapeHtmlLabel( $name ) ,
            'description' => $this->faker->text() ,
            'permissions' => array_keys( gate()->abilities() ) ,
            'default'     => true
        ];
    }
}
