<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateUsersTable extends Migration
    {
        /**
         * Table name
         *
         * @var string
         */
        protected $table = 'users';

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create( $this->table , function ( Blueprint $table ) {
                $table->increments( 'id' );
                $table->string( 'first_name' );
                $table->string( 'last_name' );
                $table->string( 'email' )->unique();
                $table->string( 'password' );
                $table->dateTime( 'last_login' )->nullable();
                $table->rememberToken();
                $table->timestamps();
                $table->softDeletes();
            } );
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists( $this->table );
        }
    }
