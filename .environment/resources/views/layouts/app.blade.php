@extends('main')

@section('layout')

    <header class="mb-4">
        <div class="container-fluid" style="max-width: 1400px">
            <div class="row">
                <div class="col-md-8">
                    <h2>@yield('title')</h2>
                </div>
                <div class="col-md-4 text-right">
                    @yield('buttons')
                    @if( isset( $headerButtons ) )
                        @foreach( $headerButtons as $button )
                            {!! $button !!}
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </header>

    <!-- Content -->
    <div class="container-fluid" style="max-width: 1400px">
        @yield('content')
    </div>

    @include('dtv.base::includes.modals_wrapper')
@endsection