@extends( 'layouts.app' )

@section('title')
    @yield('view-title')
@endsection

@section('content')
    {!! $view !!}
@endsection