<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>DTV Laravel Framework Test Environment</title>

        <!-- Styles -->
        <link href="{{ asset(mix('css/vendor.css')) }}" rel="stylesheet">
        <link href="{{ asset(mix('css/dtv-framework.css')) }}" rel="stylesheet">

    </head>
    <body>
        <div id="app">
            <main>
                @yield('layout')
            </main>
        </div>

        <!-- Scripts -->
        <script src="{{ asset(mix('js/vendor.js')) }}"></script>
        <script src="{{ asset(mix('js/dtv-framework.js')) }}"></script>
        @include('dtv.base::includes.app_js_init')
        @stack('scripts')
    </body>
</html>
