<?php

    if ( !defined( 'DS' ) ) {
        define( 'DS' , DIRECTORY_SEPARATOR );
    }

    $app = new App\Services\Application(
        realpath( __DIR__ . '/../' )
    );

    /*
    |--------------------------------------------------------------------------
    | Bind Important Interfaces
    |--------------------------------------------------------------------------
    |
    | Next, we need to bind some important interfaces into the container so
    | we will be able to resolve them when needed. The kernels serve the
    | incoming requests to this application from both the web and CLI.
    |
    */

    $app->singleton(
        Illuminate\Contracts\Http\Kernel::class ,
        App\Http\Kernel::class
    );

    $app->singleton(
        Illuminate\Contracts\Console\Kernel::class ,
        Illuminate\Foundation\Console\Kernel::class
    );

    $app->singleton(
        Illuminate\Contracts\Debug\ExceptionHandler::class ,
        DTV\BaseHandler\Exceptions\Handler::class
    );

    return $app;
