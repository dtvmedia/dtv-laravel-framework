<?php

namespace App\Services;


class Application extends \Illuminate\Foundation\Application
{
    public function getNamespace()
    {
        return 'App\\';
    }
}
