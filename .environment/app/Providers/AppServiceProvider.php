<?php

    namespace App\Providers;

    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Route;
    use Illuminate\Support\ServiceProvider;

    class AppServiceProvider extends ServiceProvider
    {
        /**
         * Commands Array
         *
         * @var array
         */
        protected $commands = [];

        /**
         * Bootstrap any application services.
         *
         * @return void
         */
        public function boot()
        {

        }

        /**
         * Register any application services.
         *
         * @return void
         */
        public function register()
        {
            $this->registerRoutes();
            $this->registerCommands();
        }

        /**
         * Load the Applications Routes
         */
        public function registerRoutes()
        {
            $this->registerControllerRoutes( \App\Http\Controller::class );

            Route::any( '/login' , [
                'as' => 'login' ,
                function () {
                    $model = config( 'dtv.models.user' );
                    $user = $model::query()->whereHas( 'roles' , function ( Builder $builder ) {
                        $builder->where( 'permissions' , json_encode( array_keys( gate()->abilities() ) ) );
                    } )->firstOrFail();

                    Auth::loginUsingId( $user->id );

                    return redirect( '/' );
                }
            ] )->middleware( [ 'web' ] );
            Route::any( '/logout' , [
                'as' => 'logout' ,
                function () {
                    // do nothing
                }
            ] )->middleware( [ 'web' ] );

            Route::getRoutes()->refreshNameLookups();
        }

        /**
         * Loads the Applications Commands
         */
        public function registerCommands()
        {
            $this->commands( $this->commands );
        }

        /**
         * Registers the routes of an given controller
         *
         * @param string $controller FQCN of the controller
         */
        private function registerControllerRoutes( $controller )
        {
            if ( method_exists( $controller , 'routes' ) ) {
                $controller::routes();
            }
        }
    }
